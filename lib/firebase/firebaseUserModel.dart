import 'package:e4t/firebase/firebaseHelper.dart';

class FirebaseUserModel {
  final String? name;
  final String uid;
  final String? profilePic;
  final bool isOnline;
  final String? categoryTitle;
  final int subscriptionId;
  final String? subscriptionValidity;
  final List<String> groupId;
  var isSelected = false;

  FirebaseUserModel({
    required this.name,
    required this.uid,
    required this.profilePic,
    required this.isOnline,
    required this.groupId,
    required this.categoryTitle,
    required this.subscriptionId,
    required this.subscriptionValidity,
    this.isSelected = false,
  });

  factory FirebaseUserModel.fromMap(Map<String, dynamic> map) {
    return FirebaseUserModel(
      name: map[FireBaseHelperClass.NAME_KEY] ?? '',
      uid: map[FireBaseHelperClass.UID_KEY] ?? '',
      profilePic: map[FireBaseHelperClass.PROFILE_PIC_KEY] ?? '',
      isOnline: map[FireBaseHelperClass.IS_ONLINE_KEY] ?? false,
      categoryTitle: map[FireBaseHelperClass.CATEGORY_TITLE_KEY],
      subscriptionId: map[FireBaseHelperClass.SUBSCRIPTION_ID_KEY] ?? 0,
      subscriptionValidity: map[FireBaseHelperClass.SUBSCRIPTION_VALIDITY_KEY],
      groupId: List<String>.from(map[FireBaseHelperClass.GROUP_ID_KEY]),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      FireBaseHelperClass.NAME_KEY: name,
      FireBaseHelperClass.UID_KEY: uid,
      FireBaseHelperClass.PROFILE_PIC_KEY: profilePic,
      FireBaseHelperClass.IS_ONLINE_KEY: isOnline,
      FireBaseHelperClass.GROUP_ID_KEY: groupId,
      FireBaseHelperClass.CATEGORY_TITLE_KEY: categoryTitle,
      FireBaseHelperClass.SUBSCRIPTION_ID_KEY: subscriptionId,
      FireBaseHelperClass.SUBSCRIPTION_VALIDITY_KEY: subscriptionValidity,
    };
  }
}
