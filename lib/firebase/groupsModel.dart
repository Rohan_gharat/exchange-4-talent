import 'package:e4t/firebase/firebaseHelper.dart';

class GroupModel {
  final String senderId;
  final String name;
  final String groupId;
  final String? lastMessage;
  final String? senderName;
  final String groupPic;
  final List<String> membersUid;
  final DateTime timeSent;

  GroupModel({
    required this.senderId,
    required this.name,
    required this.groupId,
    required this.lastMessage,
    required this.senderName,
    required this.groupPic,
    required this.membersUid,
    required this.timeSent,
  });

  Map<String, dynamic> toMap() {
    return {
      FireBaseHelperClass.SENDER_ID_KEY: senderId,
      FireBaseHelperClass.NAME_KEY: name,
      FireBaseHelperClass.GROUP_ID_KEY: groupId,
      FireBaseHelperClass.LAST_MESSAGE_KEY: lastMessage,
      FireBaseHelperClass.SENDER_NAME_KEY: senderName,
      FireBaseHelperClass.GROUP_PIC_KEY: groupPic,
      FireBaseHelperClass.MEMBERS_UID_KEY: membersUid,
      FireBaseHelperClass.TIME_SEND_KEY: timeSent.millisecondsSinceEpoch,
    };
  }

  factory GroupModel.fromMap(Map<String, dynamic> map) {
    return GroupModel(
      senderId: map[FireBaseHelperClass.SENDER_ID_KEY] ?? '',
      name: map[FireBaseHelperClass.NAME_KEY] ?? '',
      groupId: map[FireBaseHelperClass.GROUP_ID_KEY] ?? '',
      lastMessage: map[FireBaseHelperClass.LAST_MESSAGE_KEY] ?? '',
      senderName: map[FireBaseHelperClass.SENDER_NAME_KEY],
      groupPic: map[FireBaseHelperClass.GROUP_PIC_KEY] ?? '',
      membersUid: List<String>.from(map[FireBaseHelperClass.MEMBERS_UID_KEY]),
      timeSent: DateTime.fromMillisecondsSinceEpoch(
          map[FireBaseHelperClass.TIME_SEND_KEY]),
    );
  }
}
