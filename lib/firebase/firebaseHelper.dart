import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/firebase/chatContact.dart';
import 'package:e4t/firebase/firebaseUserModel.dart';
import 'package:e4t/firebase/groupsModel.dart';
import 'package:e4t/firebase/messageModel.dart';
import 'package:e4t/firebase/messageEnum.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class FireBaseHelperClass {
  static const TAG = "chat_logs ";
  static const GROUP_CHAT_TAG = "group_chat_logs ";
  static const USER_COLLECTION_NAME = "users";
  static const CHAT_COLLECTION_NAME = "chats";
  static const MESSAGES_COLLECTION_NAME = "messages";
  static const GROUP_COLLECTION_NAME = "groups";
  static const SECRET_SERVICE_COLLECTION_NAME = "secretServices";

  static const NAME_KEY = "name";
  static const UID_KEY = "uid";
  static const PROFILE_PIC_KEY = "profilePic";
  static const IS_ONLINE_KEY = "isOnline";
  static const GROUP_ID_KEY = "groupId";
  static const CATEGORY_TITLE_KEY = "category_title";
  static const SUBSCRIPTION_ID_KEY = "subscription_id";
  static const SUBSCRIPTION_VALIDITY_KEY = "subscription_validity";
  static const TIME_SEND_KEY = "timeSent";
  static const IS_SEEN_KEY = "isSeen";
  static const LAST_MESSAGE_KEY = "lastMessage";

  static const SENDER_ID_KEY = "senderId";
  static const RECEIVER_ID_KEY = "receiverid";
  static const MESSAGE_KEY = "message";
  static const MESSAGE_TYPE_ID_KEY = "type";
  static const MESSAGE_ID_KEY = "messageId";
  static const SENDER_NAME_KEY = "senderName";

  static const CONTACT_ID_KEY = "contactId";

  static const MEMBERS_UID_KEY = "membersUid";
  static const GROUP_PIC_KEY = "groupPic";

  static const CHAT_PAGINATION_USERS_LIMIT = 30;

  static Future<bool> createUserInFireBase(
      {required FirebaseFirestore fireStore,
      required UserModel usermodel}) async {
    var docExistsCheck = await checkIfDocExists(
        fireStore: fireStore,
        docId: usermodel.mobile,
        collectionName: USER_COLLECTION_NAME);
    var user = FirebaseUserModel(
      name: usermodel.name ?? "",
      uid: usermodel.mobile,
      profilePic: usermodel.avatar,
      isOnline: true,
      categoryTitle: usermodel.category_title,
      subscriptionId: usermodel.subscriptionId ?? 0,
      subscriptionValidity: usermodel.subscriptionValidity,
      groupId: [],
    );
    try {
      if (docExistsCheck) {
        /// document exists update the document
        await fireStore
            .collection(USER_COLLECTION_NAME)
            .doc(usermodel.mobile)
            .update(user.toMap());
      } else {
        await fireStore
            .collection(USER_COLLECTION_NAME)
            .doc(usermodel.mobile)
            .set(user.toMap());
      }
      return true;
    } catch (error) {
      print("$TAG createUserInFireBase: error: ${error.toString()}");
      return false;
    }
  }

  static Future<bool> createUserInFireBaseIfNotExists(
      {required FirebaseFirestore fireStore,
      required UserModel usermodel}) async {
    var docExistsCheck = await checkIfDocExists(
        fireStore: fireStore,
        docId: usermodel.mobile,
        collectionName: USER_COLLECTION_NAME);
     var user = FirebaseUserModel(
      name: usermodel.name ?? "",
      uid: usermodel.mobile,
      profilePic: usermodel.avatar,
      isOnline: true,
      categoryTitle: usermodel.category_title,
      subscriptionId: usermodel.subscriptionId ?? 0,
      subscriptionValidity: usermodel.subscriptionValidity,
      groupId: [],
    );
    try {
      if (docExistsCheck) {
        /// document exists update the document
        // await fireStore
        //     .collection(USER_COLLECTION_NAME)
        //     .doc(usermodel.mobile)
        //     .update(user.toMap());
      } else {
        await fireStore
            .collection(USER_COLLECTION_NAME)
            .doc(usermodel.mobile)
            .set(user.toMap());
      }
      return true;
    } catch (error) {
      print("$TAG createUserInFireBase: error: ${error.toString()}");
      return false;
    }
  }

  static Future updateUserProfileInFirebase(
      {required FirebaseFirestore firestore,
      required UserModel userModel}) async {
    Map<String, dynamic> data = {};

    if (userModel.name != null && userModel.name.toString().trim() != "") {
      data[NAME_KEY] = userModel.name;
    }
    if (userModel.avatar != null && userModel.avatar.toString().trim() != "") {
      data[PROFILE_PIC_KEY] = userModel.avatar;
    }
    if (userModel.category_title != null) {
      data[CATEGORY_TITLE_KEY] = userModel.category_title;
    }
    data[SUBSCRIPTION_ID_KEY] = userModel.subscriptionId;
    data[SUBSCRIPTION_VALIDITY_KEY] = userModel.subscriptionValidity;


    try {
      await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(userModel.mobile.toString())
          .update(data);

      var allUsers = await firestore.collection(USER_COLLECTION_NAME).get();
      allUsers.docs.map((allUsersSnapshot) async {
       QuerySnapshot subcollectionSnapshot = await allUsersSnapshot.reference
            .collection(CHAT_COLLECTION_NAME)
            .get();
         if (subcollectionSnapshot.docs.isNotEmpty) {
          var userIds = await allUsersSnapshot.reference
              .collection(CHAT_COLLECTION_NAME)
              .get();
          userIds.docs.map((userChatIdSnap) {

            if (userChatIdSnap.id == userModel.mobile) {
              userChatIdSnap.reference.update(data);
            }
          }).toList();
        }
      }).toList();
    } catch (error) {
      print("$TAG updateUserProfileInFirebase: data: $data");
    }

    updateUserProfileInGroupsFirebase(
      firestore: firestore,
      userModel: userModel,
      isSecretService: false,
    );

    updateUserProfileInGroupsFirebase(
      firestore: firestore,
      userModel: userModel,
      isSecretService: true,
    );
  }

  static Future updateUserProfileInGroupsFirebase(
      {required FirebaseFirestore firestore,
      required UserModel userModel,
      required bool isSecretService}) async {
    try {
      var group = await firestore
          .collection(isSecretService
              ? SECRET_SERVICE_COLLECTION_NAME
              : GROUP_COLLECTION_NAME)
          .where(MEMBERS_UID_KEY, arrayContains: userModel.mobile)
          .get();
      group.docs.map((groupsSnapshot) async {
         QuerySnapshot subCollectionSnapshot = await groupsSnapshot.reference
            .collection(CHAT_COLLECTION_NAME)
            .get();

        if (group.docs.isNotEmpty) {
          var groupIds = await groupsSnapshot.reference
              .collection(CHAT_COLLECTION_NAME)
              .where(SENDER_ID_KEY, isEqualTo: userModel.mobile)
              .get();

          Future.forEach(groupIds.docs, (gcSnap) async {
            print("$GROUP_CHAT_TAG:$isSecretService: gcSnap id: ${gcSnap.id}");
            await gcSnap.reference.update({
              SENDER_NAME_KEY: userModel.name,
            });
          });
        }
      }).toList();
    } catch (error) {
      print(
          "$GROUP_CHAT_TAG:$isSecretService: updateUserProfileInGroupsFirebase: error : ${error.toString()}");
    }
  }

  static Future updateUserProfilePhotoInFirebase(
      {required FirebaseFirestore firestore,
      required String photoUrl,
      required String mobileNo}) async {
    Map<String, dynamic> data = {};

    if (photoUrl != null && photoUrl != "") {
      data[PROFILE_PIC_KEY] = photoUrl;
    }

    try {
      await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(mobileNo)
          .update(data);

      firestore.collection(USER_COLLECTION_NAME).snapshots().map((snapshot) {
        return snapshot.docs.map((userSnapshot) async {

          QuerySnapshot subcollectionSnapshot = await userSnapshot.reference
              .collection(CHAT_COLLECTION_NAME)
              .get();

          if (subcollectionSnapshot.docs.isNotEmpty) {
            userSnapshot.reference
                .collection(CHAT_COLLECTION_NAME)
                .snapshots()
                .map((chatSnapshot) {
              return chatSnapshot.docs.map((chaSnapshotData) {

                if (chaSnapshotData.id == mobileNo) {
                  chaSnapshotData.reference.update(data);
                }
              }).toList();
            }).toList();
          }
        }).toList();
      }).toList();
    } catch (error) {
      print("$TAG updateUserProfileInFirebase: data: $data");
    }
  }

  static Future updateSubscriptionId(
      {required FirebaseFirestore firestore,
      required String mobileNo,
      required int subscriptionId,
      required String? subscriptionValidity}) async {
    await firestore.collection(USER_COLLECTION_NAME).doc(mobileNo).update({
      SUBSCRIPTION_ID_KEY: subscriptionId,
      SUBSCRIPTION_VALIDITY_KEY: subscriptionValidity,
    });
  }

  /// Check If Document Exists
  static Future<bool> checkIfDocExists({
    required FirebaseFirestore fireStore,
    required String docId,
    required String collectionName,
  }) async {
    try {
      var doc = await fireStore.collection(collectionName).doc(docId).get();
      print("$TAG doc exists: ${doc.exists}");
      return doc.exists;
    } catch (e) {
      return false;
    }
  }

  static Stream<List<FirebaseUserModel>> getAllUsersFromFirebase(
      {required FirebaseFirestore fireStore, required String currentUserUid}) {
    List<FirebaseUserModel> fireBaseUsersList = [];
    return fireStore
        .collection(USER_COLLECTION_NAME)
        .where(UID_KEY, isNotEqualTo: currentUserUid)
        .snapshots()
        .map((snapshot) {
      fireBaseUsersList = snapshot.docs.map((doc) {
        var user = FirebaseUserModel.fromMap(doc.data());
        return user;
      }).toList();
      return fireBaseUsersList;
    });
  }

  static Stream<QuerySnapshot<Map<String, dynamic>>> getFirstUsersFromFirebase({
    required FirebaseFirestore fireStore,
    required String currentUserUid,
  }) {
    return fireStore
        .collection(USER_COLLECTION_NAME)
        .orderBy(UID_KEY)
        .where(UID_KEY, isNotEqualTo: currentUserUid)
        .limit(30)
        .snapshots();

  }

  static Stream<QuerySnapshot<Map<String, dynamic>>> getNextUsersFromFirebase(
      {required FirebaseFirestore fireStore,
      required String currentUserUid,
      required DocumentSnapshot? lastDoc}) {
    List<FirebaseUserModel> fireBaseUsersList = [];
    return fireStore
        .collection(USER_COLLECTION_NAME)
        .orderBy(UID_KEY)
        .where(UID_KEY, isNotEqualTo: currentUserUid)
        .startAfterDocument(lastDoc!)
        .limit(30)
        .snapshots();

  }

  static Stream<FirebaseUserModel> userData(
      {required FirebaseFirestore fireStore, required String userId}) {
    return fireStore
        .collection(USER_COLLECTION_NAME)
        .doc(userId)
        .snapshots()
        .map(
          (event) => FirebaseUserModel.fromMap(
            event.data()!,
          ),
        );
  }


  static Stream<List<MessageModel>> getChatStream(
      {required FirebaseFirestore fireStore,
      required String receiverUserId,
      required String currentUserid}) {
    return fireStore
        .collection(USER_COLLECTION_NAME)
        .doc(currentUserid)
        .collection(CHAT_COLLECTION_NAME)
        .doc(receiverUserId)
        .collection(MESSAGES_COLLECTION_NAME)
        .orderBy(TIME_SEND_KEY)
        .snapshots()
        .map((event) {
      List<MessageModel> messages = [];
      for (var document in event.docs) {
        messages.add(MessageModel.fromMap(document.data()));
      }
      return messages;
    });
  }

  static void sendTextMessage({
    required FirebaseFirestore firestore,
    required BuildContext context,
    required String text,
    required String receiverUserId,
    required FirebaseUserModel senderUser,
    required bool isGroupChat,
    required bool isSecretService,
    required String chatProfilePic,
  }) async {
    try {
    var timeSent = DateTime.now();
    FirebaseUserModel? receiverUserData;

    if (!isGroupChat) {
      var userDataMap = await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(receiverUserId)
          .get();
      receiverUserData = FirebaseUserModel.fromMap(userDataMap.data()!);
    } else {
      receiverUserData = senderUser;
    }

    var messageId = const Uuid().v1();

    _saveDataToContactsSubCollection(
      senderUserData: senderUser,
      receiverUserData: receiverUserData,
      text: text,
      timeSent: timeSent,
      receiverUserId: receiverUserId,
      isGroupChat: isGroupChat,
      firestore: firestore,
      isSecretService: isSecretService,
    );

    _saveMessageToMessageSubCollection(
        receiverUserId: receiverUserId,
        text: text,
        timeSent: timeSent,
        messageType: MessageEnum.text,
        messageId: messageId,
        username: senderUser.name ?? "",
        receiverUserName: receiverUserData.name,
        senderUsername: senderUser.name ?? "",
        firestore: firestore,
        currentUserUid: senderUser.uid,
        isGroupChat: isGroupChat,
        isSecretService: isSecretService,
        chatProfilePic: chatProfilePic);
    } catch (e) {
      print("$TAG sendTextMessage: error: ${e.toString()}");
    }
  }

  static void _saveDataToContactsSubCollection(
      {required FirebaseUserModel senderUserData,
      required FirebaseUserModel? receiverUserData,
      required String text,
      required DateTime timeSent,
      required String receiverUserId,
      required bool isGroupChat,
      required FirebaseFirestore firestore,
      required isSecretService}) async {
    if (isGroupChat) {
      var collectionName = isSecretService
          ? SECRET_SERVICE_COLLECTION_NAME
          : GROUP_COLLECTION_NAME;

      await firestore.collection(collectionName).doc(receiverUserId).update({
        LAST_MESSAGE_KEY: text,
        TIME_SEND_KEY: DateTime.now().millisecondsSinceEpoch,
        SENDER_NAME_KEY: receiverUserData!.name,
      });
    } else {
    // users -> receiver user id => chats -> current user id -> set data
      var receiverChatContact = ChatContact(
          name: senderUserData.name ?? "",
          profilePic: senderUserData.profilePic ?? "",
          contactId: senderUserData.uid,
          timeSent: timeSent,
          lastMessage: text,
          categoryTitle: senderUserData.categoryTitle);
      await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(receiverUserId)
          .collection(CHAT_COLLECTION_NAME)
          .doc(senderUserData.uid)
          .set(
            receiverChatContact.toMap(),
          );
      // users -> current user id  => chats -> receiver user id -> set data
      var senderChatContact = ChatContact(
          name: receiverUserData!.name ?? "",
          profilePic: receiverUserData.profilePic ?? "",
          contactId: receiverUserData.uid,
          timeSent: timeSent,
          lastMessage: text,
          categoryTitle: receiverUserData.categoryTitle);
      await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(senderUserData.uid)
          .collection(CHAT_COLLECTION_NAME)
          .doc(receiverUserId)
          .set(
            senderChatContact.toMap(),
          );
    }
  }

  static void _saveMessageToMessageSubCollection(
      {required FirebaseFirestore firestore,
      required String receiverUserId,
      required String text,
      required DateTime timeSent,
      required String messageId,
      required String username,
      required MessageEnum messageType,
      required String senderUsername,
      required String? receiverUserName,
      required bool isGroupChat,
      required String currentUserUid,
      required bool isSecretService,
      required String chatProfilePic}) async {

    final messageModel = MessageModel(
        senderId: currentUserUid,
        receiverid: receiverUserId,
        text: text,
        type: messageType,
        timeSent: timeSent,
        messageId: messageId,
        isSeen: false,
        senderName: senderUsername
        );
    if (isGroupChat) {
      // groups -> group id -> chat -> message

      var collectionName = isSecretService
          ? SECRET_SERVICE_COLLECTION_NAME
          : GROUP_COLLECTION_NAME;


      var groupData = firestore.collection(collectionName).doc(receiverUserId);

      var sendMsg = await groupData.get();

      var groupModel =
          GroupModel.fromMap(sendMsg.data() as Map<String, dynamic>);

      var membersList = groupModel.membersUid;
      membersList.removeWhere((element) => element == currentUserUid);


      await sendMsg.reference
          .collection(CHAT_COLLECTION_NAME)
          .doc(messageId)
          .set(
            messageModel.toMap(),
          );
      ApisServices.sendChatNotifications(
          name: groupModel.name,
          chatid: receiverUserId,
          isgroupchat: isGroupChat ? 1 : 0,
          profilepic: chatProfilePic,
          senderid: currentUserUid,
          issecretservice: isSecretService ? 1 : 0,
          message: text,
          receivers: membersList,
          authToken: SharedPrefs.getAccessToken());
    } else {
      // users -> sender id -> receiver id -> messages -> message id -> store message
      await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(currentUserUid)
          .collection(CHAT_COLLECTION_NAME)
          .doc(receiverUserId)
          .collection(MESSAGES_COLLECTION_NAME)
          .doc(messageId)
          .set(
            messageModel.toMap(),
          );
      // users -> receiver id  -> sender id -> messages -> message id -> store message
      await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(receiverUserId)
          .collection(CHAT_COLLECTION_NAME)
          .doc(currentUserUid)
          .collection(MESSAGES_COLLECTION_NAME)
          .doc(messageId)
          .set(
            messageModel.toMap(),
          );

      ApisServices.sendChatNotifications(
          name: senderUsername,
          chatid: currentUserUid,
          isgroupchat: isGroupChat ? 1 : 0,
          profilepic: chatProfilePic,
          senderid: receiverUserId,
          issecretservice: isSecretService ? 1 : 0,
          message: text,
          receivers: [receiverUserId],
          authToken: SharedPrefs.getAccessToken());
    }
  }

  static Stream<List<ChatContact>> getChatContacts(
      {required FirebaseFirestore firestore, required String currentUserUid}) {
    try {
      return firestore
          .collection(USER_COLLECTION_NAME)
          .doc(currentUserUid)
          .collection(CHAT_COLLECTION_NAME)
          .orderBy(TIME_SEND_KEY, descending: true)
          .snapshots()
          .asyncMap((event) async {
        List<ChatContact> contacts = [];
        for (var document in event.docs) {
          var chatContact = ChatContact.fromMap(document.data());
          var userData = await firestore
              .collection(USER_COLLECTION_NAME)
              .doc(chatContact.contactId)
              .get();
          var user = FirebaseUserModel.fromMap(userData.data()!);
          contacts.add(
            ChatContact(
                name: user.name ?? "",
                profilePic: user.profilePic ?? "",
                contactId: chatContact.contactId,
                timeSent: chatContact.timeSent,
                lastMessage: chatContact.lastMessage,
                categoryTitle: chatContact.categoryTitle),
          );
        }
        return contacts;
      });
    } catch (error) {
      print("$TAG getChatContacts: error: ${error.toString()}");
      return Stream.empty();
    }
  }

  static void setChatMessageSeen({
    required FirebaseFirestore firestore,
    required BuildContext context,
    required String receiverUserId,
    required String messageId,
    required String currentUserUid,
  }) async {
    try {
      await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(currentUserUid)
          .collection(CHAT_COLLECTION_NAME)
          .doc(receiverUserId)
          .collection(MESSAGES_COLLECTION_NAME)
          .doc(messageId)
          .update({IS_SEEN_KEY: true});

      await firestore
          .collection(USER_COLLECTION_NAME)
          .doc(receiverUserId)
          .collection(CHAT_COLLECTION_NAME)
          .doc(currentUserUid)
          .collection(MESSAGES_COLLECTION_NAME)
          .doc(messageId)
          .update({IS_SEEN_KEY: true});
    } catch (e) {
      print("$TAG setChatMessageSeen: error : ${e.toString()}");
    }
  }

  static void sendFileMessage({
    required FirebaseFirestore firestore,
    required FirebaseStorage firebaseStorage,
    required BuildContext context,
    required File file,
    required String receiverUserId,
    required FirebaseUserModel senderUserData,
    required MessageEnum messageEnum,
    required bool isGroupChat,
    required String currentUserUid,
    required bool isSecretService,
    required String chatProfilePic,
  }) async {
    try {
      var timeSent = DateTime.now();
      var messageId = const Uuid().v1();


      FirebaseUserModel? receiverUserData;
      if (!isGroupChat) {
        var userDataMap = await firestore
            .collection(USER_COLLECTION_NAME)
            .doc(receiverUserId)
            .get();
        receiverUserData = FirebaseUserModel.fromMap(userDataMap.data()!);
      } else {
        receiverUserData = senderUserData;
      }
      String imageUrl, filePath;
      if (!isGroupChat) {
        filePath =
            '$CHAT_COLLECTION_NAME/${messageEnum.type}/${senderUserData.uid}/$receiverUserId/$messageId';
        imageUrl = await storeFileToFirebase(
          firebaseStorage: firebaseStorage,
          filePathLocation: filePath,
          file: file,
        );
      } else {
        var collectionName = isSecretService
            ? SECRET_SERVICE_COLLECTION_NAME
            : GROUP_COLLECTION_NAME;
        filePath =
            '$collectionName/${messageEnum.type}/$receiverUserId/${senderUserData.uid}/$messageId';
        imageUrl = await storeFileToFirebase(
          firebaseStorage: firebaseStorage,
          filePathLocation: filePath,
          file: file,
        );
      }

      String contactMsg;

      switch (messageEnum) {
        case MessageEnum.image:
          contactMsg = '📷 Photo';
          break;
        case MessageEnum.video:
          contactMsg = '📸 Video';
          break;
        case MessageEnum.audio:
          contactMsg = '🎵 Audio';
          break;
        case MessageEnum.gif:
          contactMsg = 'GIF';
          break;
        default:
          contactMsg = '';
      }
      _saveDataToContactsSubCollection(
        senderUserData: senderUserData,
        receiverUserData: receiverUserData,
        text: contactMsg,
        timeSent: timeSent,
        receiverUserId: receiverUserId,
        isGroupChat: isGroupChat,
        firestore: firestore,
        isSecretService: isSecretService,
      );
      //
      _saveMessageToMessageSubCollection(
          receiverUserId: receiverUserId,
          text: imageUrl,
          timeSent: timeSent,
          messageId: messageId,
          username: senderUserData.name ?? "",
          messageType: messageEnum,
          receiverUserName: receiverUserData.name,
          senderUsername: senderUserData.name ?? "",
          isGroupChat: isGroupChat,
          firestore: firestore,
          currentUserUid: currentUserUid,
          isSecretService: isSecretService,
          chatProfilePic: chatProfilePic);
    } catch (e) {
      print("sendFileMessage : ${e.toString()}");
      HelperClass.show_snackbar(context, e.toString());
    }
  }

  static Future<String> storeFileToFirebase(
      {required FirebaseStorage firebaseStorage,
      required String filePathLocation,
      required File file}) async {
    UploadTask uploadTask =
        firebaseStorage.ref().child(filePathLocation).putFile(file);
    TaskSnapshot snap = await uploadTask;
    String downloadUrl = await snap.ref.getDownloadURL();
    return downloadUrl;
  }

  static Future<GroupModel?> createGroup({
    required FirebaseFirestore firestore,
    required FirebaseStorage firebaseStorage,
    required BuildContext context,
    required String groupName,
    required File imageFile,
    required List<FirebaseUserModel> selectedContacts,
    required String currentUserUid,
    required bool isSecretService,
  }) async {
    try {
      List<String> uidsList = [];
      uidsList.add(currentUserUid);
      for (var user in selectedContacts) {
        uidsList.add(user.uid);
      }

      var groupId = const Uuid().v1();

      var collectionName = isSecretService
          ? SECRET_SERVICE_COLLECTION_NAME
          : GROUP_COLLECTION_NAME;
      print("collection name: $collectionName");
      String imageUrl = await storeFileToFirebase(
        firebaseStorage: firebaseStorage,
        filePathLocation: '$collectionName/$groupId',
        file: imageFile,
      );

      var group = GroupModel(
        senderId: currentUserUid,
        name: groupName,
        groupId: groupId,
        lastMessage: "",
        senderName: "",
        groupPic: imageUrl,
        membersUid: uidsList,
        timeSent: DateTime.now(),
      );

      await firestore
          .collection(collectionName)
          .doc(groupId)
          .set(group.toMap());

      print(
          "$TAG secretService: $isSecretService :: createGroup: group :${group.toMap()}");
      return group;
    } catch (error) {
      print(
          "$TAG secretService: $isSecretService :: createGroup: error :${error.toString()}");
      HelperClass.show_snackbar(context, error.toString());
      return null;
    }
  }

  static Stream<List<GroupModel>> getChatGroups({
    required FirebaseFirestore firestore,
    required String currentUserUid,
    required bool isSecretService,
  }) {
    var collectionName = isSecretService
        ? SECRET_SERVICE_COLLECTION_NAME
        : GROUP_COLLECTION_NAME;

    return firestore
        .collection(collectionName)
        .orderBy(TIME_SEND_KEY, descending: true)
        .snapshots()
        .map((event) {
      List<GroupModel> groups = [];
      for (var document in event.docs) {
        var group = GroupModel.fromMap(document.data());
        if (group.membersUid.contains(currentUserUid)) {
          groups.add(group);
        }
      }
      return groups;
    });
  }

  static Stream<List<MessageModel>> getGroupChatStream(
      {required FirebaseFirestore firestore,
      required String groupId,
      required bool isSecretService}) {
    var collectionName = isSecretService
        ? SECRET_SERVICE_COLLECTION_NAME
        : GROUP_COLLECTION_NAME;

    return firestore
        .collection(collectionName)
        .doc(groupId)
        .collection(CHAT_COLLECTION_NAME)
        .orderBy(TIME_SEND_KEY)
        .snapshots()
        .map((event) {
      List<MessageModel> messages = [];
      for (var document in event.docs) {
        messages.add(MessageModel.fromMap(document.data()));
      }
      return messages;
    });
  }

  static Future<bool> deleteChat({
    required FirebaseFirestore firestore,
    required var myUserId,
    required var contactId,
  }) async {
    print("delete chat: my user id: $myUserId :: contact id: $contactId");

    /// delete from my chats
    try {
      final chatDocumentReference = firestore
          .collection(USER_COLLECTION_NAME)
          .doc(myUserId)
          .collection(CHAT_COLLECTION_NAME)
          .doc(contactId);

      // Reference to the subcollection
      final subcollectionReference =
          chatDocumentReference.collection(MESSAGES_COLLECTION_NAME);

      // Query all documents in the subcollection
      QuerySnapshot subcollectionQuery = await subcollectionReference.get();

      // Delete all documents in the subcollection
      for (QueryDocumentSnapshot doc in subcollectionQuery.docs) {
        print(
            "delete chat:delete from my chats: message id : ${doc.reference.id}");
        await doc.reference.delete();
      }

      // Delete the main document
      await chatDocumentReference.delete();

      // Document and subcollection successfully deleted

      print("delete chat:delete from my chats: try: doc deleted ");
      return true;
    } catch (e) {
      print("delete chat:delete from my chats: catch: ${e.toString()}");
      return false;
    }
  }

  static Future<List<String>> getGroupChatUserNamesList({
    required FirebaseFirestore firestore,
    required var groupId,
  }) async {
    print("getGroupChatUserNamesList: groupId: ${groupId}");
    List<String> userNames = [];
    try {
      var docRef =
          await firestore.collection(GROUP_COLLECTION_NAME).doc(groupId).get();
      if (docRef.exists &&
          docRef.data() != null &&
          docRef.data()!.containsKey(MEMBERS_UID_KEY)) {
        final data = docRef.data();
        userNames = data![MEMBERS_UID_KEY];
        return userNames;
      } else {
        return userNames;
      }
    } catch (e) {
      print("getGroupChatUserNamesList: error: ${e.toString()}");
      return userNames;
    }
  }

  static Future<bool> exitGroupChat(
      {required FirebaseFirestore firestore,
      required var groupId,
      required var userUid,
      required bool isSecretService}) async {
    print("exitGroupChat: group: event groupId: $groupId :: userUid: $userUid");
    try {
      var groupDocRef = firestore
          .collection(isSecretService
              ? SECRET_SERVICE_COLLECTION_NAME
              : GROUP_COLLECTION_NAME)
          .doc(groupId);

      var docRef = await groupDocRef.get();

      // final data =
      //     docRef.data() as Map<String, dynamic>; // Correct type casting
      final data = GroupModel.fromMap(docRef.data() as Map<String, dynamic>);
      print("exitGroupChat: group: event data: $data");

      if (docRef.exists && data.membersUid.isNotEmpty) {
        // List<String> usersList = List<String>.from(data[MEMBERS_UID_KEY]);
        List<String> usersList = data.membersUid;
        print(
            "exitGroupChat: userList: $usersList :: uid check: ${usersList.contains(userUid)}"
            " array length: ${usersList.length} :: array length check: ${usersList.length <= 1}");
        if (usersList.length <= 1) {
          /// delete the entire doc

          final groupChatCollectionReference =
              groupDocRef.collection(CHAT_COLLECTION_NAME);
          QuerySnapshot groupChatCollectionQuery =
              await groupChatCollectionReference.get();

          // Delete all documents in the subCollection
          for (QueryDocumentSnapshot doc in groupChatCollectionQuery.docs) {
            // print(
            //     "exitGroupChat: delete chat:delete from my chats: message id : ${doc.reference.id}");
            await doc.reference.delete();
          }
          await groupDocRef.delete();
          print("exitGroupChat: deleted entire doc");
          return true;
        } else {
          /// delete user

          if (usersList.contains(userUid)) {
            print(
                "exitGroupChat: admin check: ${data.senderId == userUid} :: usersList before delete: $usersList");
            var response = await updateMembersInGroup(
              firestore: firestore,
              groupId: groupId,
              isRemove: true,
              userUid: [userUid],
              isSecretService: isSecretService,
            );
            if (response) {
              usersList.remove(userUid);
              print(
                  "exitGroupChat: admin check: ${data.senderId == userUid} :: "
                  "usersList after delete: $usersList :: first item: ${usersList[0]}");
              try {
                await firestore
                    .collection(isSecretService
                        ? SECRET_SERVICE_COLLECTION_NAME
                        : GROUP_COLLECTION_NAME)
                    .doc(groupId)
                    .update({
                  SENDER_ID_KEY: usersList[0],
                });
                return true;
              } catch (err) {
                return false;
              }
            }
            return response;
          }
          return false;
        }
      }
      return false;
    } catch (error) {
      print("exitGroupChat: error: ${error.toString()}");
      return false;
    }
  }

  static Future<bool> updateMembersInGroup({
    required FirebaseFirestore firestore,
    required var groupId,
    required bool isRemove,
    required List<String> userUid,
    required bool isSecretService,
  }) async {
    print(
        "updateMembersInGroup: user uid: $userUid :: isRemove: $isRemove :: groupId: $groupId");
    var fieldValue = isRemove
        ? FieldValue.arrayRemove(userUid)
        : FieldValue.arrayUnion(userUid);
    try {
      await firestore
          .collection(isSecretService
              ? SECRET_SERVICE_COLLECTION_NAME
              : GROUP_COLLECTION_NAME)
          .doc(groupId)
          .update({
        MEMBERS_UID_KEY: fieldValue,
      });

      print("updateMembersInGroup: group data updated");
      return true;
    } catch (e) {
      print("updateMembersInGroup: error: ${e.toString()}");
      return false;
    }
  }

  static Stream<GroupModel> getGroupDataStreamFromGroupId({
    required FirebaseFirestore firestore,
    required var groupId,
  }) {
    return firestore
        .collection(GROUP_COLLECTION_NAME)
        .doc(groupId)
        .snapshots()
        .map((event) {
      // print("getMembersUidFromGroupId: group: event data: ${event.data()}");
      // List<String> userNames = [];
      // if (event.exists &&
      //     event.data() != null &&
      //     event.data()!.containsKey(MEMBERS_UID_KEY)) {
      //   userNames = event.data()![MEMBERS_UID_KEY];
      // }
      // print("getMembersUidFromGroupId: group: userNames: $userNames");
      final data = event.data() as Map<String, dynamic>; // Correct type casting
      print("getMembersUidFromGroupId: group: event data: $data");

      return GroupModel.fromMap(data);

      // List<String> userNames = [];
      //
      // if (data.containsKey(MEMBERS_UID_KEY)) {
      //   userNames = List<String>.from(data[MEMBERS_UID_KEY]);
      // }
      //
      // print("getMembersUidFromGroupId: group: userNames: $userNames");
      // return userNames;
    });
  }

  static Stream<List<FirebaseUserModel>> userNamesFromUid({
    required FirebaseFirestore firestore,
    required List uids,
  }) {
    print("userNamesFromUid: uid: $uids");
    return firestore
        .collection(USER_COLLECTION_NAME)
        .where(UID_KEY, whereIn: uids)
        .snapshots()
        .map((event) {
      print("userNamesFromUid:event: ${event.docs}");
      List<FirebaseUserModel> userNames = [];
      for (var user in event.docs) {
        userNames.add(FirebaseUserModel.fromMap(user.data()));
      }
      print("userNamesFromUid:userNames: $userNames");
      return userNames;
    });
  }

  static deleteUserFromFirebase({
    required FirebaseFirestore firestore,
    required var uid,
  }) async {
    print("$TAG : deleteUserFromFirebase: uid: $uid");
    try {
      var allUsers = await firestore
          .collection(USER_COLLECTION_NAME)
          .where(UID_KEY, isEqualTo: uid)
          .get();
      allUsers.docs.map((allUsersSnapshot) async {
        print(
            "$TAG: deleteUserFromFirebase:allUsersSnapshot id: ${allUsersSnapshot.reference.id} :: allUsersSnapshot length: ${allUsers.docs.length}");
        QuerySnapshot chatSubCollectionSnapshot = await allUsersSnapshot
            .reference
            .collection(CHAT_COLLECTION_NAME)
            .get();
        print(
            "$TAG:deleteUserFromFirebase: check chats available: ${chatSubCollectionSnapshot.docs.isNotEmpty}");
        if (chatSubCollectionSnapshot.docs.isNotEmpty) {
          var userIds = await allUsersSnapshot.reference
              .collection(CHAT_COLLECTION_NAME)
              .get();

          userIds.docs.map((userChatIdSnap) async {
            // print(
            //     "$TAG:deleteUserFromFirebase: allUsersSnapshot id: ${allUsersSnapshot.reference.id}"
            //     " :: userSnap id: ${userChatIdSnap.id}");

            var userMessagesQuerySnapshot = await userChatIdSnap.reference
                .collection(MESSAGES_COLLECTION_NAME)
                .get();

            print(
                "$TAG:deleteUserFromFirebase: check messages available: ${userMessagesQuerySnapshot.docs.isNotEmpty}");

            userMessagesQuerySnapshot.docs.map((msgSnapshot) async {
              print(
                  "$TAG : deleteUserFromFirebase:userChatIdSnap: ${userChatIdSnap.id} :: message id: ${msgSnapshot.reference.id}");
              await msgSnapshot.reference.delete();
              print("$TAG:deleteUserFromFirebase: message deleted");
            }).toList();
            await userChatIdSnap.reference.delete();
            print("$TAG:deleteUserFromFirebase: userChatIdSnap deleted");
          }).toList();
        }
        await allUsersSnapshot.reference.delete();
        print("$TAG:deleteUserFromFirebase: user doc deleted");
      }).toList();

      /// group chats check
      var groupDataCheck = await firestore
          .collection(GROUP_COLLECTION_NAME)
          .where(MEMBERS_UID_KEY, arrayContains: uid)
          .get();

      print(
          "$TAG deleteUserFromFirebase: groupDataCheck: ${groupDataCheck.docs.length}");

      groupDataCheck.docs.map((groupDataSnapshot) {
        print(
            "$TAG deleteUserFromFirebase: groupDataCheck:groupDataSnapshot ${groupDataSnapshot.reference.id}");
        exitGroupChat(
          firestore: firestore,
          groupId: groupDataSnapshot.reference.id,
          userUid: uid,
          isSecretService: false,
        );
      }).toList();

      /// secret group chats check
      var secretGroupDataCheck = await firestore
          .collection(SECRET_SERVICE_COLLECTION_NAME)
          .where(MEMBERS_UID_KEY, arrayContains: uid)
          .get();

      print(
          "$TAG deleteUserFromFirebase: secretGroupDataCheck: ${secretGroupDataCheck.docs.length}");

      secretGroupDataCheck.docs.map((secretGroupDataSnapshot) {
        print(
            "$TAG deleteUserFromFirebase: secretGroupDataCheck:groupDataSnapshot ${secretGroupDataSnapshot.reference.id}");
        exitGroupChat(
          firestore: firestore,
          groupId: secretGroupDataSnapshot.reference.id,
          userUid: uid,
          isSecretService: true,
        );
      }).toList();
    } catch (e) {
      print("$TAG deleteUserFromFirebase: error: ${e.toString()}");
    }
  }
}
