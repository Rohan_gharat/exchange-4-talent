import 'package:e4t/firebase/firebaseHelper.dart';

class ChatContact {
  final String? name;
  final String? profilePic;
  final String contactId;
  final DateTime timeSent;
  final String lastMessage;
  final String? categoryTitle;

  ChatContact({
    required this.name,
    required this.profilePic,
    required this.contactId,
    required this.timeSent,
    required this.lastMessage,
    required this.categoryTitle,
  });

  Map<String, dynamic> toMap() {
    return {
      FireBaseHelperClass.NAME_KEY: name,
      FireBaseHelperClass.PROFILE_PIC_KEY: profilePic,
      FireBaseHelperClass.CONTACT_ID_KEY: contactId,
      FireBaseHelperClass.TIME_SEND_KEY: timeSent.millisecondsSinceEpoch,
      FireBaseHelperClass.LAST_MESSAGE_KEY: lastMessage,
      FireBaseHelperClass.CATEGORY_TITLE_KEY: categoryTitle,
    };
  }

  factory ChatContact.fromMap(Map<String, dynamic> map) {
    return ChatContact(
        name: map[FireBaseHelperClass.NAME_KEY] ?? '',
        profilePic: map[FireBaseHelperClass.PROFILE_PIC_KEY] ?? '',
        contactId: map[FireBaseHelperClass.CONTACT_ID_KEY] ?? '',
        timeSent: DateTime.fromMillisecondsSinceEpoch(
            map[FireBaseHelperClass.TIME_SEND_KEY]),
        lastMessage: map[FireBaseHelperClass.LAST_MESSAGE_KEY] ?? '',
        categoryTitle: map[FireBaseHelperClass.CATEGORY_TITLE_KEY]);
  }
}
