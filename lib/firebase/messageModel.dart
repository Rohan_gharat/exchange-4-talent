import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/firebase/messageEnum.dart';

class MessageModel {
  final String senderId;
  final String receiverid;
  final String? text;
  final MessageEnum type;
  final DateTime timeSent;
  final String messageId;
  final bool isSeen;
final String? senderName;
  MessageModel({
    required this.senderId,
    required this.receiverid,
    required this.text,
    required this.type,
    required this.timeSent,
    required this.messageId,
    required this.isSeen,
    required this.senderName,
  });

  Map<String, dynamic> toMap() {
    return {
      FireBaseHelperClass.SENDER_ID_KEY: senderId,
      FireBaseHelperClass.RECEIVER_ID_KEY: receiverid,
      FireBaseHelperClass.MESSAGE_KEY: text,
      FireBaseHelperClass.MESSAGE_TYPE_ID_KEY: type.type,
      FireBaseHelperClass.TIME_SEND_KEY: timeSent.millisecondsSinceEpoch,
      FireBaseHelperClass.MESSAGE_ID_KEY: messageId,
      FireBaseHelperClass.IS_SEEN_KEY: isSeen,
      FireBaseHelperClass.SENDER_NAME_KEY:senderName,
    };
  }

  factory MessageModel.fromMap(Map<String, dynamic> map) {
    return MessageModel(
      senderId: map[FireBaseHelperClass.SENDER_ID_KEY] ?? '',
      receiverid: map[FireBaseHelperClass.RECEIVER_ID_KEY] ?? '',
      text: map[FireBaseHelperClass.MESSAGE_KEY] ?? '',
      type: (map[FireBaseHelperClass.MESSAGE_TYPE_ID_KEY] as String).toEnum(),
      timeSent: DateTime.fromMillisecondsSinceEpoch(
          map[FireBaseHelperClass.TIME_SEND_KEY]),
      messageId: map[FireBaseHelperClass.MESSAGE_ID_KEY] ?? '',
      isSeen: map[FireBaseHelperClass.IS_SEEN_KEY] ?? false,
      senderName: map[FireBaseHelperClass.SENDER_NAME_KEY]
    );
  }
}
