import 'package:e4t/routes.dart';
import 'package:flutter/material.dart';

import '../screens/search/search_screen.dart';

class SearchNavigator extends StatelessWidget {
  var navigator_Key;

  SearchNavigator({Key? key, required this.navigator_Key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
        key: navigator_Key,
        initialRoute: SearchScreen.SearchRoute,
        onGenerateRoute: (settings) => AppRoutes.generateRoute(settings));
  }
}
