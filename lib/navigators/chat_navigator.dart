import 'package:e4t/routes.dart';
import 'package:e4t/screens/chat/chat_screen.dart';
import 'package:e4t/screens/subscriptions/add_subscription_screen.dart';
import 'package:flutter/material.dart';

class ChatNavigator extends StatelessWidget {
  var navigator_Key;
  var subscriptionValidity;

  ChatNavigator(
      {Key? key,
      required this.navigator_Key,
      required this.subscriptionValidity})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
        key: navigator_Key,
        initialRoute: subscriptionValidity == null ||
                subscriptionValidity?.toString().trim() == ""
            ? AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN
            : ChatScreen.CHAT_ROUTE,
        onGenerateRoute: (settings) => AppRoutes.generateRoute(settings));
  }
}
