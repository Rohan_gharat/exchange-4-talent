import 'package:e4t/routes.dart';
import 'package:e4t/screens/Accounts/accounts_screen.dart';
import 'package:flutter/material.dart';


class AccountNavigator extends StatelessWidget {
  var navigator_Key;

  AccountNavigator({Key? key, required this.navigator_Key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigator_Key,
      initialRoute: AccountsScreen.ACCOUNTS_ROUTE,
      onGenerateRoute: (settings) =>
          AppRoutes.generateRoute(settings)
    );
  }
}
