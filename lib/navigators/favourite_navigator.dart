import 'package:e4t/routes.dart';
import 'package:e4t/screens/favourite/favourite_screen.dart';
import 'package:flutter/material.dart';


class FavouriteNavigator extends StatelessWidget {
  var navigator_Key;

  FavouriteNavigator({Key? key, required this.navigator_Key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigator_Key,
      initialRoute: FavouriteScreen.FAVOURITE_ROUTE,
      onGenerateRoute: (settings) =>
          AppRoutes.generateRoute(settings)

    );
  }
}
