import 'package:e4t/routes.dart';
import 'package:e4t/screens/home/homeScreenV1.dart';
import 'package:flutter/material.dart';

class HomeNavigator extends StatelessWidget {
  var navigator_Key;

  HomeNavigator({Key? key, required this.navigator_Key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigator_Key,
      initialRoute: HomeScreenV1.HomeRouteV1,
      onGenerateRoute: (settings) => AppRoutes.generateRoute(settings),
    );
  }
}
