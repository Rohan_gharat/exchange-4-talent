class RatingDetailsModel {
  int total;
  String average;
  int star;

  RatingDetailsModel({
    required this.total,
    required this.average,
    required this.star,
  });

  factory RatingDetailsModel.fromJson(Map<String, dynamic> json) =>
      RatingDetailsModel(
        total: json["total"],
        average: json["average"],
        star: json["star"],
      );
}
