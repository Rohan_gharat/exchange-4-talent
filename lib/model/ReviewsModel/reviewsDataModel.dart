class ReviewsDataModel {
  int? user_id;
  String? username;
  String? avatar;
  String? comment;
  String? rating;

  ReviewsDataModel({
    this.user_id,
    this.username,
    this.avatar,
    this.comment,
    this.rating,
  });

  factory ReviewsDataModel.fromJson(Map<String, dynamic> json) =>
      ReviewsDataModel(
        user_id: json["user_id"],
        username: json["username"],
        avatar: json["avatar"],
        comment: json["comment"],
        rating: json["rating"],
      );
}
