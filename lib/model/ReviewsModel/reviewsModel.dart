import 'package:e4t/model/ReviewsModel/reviewsDataModel.dart';

class ReviewsModel {
  int status;
  String message;
  List<ReviewsDataModel> reviewsList;

  ReviewsModel({
    required this.status,
    required this.message,
    required this.reviewsList,
  });

  factory ReviewsModel.fromJson(Map<String, dynamic> json) => ReviewsModel(
        status: json["status"],
        message: json["message"],
        reviewsList: List<ReviewsDataModel>.from(
            json["reviews"].map((x) => ReviewsDataModel.fromJson(x))),
      );
}
