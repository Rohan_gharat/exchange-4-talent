class AddReviewModel {
  int? status;
  String? average_rating;
  String? message;

  AddReviewModel({
    this.status,
    this.average_rating,
    this.message,
  });

  factory AddReviewModel.fromJson(Map<String, dynamic> json) => AddReviewModel(
        status: json["status"],
        average_rating: json["average_rating"],
        message: json["message"],
      );
}
