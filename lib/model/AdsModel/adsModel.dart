import 'dart:convert';

AdsModel adsModelFromJson(String str) => AdsModel.fromJson(json.decode(str));

String adsModelToJson(AdsModel data) => json.encode(data.toJson());

class AdsModel {
  int status;
  List<AdsDataModel>? adsDataList;

  AdsModel({
    required this.status,
    this.adsDataList,
  });

  factory AdsModel.fromJson(Map<String, dynamic> json) => AdsModel(
        status: json["status"],
        adsDataList: json["ads"] == null
            ? []
            : List<AdsDataModel>.from(
                json["ads"].map((x) => AdsDataModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "ads": List<dynamic>.from(adsDataList!.map((x) => x.toJson())),
      };
}

class AdsDataModel {
  int id;
  String? videos;
  dynamic images;

  AdsDataModel({
    required this.id,
    this.videos,
    this.images,
  });

  factory AdsDataModel.fromJson(Map<String, dynamic> json) => AdsDataModel(
        id: json["id"],
        videos: json["videos"],
        images: json["images"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "videos": videos,
        "images": images,
      };
}
