class UserBannerModel {
  String? url;
  String? thumbnail;
  String type;
  var host;

  UserBannerModel({
    this.url,
    this.thumbnail,
    required this.type,
    this.host,
  });

  factory UserBannerModel.fromJson(Map<String, dynamic> json) =>
      UserBannerModel(
        type: json["type"],
        url: json["url"],
        thumbnail: json["thumbnail"],
        host: json["host"],
      );

  toJson() => {
        "type": type,
        "url": url,
        "thumbnail": thumbnail,
        "host": host,
      };
}
