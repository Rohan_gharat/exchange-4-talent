
import 'dart:convert';

UploadImageModel uploadImageModelFromJson(String str) =>
    UploadImageModel.fromJson(json.decode(str));

String uploadImageModelToJson(UploadImageModel data) =>
    json.encode(data.toJson());

class UploadImageModel {
  int? status;
  int? subscription_id;
  String? message;

  UploadImageModel({
    this.status,
    this.subscription_id,
    this.message,
  });

  factory UploadImageModel.fromJson(Map<String, dynamic> json) =>
      UploadImageModel(
        status: json["status"],
        subscription_id: json["subscription_id"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "subscription_id": subscription_id,
        "message": message,
      };
}
