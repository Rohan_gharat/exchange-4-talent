import 'dart:convert';

import 'dart:convert';

class UserDeleteModel {
  var status;
  var message;

  UserDeleteModel({
    this.status,
    this.message,
  });

  factory UserDeleteModel.fromJson(Map<String, dynamic> json) =>
      UserDeleteModel(
        status: json["status"],
        message: json["message"],
      );
}
