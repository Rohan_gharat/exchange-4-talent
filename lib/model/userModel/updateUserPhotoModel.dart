class UpdateUserPhotoModel {
  var status;
  var message;
  var path;

  UpdateUserPhotoModel({
    this.status,
    this.message,
    this.path,
  });

  factory UpdateUserPhotoModel.fromJson(Map<String, dynamic> json) =>
      UpdateUserPhotoModel(
        status: json["status"],
        message: json["message"],
        path: json["path"],
      );
}
