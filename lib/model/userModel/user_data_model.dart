import 'package:e4t/model/userModel/userBannerModel.dart';
import 'package:e4t/model/userModel/user_model.dart';

import '../ReviewsModel/ratingDetailsModel.dart';

class UserDataModel {
  int status;
  String? message;
  UserModel? userModel;
  List<UserModel>? randomUsersList;
  List<UserModel>? allUsersList;
  List<RatingDetailsModel>? ratingDetailsList;
  List<UserBannerModel>? userBannersList;
  int? video_duration;

  UserDataModel({
    required this.status,
    required this.userModel,
    required this.randomUsersList,
    this.message,
    this.allUsersList,
    this.ratingDetailsList,
    this.userBannersList,
    this.video_duration,
  });

  factory UserDataModel.fromJson(Map<String, dynamic> json) => UserDataModel(
        status: json["status"],
        userModel:
            json["user"] == null ? null : UserModel.fromJson(json["user"]),
        randomUsersList: json["random_users"] == null
            ? []
            : List<UserModel>.from(
                json["random_users"].map((x) => UserModel.fromJson(x))),
        allUsersList: json["users"] == null
            ? []
            : List<UserModel>.from(
                json["users"].map((x) => UserModel.fromJson(x))),
        ratingDetailsList: json["ratings"] == null
            ? []
            : List<RatingDetailsModel>.from(
                json["ratings"].map((x) => RatingDetailsModel.fromJson(x))),
        userBannersList: json["banner"] == null
            ? []
            : List<UserBannerModel>.from(
                json["banner"].map((x) => UserBannerModel.fromJson(x))),
        message: json["message"],
        video_duration:
            json["video_duration"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "user": userModel?.toJson(),
        "message": message,
      };
}
