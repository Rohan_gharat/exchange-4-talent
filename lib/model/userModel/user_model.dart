class UserModel {
  var id;
  var firebaseId;
  var name;
  var mobile;
  var phone_code;
  var email;
  var description;
  var mobileOtp;
  var emailOtp;
  var mobileVerified;
  var emailVerified;
  var avatar;
  var dob;
  var talentType;
  var gender;
  var address;
  var categoryId;
  var subcategoryId;
  int? isfavourite;
  var tradeBarter;
  var subscriptionId;
  var subscriptionValidity;
  var subscribedOn;
  var bookingFee;
  var tags;
  var rating;
  var createdAt;
  var updatedAt;
  var country;
  var state;
  var city;
  var fb_link;
  var insta_link;
  var linkedin_link;
  var wikipedia_link;
  var imdb_link;
  var color_code;
  var category_title;
  var barter_description;
  var flag;

  UserModel({
    this.id,
    this.firebaseId,
    this.name,
    this.mobile,
    this.phone_code,
    this.email,
    this.description,
    this.mobileOtp,
    this.emailOtp,
    this.mobileVerified,
    this.emailVerified,
    this.avatar,
    this.dob,
    this.talentType,
    this.gender,
    this.address,
    this.categoryId,
    this.subcategoryId,
    this.isfavourite,
    this.tradeBarter,
    this.subscriptionId,
    this.subscriptionValidity,
    this.subscribedOn,
    this.bookingFee,
    this.tags,
    this.rating,
    this.createdAt,
    this.updatedAt,
    this.country,
    this.state,
    this.city,
    this.fb_link,
    this.insta_link,
    this.linkedin_link,
    this.wikipedia_link,
    this.imdb_link,
    this.color_code,
    this.category_title,
    this.barter_description,
    this.flag,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json['id'],
        firebaseId: json['firebase_id'],
        name: json['name'],
        mobile: json['mobile'],
        phone_code: json['phone_code'],
        email: json['email'],
        description: json['description'],
        mobileOtp: json['mobile_otp'],
        emailOtp: json['email_otp'],
        mobileVerified: json['mobile_verified'],
        emailVerified: json['email_verified'],
        avatar: json['avatar'],
        dob: json['dob'],
        talentType: json['talent_type'],
        gender: json['gender'],
        address: json['address'],
        categoryId: json['category_id'],
        subcategoryId: json['subcategory_id'],
        isfavourite: json['isfavourite'],
        tradeBarter: json['trade_barter'],
        subscriptionId: json['subscription_id'],
        subscriptionValidity: json['subscription_validity'],
        subscribedOn: json['subscribed_on'],
        bookingFee: json['booking_fee'],
        tags: json['tags'],
        rating: json['rating'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        country: json['country'],
        state: json['state'],
        city: json['city'],
        fb_link: json['facebook_link'],
        insta_link: json['instagram_link'],
        linkedin_link: json['linkedIn_link'],
        wikipedia_link: json['wikipedia_link'],
        imdb_link: json['imdb_link'],
        color_code: json['color_code'],
        category_title: json['category_title'],
        barter_description: json['barter_description'],
        flag: json['flag'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['firebase_id'] = firebaseId;
    data['name'] = name;
    data['mobile'] = mobile;
    data['phone_code'] = phone_code;
    data['description'] = description;
    data['email'] = email;
    data['mobile_otp'] = mobileOtp;
    data['email_otp'] = emailOtp;
    data['mobile_verified'] = mobileVerified;
    data['email_verified'] = emailVerified;
    data['avatar'] = avatar;
    data['dob'] = dob;
    data['talent_type'] = talentType;
    data['gender'] = gender;
    data['address'] = address;
    data['category_id'] = categoryId;
    data['subcategory_id'] = subcategoryId;
    data['isfavourite'] = isfavourite;
    data['trade_barter'] = tradeBarter;
    data['subscription_id'] = subscriptionId;
    data['subscription_validity'] = subscriptionValidity;
    data['subscribed_on'] = subscribedOn;
    data['booking_fee'] = bookingFee;
    data['tags'] = tags;
    data['rating'] = rating;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['country'] = country;
    data['state'] = state;
    data['city'] = city;
    data['facebook_link'] = fb_link;
    data['instagram_link'] = insta_link;
    data['linkedIn_link'] = linkedin_link;
    data['wikipedia_link'] = wikipedia_link;
    data['imdb_link'] = imdb_link;
    data['color_code'] = color_code;
    data['category_title'] = category_title;
    data['barter_description'] = barter_description;
    data['flag'] = flag;
    return data;
  }
}
