import 'package:e4t/model/userModel/user_model.dart';

class OTPDataModel {
  var token;
  var userId;
  var category;
  var subscription;
  UserModel? usermodel;

  OTPDataModel(
      {this.token,
      this.userId,
      this.category,
      this.subscription,
      this.usermodel});

  factory OTPDataModel.fromJson(Map<String, dynamic> json) => OTPDataModel(
        token: json["token"],
        userId: json["userId"],
        category: json["category"],
        subscription: json["subscription"],
        usermodel: json["User_data"] == null
            ? null
            : UserModel.fromJson(json["User_data"]),
      );

  toJson() => {
        "token": token,
        "userId": userId,
        "category": category,
        "subscription": subscription,
        "User_data": usermodel,
      };
}
