

import 'package:e4t/model/authModel/otpDataModel.dart';

class OTPModel {
  var status;
  var message;
  OTPDataModel? otpDataModel;

  OTPModel({this.status, this.message, this.otpDataModel});

  factory OTPModel.fromJson(Map<String, dynamic> json) => OTPModel(
        status: json['status'],
        message: json['message'],
        otpDataModel:
            json['data'] != null ? OTPDataModel.fromJson(json['data']) : null,
      );

  toJson() => {
        "status": status,
        "message": message,
        "data": otpDataModel?.toJson(),
      };
}
