class LoginModel {
  var status;
  var message;
  LoginDataModel? loginDataModel;

  LoginModel({this.status, this.message, this.loginDataModel});

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        status: json["status"],
        message: json["message"],
        loginDataModel:
            json["data"] == null ? null : LoginDataModel.fromJson(json["data"]),
      );

  toJson() => {
        "status": status,
        "message": message,
      };
}

class LoginDataModel {
  var otp;

  LoginDataModel({
    this.otp,
  });

  factory LoginDataModel.fromJson(Map<String, dynamic> json) => LoginDataModel(
        otp: json["otp"],
      );
}
