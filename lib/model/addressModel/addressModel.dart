class AddressModel {
  int? id;
  String? name;
  var phonecode;
  var iso3;

  AddressModel({
    this.id,
    this.name,
    this.phonecode,
    this.iso3,
  });

  factory AddressModel.fromJson(Map<String, dynamic> json) => AddressModel(
        id: json["id"],
        name: json["name"],
        phonecode: json["phonecode"],
        iso3: json["iso3"],
      );
}
