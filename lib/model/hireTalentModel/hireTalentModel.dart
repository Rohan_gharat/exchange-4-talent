class HireTalentModel {
  var status;
  var message;
  var subcriptions_id;
  HireTalentDataModel? hireTalentDataModel;

  HireTalentModel({
    this.status,
    this.message,
    this.subcriptions_id,
    this.hireTalentDataModel,
  });

  factory HireTalentModel.fromJson(Map<String, dynamic> json) =>
      HireTalentModel(
        status: json["status"],
        message: json["message"],
        subcriptions_id: json["subcriptions_id"],
        hireTalentDataModel: json["talent_data"] == null
            ? null
            : HireTalentDataModel.fromJson(json["talent_data"]),
      );
}

class HireTalentDataModel {
  String? mobile;
  String? email;

  HireTalentDataModel({
    this.mobile,
    this.email,
  });

  factory HireTalentDataModel.fromJson(Map<String, dynamic> json) =>
      HireTalentDataModel(
        mobile: json["mobile"],
        email: json["email"],
      );
}
