
import 'dart:convert';

import 'package:e4t/model/userModel/user_model.dart';

GetTradeBarterModel getTradeBarterModelFromJson(String str) =>
    GetTradeBarterModel.fromJson(json.decode(str));

String getTradeBarterModelToJson(GetTradeBarterModel data) =>
    json.encode(data.toJson());

class GetTradeBarterModel {
  int status;
  TradeBarterDataModel? tradeBarterDataModel;
  String? message;

  GetTradeBarterModel({
    required this.status,
    required this.tradeBarterDataModel,
    this.message,
  });

  factory GetTradeBarterModel.fromJson(Map<String, dynamic> json) =>
      GetTradeBarterModel(
        status: json["status"],
        tradeBarterDataModel: json["data"] == null
            ? null
            : TradeBarterDataModel.fromJson(json["data"]),
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": tradeBarterDataModel!.toJson(),
        "message": message,
      };
}

class TradeBarterDataModel {
  List<UserModel>? tradeBartersList;

  String? nextPageUrl;

  TradeBarterDataModel({
    required this.tradeBartersList,
    required this.nextPageUrl,
  });

  factory TradeBarterDataModel.fromJson(Map<String, dynamic> json) =>
      TradeBarterDataModel(
        tradeBartersList: json["data"] == null
            ? []
            : List<UserModel>.from(
                json["data"].map((x) => UserModel.fromJson(x))),
        nextPageUrl:
            json["next_page_url"] == null ? null : json["next_page_url"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(tradeBartersList!.map((x) => x.toJson())),
        "next_page_url": nextPageUrl,
      };
}
