class VideosBannerModel {
  int? id;
  String? title;
  String? description;
  String? image;
  String? video;
  String? types;

  VideosBannerModel({
    this.id,
    this.title,
    this.description,
    this.image,
    this.video,
    this.types,
  });

  factory VideosBannerModel.fromJson(Map<String, dynamic> json) => VideosBannerModel(
    id: json["id"],
    title: json["title"],
    description: json["description"],
    image: json["image"],
    video: json["video"],
    types: json["types"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "description": description,
    "image": image,
    "video": video,
    "types": types,
  };
}