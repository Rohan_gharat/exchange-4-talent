import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:e4t/model/homeModel/videosBannerModel.dart';
import 'package:e4t/model/recruitersModel/recruitersModel.dart';
import 'package:e4t/model/subscriptionsModel/subcriptions_model.dart';
import 'package:e4t/model/userModel/user_model.dart';

class HomeModel {
  var status;
  List<CategoriesModel>? homeCategoriesList;
  List<UserModel>? randomUsersList;
  List<VideosBannerModel>? bannersVideosList;
  List<SubscriptionsModel>? horizontalSubscriptionsList;
  List<SubscriptionsModel>? verticalSubscriptionsList;
  List<RecruitersModel>? recruitersList;

  HomeModel({
    this.status,
    this.homeCategoriesList,
    this.randomUsersList,
    this.bannersVideosList,
    this.horizontalSubscriptionsList,
    this.verticalSubscriptionsList,
    this.recruitersList,
  });

  factory HomeModel.fromJson(Map<String, dynamic> json) => HomeModel(
        status: json["status"],
        homeCategoriesList: json["categories"] == null
            ? []
            : List<CategoriesModel>.from(
                json["categories"].map((x) => CategoriesModel.fromJson(x))),
        horizontalSubscriptionsList: json["horizontalSubscriptions"] == null
            ? []
            : List<SubscriptionsModel>.from(json["horizontalSubscriptions"]
                .map((x) => SubscriptionsModel.fromJson(x))),
        verticalSubscriptionsList: json["verticalSubscriptions"] == null
            ? []
            : List<SubscriptionsModel>.from(json["verticalSubscriptions"]
                .map((x) => SubscriptionsModel.fromJson(x))),
        randomUsersList: json["random_users"] == null
            ? []
            : List<UserModel>.from(
                json["random_users"].map((x) => UserModel.fromJson(x))),
        bannersVideosList: json["banners_videos"] == null
            ? []
            : List<VideosBannerModel>.from(json["banners_videos"]
                .map((x) => VideosBannerModel.fromJson(x))),
        recruitersList: json["recruiters"] == null
            ? []
            : List<RecruitersModel>.from(
                json["recruiters"].map((x) => RecruitersModel.fromJson(x))),
      );
}
