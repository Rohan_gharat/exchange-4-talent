class UpdateFavouriteModel {
  var status;
  var message;
  var data;

  UpdateFavouriteModel({
    this.status,
    this.message,
    this.data,
  });

  factory UpdateFavouriteModel.fromJson(Map<String, dynamic> json) =>
      UpdateFavouriteModel(
        status: json["status"],
        message: json["message"],
        data: json["data"],
      );
}
