import 'package:e4t/model/recruitersModel/recruitersPostModel.dart';

class GetRecruitersPostByIdModel {
  int status;
  RecruitersPostModel? recruitersPostModel;
  var message;

  GetRecruitersPostByIdModel({
    required this.status,
    this.recruitersPostModel,
    this.message,
  });

  factory GetRecruitersPostByIdModel.fromJson(Map<String, dynamic> json) =>
      GetRecruitersPostByIdModel(
        status: json["status"],
        message: json["message"],
        recruitersPostModel: json["data"] == null
            ? null
            : RecruitersPostModel.fromJson(json["data"]),
      );
}
