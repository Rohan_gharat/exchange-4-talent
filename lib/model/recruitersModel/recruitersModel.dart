class RecruitersModel {
  var id;
  var name;
  var description;
  var logo;
  var url;
  var link;
  var type;
  var color_code;
  var Post_count;

  RecruitersModel({
    this.id,
    this.name,
    this.description,
    this.logo,
    this.url,
    this.link,
    this.type,
    this.color_code,
    this.Post_count,
  });

  factory RecruitersModel.fromJson(Map<String, dynamic> json) =>
      RecruitersModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        logo: json["logo"],
        url: json["url"],
        link: json["link"],
        type: json["type"],
        color_code: json["color_code"],
        Post_count: json["Post_count"],
      );
}
