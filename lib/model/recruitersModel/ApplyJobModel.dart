import 'package:e4t/model/recruitersModel/recruitersPostModel.dart';

class ApplyJobModel {
  int status;
  String? message;
  RecruitersPostModel? recruitersPostModel;

  ApplyJobModel({
    required this.status,
    this.message,
    this.recruitersPostModel,
  });

  factory ApplyJobModel.fromJson(Map<String, dynamic> json) => ApplyJobModel(
        status: json["status"],
        message: json["message"],
        recruitersPostModel: json["data"] == null
            ? null
            : RecruitersPostModel.fromJson(json["data"]),
      );
}
