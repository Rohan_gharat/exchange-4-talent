import 'package:e4t/model/recruitersModel/recruitersModel.dart';

class GetRecruitersPostsModel {
  int status;
  RecruitersPostDataModel? recruitersPostDataModel;
  String? message;

  GetRecruitersPostsModel({
    required this.status,
    this.recruitersPostDataModel,
    this.message,
  });

  factory GetRecruitersPostsModel.fromJson(Map<String, dynamic> json) =>
      GetRecruitersPostsModel(
        status: json["status"],
        recruitersPostDataModel: json["data"] == null
            ? null
            : RecruitersPostDataModel.fromJson(
                json["data"],
              ),
        message: json["message"],
      );
}

class RecruitersPostDataModel {
  List<RecruitersPostModel> recruitersPostsList;
  String? next_page_url;

  RecruitersPostDataModel({
    required this.recruitersPostsList,
    this.next_page_url,
  });

  factory RecruitersPostDataModel.fromJson(Map<String, dynamic> json) =>
      RecruitersPostDataModel(
        recruitersPostsList:
            List.from(json["data"].map((a) => RecruitersPostModel.fromJson(a))),
        next_page_url: json["next_page_url"],
      );
}

class RecruitersPostModel {
  var id;
  var recuriter_id;
  var title;
  var description;
  var category_ids;
  var category_title;
  var location;
  var status;
  var start_date;
  var end_date;
  var audition_date;
  var created_at;
  var updated_at;
  var jobStatus;
  RecruitersModel? recruitersModel;

  RecruitersPostModel({
    this.id,
    this.recuriter_id,
    this.title,
    this.description,
    this.category_ids,
    this.category_title,
    this.location,
    this.status,
    this.start_date,
    this.end_date,
    this.audition_date,
    this.created_at,
    this.updated_at,
    this.jobStatus,
    this.recruitersModel,
  });

  factory RecruitersPostModel.fromJson(Map<String, dynamic> json) =>
      RecruitersPostModel(
        id: json["id"],
        recuriter_id: json["recuriter_id"],
        title: json["title"],
        description: json["description"],
        category_ids: json["category_ids"],
        category_title: json["category_title"],
        location: json["location"],
        status: json["status"],
        start_date: json["start_date"],
        end_date: json["end_date"],
        audition_date: json["audition_date"],
        created_at: json["created_at"],
        updated_at: json["updated_at"],
        jobStatus: json["jobStatus"] ?? 0,
        recruitersModel: json["recruiter"] == null
            ? null
            : RecruitersModel.fromJson(
                json["recruiter"],
              ),
      );
}
