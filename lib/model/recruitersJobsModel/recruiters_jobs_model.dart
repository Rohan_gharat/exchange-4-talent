import 'dart:convert';

import 'package:e4t/model/recruitersModel/recruitersModel.dart';

class GetRecruitersJobsModel {
  int? status;
  GetRecruitersJobsDataModel? getRecruitersJobsDataModel;
  String? message;

  GetRecruitersJobsModel({
    this.status,
    this.getRecruitersJobsDataModel,
    this.message,
  });

  factory GetRecruitersJobsModel.fromJson(Map<String, dynamic> json) =>
      GetRecruitersJobsModel(
        status: json["status"],
        getRecruitersJobsDataModel: json["data"] == null
            ? null
            : GetRecruitersJobsDataModel.fromJson(json["data"]),
        message: json["message"],
      );
}

class GetRecruitersJobsDataModel {
  List<RecruitersModel>? recruitersList;

  String? nextPageUrl;

  GetRecruitersJobsDataModel({
    this.recruitersList,
    this.nextPageUrl,
  });

  factory GetRecruitersJobsDataModel.fromJson(Map<String, dynamic> json) =>
      GetRecruitersJobsDataModel(
        recruitersList: json["data"] == null
            ? []
            : List<RecruitersModel>.from(
                json["data"]!.map((x) => RecruitersModel.fromJson(x))),
        nextPageUrl: json["next_page_url"],
      );
}
