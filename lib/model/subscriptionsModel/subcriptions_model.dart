import 'dart:convert';

List<SubscriptionsModel> subscriptionsModelFromJson(String str) =>
    List<SubscriptionsModel>.from(
        json.decode(str).map((x) => SubscriptionsModel.fromJson(x)));


class SubscriptionsModel {
  int id;
  String title;
  String? productId;
  SubscriptionDataModel subscriptionData;
  String? colorCode;

  SubscriptionsModel({
    required this.id,
    required this.title,
    required this.subscriptionData,
    this.productId,
    this.colorCode,
  });

  factory SubscriptionsModel.fromJson(Map<String, dynamic> json) =>
      SubscriptionsModel(
        id: json["id"],
        title: json["title"],
        productId: json["product_id"],
        subscriptionData:
            SubscriptionDataModel.fromJson(json["subscriptionData"]),
        colorCode: json["color_code"],
      );


}

class SubscriptionDataModel {
  int price;
  String validity;
  String? images;
  String? plan;
  List<String>? descriptionList;

  SubscriptionDataModel({
    required this.price,
    required this.validity,
    required this.images,
    required this.plan,
    required this.descriptionList,
  });

  factory SubscriptionDataModel.fromJson(Map<String, dynamic> json) =>
      SubscriptionDataModel(
        price: json["price"],
        validity: json["validity"],
        images: json["images"],
        plan: json["plan"],
        descriptionList: json["description"] == null
            ? []
            : List<String>.from(json["description"].map((x) => x)),
      );


}
