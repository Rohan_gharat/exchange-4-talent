class VerifyCouponModel {
  var status;
  var userId;
  var message;
  var subscription_id;
  var final_price;
  var discount;
  var expire_date;

  VerifyCouponModel({
    this.status,
    this.userId,
    this.message,
    this.subscription_id,
    this.final_price,
    this.discount,
    this.expire_date,
  });

  factory VerifyCouponModel.fromJson(Map<String, dynamic> json) =>
      VerifyCouponModel(
        status: json["status"],
        userId: json["userId"],
        message: json["message"],
        subscription_id: json["subscription_id"],
        final_price: json["final_price"],
        discount: json["discount"],
        expire_date: json["expire_date"],
      );
}
