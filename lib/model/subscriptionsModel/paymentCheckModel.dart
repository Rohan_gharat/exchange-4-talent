class PaymentCheckModel {
  int status;
  bool paymentStatus;
  String? expiry_date;
  String? message;

  PaymentCheckModel({
    required this.status,
    required this.paymentStatus,
    this.expiry_date,
    this.message,
  });

  factory PaymentCheckModel.fromJson(Map<String, dynamic> json) =>
      PaymentCheckModel(
        status: json["status"],
        paymentStatus: json["payment_status"],
        expiry_date: json["expiry_date"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
    "status": status,
    "payment_status": paymentStatus,
    "expiry_date": expiry_date,
    "message": message,
  };
}