class SubscriptionsDrawerMenuModel {
  int id;
  String title;

  SubscriptionsDrawerMenuModel({
    required this.id,
    required this.title,
  });

}
