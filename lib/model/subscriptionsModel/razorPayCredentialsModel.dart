class RazorPayCredentialsModel {
  var status;
  var key_id;
  var key_secret;
  var receiptId;

  RazorPayCredentialsModel({
    this.status,
    this.key_id,
    this.key_secret,
    this.receiptId,
  });

  factory RazorPayCredentialsModel.fromJson(Map<String, dynamic> json) =>
      RazorPayCredentialsModel(
        status: json["status"],
        key_id: json["key_id"],
        key_secret: json["key_secret"],
        receiptId: json["receiptId"],
      );
}
