class GetAvailableCouponCodesModel {
  var status;
  var message;
  List<CouponModel>? couponCodesList;

  GetAvailableCouponCodesModel({
    this.status,
    this.message,
    this.couponCodesList,
  });

  factory GetAvailableCouponCodesModel.fromJson(Map<String, dynamic> json) =>
      GetAvailableCouponCodesModel(
        status: json["status"],
        message: json["message"],
        couponCodesList: json["couponCode"] == null
            ? []
            : List<CouponModel>.from(
                json["couponCode"].map((e) => CouponModel.fromJson(e))),
      );
}

class CouponModel {
  var id;
  var coupon_code;
  var coupon_discount;

  CouponModel({
    this.id,
    this.coupon_code,
    this.coupon_discount,
  });

  factory CouponModel.fromJson(Map<String, dynamic> json) => CouponModel(
        id: json["id"],
        coupon_code: json["coupon_code"],
        coupon_discount: json["coupon_discount"],
      );
}
