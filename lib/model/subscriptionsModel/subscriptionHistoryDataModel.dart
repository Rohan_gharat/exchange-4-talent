class SubscriptionHistoryDataModel {
  var subscription_id;
  var title;
  var price;
  var validity;
  var images;
  var buy_date;
  var expire_date;
  var isActive;

  SubscriptionHistoryDataModel({
    this.subscription_id,
    this.title,
    this.price,
    this.validity,
    this.images,
    this.buy_date,
    this.expire_date,
    this.isActive,
  });

  factory SubscriptionHistoryDataModel.fromJson(Map<String, dynamic> json) =>
      SubscriptionHistoryDataModel(
        subscription_id: json["subscription_id"],
        title: json["title"],
        price: json["price"],
        validity: json["validity"],
        images: json["images"],
        buy_date: json["buy_date"],
        expire_date: json["expire_date"],
        isActive: json["isActive"],
      );
}
