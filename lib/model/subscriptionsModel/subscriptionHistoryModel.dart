import 'package:e4t/model/subscriptionsModel/subscriptionHistoryDataModel.dart';

class SubscriptionHistoryModel{

  int status;
  int userId;
  List<SubscriptionHistoryDataModel> subscriptionHistoriesList;

  SubscriptionHistoryModel({
    required this.status,
    required this.userId,
    required this.subscriptionHistoriesList,
  });

  factory SubscriptionHistoryModel.fromJson(Map<String, dynamic> json) => SubscriptionHistoryModel(
    status: json["status"],
    userId: json["user_id"],
    subscriptionHistoriesList: List<SubscriptionHistoryDataModel>.from(json["subscription_histories"].map((x) =>
        SubscriptionHistoryDataModel.fromJson(x))),
  );
}