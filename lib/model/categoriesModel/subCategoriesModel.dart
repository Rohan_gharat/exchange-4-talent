class SubCategoriesModel {
  var id;
  var category_id;
  var title;
  var description;
  var image;
  var created_at;
  var updated_at;

  SubCategoriesModel({
    this.id,
    this.category_id,
    this.title,
    this.description,
    this.image,
    this.created_at,
    this.updated_at,
  });

  factory SubCategoriesModel.fromJson(Map<String, dynamic> json) =>
      SubCategoriesModel(
        id: json["id"],
        category_id: json["category_id"],
        title: json["title"],
        description: json["description"],
        image: json["image"],
        created_at: json["created_at"],
        updated_at: json["updated_at"],
      );
}
