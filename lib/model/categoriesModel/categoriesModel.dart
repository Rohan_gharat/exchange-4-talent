import 'package:e4t/model/categoriesModel/subCategoriesModel.dart';

class CategoriesModel {
  var id;
  var title;
  var description;
  var image;
  List<SubCategoriesModel>? subcategoriesList;
  var isSelected = false;
  var color_code;

  CategoriesModel({
    this.id,
    this.title,
    this.description,
    this.image,
    this.subcategoriesList,
    this.isSelected = false,
    this.color_code,
  });

  factory CategoriesModel.fromJson(Map<String, dynamic> json) =>
      CategoriesModel(
        id: json['id'],
        title: json['title'],
        description: json['description'],
        image: json['image'],
        color_code: json['color_code'],
        subcategoriesList: json["subcategories"] == null
            ? []
            : List<SubCategoriesModel>.from(
                json["subcategories"].map(
                  (x) => SubCategoriesModel.fromJson(x),
                ),
              ),
      );
}
