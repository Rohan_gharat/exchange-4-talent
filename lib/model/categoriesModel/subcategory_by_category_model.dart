import 'package:e4t/model/categoriesModel/subCategoriesModel.dart';
import 'package:e4t/model/userModel/user_model.dart';

import 'categoriesModel.dart';

class SubcategoryByCategoryModel {
  List<SubCategoriesModel>? subcategoriesList;

  List<CategoriesModel>? randomCategoriesList;

  List<UserModel> usersList;

  SubcategoryByCategoryModel({
    this.subcategoriesList,
    this.randomCategoriesList,
   required this.usersList,
  });

  factory SubcategoryByCategoryModel.fromJson(Map<String, dynamic> json) =>
      SubcategoryByCategoryModel(
        subcategoriesList: List<SubCategoriesModel>.from(
            json["subcategories"].map((x) => SubCategoriesModel.fromJson(x))),
        randomCategoriesList: List<CategoriesModel>.from(
            json["random_categories"].map((x) => CategoriesModel.fromJson(x))),
        usersList: json["banner"] == null
            ? []
            : List<UserModel>.from(
                json["banner"].map((x) => UserModel.fromJson(x))),
      );
}
