import 'package:e4t/model/categoriesModel/subCategoriesModel.dart';
import 'package:e4t/model/userModel/user_model.dart';

import 'categoriesModel.dart';

class UsersByCategoryModel {
  List<CategoriesModel>? randomCategoriesList;

  List<UserModel> userBannersList;

  List<UserModel> usersList;

  UsersByCategoryModel({
    required this.usersList,
    this.randomCategoriesList,
    required this.userBannersList,
  });

  factory UsersByCategoryModel.fromJson(Map<String, dynamic> json) =>
      UsersByCategoryModel(
        usersList: List<UserModel>.from(
            json["Users"].map((x) => UserModel.fromJson(x))),
        randomCategoriesList: List<CategoriesModel>.from(
            json["random_categories"].map((x) => CategoriesModel.fromJson(x))),
        userBannersList: json["banner"] == null
            ? []
            : List<UserModel>.from(
                json["banner"].map((x) => UserModel.fromJson(x))),
      );
}
