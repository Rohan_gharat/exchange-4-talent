class FeedbackModel {
  var status;
  var message;

  FeedbackModel({
    this.status,
    this.message,
  });

  factory FeedbackModel.fromJson(Map<String, dynamic> json) => FeedbackModel(
        status: json["status"],
        message: json["message"],
      );
}
