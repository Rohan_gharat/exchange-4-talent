import 'dart:io';

import 'package:e4t/blocks/AdsBloc/ads_bloc.dart';
import 'package:e4t/blocks/AdsVideosBannerBloc/ads_videos_banner_bloc.dart';
import 'package:e4t/blocks/AllChatUsersBloc/all_chat_users_bloc.dart';
import 'package:e4t/blocks/ApplyCouponBloc/apply_coupon_bloc.dart';
import 'package:e4t/blocks/CategoriesBlocNew/categories_new_bloc.dart';
import 'package:e4t/blocks/CreateGroupAllUsersBloc/create_group_all_users_bloc.dart';
import 'package:e4t/blocks/FeedbackBloc/feedback_bloc.dart';
import 'package:e4t/blocks/GroupProfilePhotoBloc/group_profile_photo_bloc.dart';
import 'package:e4t/blocks/HireDataShowBloc/hire_data_show_bloc.dart';
import 'package:e4t/blocks/HomeBloc/home_bloc.dart';
import 'package:e4t/blocks/HomeVideoBannerBloc/home_videos_banner_bloc.dart';
import 'package:e4t/blocks/MessagesUserDataBloc/messages_user_data_bloc.dart';
import 'package:e4t/blocks/RecruitersApplyBloc/recruiters_apply_bloc.dart';
import 'package:e4t/blocks/RecruitersBloc/recruiters_bloc.dart';
import 'package:e4t/blocks/RecruitersJobPostsBloc/recruiters_job_posts_bloc.dart';
import 'package:e4t/blocks/RecruitersJobsActivePostsBloc/recruiters_jobs_active_posts_bloc.dart';
import 'package:e4t/blocks/RecruitersJobsBloc/recruiters_jobs_bloc.dart';
import 'package:e4t/blocks/RecruitersJobsInActivePostsBloc/recruiters_jobs_in_active_posts_bloc.dart';
import 'package:e4t/blocks/RecruitersPostDetailsBloc/recruiters_post_details_bloc.dart';
import 'package:e4t/blocks/ReviewsBloc/reviews_bloc.dart';
import 'package:e4t/blocks/SubscriptionHistoryBloc/subscription_history_bloc.dart';
import 'package:e4t/blocks/SubscriptionsDrawerMenuBloc/subscriptions_drawer_menu_bloc.dart';
import 'package:e4t/blocks/VideoDetailsSliderBloc/video_details_slider_bloc.dart';
import 'package:e4t/blocks/VideoMuteBloc/video_mute_bloc.dart';
import 'package:e4t/blocks/auth/auth_bloc.dart';
import 'package:e4t/blocks/availableCouponsBloc/available_coupons_bloc.dart';
import 'package:e4t/blocks/categoriesBloc/category_bloc.dart';
import 'package:e4t/blocks/favourite/favourite_bloc.dart';
import 'package:e4t/blocks/mainNavigationBloc/main_navigation_bloc.dart';
import 'package:e4t/blocks/profilePhotoUpdateBloc/profile_photo_update_bloc.dart';
import 'package:e4t/blocks/subscriptionBloc/subscription_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/routes.dart';
import 'package:e4t/screens/splashScreen.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

import 'blocks/AddReviewBloc/add_review_bloc.dart';
import 'blocks/ChoosePhotoBloc/choose_photo_bloc.dart';
import 'blocks/CitiesBloc/cities_bloc.dart';
import 'blocks/CountriesBloc/countries_bloc.dart';
import 'blocks/GenderBloc/gender_bloc.dart';
import 'blocks/RatingBloc/rating_bloc.dart';
import 'blocks/StatesBloc/states_bloc.dart';
import 'blocks/SubCategoryBloc/sub_category_drop_down_bloc.dart';
import 'blocks/TalentTypeBloc/talent_type_bloc.dart';
import 'blocks/ToggleBarterBloc/toggle_barter_bloc.dart';
import 'blocks/VerifyCouponBloc/verify_coupon_bloc.dart';
import 'blocks/allCategoryBloc/all_category_bloc.dart';
import 'blocks/categoriesDropDownBloc/categories_drop_down_bloc.dart';
import 'blocks/dobBloc/dob_bloc.dart';
import 'blocks/talentBarterBloc/talent_barter_bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:e4t/firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  await SharedPrefs.initSharedPref();
  if (Platform.isIOS) {
    /// app store revenuecat api key
    Purchases.configure(
        PurchasesConfiguration("appl_fHkIAyMnZvFFepOZbAdPuiXPkjf"));
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (ctx) => MainNavigationBloc()),
        BlocProvider(create: (ctx) => AuthBloc()),
        BlocProvider(create: (ctx) => UserBloc()),
        BlocProvider(create: (ctx) => SubscriptionBloc()),
        BlocProvider(create: (ctx) => CategoryBloc()),
        BlocProvider(create: (ctx) => AllCategoryBloc()),
        BlocProvider(create: (ctx) => FavouriteBloc()),
        BlocProvider(create: (ctx) => ProfilePhotoUpdateBloc()),
        BlocProvider(create: (ctx) => DobBloc()),
        BlocProvider(create: (ctx) => GenderBloc()),
        BlocProvider(create: (ctx) => CategoriesDropDownBloc()),
        BlocProvider(create: (ctx) => SubCategoryDropDownBloc()),
        BlocProvider(create: (ctx) => ToggleBarterBloc()),
        BlocProvider(create: (ctx) => TalentBarterBloc()),
        BlocProvider(create: (ctx) => TalentTypeBloc()),
        BlocProvider(create: (ctx) => ChoosePhotoBloc()),
        BlocProvider(create: (ctx) => CountriesBloc()),
        BlocProvider(create: (ctx) => StatesBloc()),
        BlocProvider(create: (ctx) => CitiesBloc()),
        BlocProvider(create: (ctx) => VerifyCouponBloc()),
        BlocProvider(create: (ctx) => SubscriptionHistoryBloc()),
        BlocProvider(create: (ctx) => ReviewsBloc()),
        BlocProvider(create: (ctx) => AddReviewBloc()),
        BlocProvider(create: (ctx) => RatingBloc()),
        BlocProvider(create: (ctx) => HomeVideosBannerBloc()),
        BlocProvider(create: (ctx) => HomeBloc()),
        BlocProvider(create: (ctx) => ApplyCouponBloc()),
        BlocProvider(create: (ctx) => SubscriptionsDrawerMenuBloc()),
        BlocProvider(create: (ctx) => VideoMuteBloc()),
        BlocProvider(create: (ctx) => AdsBloc()),
        BlocProvider(create: (ctx) => CategoriesNewBloc()),
        BlocProvider(create: (ctx) => AdsVideosBannerBloc()),
        BlocProvider(create: (ctx) => VideoDetailsSliderBloc()),
        BlocProvider(create: (ctx) => HireDataShowBloc()),
        BlocProvider(create: (ctx) => AvailableCouponsBloc()),
        BlocProvider(create: (ctx) => AllChatUsersBloc()),
        BlocProvider(create: (ctx) => CreateGroupAllUsersBloc()),
        BlocProvider(create: (ctx) => GroupProfilePhotoBloc()),
        BlocProvider(create: (ctx) => FeedbackBloc()),
        BlocProvider(create: (ctx) => RecruitersBloc()),
        BlocProvider(create: (ctx) => RecruitersPostDetailsBloc()),
        BlocProvider(create: (ctx) => RecruitersApplyBloc()),
        BlocProvider(create: (ctx) => RecruitersJobsBloc()),
        BlocProvider(create: (ctx) => RecruitersJobPostsBloc()),
        BlocProvider(create: (ctx) => RecruitersJobsActivePostsBloc()),
        BlocProvider(create: (ctx) => RecruitersJobsInActivePostsBloc()),
        BlocProvider(create: (ctx) => MessagesUserDataBloc()),
      ],
      child: MaterialApp(
        title: '',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          scaffoldBackgroundColor: MyColors.BG_COLOR,
        ),
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
        onGenerateRoute: (settings) => AppRoutes.generateRoute(settings),
      ),
    );
  }
}
