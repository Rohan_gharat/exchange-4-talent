import 'dart:ui';

import 'package:flutter/material.dart';

class MyColors {
  static const ORANGE_COLOR = Color(0xffBB8639);

  static const LIGHT_ORANGE_COLOR = Color(0xffEDDCC3);

  static const LIGHT_BLUE_COLOR = Color(0xffF3F4F6);

  static const BG_COLOR = Color(0xff222429);

  static const BG_COLOR_TEXT_FILED = Color(0xff343434);

  static const GREY_COLOR = Color(0xff9095A1);

  static const GREY_COLOR1 = Color(0xffCFD2DA);

  static const YELLOW_COLOR = Color(0xffF3C63F);

  static const WHITE_COLOR = Colors.white;

  static const BLACK_COLOR = Colors.black;

  static const RED_COLOR = Colors.red;

  static const STARRY_EYED_COLOR = Color(0xffFC3838);

  static const CATEGORY_BG_COLOR = Color(0xff5ebd60);
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    // print("decoded color: ${Color(int.parse(buffer.toString(), radix: 16))}");
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
