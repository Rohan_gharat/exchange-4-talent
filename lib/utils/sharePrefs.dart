import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static late final SharedPreferences _instance;

  static const accessTokenKey = 'accessToken';
  static const isOtpCompletedKey = "isOtpCompleted";
  static const isUserDataCompletedKey = "isUserDataCompleted";
  static const userIdKey = "userIdKey";

  static Future<SharedPreferences> initSharedPref() async =>
      _instance = await SharedPreferences.getInstance();

  static bool? getBool(String key) => _instance.getBool(key);

  static Future<bool> setBool(String key, bool value) =>
      _instance.setBool(key, value);

  static Future<bool> setAccessToken(String value) =>
      _instance.setString(accessTokenKey, value);

  static String? getAccessToken() => _instance.getString(accessTokenKey);

  static Future<bool> setUserId(int value) =>
      _instance.setInt(userIdKey, value);

  static int? getUserId() => _instance.getInt(userIdKey);

  static Future<bool> clearSharedPref() => _instance.clear();
}
