import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:e4t/model/hireTalentModel/hireTalentModel.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/subscriptions/add_subscription_screen.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../screens/widgets/circular_indicator_widget.dart';

enum AddressEnum { countryE, stateE, cityE }

class HelperClass {
  static final String appCurrency = '\₹';
  static final String RAZOR_PAY_KEY_ID_TEST = 'rzp_test_lydFu3goHLFkeK';
  static final String RAZOR_PAY_KEY_SECRET_TEST = 'H1WI5HJLFyXjcgl81x2odTaB';

  static const ERROR_TEXT = "something went wrong please try again";

  static const PRIVACY_POLICY_URL =
      "https://demo.nimayate.com/e4t_new/public/privacy";
  static const CONTACT_US_URL =
      "https://www.exchange4talentglobal.com/contact-us.php";
  static const TERMS_AND_CONDITIONS_URL =
      "https://www.exchange4talentglobal.com/terms-and-conditions.php";
  static const SHIPPING_URL =
      "https://www.exchange4talentglobal.com/shipping-and-delivery-policy.php";
  static const REFUND_URL =
      "https://www.exchange4talentglobal.com/cancellation-and-refund-policy.php";

  static const APP_LOGO_PATH = "images/e4t_logo.png";
  static const PLACEHOLDER_IMAGE_PATH = "images/circular_image_placeholder.jpg";

  static const MALE_RADIO_BUTTON_INDEX = 1;
  static const FEMALE_RADIO_BUTTON_INDEX = 2;
  static const OTHERS_RADIO_BUTTON_INDEX = 3;

  static const MALE_VALUE = "Male";
  static const FEMALE_VALUE = "Female";
  static const OTHERS_VALUE = "Others";

  static const BOTH_DATA_RADIO_BUTTON_INDEX = 1;
  static const MOBILE_RADIO_BUTTON_INDEX = 2;
  static const EMAIL_RADIO_BUTTON_INDEX = 3;

  static const BOTH_DATA_VALUE = "Both";
  static const MOBILE_VALUE = "Mobile";
  static const EMAIL_VALUE = "Email";

  static const NAME_KEY = "name";
  static const MOBILE_KEY = "mobile";
  static const EMAIL_KEY = "email";
  static const DOB_KEY = "dob";
  static const TALENT_TYPE_KEY = "talent_type";
  static const GENDER_KEY = "gender";
  static const CATEGORY_ID_KEY = "category_id";
  static const CATEGORY_IDS_NEW_KEY = "category_ids";
  static const SUBCATEGORY_ID_KEY = "subcategory_id";
  static const TRADE_BARTER_KEY = "trade_barter";
  static const TAGS_KEY = "tags";
  static const DESCRIPTION_KEY = "description";
  static const FB_LINK_KEY = "facebook_link";
  static const INSTA_LINK_KEY = "instagram_link";
  static const LINKED_IN_LINK_KEY = "linkedIn_link";
  static const WIKIPEDIA_LINK_KEY = "wikipedia_link";
  static const IMDB_LINK_KEY = "imdb_link";
  static const COUNTRY_KEY = "country";
  static const STATE_KEY = "state";
  static const CITY_KEY = "city";
  static const BARTER_DESCRIPTION_KEY = "barter_description";
  static const HIRE_DATA_FLAG_KEY = "flag";

  // static const DOB_FORMAT ="dd-MM-yyyy";
  static const DOB_FORMAT = "yyyy-MM-dd";

  static const DATE_TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";

  static const SECRET_SERVICE_TEXT =
      "Secret service is a chat service to connect with fellow colleagues, family or friends discuss internal secrets  or ask advices from each other as anonymous users without names. The data will be deleted as the chat will last for 5 mins. Up to 20 users max.";

  static const TALENT_BARTER_TEXT =
      "Exchange 4 Talent introduces talent barter, a ground breaking system where subscribers can freely seek and offer services without financial exchange";

  static const SELF_HOST = "self";
  static const YOUTUBE_HOST = "youtube";

  static const VIDEO_TYPE = "video";
  static const IMAGE_TYPE = "image";

  static const NOTIFICATIONS_TYPE_KEY = "type";
  static const NOTIFICATIONS_JOB_TYPE_KEY = "job";
  static const NOTIFICATIONS_JOB_ID_KEY = "jobid";

  static const NOTIFICATIONS_CHAT_TYPE_KEY = "chat";
  // static const NOTIFICATIONS_CHAT_DATA_KEY = "chatData";

  static const NOTIFICATION_CHAT_NAME_KEY = "name";
  static const NOTIFICATION_CHAT_ID_KEY = "chatid";
  static const NOTIFICATIONS_IS_GROUP_CHAT_KEY = "isgroupchat";
  static const NOTIFICATIONS_PROFILE_PIC_KEY = "profilepic";
  static const NOTIFICATIONS_SENDER_ID_KEY = "senderid";
  static const NOTIFICATION_IS_SECRET_SERVICE_KEY = "issecretservice";
  static const NOTIFICATION_CHAT_MESSAGE_KEY = "message";

  static const NOT_APPLIED_JOB_ID = 0;
  static const APPLIED_JOB_ID = 1;
  static const SHORTELISTED_JOB_ID = 2;
  static const REJECTED_JOB_ID = 3;
  static const HIRED_JOB_ID = 4;

  static const NOT_APPLIED_JOB_key = "Apply";
  static const APPLIED_JOB_key = "Applied";
  static const SHORTELISTED_JOB_key = "Shortlisted";
  static const REJECTED_JOB_key = "Rejected";
  static const HIRED_JOB_key = "Hired";

  static void checkSubscription(
      UserBloc userBloc, BuildContext context, UserModel? userModel) async {
    if (userBloc.myUserData?.subscriptionValidity != null) {

      /// api call to check if user can hire this talent by checking its subscription plan  ... pending
      show_alert_Dialog(context, "processing");

      var response = await ApisServices.hireTalent(
          token: SharedPrefs.getAccessToken(), talent_id: userModel!.id);
      Navigator.of(context, rootNavigator: true).pop();
      if (response != null) {
        if (response.status == 200) {
          showHireDialog(context, response.hireTalentDataModel, userModel.name);
        } else if (response.status == 204) {
          Navigator.pushNamed(
              context, AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN,
              arguments: {
                AddSubscriptionsScreen.SHOW_SKIP_KEY: false,
                AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key:
                    response.subcriptions_id ?? 0
              });
        }
        show_snackbar(context, response.message ?? "");
      } else {
        show_snackbar(context, ERROR_TEXT);
      }
    } else {
      // showWarningDialog(context, userModel!);
      showWarningDialog(context, userModel!);
    }
  }

  static void showWarningDialog(BuildContext context, UserModel userModel) {
    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(
            18.0,
          ),
        ),
      ),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.25,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: MyColors.WHITE_COLOR,
                borderRadius: BorderRadius.circular(
                  50.0,
                ),
              ),
              padding: EdgeInsets.all(
                8.0,
              ),
              child: Icon(
                Icons.cancel,
                color: MyColors.ORANGE_COLOR,
                size: 24.0,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Oops!",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.ORANGE_COLOR,
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              "It seems your current plan does not support ${userModel.name}'s Contact details Click the link below to upgrade to get contact",
              style: GoogleFonts.workSans(
                textStyle: TextStyle(
                  color: MyColors.GREY_COLOR,
                  fontSize: 14.0,
                ),
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Cancel",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
                Navigator.pushNamed(
                    context, AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN,
                    arguments: {
                      AddSubscriptionsScreen.SHOW_SKIP_KEY: false,
                      AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key: 0
                    });
              },
              child: Text(
                "Upgrade",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );

    showDialog(
      context: context,
      builder: (ctx) {
        return alert;
      },
    );
  }

  static void showHireDialog(BuildContext context,
      HireTalentDataModel? hireTalentDataModel, String talentName) {
    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(
            18.0,
          ),
        ),
      ),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.4,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: MyColors.WHITE_COLOR,
                borderRadius: BorderRadius.circular(
                  50.0,
                ),
              ),
              padding: EdgeInsets.all(
                8.0,
              ),
              child: Icon(
                Icons.check,
                color: MyColors.ORANGE_COLOR,
                size: 24.0,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Contact Details",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.ORANGE_COLOR,
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            // Text(
            //   "Click the send button to receive contact details for ${userModel.name} on your registered Email id and Mobile",
            //   style: GoogleFonts.workSans(
            //     textStyle: TextStyle(
            //       color: MyColors.GREY_COLOR,
            //       fontSize: 14.0,
            //     ),
            //   ),
            //   textAlign: TextAlign.center,
            // ),
            hireTalentDataModel?.mobile != null &&
                    hireTalentDataModel?.mobile?.toString().trim() != ""
                ? Row(
                    children: [
                      Expanded(
                        child: Text(
                          "${talentName} Mobile Number: ${hireTalentDataModel?.mobile}",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR,
                              fontSize: 14.0,
                            ),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      GestureDetector(
                        onTap: () {
                          Clipboard.setData(
                              ClipboardData(text: hireTalentDataModel?.mobile));
                          HelperClass.show_snackbar(
                              context, "copied to clipboard");
                        },
                        child: Icon(
                          Icons.copy,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                    ],
                  )
                : const SizedBox.shrink(),
            SizedBox(
              height: 10.0,
            ),
            hireTalentDataModel?.email != null &&
                    hireTalentDataModel?.email?.toString().trim() != ""
                ? Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Email Address: ${hireTalentDataModel!.email}",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR,
                              fontSize: 14.0,
                            ),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      GestureDetector(
                        onTap: () {
                          Clipboard.setData(
                              ClipboardData(text: hireTalentDataModel.email));
                          HelperClass.show_snackbar(
                              context, "copied to clipboard");
                        },
                        child: Icon(
                          Icons.copy,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                    ],
                  )
                : Container(),
          ],
        ),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Cancel",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            hireTalentDataModel?.email != null &&
                    hireTalentDataModel?.email?.toString().trim() != ""
                ? ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      // fromHeight use double.infinity as width and 40 is the height
                      backgroundColor: MyColors.ORANGE_COLOR,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18),
                      ),
                    ),
                    onPressed: () async {
                      // Navigator.of(context, rootNavigator: true).pop();
                      var url = "mailto:${hireTalentDataModel!.email}";
                      if (await canLaunchUrl(Uri.parse(url))) {
                        await launchUrl(Uri.parse(url));
                      } else {
                        HelperClass.show_snackbar(
                            context, 'Could not launch $url');
                      }
                    },
                    child: Text(
                      "Email",
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                : SizedBox.shrink(),
            SizedBox(
              width: 20.0,
            ),
            hireTalentDataModel?.mobile != null &&
                    hireTalentDataModel?.mobile?.toString().trim() != ""
                ? ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      // fromHeight use double.infinity as width and 40 is the height
                      backgroundColor: MyColors.ORANGE_COLOR,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18),
                      ),
                    ),
                    onPressed: () async {
                      // Navigator.of(context, rootNavigator: true).pop();
                      var url = "tel:${hireTalentDataModel!.mobile}";
                      if (await canLaunchUrl(Uri.parse(url))) {
                        await launchUrl(Uri.parse(url));
                      } else {
                        HelperClass.show_snackbar(
                            context, 'Could not launch $url');
                      }
                    },
                    child: Text(
                      "Call",
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                : const SizedBox.shrink(),
          ],
        ),
      ],
    );

    showDialog(
      context: context,
      builder: (ctx) {
        return alert;
      },
    );
  }

  static void show_alert_Dialog(BuildContext context, String msg) {
    var alertDialog = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      content: Container(
        height: 100.0,
        child: Column(
          children: [
            CircularIndicatorWidget(),
            SizedBox(
              height: 10.0,
            ),
            Text(
              msg,
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.WHITE_COLOR,
                  fontSize: 14.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) {
          return WillPopScope(
            onWillPop: () async => false,
            child: alertDialog,
          );
        });
  }

  static void show_snackbar(BuildContext context, String msg) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          msg,
        ),
        duration: const Duration(
          seconds: 1,
        ),
      ),
    );
  }

  static void show_error_snackbar(BuildContext context, String msg) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          msg,
        ),
        duration: const Duration(
          seconds: 1,
        ),
        backgroundColor: MyColors.RED_COLOR,
      ),
    );
  }

  static String format_data_dd_mm_yyyy(String date) {
    var s = DateFormat(HelperClass.DATE_TIME_STAMP_FORMAT).parse(date);
    return DateFormat(HelperClass.DOB_FORMAT).format(s);
  }

  static String? format_recruiter_post_date_dd_mm_yyyy(String date) {
    try {
      var s = DateFormat(HelperClass.DATE_TIME_STAMP_FORMAT).parse(date);
      return DateFormat("dd MMM yyyy").format(s);
    } catch (error) {
      return null;
    }
  }

  static int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inHours / 24).round();
  }

  static List<Color> getBorderColors() {
    return [MyColors.ORANGE_COLOR, MyColors.YELLOW_COLOR, MyColors.GREY_COLOR];
  }

  static void openUrl({required var url, required BuildContext context}) async {
    if (await canLaunchUrl(Uri.parse(url))) {
      launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
    } else {
      HelperClass.show_snackbar(
          context, "this page link is not found or moved to new link");
    }
  }

  static String getTextsFromSelectedCategories(
      {required List<CategoriesModel> selectedCategoriesList}) {
    String selectedCategoriesTitle = "";

    for (var categoryModel in selectedCategoriesList) {
      selectedCategoriesTitle += "${categoryModel.title ?? ""}";
      if (selectedCategoriesList.indexOf(categoryModel) <
          selectedCategoriesList.length - 1) {
        selectedCategoriesTitle += ", ";
      }
    }
    return selectedCategoriesTitle;
  }
}
