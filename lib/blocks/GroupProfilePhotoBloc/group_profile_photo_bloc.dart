import 'dart:async';
import 'dart:ffi';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'group_profile_photo_event.dart';

part 'group_profile_photo_state.dart';

class GroupProfilePhotoBloc
    extends Bloc<GroupProfilePhotoEvent, GroupProfilePhotoState> {
  GroupProfilePhotoBloc() : super(GroupProfilePhotoState(null)) {
    on<GroupProfilePhotoEvent>((event, emit) {
      emit(GroupProfilePhotoState(event.imageFile));
    });
  }
}
