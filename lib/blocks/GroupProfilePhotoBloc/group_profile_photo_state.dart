part of 'group_profile_photo_bloc.dart';

@immutable
 class GroupProfilePhotoState {
  File? imageFile;

  GroupProfilePhotoState(this.imageFile);
}

