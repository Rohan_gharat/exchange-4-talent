part of 'group_profile_photo_bloc.dart';

@immutable
class GroupProfilePhotoEvent {
  File? imageFile;

  GroupProfilePhotoEvent(this.imageFile);
}
