part of 'main_navigation_bloc.dart';

@immutable
 class MainNavigationState {
int selectedIndex;
bool can_pop;

MainNavigationState(this.selectedIndex,this.can_pop);
}
