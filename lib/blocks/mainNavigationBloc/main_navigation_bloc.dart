import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'main_navigation_event.dart';

part 'main_navigation_state.dart';

class MainNavigationBloc
    extends Bloc<MainNavigationEvent, MainNavigationState> {
  MainNavigationBloc() : super(MainNavigationState(0,false)) {
    on<MainNavigationEvent>((event, emit) {
      emit(MainNavigationState(event.selectedIndex,event.can_pop));
    });
  }
}
