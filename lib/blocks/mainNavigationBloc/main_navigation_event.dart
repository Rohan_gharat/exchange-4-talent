part of 'main_navigation_bloc.dart';

@immutable
 class MainNavigationEvent {
  int selectedIndex;
  bool can_pop;

  MainNavigationEvent(this.selectedIndex,this.can_pop);
}
