import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'home_videos_banner_event.dart';

part 'home_videos_banner_state.dart';

class HomeVideosBannerBloc
    extends Bloc<HomeVideosBannerEvent, HomeVideosBannerState> {
  HomeVideosBannerBloc() : super(HomeVideosBannerState(0,false)) {
    on<HomeVideosBannerEvent>((event, emit) {
      emit(HomeVideosBannerState(event.currentIndex,event.isLastPlayed));
    });
  }
}
