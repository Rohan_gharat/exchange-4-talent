part of 'home_videos_banner_bloc.dart';

@immutable
class HomeVideosBannerState {
  int currentIndex;
  bool isLastPlayed;

  HomeVideosBannerState(this.currentIndex, this.isLastPlayed);
}
