part of 'home_videos_banner_bloc.dart';

@immutable
 class HomeVideosBannerEvent {
  int currentIndex;
  bool isLastPlayed;

  HomeVideosBannerEvent(this.currentIndex,this.isLastPlayed);
}
