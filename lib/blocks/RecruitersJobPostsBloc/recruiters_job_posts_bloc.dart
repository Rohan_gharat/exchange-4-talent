import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/recruitersModel/recruitersPostModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'recruiters_job_posts_event.dart';

part 'recruiters_job_posts_state.dart';

class RecruitersJobPostsBloc
    extends Bloc<RecruitersJobPostsEvent, RecruitersJobPostsState> {
  List<RecruitersPostModel> recruitersPostsListAll = [];
  String? url = "";
  bool isLoading = false;

  RecruitersJobPostsBloc() : super(RecruitersJobPostsInitial()) {
    on<RecruitersJobPostsEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<RecruitersJobPostsLoadingEvent>((event, emit) async {
      isLoading = true;
      if (recruitersPostsListAll.isEmpty) {
        emit(RecruitersJobPostsLoadingState());
      }
      try {
        var response = await ApisServices.getRecruitersJobPosts(
          recruitersId: event.recruiterId,
          token: SharedPrefs.getAccessToken(),
          url: url,
        );

        if (response != null) {
          if (response.status == 200 &&
              response.recruitersPostDataModel != null) {
            url = response.recruitersPostDataModel!.next_page_url;

            recruitersPostsListAll.addAll(response
                .recruitersPostDataModel!.recruitersPostsList
             );

            emit(RecruitersJobPostsLoadedState(
                recruitersPostsListAll,
                response.recruitersPostDataModel!.next_page_url == null
                    ? false
                    : true));
          } else {
            if (recruitersPostsListAll.isEmpty) {
              emit(RecruitersJobPostsErrorState(
                  response.message ?? HelperClass.ERROR_TEXT));
            }
          }
        } else {
          if (recruitersPostsListAll.isEmpty) {
            emit(RecruitersJobPostsErrorState(HelperClass.ERROR_TEXT));
          }
        }
        isLoading = false;
      } catch (error) {
        isLoading = false;
        if (recruitersPostsListAll.isEmpty) {
          emit(RecruitersJobPostsErrorState(HelperClass.ERROR_TEXT));
        }
      }
    });
  }
}
