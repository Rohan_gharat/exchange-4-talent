part of 'recruiters_job_posts_bloc.dart';

@immutable
abstract class RecruitersJobPostsEvent {}

class RecruitersJobPostsLoadingEvent extends RecruitersJobPostsEvent {
  int recruiterId;

  RecruitersJobPostsLoadingEvent(this.recruiterId);
}
