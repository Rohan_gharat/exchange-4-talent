part of 'recruiters_job_posts_bloc.dart';

@immutable
abstract class RecruitersJobPostsState {}

class RecruitersJobPostsInitial extends RecruitersJobPostsState {}

class RecruitersJobPostsLoadingState extends RecruitersJobPostsState {}

class RecruitersJobPostsLoadedState extends RecruitersJobPostsState {
  List<RecruitersPostModel> recruitersPostsList;
  bool hasMoreData;

  RecruitersJobPostsLoadedState(this.recruitersPostsList, this.hasMoreData);
}

class RecruitersJobPostsErrorState extends RecruitersJobPostsState {
  String message;

  RecruitersJobPostsErrorState(this.message);
}
