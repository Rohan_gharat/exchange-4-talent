import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'video_mute_event.dart';

part 'video_mute_state.dart';

class VideoMuteBloc extends Bloc<VideoMuteEvent, VideoMuteState> {
  VideoMuteBloc() : super(VideoMuteState(false)) {
    on<VideoMuteEvent>((event, emit) {
      emit(VideoMuteState(event.isMute));
    });
  }
}
