part of 'video_mute_bloc.dart';

@immutable
 class VideoMuteEvent {
  bool isMute;

  VideoMuteEvent(this.isMute);
}
