part of 'video_mute_bloc.dart';

@immutable
 class VideoMuteState {
  bool isMute;

  VideoMuteState(this.isMute);
}

