import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'toggle_barter_event.dart';

part 'toggle_barter_state.dart';

class ToggleBarterBloc extends Bloc<ToggleBarterEvent, ToggleBarterState> {
  ToggleBarterBloc() : super(ToggleBarterLoading()) {
    on<ToggleBarterChangeEvent>((event, emit) async {
      emit(ToggleBarterLoading());

      try {
        Map data = {};
        int barterValue = 0;
        if (event.toggleBarter) {
          barterValue = 1;
        } else {
          barterValue = 0;
        }
        data[HelperClass.TRADE_BARTER_KEY] = barterValue;

        var response = await ApisServices.updateTradeBarterData(
            token: SharedPrefs.getAccessToken()!,
            tradeBarterValue: barterValue,
            userId: SharedPrefs.getUserId()!);

        if (response != null) {
          if (response.status == 200) {
            bool barterState = false;
            if (response.userModel?.tradeBarter == 1) {
              barterState = true;
            } else {
              barterState = false;
            }
            emit(ToggleBarterChangeState(barterState));
          } else {
            emit(ToggleBarterChangeState(!event.toggleBarter));
          }
        } else {
          emit(ToggleBarterChangeState(!event.toggleBarter));
        }
      } catch (error) {
        print(
            "ToggleBarterBloc error : $error :: !event.toggleBarter: ${!event.toggleBarter}");
        emit(ToggleBarterChangeState(!event.toggleBarter));
      }
    });

    on<ToggleBarterInitialEvent>((event, emit) {
      emit(ToggleBarterChangeState(event.tradeBarterInitialValue));
    });
  }
}
