part of 'toggle_barter_bloc.dart';

@immutable
abstract class ToggleBarterState {}

class ToggleBarterLoading extends ToggleBarterState {}

class ToggleBarterChangeState extends ToggleBarterState{
 bool toggleBarter;

 ToggleBarterChangeState(this.toggleBarter);
}