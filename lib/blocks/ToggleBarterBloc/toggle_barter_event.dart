part of 'toggle_barter_bloc.dart';

@immutable
abstract class ToggleBarterEvent {}

class ToggleBarterInitialEvent extends ToggleBarterEvent {
  bool tradeBarterInitialValue;

  ToggleBarterInitialEvent(this.tradeBarterInitialValue);
}

class ToggleBarterChangeEvent extends ToggleBarterEvent {
  bool toggleBarter;

  ToggleBarterChangeEvent(this.toggleBarter);
}
