part of 'feedback_bloc.dart';

@immutable
abstract class FeedbackState {}

class FeedbackInitial extends FeedbackState {}

class FeedbackLoadingState extends FeedbackState {}

class FeedbackAddedState extends FeedbackState {
  var message;

  FeedbackAddedState(this.message);
}

class FeedbackErrorState extends FeedbackState {
  var message;

  FeedbackErrorState(this.message);
}
