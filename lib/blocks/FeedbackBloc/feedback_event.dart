part of 'feedback_bloc.dart';

@immutable
abstract class FeedbackEvent {}

class AddFeedbackEvent extends FeedbackEvent {
  var feedback;

  AddFeedbackEvent(this.feedback);
}
