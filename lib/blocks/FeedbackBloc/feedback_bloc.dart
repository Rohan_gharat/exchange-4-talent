import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'feedback_event.dart';

part 'feedback_state.dart';

class FeedbackBloc extends Bloc<FeedbackEvent, FeedbackState> {
  FeedbackBloc() : super(FeedbackInitial()) {
    on<FeedbackEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<AddFeedbackEvent>((event, emit) async {
      emit(FeedbackLoadingState());
      try {
        var token = SharedPrefs.getAccessToken();
        var response = await ApisServices.addFeedback(
            token: token, feedback: event.feedback);
        if (response != null) {
          if (response.status == 200) {
            emit(FeedbackAddedState(response.message));
          } else {
            emit(
                FeedbackErrorState(response.message ?? HelperClass.ERROR_TEXT));
          }
        } else {
          emit(FeedbackErrorState(HelperClass.ERROR_TEXT));
        }
      } catch (e) {
        emit(FeedbackErrorState(HelperClass.ERROR_TEXT));
      }
    });
  }
}
