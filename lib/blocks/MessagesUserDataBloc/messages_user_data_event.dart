part of 'messages_user_data_bloc.dart';

@immutable
abstract class MessagesUserDataEvent {}

class MessagesUserDataLoadingEvent extends MessagesUserDataEvent {
  final userId;

  MessagesUserDataLoadingEvent(this.userId);
}

class MessagesUserDataAvailableEvent extends MessagesUserDataEvent {
  final UserModel userModel;

  MessagesUserDataAvailableEvent(this.userModel);
}
