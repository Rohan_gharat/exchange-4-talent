part of 'messages_user_data_bloc.dart';

@immutable
abstract class MessagesUserDataState {}

class MessagesUserDataInitial extends MessagesUserDataState {}

class MessagesUserDataLoadedState extends MessagesUserDataState {
  UserModel userModel;

  MessagesUserDataLoadedState(this.userModel);
}

class MessagesUserDataErrorState extends MessagesUserDataState {
  String errorMsg;

  MessagesUserDataErrorState(this.errorMsg);
}

class MessagesUserDataLoadingState extends MessagesUserDataState {}
