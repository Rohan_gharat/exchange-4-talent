import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/userModel/user_data_model.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'messages_user_data_event.dart';

part 'messages_user_data_state.dart';

class MessagesUserDataBloc
    extends Bloc<MessagesUserDataEvent, MessagesUserDataState> {
  MessagesUserDataBloc() : super(MessagesUserDataInitial()) {
    on<MessagesUserDataEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<MessagesUserDataLoadingEvent>((event, emit) async {
      emit(MessagesUserDataLoadingState());
      try {
        var response = await ApisServices.getUserDataById(
            userId: event.userId, token: SharedPrefs.getAccessToken()!);
        if (response != null) {
          if (response.status == 200 && response.userModel != null) {
            emit(MessagesUserDataLoadedState(response.userModel!));
          } else {
            emit(MessagesUserDataErrorState(
                response.message ?? HelperClass.ERROR_TEXT));
          }
        } else {
          emit(MessagesUserDataErrorState(HelperClass.ERROR_TEXT));
        }
      } catch (error) {
        emit(MessagesUserDataErrorState(HelperClass.ERROR_TEXT));
      }
    });

    on<MessagesUserDataAvailableEvent>((event, emit) {
      emit(MessagesUserDataLoadedState(event.userModel));
    });
  }
}
