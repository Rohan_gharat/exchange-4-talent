part of 'states_bloc.dart';

@immutable
abstract class StatesState {}

class StatesInitial extends StatesState {}

class StatesLoadingState extends StatesState {}

class StatesDropDownLoadedState extends StatesState {
  List<AddressModel> statesList;
  int selected_index;
  bool isCityPreviouslyAvailable;

  StatesDropDownLoadedState(this.statesList, this.selected_index,this.isCityPreviouslyAvailable);
}

class StatesDropDownErrorState extends StatesState {
  int countryId;
  String? availableStateName;

  bool isCityPreviouslyAvailable;

  StatesDropDownErrorState(this.countryId,this.availableStateName,this.isCityPreviouslyAvailable);
}
