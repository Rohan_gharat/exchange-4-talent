part of 'states_bloc.dart';

@immutable
abstract class StatesEvent {}


class StatesDropDownLoadingEvent extends StatesEvent {
  int countryId;
  String? availableStateName;
  bool isCityPreviouslyAvailable;

  StatesDropDownLoadingEvent(this.countryId,this.availableStateName,this.isCityPreviouslyAvailable);
}

class StatesDropDownChangeEvent extends StatesEvent {
  List<AddressModel> statesList;
  int selected_index;

  StatesDropDownChangeEvent(this.statesList, this.selected_index);
}
