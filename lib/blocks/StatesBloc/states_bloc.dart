import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../model/addressModel/addressModel.dart';
import '../../restService/apis_services.dart';
import '../../utils/sharePrefs.dart';

part 'states_event.dart';
part 'states_state.dart';

class StatesBloc extends Bloc<StatesEvent, StatesState> {
  StatesBloc() : super(StatesInitial()) {
    on<StatesEvent>((event, emit) {
    });

    on<StatesDropDownLoadingEvent>((event, emit) async {
     emit(StatesLoadingState());
      try {
        var response = await ApisServices.getStatesList(
            token: SharedPrefs.getAccessToken(),
            countyId:  event.countryId);

        if (response != null) {
          List<AddressModel> statesList = response;

          var index = statesList.indexWhere(
                  (element) => element.name!.toLowerCase() == event.availableStateName?.toLowerCase());

          emit(StatesDropDownLoadedState(statesList, index,event.isCityPreviouslyAvailable));
        } else {
          emit(StatesDropDownErrorState(event.countryId,event.availableStateName,event.isCityPreviouslyAvailable));
        }
      } catch (error) {
        emit(StatesDropDownErrorState(event.countryId,event.availableStateName,event.isCityPreviouslyAvailable));
      }
    });
    on<StatesDropDownChangeEvent>((event, emit) {
      emit(StatesDropDownLoadedState(
          event.statesList, event.selected_index,false));
    });


  }
}
