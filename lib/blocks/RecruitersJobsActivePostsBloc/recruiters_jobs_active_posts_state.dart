part of 'recruiters_jobs_active_posts_bloc.dart';

@immutable
abstract class RecruitersJobsActivePostsState {}

class RecruitersJobsActivePostsInitial extends RecruitersJobsActivePostsState {}

class RecruitersJobsActivePostsLoadingState
    extends RecruitersJobsActivePostsState {}

class RecruitersJobsActivePostsLoadedState
    extends RecruitersJobsActivePostsState {
  List<RecruitersPostModel> recruitersJobsActivePostsList;
  bool hasMoreData;

  RecruitersJobsActivePostsLoadedState(
      this.recruitersJobsActivePostsList, this.hasMoreData);
}

class RecruitersJobsActivePostsErrorState
    extends RecruitersJobsActivePostsState {
  String message;

  RecruitersJobsActivePostsErrorState(this.message);
}
