part of 'recruiters_jobs_active_posts_bloc.dart';

@immutable
abstract class RecruitersJobsActivePostsEvent {}

class RecruitersJobsActivePostsLoadingEvent
    extends RecruitersJobsActivePostsEvent {
  final int recruiterId;

  RecruitersJobsActivePostsLoadingEvent(this.recruiterId);
}

class RecruitersJobsActivePostsApplyRemoveJobEvent
    extends RecruitersJobsActivePostsEvent {
  final RecruitersPostModel recruitersPostModel;

  /// true for apply , false for remove

  RecruitersJobsActivePostsApplyRemoveJobEvent(
      {required this.recruitersPostModel});
}
