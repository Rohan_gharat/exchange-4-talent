import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/recruitersModel/recruitersPostModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'recruiters_jobs_active_posts_event.dart';

part 'recruiters_jobs_active_posts_state.dart';

class RecruitersJobsActivePostsBloc extends Bloc<RecruitersJobsActivePostsEvent,
    RecruitersJobsActivePostsState> {
  List<RecruitersPostModel> recruitersJobsActivePostsListAll = [];
  String? url = "";
  bool isLoading = false;

  RecruitersJobsActivePostsBloc() : super(RecruitersJobsActivePostsInitial()) {
    on<RecruitersJobsActivePostsEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<RecruitersJobsActivePostsLoadingEvent>((event, emit) async {
      isLoading = true;
      if (recruitersJobsActivePostsListAll.isEmpty) {
        emit(RecruitersJobsActivePostsLoadingState());
      }
      try {
        var response = await ApisServices.getRecruitersJobPosts(
          recruitersId: event.recruiterId,
          token: SharedPrefs.getAccessToken(),
          url: url,
        );

        if (response != null) {
          if (response.status == 200 &&
              response.recruitersPostDataModel != null) {
            url = response.recruitersPostDataModel!.next_page_url;

            recruitersJobsActivePostsListAll
                .addAll(response.recruitersPostDataModel!.recruitersPostsList);

            emit(RecruitersJobsActivePostsLoadedState(
              recruitersJobsActivePostsListAll,
              url == null ? false : true,
            ));
          } else {
            if (recruitersJobsActivePostsListAll.isEmpty) {
              emit(RecruitersJobsActivePostsErrorState(
                  response.message ?? HelperClass.ERROR_TEXT));
            }
          }
        } else {
          if (recruitersJobsActivePostsListAll.isEmpty) {
            emit(RecruitersJobsActivePostsErrorState(HelperClass.ERROR_TEXT));
          }
        }
        isLoading = false;
      } catch (error) {
        isLoading = false;
        if (recruitersJobsActivePostsListAll.isEmpty) {
          emit(RecruitersJobsActivePostsErrorState(HelperClass.ERROR_TEXT));
        }
      }
    });

    on<RecruitersJobsActivePostsApplyRemoveJobEvent>((event, emit) {
      var index = recruitersJobsActivePostsListAll
          .indexWhere((element) => element.id == event.recruitersPostModel.id);


      if (index != -1) {
        recruitersJobsActivePostsListAll[index] = event.recruitersPostModel;
        emit(RecruitersJobsActivePostsLoadedState(
          recruitersJobsActivePostsListAll,
          url == null ? false : true,
        ));
      }
    });
  }
}
