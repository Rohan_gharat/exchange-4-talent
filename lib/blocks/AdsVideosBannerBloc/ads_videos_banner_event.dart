part of 'ads_videos_banner_bloc.dart';

@immutable
 class AdsVideosBannerEvent {
  int currentIndex;
  bool isLastPlayed;

  AdsVideosBannerEvent(this.currentIndex, this.isLastPlayed);
}
