import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'ads_videos_banner_event.dart';

part 'ads_videos_banner_state.dart';

class AdsVideosBannerBloc
    extends Bloc<AdsVideosBannerEvent, AdsVideosBannerState> {
  AdsVideosBannerBloc() : super(AdsVideosBannerState(0, false)) {
    on<AdsVideosBannerEvent>((event, emit) {
      emit(AdsVideosBannerState(event.currentIndex, event.isLastPlayed));
    });
  }
}
