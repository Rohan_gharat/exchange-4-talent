part of 'ads_videos_banner_bloc.dart';

@immutable
 class AdsVideosBannerState {
  int currentIndex;
  bool isLastPlayed;

  AdsVideosBannerState(this.currentIndex, this.isLastPlayed);
}

