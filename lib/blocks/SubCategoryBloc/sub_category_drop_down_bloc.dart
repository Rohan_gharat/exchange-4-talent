import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/categoriesModel/subCategoriesModel.dart';
import 'package:meta/meta.dart';

import '../../restService/apis_services.dart';
import '../../utils/sharePrefs.dart';

part 'sub_category_drop_down_event.dart';

part 'sub_category_drop_down_state.dart';

class SubCategoryDropDownBloc
    extends Bloc<SubCategoryDropDownEvent, SubCategoryDropDownState> {
  SubCategoryDropDownBloc() : super(SubCategoryDropDownInitial()) {
    on<SubCategoryDropDownEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<SubCategoriesDropDownLoadingEvent>((event, emit) async {
       emit(SubCategoriesDropDownLoadingState());
      try {
        var response = await ApisServices.getSubCategoriesByCategory(
            token: SharedPrefs.getAccessToken(),
            categoryId: event.currentCategoryId);
        if (response != null) {
          List<SubCategoriesModel> subCategoriesList =
              response.subcategoriesList!;
          var index = subCategoriesList.indexWhere(
              (element) => element.id == event.availableSubCategoryId);

          emit(SubCategoriesDropDownLoadedState(subCategoriesList, index));
        } else {
          emit(SubCategoriesDropDownErrorState(event.currentCategoryId,event.availableSubCategoryId));
        }
      } catch (error) {
        emit(SubCategoriesDropDownErrorState(event.currentCategoryId,event.availableSubCategoryId));
      }
    });
    on<SubCategoriesDropDownChangeEvent>((event, emit) {
      emit(SubCategoriesDropDownLoadedState(
          event.subCategoriesList, event.selected_index));
    });
  }
}
