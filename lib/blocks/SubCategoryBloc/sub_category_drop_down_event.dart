part of 'sub_category_drop_down_bloc.dart';

@immutable
abstract class SubCategoryDropDownEvent {}

class SubCategoriesDropDownLoadingEvent extends SubCategoryDropDownEvent {
  int currentCategoryId;
  int availableSubCategoryId;

  SubCategoriesDropDownLoadingEvent(this.currentCategoryId,this.availableSubCategoryId);
}

class SubCategoriesDropDownChangeEvent extends SubCategoryDropDownEvent {
  List<SubCategoriesModel> subCategoriesList;
  int selected_index;

  SubCategoriesDropDownChangeEvent(this.subCategoriesList, this.selected_index);
}
