part of 'sub_category_drop_down_bloc.dart';

@immutable
abstract class SubCategoryDropDownState {}

class SubCategoryDropDownInitial extends SubCategoryDropDownState {}

class SubCategoriesDropDownLoadingState extends SubCategoryDropDownState {}

class SubCategoriesDropDownLoadedState extends SubCategoryDropDownState {
  List<SubCategoriesModel> subCategoriesList;
  int selected_index;

  SubCategoriesDropDownLoadedState(this.subCategoriesList, this.selected_index);
}

class SubCategoriesDropDownErrorState extends SubCategoryDropDownState {
  int currentCategoryId;
  int availableSubCategoryId;

  SubCategoriesDropDownErrorState(this.currentCategoryId,this.availableSubCategoryId);
}
