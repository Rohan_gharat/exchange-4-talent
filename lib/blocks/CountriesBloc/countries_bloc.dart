import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../model/addressModel/addressModel.dart';
import '../../restService/apis_services.dart';
import '../../utils/sharePrefs.dart';

part 'countries_event.dart';

part 'countries_state.dart';

class CountriesBloc extends Bloc<CountriesEvent, CountriesState> {
  CountriesBloc() : super(CountriesInitial()) {
    on<CountriesEvent>((event, emit) {});

    on<CountriesLoadingEvent>((event, emit) async {
      emit(CountriesLoadingState());
      try {
        var response = await ApisServices.getCountriesList();
        if (response != null) {
          List<AddressModel> countriesList = response;
          var index = countriesList.indexWhere(
              (element) => element.name!.toLowerCase() == event.currentCountryTitle?.toLowerCase());

          emit(CountriesDropDownLoadedState(
              countriesList, index, event.isStatePreviouslyAvailable));
        } else {
          emit(CountriesDropDownErrorState(
              event.currentCountryTitle, event.isStatePreviouslyAvailable));
        }
      } catch (error) {
        emit(CountriesDropDownErrorState(
            event.currentCountryTitle, event.isStatePreviouslyAvailable));
      }
    });

    on<CountriesDropDownChangeEvent>((event, emit) {
      emit(CountriesDropDownLoadedState(
          event.countriesList, event.selected_index, false));
    });
  }
}
