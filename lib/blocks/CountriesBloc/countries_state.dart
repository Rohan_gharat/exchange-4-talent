part of 'countries_bloc.dart';

@immutable
abstract class CountriesState {}

class CountriesInitial extends CountriesState {}

class CountriesLoadingState extends CountriesState {}

class CountriesDropDownLoadedState extends CountriesState {
  List<AddressModel> countriesList;
  int selected_index;
  bool isStatePreviouslyAvailable;

  CountriesDropDownLoadedState(
      this.countriesList, this.selected_index, this.isStatePreviouslyAvailable);
}

class CountriesDropDownErrorState extends CountriesState {
  String? currentCountryTitle;
  bool isStatePreviouslyAvailable;

  CountriesDropDownErrorState(
      this.currentCountryTitle, this.isStatePreviouslyAvailable);
}
