part of 'countries_bloc.dart';

@immutable
abstract class CountriesEvent {}

class CountriesLoadingEvent extends CountriesEvent{
  String? currentCountryTitle;
  bool isStatePreviouslyAvailable;

  CountriesLoadingEvent(this.currentCountryTitle, this.isStatePreviouslyAvailable);
}


class CountriesDropDownChangeEvent extends CountriesEvent {
  List<AddressModel> countriesList;
  int selected_index;

  CountriesDropDownChangeEvent(this.countriesList, this.selected_index);
}
