import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'recruiters_apply_event.dart';

part 'recruiters_apply_state.dart';

class RecruitersApplyBloc
    extends Bloc<RecruitersApplyEvent, RecruitersApplyState> {
  RecruitersApplyBloc() : super(RecruitersApplyState(0)) {
    on<RecruitersApplyEvent>((event, emit) {
      emit(RecruitersApplyState(event.jobStatusId));
    });
  }
}
