part of 'recruiters_apply_bloc.dart';

@immutable
class RecruitersApplyEvent {
  int jobStatusId;

  RecruitersApplyEvent(this.jobStatusId);
}
