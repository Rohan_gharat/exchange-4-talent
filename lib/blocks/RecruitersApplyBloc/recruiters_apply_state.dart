part of 'recruiters_apply_bloc.dart';

@immutable
 class RecruitersApplyState {
  int jobStatusId;

  RecruitersApplyState(this.jobStatusId);
}
