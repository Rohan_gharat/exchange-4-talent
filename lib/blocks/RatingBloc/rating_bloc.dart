import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'rating_event.dart';

part 'rating_state.dart';

class RatingBloc extends Bloc<RatingEvent, RatingState> {
  RatingBloc() : super(RatingState(0.0)) {
    on<RatingEvent>((event, emit) {
      emit(RatingState(event.rating));
    });
  }
}
