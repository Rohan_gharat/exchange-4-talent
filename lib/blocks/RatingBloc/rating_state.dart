part of 'rating_bloc.dart';

@immutable
 class RatingState {
  double rating;

  RatingState(this.rating);
}

