part of 'rating_bloc.dart';

@immutable
 class RatingEvent {
  double rating;

  RatingEvent(this.rating);
}
