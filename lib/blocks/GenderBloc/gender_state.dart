part of 'gender_bloc.dart';

@immutable
 class GenderState {
  int index;

  GenderState(this.index);
}
