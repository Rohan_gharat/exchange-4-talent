part of 'gender_bloc.dart';

@immutable
 class GenderEvent {
  int index;

  GenderEvent(this.index);
}
