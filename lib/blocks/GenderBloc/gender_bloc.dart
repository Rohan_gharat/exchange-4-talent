import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:meta/meta.dart';

part 'gender_event.dart';

part 'gender_state.dart';

class GenderBloc extends Bloc<GenderEvent, GenderState> {
  GenderBloc() : super(GenderState(HelperClass.MALE_RADIO_BUTTON_INDEX)) {
    on<GenderEvent>((event, emit) {
      emit(GenderState(event.index));
    });
  }
}
