import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/homeModel/home_model.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial()) {
    on<HomeEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<HomeLoadingEvent>((event, emit) async {
      emit(HomeLoadingState());
      try {
        var response = await ApisServices.getHomeData(
            token: SharedPrefs.getAccessToken());
        if (response != null) {

          emit(HomeLoadedState(response));
        } else {
          emit(HomeErrorState());
        }
      } catch (error) {
        print("HomeLoadingEvent:error: $error");
        emit(HomeErrorState());
      }
    });
  }
}
