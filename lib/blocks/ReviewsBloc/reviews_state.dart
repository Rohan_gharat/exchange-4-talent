part of 'reviews_bloc.dart';

@immutable
abstract class ReviewsState {}

class ReviewsInitial extends ReviewsState {}


class ReviewsLoadingState extends ReviewsState {}

class ReviewsLoadedState extends ReviewsState {

  List<ReviewsDataModel> reviewsList;

  ReviewsLoadedState(this.reviewsList);
}

class ReviewsErrorState extends ReviewsState {
  String errrorMsg;

  ReviewsErrorState(this.errrorMsg);
}
