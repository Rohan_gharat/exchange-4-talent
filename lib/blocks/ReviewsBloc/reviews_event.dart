part of 'reviews_bloc.dart';

@immutable
abstract class ReviewsEvent {}

class ReviewsLoadingEvent extends ReviewsEvent {
  int talent_id;

  ReviewsLoadingEvent(this.talent_id);
}
