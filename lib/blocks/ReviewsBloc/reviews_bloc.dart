import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/ReviewsModel/reviewsDataModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'reviews_event.dart';

part 'reviews_state.dart';

class ReviewsBloc extends Bloc<ReviewsEvent, ReviewsState> {
  ReviewsBloc() : super(ReviewsInitial()) {
    on<ReviewsEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<ReviewsLoadingEvent>((event, emit) async {
      emit(ReviewsLoadingState());
      try {
        var response = await ApisServices.getReviews(
            token: SharedPrefs.getAccessToken(), talent_id: event.talent_id);

        if (response != null) {
          if (response.status == 200) {
            emit(ReviewsLoadedState(response.reviewsList));
          } else {
            emit(ReviewsErrorState(response.message));
          }
        } else {
          emit(ReviewsErrorState(HelperClass.ERROR_TEXT));
        }
      } catch (error) {
        emit(ReviewsErrorState(HelperClass.ERROR_TEXT));
      }
    });
  }
}
