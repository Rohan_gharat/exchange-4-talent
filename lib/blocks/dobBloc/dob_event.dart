part of 'dob_bloc.dart';

@immutable
 class DobEvent {
  String? dob;

  DobEvent(this.dob);
}
