import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'dob_event.dart';

part 'dob_state.dart';

class DobBloc extends Bloc<DobEvent, DobState> {
  DobBloc() : super(DobState("")) {
    on<DobEvent>((event, emit) {
      emit(DobState(event.dob));
    });
  }
}
