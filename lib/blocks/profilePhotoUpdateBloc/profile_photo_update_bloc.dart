import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'profile_photo_update_event.dart';

part 'profile_photo_update_state.dart';

class ProfilePhotoUpdateBloc
    extends Bloc<ProfilePhotoUpdateEvent, ProfilePhotoUpdateState> {
  ProfilePhotoUpdateBloc() : super(ProfilePhotoUpdateState("","","")) {
    on<ProfilePhotoUpdateEvent>((event, emit) {
      emit(ProfilePhotoUpdateState(event.profilePhotoUrl,event.name,event.mobile));
    });

  }
}
