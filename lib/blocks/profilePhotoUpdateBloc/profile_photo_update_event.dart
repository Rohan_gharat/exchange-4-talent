part of 'profile_photo_update_bloc.dart';

@immutable
 class ProfilePhotoUpdateEvent {
  String? profilePhotoUrl,name,mobile;
  // int id;

  ProfilePhotoUpdateEvent(this.profilePhotoUrl,this.name,this.mobile);
}

