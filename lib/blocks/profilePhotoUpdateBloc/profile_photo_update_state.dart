part of 'profile_photo_update_bloc.dart';

@immutable
 class ProfilePhotoUpdateState {
  String? profilePhotoUrl,name,mobile;
  // int id;

  ProfilePhotoUpdateState(this.profilePhotoUrl,this.name,this.mobile);
}

