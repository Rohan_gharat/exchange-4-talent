part of 'subscription_bloc.dart';

@immutable
abstract class SubscriptionState {}

class SubscriptionInitial extends SubscriptionState {}

class SubscriptionLoadingState extends SubscriptionState {}

class SubscriptionLoadedState extends SubscriptionState {
  List<SubscriptionsModel> subscriptionsList;

  SubscriptionLoadedState(this.subscriptionsList);
}

class SubscriptionErrorState extends SubscriptionState {}
