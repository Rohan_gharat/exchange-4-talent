import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/subscriptionsModel/subcriptions_model.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'subscription_event.dart';

part 'subscription_state.dart';

class SubscriptionBloc extends Bloc<SubscriptionEvent, SubscriptionState> {
  List<SubscriptionsModel> subscriptionsList = [];

  int selectedSubscriptionIndex = 0;

  SubscriptionBloc() : super(SubscriptionInitial()) {
    on<SubscriptionEvent>((event, emit) {
    });

    on<SubscriptionsLoadingEvent>((event, emit) async {
      emit(SubscriptionLoadingState());
      try {
        var response = await ApisServices.getSubscriptions(
          token: SharedPrefs.getAccessToken(),
          subscription_id: event.subscription_id,
        );
        if (response != null) {
          subscriptionsList = response;
          emit(SubscriptionLoadedState(response));
        } else {
          emit(SubscriptionErrorState());
        }
      } catch (error) {
        print("SubscriptionsLoadingEvent:error : $error");
        emit(SubscriptionErrorState());
      }
    });
  }
}
