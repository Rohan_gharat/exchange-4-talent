part of 'subscription_bloc.dart';

@immutable
abstract class SubscriptionEvent {}

class SubscriptionsLoadingEvent extends SubscriptionEvent{
  int subscription_id;

  SubscriptionsLoadingEvent(this.subscription_id);
}
