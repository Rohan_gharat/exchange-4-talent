import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'apply_coupon_event.dart';
part 'apply_coupon_state.dart';

class ApplyCouponBloc extends Bloc<ApplyCouponEvent, ApplyCouponState> {
  ApplyCouponBloc() : super(ApplyCouponState(false)) {
    on<ApplyCouponEvent>((event, emit) {
      emit(ApplyCouponState(event.appCouponButtonColorCheck));
    });
  }
}
