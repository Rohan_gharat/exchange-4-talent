part of 'apply_coupon_bloc.dart';

@immutable
 class ApplyCouponEvent {
  bool appCouponButtonColorCheck;

  ApplyCouponEvent(this.appCouponButtonColorCheck);
}
