part of 'hire_data_show_bloc.dart';

@immutable
 class HireDataShowEvent {
  int index;

  HireDataShowEvent(this.index);
}
