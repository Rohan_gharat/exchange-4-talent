import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:meta/meta.dart';

part 'hire_data_show_event.dart';

part 'hire_data_show_state.dart';

class HireDataShowBloc extends Bloc<HireDataShowEvent, HireDataShowState> {
  HireDataShowBloc()
      : super(HireDataShowState(HelperClass.BOTH_DATA_RADIO_BUTTON_INDEX)) {
    on<HireDataShowEvent>((event, emit) {
      emit(HireDataShowState(event.index));
    });
  }
}
