part of 'categories_drop_down_bloc.dart';

@immutable
abstract class CategoriesDropDownState {}

class CategoriesDropDownInitial extends CategoriesDropDownState {}

class CategoriesDropDownLoadingState extends CategoriesDropDownState {}

class CategoriesDropDownLoadedState extends CategoriesDropDownState {
  List<CategoriesModel> categoriesList;
  int selected_index;
  bool isSubcategoryPreviouslyAvailable;

  CategoriesDropDownLoadedState(this.categoriesList, this.selected_index,this.isSubcategoryPreviouslyAvailable);
}

class CategoriesDropDownErrorState extends CategoriesDropDownState {
  int currentCategoryId;
  bool isSubcategoryPreviouslyAvailable;

  CategoriesDropDownErrorState(this.currentCategoryId,this.isSubcategoryPreviouslyAvailable);
}
