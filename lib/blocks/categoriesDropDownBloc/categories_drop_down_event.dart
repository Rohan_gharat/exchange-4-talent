part of 'categories_drop_down_bloc.dart';

@immutable
abstract class CategoriesDropDownEvent {}

class CategoriesDropDownLoadingEvent extends CategoriesDropDownEvent {
  int currentCategoryId;
  bool isSubcategoryPreviouslyAvailable;

  CategoriesDropDownLoadingEvent(this.currentCategoryId,this.isSubcategoryPreviouslyAvailable);
}

class CategoriesDropDownChangeEvent extends CategoriesDropDownEvent {
  List<CategoriesModel> categoriesList;
  int selected_index;

  CategoriesDropDownChangeEvent(this.categoriesList, this.selected_index);
}
