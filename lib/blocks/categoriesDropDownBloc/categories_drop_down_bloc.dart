import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

import '../../restService/apis_services.dart';

part 'categories_drop_down_event.dart';

part 'categories_drop_down_state.dart';

class CategoriesDropDownBloc
    extends Bloc<CategoriesDropDownEvent, CategoriesDropDownState> {
  CategoriesDropDownBloc() : super(CategoriesDropDownInitial()) {
    on<CategoriesDropDownEvent>((event, emit) async {});

    on<CategoriesDropDownLoadingEvent>((event, emit) async {
      emit(CategoriesDropDownLoadingState());
      try {
        var response = await ApisServices.getCategories(
            token: SharedPrefs.getAccessToken());
        if (response != null) {
          List<CategoriesModel> categoriesList = response;
          var index = categoriesList
              .indexWhere((element) => element.id == event.currentCategoryId);

          emit(CategoriesDropDownLoadedState(categoriesList, index,event.isSubcategoryPreviouslyAvailable));
        } else {
          emit(CategoriesDropDownErrorState(event.currentCategoryId,event.isSubcategoryPreviouslyAvailable));
        }
      } catch (error) {
        emit(CategoriesDropDownErrorState(event.currentCategoryId,event.isSubcategoryPreviouslyAvailable));
      }
    });
    on<CategoriesDropDownChangeEvent>((event, emit) {
      emit(CategoriesDropDownLoadedState(
          event.categoriesList, event.selected_index,false));
    });
  }
}
