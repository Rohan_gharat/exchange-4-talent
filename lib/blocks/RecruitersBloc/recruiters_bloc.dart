import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/blocks/ReviewsBloc/reviews_bloc.dart';
import 'package:e4t/model/recruitersModel/recruitersPostModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'recruiters_event.dart';

part 'recruiters_state.dart';

class RecruitersBloc extends Bloc<RecruitersEvent, RecruitersState> {
  List<RecruitersPostModel> recruitersPostsListAll = [];

  String? url = "";
  bool isLoading = false;

  RecruitersBloc() : super(RecruitersInitial()) {
    on<RecruitersEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<RecruitersLoadingEvent>((event, emit) async {
      isLoading = true;
      if (recruitersPostsListAll.isEmpty) {
        emit(RecruitersLoadingState());
      }
      try {
        var response = await ApisServices.getRecruitersPosts(
          userId: SharedPrefs.getUserId(),
          token: SharedPrefs.getAccessToken(),
          url: url,
        );

        if (response != null) {
          if (response.status == 200 &&
              response.recruitersPostDataModel != null) {
            url = response.recruitersPostDataModel!.next_page_url;

            recruitersPostsListAll.addAll(response
                .recruitersPostDataModel!.recruitersPostsList
                .where((element) => element.recruitersModel != null)
                .toList());

            emit(RecruitersLoadedState(
                recruitersPostsListAll,
                response.recruitersPostDataModel!.next_page_url == null
                    ? false
                    : true));
          } else {
            if (recruitersPostsListAll.isEmpty) {
              emit(RecruitersErrorState(
                  response.message ?? HelperClass.ERROR_TEXT));
            }
          }
        } else {
          if (recruitersPostsListAll.isEmpty) {
            emit(RecruitersErrorState(HelperClass.ERROR_TEXT));
          }
        }
        isLoading = false;
      } catch (error) {
        isLoading = false;
        if (recruitersPostsListAll.isEmpty) {
          emit(RecruitersErrorState(HelperClass.ERROR_TEXT));
        }
      }
    });

    on<RecruitersApplyRemoveJobEvent>((event, emit) {
      var index = recruitersPostsListAll
          .indexWhere((element) => element.id == event.recruitersPostModel.id);


      if (index != -1) {
        /// true for apply , false for remove
        if (event.isApply) {
          recruitersPostsListAll.add(event.recruitersPostModel);
        } else {
          if (recruitersPostsListAll.isNotEmpty) {
            recruitersPostsListAll.removeAt(index);
          }
        }

        emit(RecruitersLoadedState(
          recruitersPostsListAll,
          url == null ? false : true,
        ));
      }
    });
  }
}
