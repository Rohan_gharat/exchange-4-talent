part of 'recruiters_bloc.dart';

@immutable
abstract class RecruitersState {}

class RecruitersInitial extends RecruitersState {}

class RecruitersLoadingState extends RecruitersState {}

class RecruitersLoadedState extends RecruitersState {
  List<RecruitersPostModel> recruitersPostsList;
  bool hasMoreData;

  RecruitersLoadedState(this.recruitersPostsList,this.hasMoreData);
}

class RecruitersErrorState extends RecruitersState {
  String message;

  RecruitersErrorState(this.message);
}
