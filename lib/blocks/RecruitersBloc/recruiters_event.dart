part of 'recruiters_bloc.dart';

@immutable
abstract class RecruitersEvent {}

class RecruitersLoadingEvent extends RecruitersEvent {}

class RecruitersApplyRemoveJobEvent extends RecruitersEvent {
  final RecruitersPostModel recruitersPostModel;
  final bool isApply;

  /// true for apply , false for remove

  RecruitersApplyRemoveJobEvent({
    required this.recruitersPostModel,
    required this.isApply,
  });
}
