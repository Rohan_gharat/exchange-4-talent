part of 'talent_barter_bloc.dart';

@immutable
 class TalentBarterState {
  bool isTalentBarter;

  TalentBarterState(this.isTalentBarter);
}

