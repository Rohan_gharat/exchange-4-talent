part of 'talent_barter_bloc.dart';

@immutable
 class TalentBarterEvent {
  bool isTalentBarter;

  TalentBarterEvent(this.isTalentBarter);
}
