import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'talent_barter_event.dart';
part 'talent_barter_state.dart';

class TalentBarterBloc extends Bloc<TalentBarterEvent, TalentBarterState> {
  TalentBarterBloc() : super(TalentBarterState(true)) {
    on<TalentBarterEvent>((event, emit) {
      emit(TalentBarterState(event.isTalentBarter));
    });
  }
}
