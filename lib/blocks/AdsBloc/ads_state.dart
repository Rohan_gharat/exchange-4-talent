part of 'ads_bloc.dart';

@immutable
abstract class AdsState {}

class AdsInitial extends AdsState {}

class AdsLoadingState extends AdsState {}

class AdsLoadedState extends AdsState {
  List<AdsDataModel> adsDataList;

  AdsLoadedState(this.adsDataList);
}

class AdsErrorState extends AdsState {}
