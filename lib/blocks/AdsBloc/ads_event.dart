part of 'ads_bloc.dart';

@immutable
abstract class AdsEvent {}

class AdsLoadingEvent extends AdsEvent {}
