import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/AdsModel/adsModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'ads_event.dart';

part 'ads_state.dart';

class AdsBloc extends Bloc<AdsEvent, AdsState> {
  AdsBloc() : super(AdsInitial()) {
    on<AdsEvent>((event, emit) {
      // TODO: implement event handler
    });
    on<AdsLoadingEvent>((event, emit) async {
      emit(AdsLoadingState());
      try {
        var response =
            await ApisServices.getAds();

        if (response != null && response.status == 200) {
          emit(AdsLoadedState(response.adsDataList!));
        } else {
          emit(AdsErrorState());
        }
      } catch (error) {
        emit(AdsErrorState());
      }
    });
  }
}
