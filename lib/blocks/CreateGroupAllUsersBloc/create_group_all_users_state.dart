part of 'create_group_all_users_bloc.dart';

@immutable
abstract class CreateGroupAllUsersState {}

class CreateGroupAllUsersInitial extends CreateGroupAllUsersState {}

class CreateGroupAllChatUsersLoadingState extends CreateGroupAllUsersState {}

class CreateGroupAllChatUsersLoadedState extends CreateGroupAllUsersState {
  List<FirebaseUserModel> allChatUsersList;
  bool hasMoreData;

  CreateGroupAllChatUsersLoadedState(this.allChatUsersList,this.hasMoreData);
}

class CreateGroupAllChatUsersErrorState extends CreateGroupAllUsersState {}