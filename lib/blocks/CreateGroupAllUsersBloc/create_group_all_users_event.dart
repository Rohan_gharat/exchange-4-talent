part of 'create_group_all_users_bloc.dart';

@immutable
abstract class CreateGroupAllUsersEvent {}

class CreateGroupAllChatUsersLoadingEvent extends CreateGroupAllUsersEvent {
  String currentUserId;
  FirebaseFirestore firestore;

  CreateGroupAllChatUsersLoadingEvent(this.currentUserId, this.firestore);
}

class CreateGroupAllChatUsersLoadedEvent extends CreateGroupAllUsersEvent {
  List<FirebaseUserModel> allChatUsers;
  bool hasMoreData;

  CreateGroupAllChatUsersLoadedEvent(this.allChatUsers, this.hasMoreData);
}
