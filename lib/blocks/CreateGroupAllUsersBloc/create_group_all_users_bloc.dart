import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/firebase/firebaseUserModel.dart';
import 'package:meta/meta.dart';

part 'create_group_all_users_event.dart';

part 'create_group_all_users_state.dart';

class CreateGroupAllUsersBloc
    extends Bloc<CreateGroupAllUsersEvent, CreateGroupAllUsersState> {
  List<FirebaseUserModel> allChatUsers = [];
  StreamSubscription? chatsSubscription;
  DocumentSnapshot? lastDoc;

  CreateGroupAllUsersBloc() : super(CreateGroupAllUsersInitial()) {
    on<CreateGroupAllUsersEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<CreateGroupAllChatUsersLoadingEvent>((event, emit) {

      List<FirebaseUserModel> users = [];
      if (allChatUsers.isEmpty) {
        emit(CreateGroupAllChatUsersLoadingState());
      }
      chatsSubscription?.cancel();
      if (lastDoc == null) {
        chatsSubscription = FireBaseHelperClass.getFirstUsersFromFirebase(
          fireStore: event.firestore,
          currentUserUid: event.currentUserId,
        ).listen((event) {

          if (event.docs.isNotEmpty) {
            lastDoc = event.docs[event.docs.length - 1];
          }
          var hasMoreData = true;
          if (event.docs.length < FireBaseHelperClass.CHAT_PAGINATION_USERS_LIMIT) {
            hasMoreData = false;
          }

          users = event.docs.map((snapshot) {
            return FirebaseUserModel.fromMap(snapshot.data());
          }).toList();

          allChatUsers.addAll(users);

          add(CreateGroupAllChatUsersLoadedEvent(allChatUsers, hasMoreData));
        }, onError: (error) {
          if (allChatUsers.isEmpty) {
            emit(CreateGroupAllChatUsersErrorState());
          }
        });
      } else {
        chatsSubscription = FireBaseHelperClass.getNextUsersFromFirebase(
          fireStore: event.firestore,
          currentUserUid: event.currentUserId,
          lastDoc: lastDoc,
        ).listen((event) {

          if (event.docs.isNotEmpty) {
            lastDoc = event.docs[event.docs.length - 1];
          }
          var hasMoreData = true;
          if (event.docs.length < FireBaseHelperClass.CHAT_PAGINATION_USERS_LIMIT) {
            hasMoreData = false;
          }
          users = event.docs.map((snapshot) {
            return FirebaseUserModel.fromMap(snapshot.data());
          }).toList();

          allChatUsers.addAll(users);

          add(CreateGroupAllChatUsersLoadedEvent(allChatUsers, hasMoreData));
        }, onError: (error) {
          if (allChatUsers.isEmpty) {
            emit(CreateGroupAllChatUsersErrorState());
          }
        });
      }
    });

    on<CreateGroupAllChatUsersLoadedEvent>((event, emit) {
      emit(CreateGroupAllChatUsersLoadedState(
          event.allChatUsers, event.hasMoreData));
    });
  }

  @override
  Future<void> close() {
    // TODO: implement close
    chatsSubscription?.cancel();
    return super.close();
  }
}
