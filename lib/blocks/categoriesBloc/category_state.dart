part of 'category_bloc.dart';

@immutable
abstract class CategoryState {}

class CategoryInitial extends CategoryState {}

class SubCategoriesLoadingState extends CategoryState {}

class SubCategoriesLoadedState extends CategoryState {
  SubcategoryByCategoryModel subcategoryByCategoryModel;

  SubCategoriesLoadedState(this.subcategoryByCategoryModel);
}

class SubCategoriesErrorState extends CategoryState {}
