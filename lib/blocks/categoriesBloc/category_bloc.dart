import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/categoriesModel/subcategory_by_category_model.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';


part 'category_event.dart';

part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  int categoryId = -1;
  int subCategoryId = -1;
  String categoryName = "";

  CategoryBloc() : super(CategoryInitial()) {
    on<CategoryEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<SubcategoryLoadingEvent>((event, emit) async {
      categoryId = event.categoryId;
      categoryName = event.categoryName;
      emit(SubCategoriesLoadingState());
      try {
        var response = await ApisServices.getSubCategoriesByCategory(
            token: SharedPrefs.getAccessToken(), categoryId: categoryId);
        if (response != null) {
          emit(SubCategoriesLoadedState(response));
        } else {
          emit(SubCategoriesErrorState());
        }
      } catch (error) {
        print("SubcategoryLoadingEvent: error: $error");
        emit(SubCategoriesErrorState());
      }
    });

  }
}
