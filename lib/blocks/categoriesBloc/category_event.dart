part of 'category_bloc.dart';

@immutable
abstract class CategoryEvent {}

class SubcategoryLoadingEvent extends CategoryEvent {
  int categoryId;
  String categoryName;

  SubcategoryLoadingEvent(this.categoryId,this.categoryName);
}

