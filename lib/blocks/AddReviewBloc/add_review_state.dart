part of 'add_review_bloc.dart';

@immutable
abstract class AddReviewState {}

class AddReviewInitial extends AddReviewState {}

class ReviewPostedState extends AddReviewState {
  double rating;
  String msg;

  ReviewPostedState(this.rating,this.msg);
}

class ReviewPostingState extends AddReviewState {}

class ReviewErrorState extends AddReviewState {
  String errorMsg;

  ReviewErrorState(this.errorMsg);
}
