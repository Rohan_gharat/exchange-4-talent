part of 'add_review_bloc.dart';

@immutable
abstract class AddReviewEvent {}

class ReviewInitialEvent extends AddReviewEvent{
  double rating;

  ReviewInitialEvent(this.rating);
}

class ReviewPostingEvent extends AddReviewEvent{
  String review;
  double rating;
  int talent_id;

  ReviewPostingEvent(this.review, this.rating, this.talent_id);
}
