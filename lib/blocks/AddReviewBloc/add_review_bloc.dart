import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

import '../../restService/apis_services.dart';

part 'add_review_event.dart';

part 'add_review_state.dart';

class AddReviewBloc extends Bloc<AddReviewEvent, AddReviewState> {
  AddReviewBloc() : super(AddReviewInitial()) {
    on<ReviewPostingEvent>((event, emit) async {
      emit(ReviewPostingState());

      var response = await ApisServices.addReviews(
          token: SharedPrefs.getAccessToken(),
          rating: event.rating,
          talent_id: event.talent_id,
          review: event.review);
      if (response != null) {
        if (response.status == 200) {
          emit(ReviewPostedState(
              double.parse( response.average_rating ?? "0.0"), response.message ?? ""));
        } else {
          emit(ReviewErrorState(response.message ?? ""));
        }
      } else {
        emit(ReviewErrorState(HelperClass.ERROR_TEXT));
      }
    });

    on<ReviewInitialEvent>((event, emit) {
      emit(ReviewPostedState(event.rating,  ""));

    });
  }
}
