part of 'favourite_bloc.dart';

@immutable
abstract class FavouriteState {}

class FavouriteInitial extends FavouriteState {}

class FavouriteLoadingState extends FavouriteState {}

class FavouriteLoadedState extends FavouriteState {
  List<UserModel> favouritesList;

  FavouriteLoadedState(this.favouritesList);
}

class FavouriteErrorState extends FavouriteState {}

class RemoveFavouriteState extends FavouriteState {
  UpdateFavouriteModel updateFavouriteModel;

  RemoveFavouriteState(this.updateFavouriteModel);
}

class RemoveFavouriteErrorState extends FavouriteState {}
