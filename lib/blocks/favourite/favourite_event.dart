part of 'favourite_bloc.dart';

@immutable
abstract class FavouriteEvent {}

class FavouriteLoadingEvent extends FavouriteEvent {}

class RemoveFavouriteEvent extends FavouriteEvent{
  int index,talent_id;

  RemoveFavouriteEvent(this.index,this.talent_id);
}
