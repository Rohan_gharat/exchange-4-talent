import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/favouriteModel/update_favourite_model.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'favourite_event.dart';

part 'favourite_state.dart';

class FavouriteBloc extends Bloc<FavouriteEvent, FavouriteState> {
  List<UserModel> favouritesList = [];

  FavouriteBloc() : super(FavouriteInitial()) {
    on<FavouriteEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<FavouriteLoadingEvent>((event, emit) async {
      emit(FavouriteLoadingState());
      try {
        var response = await ApisServices.getFavourites(
            token: SharedPrefs.getAccessToken());
        if (response != null) {
          favouritesList = response;
          emit(FavouriteLoadedState(favouritesList));
        } else {
          emit(FavouriteErrorState());
        }
      } catch (error) {
        emit(FavouriteErrorState());
      }
    });

    on<RemoveFavouriteEvent>((event, emit) async {
      emit(FavouriteLoadingState());

      try {
        var response = await ApisServices.updateFavouriteStatus(
            token: SharedPrefs.getAccessToken(), talent_id: event.talent_id);

        if (response != null) {
          if (response.status == 200) {
            if (favouritesList.isNotEmpty) {
              favouritesList.removeAt(event.index);
            }
          }
          emit(RemoveFavouriteState(response));
        } else {
          emit(RemoveFavouriteErrorState());
        }
      } catch (error) {
        emit(RemoveFavouriteErrorState());
      }
    });
  }
}
