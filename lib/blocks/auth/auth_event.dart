part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class LogInEvent extends AuthEvent {
  final phoneNo;
  var isFromResend = false;

  LogInEvent(
    this.phoneNo,
    this.isFromResend,
  );
}

class VerifyOtpEvent extends AuthEvent {
  var phoneNo;
  var otp;

  VerifyOtpEvent(
    this.phoneNo,
    this.otp,
  );
}
