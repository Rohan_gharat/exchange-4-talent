import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/model/authModel/login_model.dart';
import 'package:e4t/model/authModel/otpModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'auth_state.dart';

part 'auth_event.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  var mobileController = TextEditingController();
  var otpController = TextEditingController();
  int? unAuthorisedUserOtp;
  String? phoneCode;

  AuthBloc() : super(AuthInitialState()) {
    on<LogInEvent>((event, emit) async {
      emit(LoadingState());
      try {
        var logInResponse = await ApisServices.login(phoneNo: event.phoneNo,phoneCode: phoneCode);

        if (logInResponse == null) {
          emit(AuthErrorState(HelperClass.ERROR_TEXT));
        } else {
           unAuthorisedUserOtp = logInResponse.loginDataModel?.otp;
          emit(LoggedInState(logInResponse, event.isFromResend));
        }
      } catch (error) {
        emit(AuthErrorState(HelperClass.ERROR_TEXT));
      }
    });

    on<VerifyOtpEvent>((event, emit) async {
      emit(OTPLoadingState());
      try {
        var otpResponse =
            await ApisServices.otp(phoneNo: event.phoneNo, otp: event.otp,phoneCode: phoneCode);
        if (otpResponse == null) {
          emit(OTPErrorState(HelperClass.ERROR_TEXT));
        } else {
          if (otpResponse.status == 200) {
            FireBaseHelperClass.createUserInFireBase(
                fireStore: FirebaseFirestore.instance,
                usermodel: otpResponse.otpDataModel!.usermodel!);

            await SharedPrefs.setBool(SharedPrefs.isOtpCompletedKey, true);
            await SharedPrefs.setUserId(otpResponse.otpDataModel?.userId);
            await SharedPrefs.setAccessToken(
                "Bearer ${otpResponse.otpDataModel?.token}");
            mobileController.clear();
            otpController.clear();
            unAuthorisedUserOtp = null;
            phoneCode=null;

            emit(VerifyOTPState(otpResponse));
          } else {
            emit(OTPErrorState(otpResponse.message ?? ""));
          }
        }
      } catch (error) {
        emit(OTPErrorState(HelperClass.ERROR_TEXT));
      }
    });
  }
}
