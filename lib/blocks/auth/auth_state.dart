part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitialState extends AuthState {}

class LoadingState extends AuthState {}

class LoggedInState extends AuthState {
  final LoginModel? loginmodel;
  final isFromResend;

  LoggedInState(this.loginmodel,this.isFromResend);
}

class AuthErrorState extends AuthState {
  final String message;

  AuthErrorState(this.message);
}

class OTPLoadingState extends AuthState {}

class VerifyOTPState extends AuthState {
  final OTPModel otpModel;

  VerifyOTPState(this.otpModel);
}

class OTPErrorState extends AuthState {
  String errorMsg;

  OTPErrorState(this.errorMsg);
}
