import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/subscriptionsModel/subscriptionsDrawerMenuModel.dart';
import 'package:meta/meta.dart';

part 'subscriptions_drawer_menu_event.dart';

part 'subscriptions_drawer_menu_state.dart';

class SubscriptionsDrawerMenuBloc
    extends Bloc<SubscriptionsDrawerMenuEvent, SubscriptionsDrawerMenuState> {
  SubscriptionsDrawerMenuBloc() : super(SubscriptionsDrawerMenuState([])) {
    on<SubscriptionsDrawerMenuEvent>((event, emit) {
      emit(SubscriptionsDrawerMenuState(event.subscriptionsDrawerMenuList));
    });
  }
}
