part of 'subscriptions_drawer_menu_bloc.dart';

@immutable
class SubscriptionsDrawerMenuState {
  List<SubscriptionsDrawerMenuModel> subscriptionsDrawerMenuList;

  SubscriptionsDrawerMenuState(this.subscriptionsDrawerMenuList);
}
