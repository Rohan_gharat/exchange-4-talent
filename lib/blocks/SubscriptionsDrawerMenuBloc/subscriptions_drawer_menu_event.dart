part of 'subscriptions_drawer_menu_bloc.dart';

@immutable
class SubscriptionsDrawerMenuEvent {
  List<SubscriptionsDrawerMenuModel> subscriptionsDrawerMenuList;

  SubscriptionsDrawerMenuEvent(this.subscriptionsDrawerMenuList);
}
