
import 'package:bloc/bloc.dart';
import 'package:e4t/model/subscriptionsModel/coupon_code_model.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:meta/meta.dart';

part 'available_coupons_event.dart';

part 'available_coupons_state.dart';

class AvailableCouponsBloc
    extends Bloc<AvailableCouponsEvent, AvailableCouponsState> {
  AvailableCouponsBloc() : super(AvailableCouponsInitial()) {
    on<AvailableCouponsEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<AvailableCouponsLoadingEvent>((event, emit) async {
      emit(AvailableCouponsLoadingState());
      try {
        var response = await ApisServices.getAvailableCouponCodes();
        if (response != null) {
          if (response.status == 200) {
            emit(AvailableCouponsLoadedState(response.couponCodesList ?? []));
          } else {
            emit(AvailableCouponsErrorState());
          }
        } else {
          emit(AvailableCouponsErrorState());
        }
      } catch (error) {
        emit(AvailableCouponsErrorState());
      }
    });
  }
}
