part of 'available_coupons_bloc.dart';

@immutable
abstract class AvailableCouponsState {}

class AvailableCouponsInitial extends AvailableCouponsState {}

class AvailableCouponsLoadingState extends AvailableCouponsState {}

class AvailableCouponsLoadedState extends AvailableCouponsState {
  final List<CouponModel> availableCouponsList;

  AvailableCouponsLoadedState(this.availableCouponsList);
}

class AvailableCouponsErrorState extends AvailableCouponsState {}
