part of 'available_coupons_bloc.dart';

@immutable
abstract class AvailableCouponsEvent {}

class AvailableCouponsLoadingEvent extends AvailableCouponsEvent {}
