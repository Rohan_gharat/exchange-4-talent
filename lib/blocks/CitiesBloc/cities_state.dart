part of 'cities_bloc.dart';

@immutable
abstract class CitiesState {}

class CitiesInitial extends CitiesState {}


class CitiesLoadingState extends CitiesState {}

class CitiesDropDownLoadedState extends CitiesState {
  List<AddressModel> citiesList;
  int selected_index;

  CitiesDropDownLoadedState(this.citiesList, this.selected_index);
}

class CitiesDropDownErrorState extends CitiesState {
  int StateId;
  String? availableCityName;

  CitiesDropDownErrorState(this.StateId,this.availableCityName);
}