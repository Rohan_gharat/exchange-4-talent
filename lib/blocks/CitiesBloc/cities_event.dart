part of 'cities_bloc.dart';

@immutable
abstract class CitiesEvent {}


class CitiesDropDownLoadingEvent extends CitiesEvent {
  int stateId;
  String? availableCityName;

  CitiesDropDownLoadingEvent(this.stateId,this.availableCityName);
}

class CitiesDropDownChangeEvent extends CitiesEvent {
  List<AddressModel> citiesList;
  int selected_index;

  CitiesDropDownChangeEvent(this.citiesList, this.selected_index);
}