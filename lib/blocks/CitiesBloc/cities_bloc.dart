import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../model/addressModel/addressModel.dart';
import '../../restService/apis_services.dart';
import '../../utils/sharePrefs.dart';

part 'cities_event.dart';
part 'cities_state.dart';

class CitiesBloc extends Bloc<CitiesEvent, CitiesState> {
  CitiesBloc() : super(CitiesInitial()) {
    on<CitiesEvent>((event, emit) {
    });
    on<CitiesDropDownLoadingEvent>((event, emit) async {
     emit(CitiesLoadingState());
      try {
        var response = await ApisServices.getCitiesList(
            token: SharedPrefs.getAccessToken(),
            stateId:  event.stateId);

        if (response != null) {
          List<AddressModel> citiesList = response;

          var index = citiesList.indexWhere(
                  (element) => element.name!.toLowerCase() == event.availableCityName?.toLowerCase());

          emit(CitiesDropDownLoadedState(citiesList, index));
        } else {
          emit(CitiesDropDownErrorState(event.stateId,event.availableCityName));
        }
      } catch (error) {
        emit(CitiesDropDownErrorState(event.stateId,event.availableCityName));
      }
    });
    on<CitiesDropDownChangeEvent>((event, emit) {
      emit(CitiesDropDownLoadedState(
          event.citiesList, event.selected_index));
    });

  }
}
