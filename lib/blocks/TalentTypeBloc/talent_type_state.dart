part of 'talent_type_bloc.dart';

@immutable
 class TalentTypeState {
  bool talentType;

  TalentTypeState(this.talentType);
}

