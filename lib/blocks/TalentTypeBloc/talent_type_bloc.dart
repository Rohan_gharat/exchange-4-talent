import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'talent_type_event.dart';
part 'talent_type_state.dart';

class TalentTypeBloc extends Bloc<TalentTypeEvent, TalentTypeState> {
  TalentTypeBloc() : super(TalentTypeState(true)) {
    on<TalentTypeEvent>((event, emit) {
      emit(TalentTypeState(event.talentTYpe));
    });
  }
}
