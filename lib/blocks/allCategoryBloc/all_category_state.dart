part of 'all_category_bloc.dart';

@immutable
abstract class AllCategoryState {}

class AllCategoryInitial extends AllCategoryState {}


class AllCategoriesLoadingState extends AllCategoryState {}

class AllCategoriesLoadedState extends AllCategoryState {
  final List<CategoriesModel> categoriesList;

  AllCategoriesLoadedState(this.categoriesList);
}

class AllCategoriesErrorState extends AllCategoryState {}
