import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:meta/meta.dart';

import '../../restService/apis_services.dart';
import '../../utils/sharePrefs.dart';

part 'all_category_event.dart';
part 'all_category_state.dart';

class AllCategoryBloc extends Bloc<AllCategoryEvent, AllCategoryState> {
  AllCategoryBloc() : super(AllCategoryInitial()) {
    on<AllCategoryEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<AllCategoriesLoadingEvent>((event, emit) async {
      emit(AllCategoriesLoadingState());
      try {
        var response = await ApisServices.getCategories(
            token: SharedPrefs.getAccessToken());

        if (response != null) {
          emit(AllCategoriesLoadedState(response));
        } else {
          emit(AllCategoriesErrorState());
        }
      } catch (error) {
        print("CategoriesLoadingEvent error: $error");
        emit(AllCategoriesErrorState());
      }
    });
  }
}
