part of 'recruiters_post_details_bloc.dart';

@immutable
abstract class RecruitersPostDetailsEvent {}

class RecruitersPostByIdEvent extends RecruitersPostDetailsEvent {
  int postId;

  RecruitersPostByIdEvent(this.postId);
}
