import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/recruitersModel/recruitersPostModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'recruiters_post_details_event.dart';

part 'recruiters_post_details_state.dart';

class RecruitersPostDetailsBloc
    extends Bloc<RecruitersPostDetailsEvent, RecruitersPostDetailsState> {
  RecruitersPostDetailsBloc() : super(RecruitersPostDetailsInitial()) {
    on<RecruitersPostDetailsEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<RecruitersPostByIdEvent>((event, emit) async {
      emit(RecruitersPostDetailsLoadingState());
      var response = await ApisServices.getRecruitersPostById(
          post_id: event.postId, token: SharedPrefs.getAccessToken());
      if (response != null) {
        if (response.status == 200 && response.recruitersPostModel != null) {
          emit(RecruitersPostDetailsLoadedState(response.recruitersPostModel!));
        } else {
          emit(RecruitersPostDetailsErrorState(
              response.message ?? HelperClass.ERROR_TEXT));
        }
      } else {
        emit(RecruitersPostDetailsErrorState(HelperClass.ERROR_TEXT));
      }
    });
  }
}
