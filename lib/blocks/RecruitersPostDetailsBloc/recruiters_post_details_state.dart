part of 'recruiters_post_details_bloc.dart';

@immutable
abstract class RecruitersPostDetailsState {}

class RecruitersPostDetailsInitial extends RecruitersPostDetailsState {}

class RecruitersPostDetailsLoadingState extends RecruitersPostDetailsState {}

class RecruitersPostDetailsLoadedState extends RecruitersPostDetailsState {
  RecruitersPostModel recruitersPostModel;

  RecruitersPostDetailsLoadedState(this.recruitersPostModel);
}

class RecruitersPostDetailsErrorState extends RecruitersPostDetailsState {
  String message;

  RecruitersPostDetailsErrorState(this.message);
}
