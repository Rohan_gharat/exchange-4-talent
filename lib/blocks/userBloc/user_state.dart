part of 'user_bloc.dart';

@immutable
abstract class UserState {}

class UserInitial extends UserState {}

class MyUserLoadedState extends UserState {
  UserDataModel userDataModel;

  MyUserLoadedState(this.userDataModel);
}

class MyUserErrorState extends UserState {
  String errorMsg;

  MyUserErrorState(this.errorMsg);
}

class MyUserLoadingState extends UserState {}
