import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:e4t/model/userModel/user_data_model.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserModel? myUserData;

  UserBloc() : super(UserInitial()) {
    on<UserEvent>((event, emit) {});

    on<MyUserLoadingEvent>((event, emit) async {
      emit(MyUserLoadingState());
      try {
        var response = await ApisServices.getUserDataById(
            userId: event.userId, token: SharedPrefs.getAccessToken()!);
        if (response != null) {
          if (response.status == 200) {
            myUserData = response.userModel;

            emit(MyUserLoadedState(response));
          } else {
            emit(MyUserErrorState(response.message ?? HelperClass.ERROR_TEXT));
          }
        } else {
          emit(MyUserErrorState(HelperClass.ERROR_TEXT));
        }
      } catch (error) {
        emit(MyUserErrorState(HelperClass.ERROR_TEXT));
      }
    });
  }
}
