part of 'user_bloc.dart';

@immutable
abstract class UserEvent {}

class MyUserLoadingEvent extends UserEvent {
  final userId;

  MyUserLoadingEvent(this.userId);
}
