import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'categories_new_event.dart';

part 'categories_new_state.dart';

class CategoriesNewBloc extends Bloc<CategoriesNewEvent, CategoriesNewState> {
  CategoriesNewBloc() : super(CategoriesNewInitial()) {
    on<CategoriesNewEvent>((event, emit) {
      // TODO: implement event handler
    });
    on<CategoriesNewLoadingEvent>((event, emit) async {
      try {
        var response = await ApisServices.getCategories(
            token: SharedPrefs.getAccessToken());

        if (response != null) {
          List<CategoriesModel> categoriesList = response;
          List<CategoriesModel> selectedCategoriesList = [];

          if (event.categoryTitle != null && event.categoryTitle != "") {
            List<String> categoriesTitleList = event.categoryTitle!.split(",");

            for (var catTitle in categoriesTitleList) {
              for (var category in categoriesList) {
                if (category.title.toString().trim() == catTitle.trim()) {
                  category.isSelected = true;
                  selectedCategoriesList.add(category);
                  break;
                }
              }
            }
          }

          emit(
              CategoriesNewLoadedState(categoriesList, selectedCategoriesList));
        } else {
          emit(CategoriesNewErrorState(event.categoryTitle));
        }
      } catch (error) {
        emit(CategoriesNewErrorState(event.categoryTitle));
      }
    });

    on<CategoriesNewChangeEvent>((event, emit) {
      emit(CategoriesNewLoadedState(
          event.categoriesList, event.selectedCategoriesList));
    });
  }
}
