part of 'categories_new_bloc.dart';

@immutable
abstract class CategoriesNewState {}

class CategoriesNewInitial extends CategoriesNewState {}

class CategoriesNewLoadingState extends CategoriesNewState {}

class CategoriesNewLoadedState extends CategoriesNewState {
  List<CategoriesModel> categoriesList;
  List<CategoriesModel> selectedCategoriesList;

  CategoriesNewLoadedState(this.categoriesList, this.selectedCategoriesList);
}

class CategoriesNewErrorState extends CategoriesNewState {
  String? categoryTitle;

  CategoriesNewErrorState(this.categoryTitle);
}
