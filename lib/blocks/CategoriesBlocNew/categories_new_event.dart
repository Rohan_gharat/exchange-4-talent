part of 'categories_new_bloc.dart';

@immutable
abstract class CategoriesNewEvent {}

class CategoriesNewLoadingEvent extends CategoriesNewEvent {
  String? categoryTitle;

  CategoriesNewLoadingEvent(this.categoryTitle);
}

class CategoriesNewChangeEvent extends CategoriesNewEvent {
  List<CategoriesModel> categoriesList;
  List<CategoriesModel> selectedCategoriesList;

  CategoriesNewChangeEvent(this.categoriesList, this.selectedCategoriesList);
}
