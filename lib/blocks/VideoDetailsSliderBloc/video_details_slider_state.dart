part of 'video_details_slider_bloc.dart';

@immutable
 class VideoDetailsSliderState {
  int currentIndex;
  bool isLastPlayed;

  VideoDetailsSliderState(this.currentIndex, this.isLastPlayed);
}

