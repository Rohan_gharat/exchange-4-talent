part of 'video_details_slider_bloc.dart';

@immutable
 class VideoDetailsSliderEvent {
  int currentIndex;
  bool isLastPlayed;

  VideoDetailsSliderEvent(this.currentIndex, this.isLastPlayed);
}
