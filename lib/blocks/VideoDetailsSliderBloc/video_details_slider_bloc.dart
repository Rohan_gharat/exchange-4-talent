import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'video_details_slider_event.dart';
part 'video_details_slider_state.dart';

class VideoDetailsSliderBloc extends Bloc<VideoDetailsSliderEvent, VideoDetailsSliderState> {
  VideoDetailsSliderBloc() : super(VideoDetailsSliderState(0,false)) {
    on<VideoDetailsSliderEvent>((event, emit) {
      emit(VideoDetailsSliderState(event.currentIndex, event.isLastPlayed));
    });
  }
}
