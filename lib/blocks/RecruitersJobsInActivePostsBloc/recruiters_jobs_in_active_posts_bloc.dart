import 'package:e4t/model/recruitersModel/recruitersPostModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'recruiters_jobs_in_active_posts_event.dart';

part 'recruiters_jobs_in_active_posts_state.dart';

class RecruitersJobsInActivePostsBloc extends Bloc<
    RecruitersJobsInActivePostsEvents, RecruitersJobsInActivePostsState> {
  List<RecruitersPostModel> recruitersJobsInActivePostsListAll = [];
  String? url = "";
  bool isLoading = false;

  RecruitersJobsInActivePostsBloc() : super(RecruitersJobsInActivePostsInitialState()) {

    on<RecruitersJobsInActivePostsLoadingEvent>((event, emit) async {
      isLoading = true;
      if (recruitersJobsInActivePostsListAll.isEmpty) {
        emit(RecruitersJobsInActivePostsLoadingState());
      }
      try {
        var response = await ApisServices.getRecruitersJobPosts(
          recruitersId: event.recruiterId,
          token: SharedPrefs.getAccessToken(),
          url: url,
        );

        if (response != null) {
          if (response.status == 200 &&
              response.recruitersPostDataModel != null) {
            url = response.recruitersPostDataModel!.next_page_url;

            recruitersJobsInActivePostsListAll
                .addAll(response.recruitersPostDataModel!.recruitersPostsList);

            emit(RecruitersJobsInActivePostsLoadedState(
                recruitersJobsInActivePostsListAll,
                response.recruitersPostDataModel!.next_page_url == null
                    ? false
                    : true));
          } else {
            if (recruitersJobsInActivePostsListAll.isEmpty) {
              emit(RecruitersJobsInActivePostsErrorState(
                  response.message ?? HelperClass.ERROR_TEXT));
            }
          }
        } else {
          if (recruitersJobsInActivePostsListAll.isEmpty) {
            emit(RecruitersJobsInActivePostsErrorState(HelperClass.ERROR_TEXT));
          }
        }
        isLoading = false;
      } catch (error) {
        isLoading = false;
        if (recruitersJobsInActivePostsListAll.isEmpty) {
          emit(RecruitersJobsInActivePostsErrorState(HelperClass.ERROR_TEXT));
        }
      }
    });
  }
}
