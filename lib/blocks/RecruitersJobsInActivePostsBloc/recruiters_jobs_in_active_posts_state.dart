part of "recruiters_jobs_in_active_posts_bloc.dart";

abstract class RecruitersJobsInActivePostsState {}

class RecruitersJobsInActivePostsInitialState
    extends RecruitersJobsInActivePostsState {}

class RecruitersJobsInActivePostsLoadingState
    extends RecruitersJobsInActivePostsState {}

class RecruitersJobsInActivePostsLoadedState
    extends RecruitersJobsInActivePostsState {
  List<RecruitersPostModel> recruitersJobsInActivePostsList;
  bool hasMoreData;

  RecruitersJobsInActivePostsLoadedState(
      this.recruitersJobsInActivePostsList, this.hasMoreData);
}

class RecruitersJobsInActivePostsErrorState
    extends RecruitersJobsInActivePostsState {
  String message;

  RecruitersJobsInActivePostsErrorState(this.message);
}
