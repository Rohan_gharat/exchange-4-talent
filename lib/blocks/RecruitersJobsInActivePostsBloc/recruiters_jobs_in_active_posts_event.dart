part of "recruiters_jobs_in_active_posts_bloc.dart";

abstract class RecruitersJobsInActivePostsEvents {}

class RecruitersJobsInActivePostsLoadingEvent
    extends RecruitersJobsInActivePostsEvents {
  int recruiterId;

  RecruitersJobsInActivePostsLoadingEvent(this.recruiterId);
}
