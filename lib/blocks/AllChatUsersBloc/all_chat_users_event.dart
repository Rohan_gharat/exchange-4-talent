part of 'all_chat_users_bloc.dart';

@immutable
abstract class AllChatUsersEvent {}

class AllChatUsersLoadingEvent extends AllChatUsersEvent {
  String currentUserId;
  FirebaseFirestore firestore;

  AllChatUsersLoadingEvent(this.currentUserId, this.firestore);
}

class AllChatUsersLoadedEvent extends AllChatUsersEvent{
  List<FirebaseUserModel> allChatUsers;
  bool hasMoreData;
  AllChatUsersLoadedEvent(this. allChatUsers,this.hasMoreData);
}
