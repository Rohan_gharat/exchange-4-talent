import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/firebase/firebaseUserModel.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:meta/meta.dart';

part 'all_chat_users_event.dart';

part 'all_chat_users_state.dart';

class AllChatUsersBloc extends Bloc<AllChatUsersEvent, AllChatUsersState> {
  List<FirebaseUserModel> allChatUsers = [];
  StreamSubscription? chatsSubscription;
  DocumentSnapshot? lastDoc;

  AllChatUsersBloc() : super(AllChatUsersInitial()) {
    on<AllChatUsersEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<AllChatUsersLoadingEvent>((event, emit) {
      List<FirebaseUserModel> users = [];
      if (allChatUsers.isEmpty) {
        emit(AllChatUsersLoadingState());
      }
      chatsSubscription?.cancel();
      if (lastDoc == null) {
        chatsSubscription = FireBaseHelperClass.getFirstUsersFromFirebase(
          fireStore: event.firestore,
          currentUserUid: event.currentUserId,
        ).listen((event) {
          if (event.docs.isNotEmpty) {
            lastDoc = event.docs[event.docs.length - 1];
          }

          var hasMoreData = true;
          if (event.docs.length < 30) {
            hasMoreData = false;
          }

          users = event.docs.map((snapshot) {
            return FirebaseUserModel.fromMap(snapshot.data());
          }).toList();

          allChatUsers.addAll(users);

           add(AllChatUsersLoadedEvent(allChatUsers, hasMoreData));
        }, onError: (error) {
          if (allChatUsers.isEmpty) {
            emit(AllChatUsersErrorState());
          }
        });
      } else {
        chatsSubscription = FireBaseHelperClass.getNextUsersFromFirebase(
          fireStore: event.firestore,
          currentUserUid: event.currentUserId,
          lastDoc: lastDoc,
        ).listen((event) {
          if (event.docs.isNotEmpty) {
            lastDoc = event.docs[event.docs.length - 1];
          }
          var hasMoreData = true;
          if (event.docs.length < 30) {
            hasMoreData = false;
          }
          users = event.docs.map((snapshot) {
            return FirebaseUserModel.fromMap(snapshot.data());
          }).toList();

          allChatUsers.addAll(users);
          add(AllChatUsersLoadedEvent(allChatUsers, hasMoreData));
        }, onError: (error) {
          if (allChatUsers.isEmpty) {
            emit(AllChatUsersErrorState());
          }
        });
      }
    });

    on<AllChatUsersLoadedEvent>((event, emit) {
      emit(AllChatUsersLoadedState(event.allChatUsers, event.hasMoreData));
    });
  }

  @override
  Future<void> close() {
    // TODO: implement close
    chatsSubscription?.cancel();
    return super.close();
  }
}
