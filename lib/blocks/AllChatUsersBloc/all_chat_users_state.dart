part of 'all_chat_users_bloc.dart';

@immutable
abstract class AllChatUsersState {}

class AllChatUsersInitial extends AllChatUsersState {}

class AllChatUsersLoadingState extends AllChatUsersState {}

class AllChatUsersLoadedState extends AllChatUsersState {
List<FirebaseUserModel> allChatUsersList;
bool hasMoreData;

AllChatUsersLoadedState(this.allChatUsersList,this.hasMoreData);
}

class AllChatUsersErrorState extends AllChatUsersState {}
