part of 'choose_photo_bloc.dart';

@immutable
class ChoosePhotoState {
  File? image_File;

  ChoosePhotoState(this.image_File);
}
