import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'choose_photo_event.dart';
part 'choose_photo_state.dart';

class ChoosePhotoBloc extends Bloc<ChoosePhotoEvent, ChoosePhotoState> {
  ChoosePhotoBloc() : super(ChoosePhotoState(null)) {
    on<ChoosePhotoEvent>((event, emit) {
      emit(ChoosePhotoState(event.image_File));
    });
  }
}
