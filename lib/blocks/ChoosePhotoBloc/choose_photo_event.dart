part of 'choose_photo_bloc.dart';

@immutable
 class ChoosePhotoEvent {

  File? image_File;

  ChoosePhotoEvent(this.image_File);
}
