part of 'recruiters_jobs_bloc.dart';

@immutable
abstract class RecruitersJobsState {}

class RecruitersJobsInitial extends RecruitersJobsState {}

class RecruitersJobsLoadingState extends RecruitersJobsState {}

class RecruitersJobsLoadedState extends RecruitersJobsState {
  List<RecruitersModel> recruitersList;
  bool hasMoreData;

  RecruitersJobsLoadedState({
    required this.recruitersList,
    required this.hasMoreData,

  });
}

class RecruitersJobsErrorState extends RecruitersJobsState {
  String message;

  RecruitersJobsErrorState(this.message);
}
