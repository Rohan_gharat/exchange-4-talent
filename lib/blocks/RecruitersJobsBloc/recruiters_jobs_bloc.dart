import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/recruitersModel/recruitersModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'recruiters_jobs_event.dart';

part 'recruiters_jobs_state.dart';

class RecruitersJobsBloc
    extends Bloc<RecruitersJobsEvent, RecruitersJobsState> {
  List<RecruitersModel> recruitersPostsListAll = [];

  String? url = "";
  bool isLoading = false;

  RecruitersJobsBloc() : super(RecruitersJobsInitial()) {
    on<RecruitersJobsEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<RecruitersJobsLoadingEvent>((event, emit) async {
      isLoading = true;
      if (recruitersPostsListAll.isEmpty) {
        emit(RecruitersJobsLoadingState());
      }
      try {
        var response = await ApisServices.getRecruitersJobs(
          userId: SharedPrefs.getUserId(),
          token: SharedPrefs.getAccessToken(),
          url: url,
        );
        if (response != null) {
          if (response.status == 200 &&
              response.getRecruitersJobsDataModel != null) {
            url = response.getRecruitersJobsDataModel!.nextPageUrl;

            recruitersPostsListAll
                .addAll(response.getRecruitersJobsDataModel!.recruitersList!);

            emit(RecruitersJobsLoadedState(
              recruitersList: recruitersPostsListAll,
              hasMoreData:
                  response.getRecruitersJobsDataModel!.nextPageUrl == null
                      ? false
                      : true,
            ));
          } else {
            if (recruitersPostsListAll.isEmpty) {
              emit(RecruitersJobsErrorState(
                  response.message ?? HelperClass.ERROR_TEXT));
            }
          }
        } else {
          if (recruitersPostsListAll.isEmpty) {
            emit(RecruitersJobsErrorState(HelperClass.ERROR_TEXT));
          }
        }
        isLoading = false;
      } catch (error) {
        isLoading = false;
        if (recruitersPostsListAll.isEmpty) {
          emit(RecruitersJobsErrorState(HelperClass.ERROR_TEXT));
        }
      }
    });
  }
}
