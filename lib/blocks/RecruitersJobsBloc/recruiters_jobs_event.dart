part of 'recruiters_jobs_bloc.dart';

@immutable
abstract class RecruitersJobsEvent {}

class RecruitersJobsLoadingEvent extends RecruitersJobsEvent {}
