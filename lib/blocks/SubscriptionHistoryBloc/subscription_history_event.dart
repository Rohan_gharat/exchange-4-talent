part of 'subscription_history_bloc.dart';

@immutable
abstract class SubscriptionHistoryEvent {}

 class SubscriptionHistoryLoadingEvent extends SubscriptionHistoryEvent {}
