import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e4t/model/subscriptionsModel/subscriptionHistoryDataModel.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:meta/meta.dart';

part 'subscription_history_event.dart';

part 'subscription_history_state.dart';

class SubscriptionHistoryBloc
    extends Bloc<SubscriptionHistoryEvent, SubscriptionHistoryState> {
  SubscriptionHistoryBloc() : super(SubscriptionHistoryInitial()) {
    on<SubscriptionHistoryEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<SubscriptionHistoryLoadingEvent>((event, emit) async {
      emit(SubscriptionHistoryLoadingState());
      try {
        var response = await ApisServices.getSubscriptionHistory(
            token: SharedPrefs.getAccessToken());

        if (response != null) {
          if (response.status == 200) {
            emit(
                SubscriptionHistoryLoadedState(response.subscriptionHistoriesList));
          } else {
            emit(SubscriptionHistoryErrorState());
          }
        } else {
          emit(SubscriptionHistoryErrorState());
        }
      } catch (error) {
        emit(SubscriptionHistoryErrorState());
      }
    });
  }
}
