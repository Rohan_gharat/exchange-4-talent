part of 'subscription_history_bloc.dart';

@immutable
abstract class SubscriptionHistoryState {}

class SubscriptionHistoryInitial extends SubscriptionHistoryState {}

class SubscriptionHistoryLoadingState extends SubscriptionHistoryState {}

class SubscriptionHistoryLoadedState extends SubscriptionHistoryState {

  List<SubscriptionHistoryDataModel> subscriptionHistoriesList;

  SubscriptionHistoryLoadedState(this.subscriptionHistoriesList);
}

class SubscriptionHistoryErrorState extends SubscriptionHistoryState {}
