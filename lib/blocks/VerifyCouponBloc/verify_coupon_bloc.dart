import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'verify_coupon_event.dart';
part 'verify_coupon_state.dart';

class VerifyCouponBloc extends Bloc<VerifyCouponEvent, VerifyCouponState> {
  VerifyCouponBloc() : super(VerifyCouponState(0)) {
    on<VerifyCouponEvent>((event, emit) {
      emit(VerifyCouponState(event.price));
    });
  }
}
