part of 'verify_coupon_bloc.dart';

@immutable
 class VerifyCouponEvent {
  int price;

  VerifyCouponEvent(this.price);
}
