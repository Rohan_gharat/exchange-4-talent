import 'package:e4t/myColors/myColors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NoDataWidget extends StatelessWidget {
  NoDataWidget({Key? key, this.on_tap}) : super(key: key);

  void Function()? on_tap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: on_tap,
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            color: MyColors.BG_COLOR_TEXT_FILED,
            borderRadius: BorderRadius.circular(
              18.0,
            ),
          ),
          padding: EdgeInsets.all(
            16.0,
          ),
          child: Text(
            "No Data Found",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.ORANGE_COLOR,
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.none,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
