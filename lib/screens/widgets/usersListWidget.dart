import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';

class UsersListWidget extends StatelessWidget {
  UsersListWidget({super.key, required this.usersList});

  List<UserModel> usersList;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: usersList.length,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 15.0,
        mainAxisSpacing: 15.0,
        childAspectRatio: 0.7,
      ),
      itemBuilder: (ctx, index) {
        return GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
            Navigator.pushNamed(context, UserDetailsScreen.USER_DETAILS_ROUTE,
                arguments: {
                  UserDetailsScreen.USER_ID: usersList[index].id,
                  UserDetailsScreen.IS_ME: false,
                });
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                18.0,
              ),
              border: Border.all(
                width: 5.0,
                color: usersList[index].color_code != null &&
                        usersList[index].color_code.trim() != ""
                    ? HexColor.fromHex(usersList[index].color_code.trim())
                    : Colors.transparent,
              ),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: MyColors.BG_COLOR_TEXT_FILED,
                borderRadius: BorderRadius.circular(
                  18.0,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(
                      9.0,
                    ),
                    child: usersList[index].avatar != null &&
                            usersList[index].avatar != ""
                        ? CachedNetworkImage(
                            alignment: Alignment.topCenter,
                            imageUrl: usersList[index].avatar,
                            fit: BoxFit.cover,
                            height: 120.0,
                            width: MediaQuery.of(context).size.width,
                            placeholder: (context, url) => Image.asset(
                              HelperClass.APP_LOGO_PATH,
                              fit: BoxFit.cover,
                              height: 120.0,
                              width: MediaQuery.of(context).size.width,
                            ),
                            errorWidget: (context, url, error) => Image.asset(
                              HelperClass.APP_LOGO_PATH,
                              fit: BoxFit.cover,
                              height: 120.0,
                              width: MediaQuery.of(context).size.width,
                            ),
                          )
                        : Image.asset(
                            HelperClass.APP_LOGO_PATH,
                            fit: BoxFit.cover,
                            height: 120.0,
                            width: MediaQuery.of(context).size.width,
                          ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 8.0,
                      left: 4.0,
                      right: 4.0,
                    ),
                    child: Text(
                      usersList[index].name ?? "",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.GREY_COLOR1,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  RatingBar.builder(
                    initialRating:
                        double.parse(usersList[index].rating ?? "0.0"),
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemSize: 18,
                    unratedColor: MyColors.GREY_COLOR1,
                    itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: MyColors.ORANGE_COLOR,
                    ),
                    onRatingUpdate: (rating) {
                    },
                    ignoreGestures: true,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
