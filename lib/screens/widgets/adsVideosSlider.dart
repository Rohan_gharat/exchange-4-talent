import 'package:e4t/blocks/AdsVideosBannerBloc/ads_videos_banner_bloc.dart';
import 'package:e4t/blocks/VideoMuteBloc/video_mute_bloc.dart';
import 'package:e4t/model/AdsModel/adsModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:video_player/video_player.dart';

class AdsVideosSlider extends StatefulWidget {
  AdsVideosSlider({super.key, required this.adsDataList, this.on_tap});

  List<AdsDataModel> adsDataList;
  Function()? on_tap;

  @override
  State<AdsVideosSlider> createState() => _AdsVideosSliderState();
}

class _AdsVideosSliderState extends State<AdsVideosSlider> {
  int currentIndex = 0;

  PageController pageController = PageController(initialPage: 0);

  bool isMute = true;
  late var videoMuteBloc;
  var tag = "video player";
  late Orientation orientation;
  var adsVideosBannerBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    videoMuteBloc = BlocProvider.of<VideoMuteBloc>(context);
    videoMuteBloc.add(VideoMuteEvent(isMute));
    orientation = Orientation.portrait;

    adsVideosBannerBloc = BlocProvider.of<AdsVideosBannerBloc>(context);
    adsVideosBannerBloc.add(AdsVideosBannerEvent(0, false));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.8,
        child: BlocConsumer<AdsVideosBannerBloc, AdsVideosBannerState>(
          listener: (context, state) {
            if (state.currentIndex > widget.adsDataList.length - 1) {
              currentIndex = 0;
            } else {
              currentIndex = state.currentIndex;
            }
            pageController.animateToPage(
              currentIndex,
              duration: Duration(milliseconds: 300),
              curve: Curves.easeIn,
            );
          },
          builder: (context, state) {
            return GestureDetector(
              onTap: widget.on_tap,
              child: Stack(
                children: [
                  PageView.builder(
                    controller: pageController,
                    itemCount: widget.adsDataList.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      return Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: MyColors.ORANGE_COLOR,
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(
                            18.0,
                          ),
                        ),
                        child: VideoPlayerSlider(
                          url: widget.adsDataList[index].videos ?? "",
                          index: index,
                          isMute: isMute,
                          totalLength: widget.adsDataList.length - 1,
                          orientation: orientation,
                        ),
                      );
                    },
                    onPageChanged: (pageIndex) {
                      if (!state.isLastPlayed) {
                        adsVideosBannerBloc
                            .add(AdsVideosBannerEvent(pageIndex, false));
                      }
                    },
                  ),
                  Positioned(
                    left: 20.0,
                    top: 10.0,
                    child: GestureDetector(
                      onTap: () {
                        isMute = !isMute;
                        setState(() {});
                        videoMuteBloc.add(VideoMuteEvent(isMute));
                      },
                      child: Icon(
                        isMute
                            ? FontAwesomeIcons.volumeXmark
                            : FontAwesomeIcons.volumeHigh,
                        color: MyColors.ORANGE_COLOR,
                      ),
                    ),
                  ),
                  Positioned(
                    right: 10.0,
                    top: 10.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(
                        18.0,
                      ),
                      child: Image.asset(
                        HelperClass.APP_LOGO_PATH,
                        width: 50.0,
                        height: 50.0,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class VideoPlayerSlider extends StatefulWidget {
  VideoPlayerSlider(
      {super.key,
      required this.url,
      required this.index,
      required this.isMute,
      required this.totalLength,
      required this.orientation});

  String url;
  int index, totalLength;
  bool isMute;
  Orientation orientation;

  var tag = "video player";

  @override
  State<VideoPlayerSlider> createState() => _VideoPlayerSliderState();
}

class _VideoPlayerSliderState extends State<VideoPlayerSlider> {
  late VideoPlayerController videoPlayerController;
  late Future<void> _initializeVideoPlayerFuture;

  late var adsVideosBannerBloc;
  bool videoEndedFlag = false;

  @override
  void initState() {
    // TODO: implement initState
    adsVideosBannerBloc = BlocProvider.of<AdsVideosBannerBloc>(context);
    super.initState();
    videoPlayerController =
        VideoPlayerController.networkUrl(Uri.parse(widget.url));

    _initializeVideoPlayerFuture =
        videoPlayerController.initialize().then((value) {
      if (widget.isMute) {
        videoPlayerController.setVolume(0.0);
      } else {
        videoPlayerController.setVolume(1.0);
      }
      videoPlayerController.play();
      setState(() {});
    });

    videoPlayerController.addListener(() {
      if (!videoPlayerController.value.isPlaying &&
          videoPlayerController.value.isInitialized &&
          (videoPlayerController.value.duration ==
              videoPlayerController.value.position)) {
        if (videoEndedFlag) {
          bool isLastPlayed = false;
          if (widget.index >= widget.totalLength) {
            isLastPlayed = true;
          }
          adsVideosBannerBloc
              .add(AdsVideosBannerEvent(widget.index + 1, isLastPlayed));
        }
        videoEndedFlag = true;
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    videoPlayerController.pause();
    videoPlayerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        child: FutureBuilder(
          future: _initializeVideoPlayerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return BlocConsumer<VideoMuteBloc, VideoMuteState>(
                listener: (context, state) {
                  if (state.isMute) {
                    videoPlayerController.setVolume(0.0);
                  } else {
                    videoPlayerController.setVolume(1.0);
                  }
                },
                builder: (context, state) {
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(
                      18.0,
                    ),
                    child: MediaQuery.of(context).orientation ==
                            Orientation.landscape
                        ? VideoPlayer(
                            videoPlayerController,
                          )
                        : AspectRatio(
                            aspectRatio:
                                videoPlayerController.value.aspectRatio,
                            child: VideoPlayer(
                              videoPlayerController,
                            ),
                          ),
                  );
                },
              );
            }
            return Stack(
              alignment: Alignment.center,
              children: const [
                Center(
                  child: CircularIndicatorWidget(),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
