import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/SubscriptionsDrawerMenuBloc/subscriptions_drawer_menu_bloc.dart';
import 'package:e4t/blocks/mainNavigationBloc/main_navigation_bloc.dart';
import 'package:e4t/blocks/profilePhotoUpdateBloc/profile_photo_update_bloc.dart';
import 'package:e4t/model/subscriptionsModel/subscriptionsDrawerMenuModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/RecruitersJobs/RecruitersTabsScreen.dart';
import 'package:e4t/screens/TradeBarter/tradeBarterScreen.dart';
import 'package:e4t/screens/ads/adsScreen.dart';
import 'package:e4t/screens/category/all_categories_screen.dart';
import 'package:e4t/screens/category/all_users_screen.dart';
import 'package:e4t/screens/feedbackForm/feedbackForm.dart';
import 'package:e4t/screens/main_navigator_screen.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class DrawerMenu extends StatelessWidget {
  DrawerMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mainNavigationBloc = BlocProvider.of<MainNavigationBloc>(context);

    return Drawer(
      backgroundColor: MyColors.BG_COLOR,
      child: ListView(
        physics: const ScrollPhysics(),
        children: [
          BlocBuilder<ProfilePhotoUpdateBloc, ProfilePhotoUpdateState>(
            builder: (context, state) {
              return GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(
                      context, UserDetailsScreen.USER_DETAILS_ROUTE,
                      arguments: {
                        UserDetailsScreen.USER_ID: SharedPrefs.getUserId(),
                        UserDetailsScreen.IS_ME: true
                      });
                },
                child: DrawerHeader(
                  decoration: BoxDecoration(
                    color: MyColors.BG_COLOR_TEXT_FILED,
                  ), //BoxDecoration
                  child: UserAccountsDrawerHeader(
                    decoration: BoxDecoration(
                      color: MyColors.BG_COLOR_TEXT_FILED,
                    ),
                    accountName: Text(
                      state.name ?? "",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    accountEmail: Text(
                      state.mobile ?? "",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    currentAccountPictureSize: Size.square(50),
                    currentAccountPicture: ClipRRect(
                      borderRadius: BorderRadius.circular(
                        60.0,
                      ),
                      child: state.profilePhotoUrl != null &&
                              state.profilePhotoUrl != ""
                          ? CachedNetworkImage(
                              imageUrl: state.profilePhotoUrl ?? "",
                              fit: BoxFit.cover,
                              height: 50,
                              width: 50,
                              placeholder: (context, url) => Image.asset(
                                HelperClass.APP_LOGO_PATH,
                                fit: BoxFit.cover,
                                height: 50,
                                width: 50,
                              ),
                              errorWidget: (context, url, error) => Image.asset(
                                HelperClass.APP_LOGO_PATH,
                                fit: BoxFit.cover,
                                height: 50,
                                width: 50,
                              ),
                            )
                          : Image.asset(
                              HelperClass.APP_LOGO_PATH,
                              fit: BoxFit.cover,
                              height: 50,
                              width: 50,
                            ),
                    ), //circleAvatar
                  ), //UserAccountDrawerHeader
                ),
              );
            },
          ),
          ListTile(
            // leading: const Icon(Icons.person),
            title: Text(
              "Talent Introductions",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.WHITE_COLOR,
                  fontSize: 16.0,
                ),
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context, rootNavigator: true).pushNamed(
                AdsScreen.ADS_SCREEN_ROUTE,
                arguments: false,
              );
            },
          ),
          BlocBuilder<SubscriptionsDrawerMenuBloc,
                  SubscriptionsDrawerMenuState>(
              builder: (context, subscriptionMenuState) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: createSubscriptionsMenu(
                  subscriptionsDrawerMenuList:
                      subscriptionMenuState.subscriptionsDrawerMenuList,
                  context: context),
            );
          }),
          ListTile(
            title: Text(
              "All Categories",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.WHITE_COLOR,
                  fontSize: 16.0,
                ),
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(
                  context, AllCategoriesScreen.ALL_CATEGORIES_ROUTE);
            },
          ),
          ListTile(
            title: Row(
              children: [
                Text(
                  "Secret Services",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                GestureDetector(
                  onTap: () {
                    var alertDialog = AlertDialog(
                      backgroundColor: MyColors.BG_COLOR,
                      content: Container(
                        // height: 100.0,
                        child: Text(
                          HelperClass.SECRET_SERVICE_TEXT,
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                              color: MyColors.WHITE_COLOR,
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    );
                    showDialog(
                        context: context,
                        // barrierDismissible: false,
                        builder: (ctx) {
                          return alertDialog;
                        });
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(
                      8.0,
                    ),
                    child: Icon(
                      Icons.help,
                      color: MyColors.ORANGE_COLOR,
                      size: 16,
                    ),
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              mainNavigationBloc.add(
                  MainNavigationEvent(MainNavigationScreen.CHAT_INDEX, false));
            },
          ),
          ListTile(
            title: Row(
              children: [
                Text(
                  "Talent Barter",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                GestureDetector(
                  onTap: () {
                    var alertDialog = AlertDialog(
                      backgroundColor: MyColors.BG_COLOR,
                      content: Container(
                        // height: 100.0,
                        child: Text(
                          HelperClass.TALENT_BARTER_TEXT,
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                              color: MyColors.WHITE_COLOR,
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    );
                    showDialog(
                        context: context,
                        // barrierDismissible: false,
                        builder: (ctx) {
                          return alertDialog;
                        });
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(
                      8.0,
                    ),
                    child: Icon(
                      Icons.help,
                      color: MyColors.ORANGE_COLOR,
                      size: 16,
                    ),
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(
                  context, TradeBarterScreen.TRADE_BARTER_SCREEN_ROUTE);
            },
          ),
          ListTile(
            title: Text(
              "Feedback Form",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.WHITE_COLOR,
                  fontSize: 16.0,
                ),
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(
                  context, FeedbackFormScreen.FEEDBACK_FORM_SCREEN_ROUTE);
            },
          ),
          ListTile(
            title: Text(
              "Jobs",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.WHITE_COLOR,
                  fontSize: 16.0,
                ),
              ),
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(
                  context, RecruitersTabsScreen.RECRUITERS_TABS_ROUTE,
                  arguments: false);
            },
          ),
        ],
      ),
    );
  }

  void gotoSubscriptionsUsers(
      {required BuildContext context, required title, required id}) {
    Navigator.pushNamed(context, AllUsersScreen.All_USERS_ROUTE, arguments: {
      AllUsersScreen.IS_ALL_USERS_KEY: false,
      AllUsersScreen.TITLE_KEY: title,
      AllUsersScreen.ID_KEY: id,
    });
  }

  List<Widget> createSubscriptionsMenu({
    required List<SubscriptionsDrawerMenuModel> subscriptionsDrawerMenuList,
    required BuildContext context,
  }) {
    List<Widget> menuList = [];

    for (var subscription in subscriptionsDrawerMenuList) {
      menuList.add(
        ListTile(
          title: Text(
            subscription.title,
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 16.0,
              ),
            ),
          ),
          onTap: () {
            Navigator.pop(context);
            gotoSubscriptionsUsers(
                context: context,
                title: subscription.title,
                id: subscription.id);
          },
        ),
      );
    }

    return menuList;
  }
}
