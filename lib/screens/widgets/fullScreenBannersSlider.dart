import 'package:e4t/model/userModel/userBannerModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/fullBannerImageWidget.dart';
import 'package:e4t/screens/widgets/fullScreenBannerVideoPlayerPage.dart';
import 'package:e4t/screens/widgets/youtubeVideoPlayerPage.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class FullScreenBannerSlider extends StatefulWidget {
  FullScreenBannerSlider({
    super.key,
    required this.bannersVideosList,
    required this.currentIndex,
    required this.userName,
  });

  List<UserBannerModel> bannersVideosList;

  int currentIndex = 0;
  String userName;

  @override
  State<FullScreenBannerSlider> createState() => _FullScreenBannerSliderState();
}

class _FullScreenBannerSliderState extends State<FullScreenBannerSlider> {
  late PageController pageController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    pageController = PageController(initialPage: widget.currentIndex);

  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (MediaQuery.of(context).orientation == Orientation.landscape) {
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        } else {
          return true;
        }
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            widget.userName!,
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              if (MediaQuery.of(context).orientation == Orientation.landscape) {
                SystemChrome.setPreferredOrientations(
                    [DeviceOrientation.portraitUp]);
              } else {
                Navigator.pop(context);
              }
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              PageView.builder(
                controller: pageController,
                itemCount: widget.bannersVideosList.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  if (widget.bannersVideosList[index].type
                          .trim()
                          .toLowerCase() ==
                      HelperClass.IMAGE_TYPE) {
                    return FullBannerImageWidget(
                      imageUrl: widget.bannersVideosList[index].url,
                    );
                  } else {
                    if (widget.bannersVideosList[index].host != null &&
                        widget.bannersVideosList[index].url != null &&
                        widget.bannersVideosList[index].url!.trim() != "") {
                      if (widget.bannersVideosList[index].host
                              .toString()
                              .toLowerCase() ==
                          HelperClass.YOUTUBE_HOST) {
                        return YoutubeVideoPlayerScreen(
                            videoUrl: widget.bannersVideosList[index].url!);
                      } else {
                        return FullScreenBannerVideoPlayerScreen(
                            videoUrl: widget.bannersVideosList[index].url!);
                      }
                    } else {
                      return Container();
                    }
                  }
                },
                onPageChanged: (value) {
                  widget.currentIndex = value;
                  setState(() {});
                  if (MediaQuery.of(context).orientation ==
                      Orientation.landscape) {
                    if (widget.bannersVideosList[widget.currentIndex].type
                            .trim()
                            .toLowerCase() ==
                        HelperClass.IMAGE_TYPE) {
                      SystemChrome.setPreferredOrientations(
                          [DeviceOrientation.portraitUp]);
                    }
                  }
                },
              ),
              Positioned(
                bottom: 10.0,
                left: 0.0,
                right: 0.0,
                child: Center(
                  child: AnimatedSmoothIndicator(
                    activeIndex: widget.currentIndex,
                    effect: const ExpandingDotsEffect(
                      dotHeight: 10,
                      dotWidth: 10,
                      dotColor: Colors.white,
                      activeDotColor: MyColors.ORANGE_COLOR,
                      spacing: 5.0,
                    ),
                    count: widget.bannersVideosList.length,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
