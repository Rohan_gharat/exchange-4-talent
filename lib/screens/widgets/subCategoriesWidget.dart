import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/model/categoriesModel/subcategory_by_category_model.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../myColors/myColors.dart';
import '../category/all_users_screen.dart';

class SubCategoriesWidget extends StatelessWidget {
   SubCategoriesWidget({Key? key,required this.subcategoryByCategoryModel}) : super(key: key);
  SubcategoryByCategoryModel subcategoryByCategoryModel;

  @override
  Widget build(BuildContext context) {
    return  Container(
      margin: EdgeInsets.only(
        top: 16.0,
      ),
      child: GridView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: subcategoryByCategoryModel.subcategoriesList?.length,
        gridDelegate:
        SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 15.0,
          mainAxisSpacing: 15.0,
          childAspectRatio: 0.85,
        ),
        itemBuilder: (ctx, index) {
          return GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                  context, AllUsersScreen.All_USERS_ROUTE,arguments: {
                AllUsersScreen
                    .IS_ALL_USERS_KEY:
                false,
                AllUsersScreen.TITLE_KEY:
                subcategoryByCategoryModel
                    .subcategoriesList?[
                index]
                    .title ??
                    "",
                AllUsersScreen
                    .ID_KEY:
                subcategoryByCategoryModel
                    .subcategoriesList?[
                index]
                    .id
              });
            },
            child: Container(
              decoration: BoxDecoration(
                color: MyColors.BG_COLOR_TEXT_FILED,
                borderRadius: BorderRadius.circular(
                  18.0,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment:
                CrossAxisAlignment.center,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(
                      9.0,
                    ),
                    child: subcategoryByCategoryModel
                        .subcategoriesList?[
                    index]
                        .image !=
                        null &&
                        subcategoryByCategoryModel
                            .subcategoriesList?[
                        index]
                            .image !=
                            ""
                        ? CachedNetworkImage(
                      imageUrl:
                      subcategoryByCategoryModel
                          .subcategoriesList?[
                      index]
                          .image,
                      fit: BoxFit.cover,
                      height: 120.0,
                      width: MediaQuery.of(context)
                          .size
                          .width,
                      placeholder: (context, url) =>
                          Image.asset(
                             HelperClass.APP_LOGO_PATH,
                            fit: BoxFit.cover,
                            height: 120.0,
                            width: MediaQuery.of(context)
                                .size
                                .width,
                          ),
                      errorWidget:
                          (context, url, error) =>
                          Image.asset(
                             HelperClass.APP_LOGO_PATH,
                            fit: BoxFit.cover,
                            height: 120.0,
                            width: MediaQuery.of(context)
                                .size
                                .width,
                          ),
                    )
                        : Image.asset(
                       HelperClass.APP_LOGO_PATH,
                      fit: BoxFit.cover,
                      height: 120.0,
                      width: MediaQuery.of(context)
                          .size
                          .width,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(
                     8.0,
                    ),
                    child: Text(
                      subcategoryByCategoryModel
                          .subcategoriesList?[index]
                          .title ??
                          "",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.GREY_COLOR1,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
