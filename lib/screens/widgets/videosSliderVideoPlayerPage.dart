import 'package:e4t/blocks/VideoDetailsSliderBloc/video_details_slider_bloc.dart';
import 'package:e4t/model/homeModel/videosBannerModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:video_player/video_player.dart';

class VideosSliderVideoPlayerPage extends StatelessWidget {
  VideosSliderVideoPlayerPage(
      {super.key, required this.bannersVideosList, required this.currentIndex});

  List<VideosBannerModel> bannersVideosList;

  int currentIndex = 0;

  var tag = "video player";

  @override
  Widget build(BuildContext context) {
    PageController pageController = PageController(initialPage: currentIndex);
    var videoDetailsSliderBloc =
        BlocProvider.of<VideoDetailsSliderBloc>(context);
    videoDetailsSliderBloc.add(VideoDetailsSliderEvent(currentIndex, false));
    return WillPopScope(
      onWillPop: () async {
        if (MediaQuery.of(context).orientation == Orientation.landscape) {
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        } else {
          return true;
        }

        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              if (MediaQuery.of(context).orientation == Orientation.landscape) {
                SystemChrome.setPreferredOrientations(
                    [DeviceOrientation.portraitUp]);
              } else {
                Navigator.pop(context);
              }
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
        ),
        body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 200,
            child:
                BlocConsumer<VideoDetailsSliderBloc, VideoDetailsSliderState>(
              listener: (context, state) {
                if (state.currentIndex > bannersVideosList.length - 1) {
                  currentIndex = 0;
                } else {
                  currentIndex = state.currentIndex;
                }
                pageController.animateToPage(
                  currentIndex,
                  duration: Duration(milliseconds: 300),
                  curve: Curves.easeIn,
                );
              },
              builder: (context, state) {
                return Stack(
                  children: [
                    PageView.builder(
                      controller: pageController,
                      itemCount: bannersVideosList.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: MyColors.ORANGE_COLOR,
                              width: 2.0,
                            ),
                            borderRadius: BorderRadius.circular(
                              18.0,
                            ),
                          ),
                          child: AllVideoPlayerSlider(
                            url: bannersVideosList[index].video ?? "",
                            index: index,
                            totalLength: bannersVideosList.length - 1,
                          ),
                        );
                      },
                      onPageChanged: (pageIndex) {
                        if (!state.isLastPlayed) {
                          videoDetailsSliderBloc
                              .add(VideoDetailsSliderEvent(pageIndex, false));
                        }
                      },
                    ),
                    Positioned(
                      bottom: 10.0,
                      left: 0.0,
                      right: 0.0,
                      child: Center(
                        child: AnimatedSmoothIndicator(
                          activeIndex: currentIndex,
                          effect: ExpandingDotsEffect(
                            dotHeight: 10,
                            dotWidth: 10,
                            dotColor: Colors.white,
                            activeDotColor: MyColors.ORANGE_COLOR,
                            spacing: 5.0,
                          ),
                          count: bannersVideosList.length,
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 10.0,
                      right: 0.0,
                      child: IconButton(
                        onPressed: () {
                          if (MediaQuery.of(context).orientation ==
                              Orientation.portrait) {
                            SystemChrome.setPreferredOrientations(
                                [DeviceOrientation.landscapeLeft]);
                          } else {
                            SystemChrome.setPreferredOrientations(
                                [DeviceOrientation.portraitUp]);
                          }
                        },
                        icon: Icon(
                          MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? Icons.fullscreen
                              : Icons.fullscreen_exit,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class AllVideoPlayerSlider extends StatefulWidget {
  AllVideoPlayerSlider({
    super.key,
    required this.url,
    required this.index,
    required this.totalLength,
  });

  String url;
  int index, totalLength;

  var tag = "video player";

  @override
  State<AllVideoPlayerSlider> createState() => _AllVideoPlayerSliderState();
}

class _AllVideoPlayerSliderState extends State<AllVideoPlayerSlider> {
  late VideoPlayerController videoPlayerController;
  late Future<void> _initializeVideoPlayerFuture;

  late var videoDetailsSliderBloc;
  bool videoEndedFlag = false;

  @override
  void initState() {
    // TODO: implement initState
    videoDetailsSliderBloc = BlocProvider.of<VideoDetailsSliderBloc>(context);
    super.initState();
    videoPlayerController =
        VideoPlayerController.networkUrl(Uri.parse(widget.url));

    _initializeVideoPlayerFuture =
        videoPlayerController.initialize().then((value) {
      videoPlayerController.play();
      setState(() {});
    });

    videoPlayerController.addListener(() {
      if (!videoPlayerController.value.isPlaying &&
          videoPlayerController.value.isInitialized &&
          (videoPlayerController.value.duration ==
              videoPlayerController.value.position)) {
        if (videoEndedFlag) {
          bool isLastPlayed = false;
          if (widget.index >= widget.totalLength) {
            isLastPlayed = true;
          }
          videoDetailsSliderBloc
              .add(VideoDetailsSliderEvent(widget.index + 1, isLastPlayed));
        }
        videoEndedFlag = true;
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    videoPlayerController.pause();
    videoPlayerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        height: 200,
        child: FutureBuilder(
          future: _initializeVideoPlayerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return ClipRRect(
                borderRadius: BorderRadius.circular(
                  18.0,
                ),
                child: MediaQuery.of(context).orientation ==
                        Orientation.landscape
                    ? VideoPlayer(
                        videoPlayerController,
                      )
                    : AspectRatio(
                        aspectRatio: videoPlayerController.value.aspectRatio,
                        child: VideoPlayer(
                          videoPlayerController,
                        ),
                      ),
              );
            }
            return Stack(
              alignment: Alignment.center,
              children: [
                const Center(
                  child: CircularIndicatorWidget(),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
