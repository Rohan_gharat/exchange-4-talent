import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:e4t/model/userModel/userBannerModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/fullScreenBannersSlider.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class ImageSliders extends StatefulWidget {
  ImageSliders(
      {super.key,
      required this.userBannersList,
      required this.userName,
      required this.color_code});

  List<UserBannerModel>? userBannersList;
  String userName;
  var color_code;

  @override
  State<ImageSliders> createState() => _ImageSlidersState();
}

class _ImageSlidersState extends State<ImageSliders> {
  var currentIndex = 0;
  double SLIDER_HEIGHT = 350;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider.builder(
          itemCount: widget.userBannersList!.length,
          itemBuilder: (context, index, realIndex) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context, rootNavigator: true).push(
                  MaterialPageRoute(
                    builder: (ctx) {
                      return FullScreenBannerSlider(
                        bannersVideosList: widget.userBannersList!,
                        currentIndex: currentIndex,
                        userName: widget.userName,
                      );
                    },
                  ),
                );
              },
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: MyColors.BG_COLOR,
                      borderRadius: BorderRadius.circular(
                        18.0,
                      ),
                      border: Border.all(
                        color: widget.color_code,
                        width: 2.0,
                      ),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(
                        18.0,
                      ),
                      child: widget.userBannersList![index].thumbnail != null &&
                              widget.userBannersList![index].thumbnail!
                                      .trim() !=
                                  ""
                          ? CachedNetworkImage(
                              imageUrl:
                                  widget.userBannersList![index].thumbnail!,
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                              height: SLIDER_HEIGHT,
                              alignment: Alignment.topCenter,
                              placeholder: (context, url) => Image.asset(
                                HelperClass.APP_LOGO_PATH,
                                fit: BoxFit.cover,
                                width: MediaQuery.of(context).size.width,
                                height: SLIDER_HEIGHT,
                                alignment: Alignment.topCenter,
                              ),
                              errorWidget: (context, url, error) => Image.asset(
                                HelperClass.APP_LOGO_PATH,
                                fit: BoxFit.cover,
                                width: MediaQuery.of(context).size.width,
                                height: SLIDER_HEIGHT,
                                alignment: Alignment.topCenter,
                              ),
                            )
                          : Image.asset(
                              HelperClass.APP_LOGO_PATH,
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                              height: SLIDER_HEIGHT,
                              alignment: Alignment.topCenter,
                            ),
                    ),
                  ),
                  widget.userBannersList![index].type.trim().toLowerCase() ==
                          HelperClass.VIDEO_TYPE
                      ? Positioned(
                          child: Container(
                            child: Icon(
                              Icons.play_arrow,
                              color: MyColors.ORANGE_COLOR,
                              size: 30,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                100.0,
                              ),
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            padding: EdgeInsets.all(
                              8.0,
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            );
          },
          options: CarouselOptions(
            height: SLIDER_HEIGHT,
            autoPlay: true,
            aspectRatio: 2.0,
            disableCenter: true,
            viewportFraction: 1,
            enlargeCenterPage: false,
            onPageChanged: (i, reason) {
              currentIndex = i;
              setState(() {});
            },
          ),
        ),
        const SizedBox(
          height: 10.0,
        ),
        Center(
          child: dotIndicator(),
        ),
      ],
    );
  }

  Widget dotIndicator() => AnimatedSmoothIndicator(
        activeIndex: currentIndex,
        count: widget.userBannersList!.length,
        effect: ExpandingDotsEffect(
          dotHeight: 10,
          dotWidth: 10,
          dotColor: Colors.white,
          activeDotColor: MyColors.ORANGE_COLOR,
          spacing: 5.0,
        ),
      );
}
