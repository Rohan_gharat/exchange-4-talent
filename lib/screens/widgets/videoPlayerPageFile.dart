import 'dart:async';
import 'dart:io';

import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerFileScreen extends StatefulWidget {
  VideoPlayerFileScreen({super.key, required this.videoFile});

  File videoFile;

  @override
  State<VideoPlayerFileScreen> createState() => _VideoPlayerFileScreenState();
}

class _VideoPlayerFileScreenState extends State<VideoPlayerFileScreen> {
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;
  int currentDurationInSecond = 0;

  @override
  void initState() {
    super.initState();

    _controller = VideoPlayerController.file(widget.videoFile);

    _initializeVideoPlayerFuture = _controller.initialize().then((value) {
      if (_controller.value.duration.inSeconds > 60) {}

      _controller.play();
      setState(() {});
    });

    _controller.addListener(
      () => setState(
          () => currentDurationInSecond = _controller.value.position.inSeconds),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  getVideoPosition(int timeValue) {
    var duration = Duration(milliseconds: timeValue.round());
    return [duration.inMinutes, duration.inSeconds]
        .map((seg) => seg.remainder(60).toString().padLeft(2, '0'))
        .join(':');
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (MediaQuery.of(context).orientation == Orientation.landscape) {
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        } else {
          return true;
        }

        return false;
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
            elevation: 0,
            title: Text(
              "Video Preview",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.WHITE_COLOR,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            leading: IconButton(
              onPressed: () {
                if (MediaQuery.of(context).orientation ==
                    Orientation.landscape) {
                  SystemChrome.setPreferredOrientations(
                      [DeviceOrientation.portraitUp]);
                } else {
                  Navigator.pop(context);
                }
              },
              icon: Icon(
                Icons.arrow_back_ios_new,
                color: MyColors.ORANGE_COLOR,
                size: 16,
              ),
            ),
          ),
          body: FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Stack(
                  children: [
                    MediaQuery.of(context).orientation == Orientation.landscape
                        ? Column(
                            children: [
                              Expanded(
                                child: Stack(
                                  children: [
                                    VideoPlayer(_controller),
                                    Positioned(
                                      bottom: 10.0,
                                      left: 0.0,
                                      right: 0.0,
                                      child: Row(
                                        children: [
                                          IconButton(
                                            onPressed: () {
                                              setState(() {
                                                // If the video is playing, pause it.
                                                if (_controller
                                                    .value.isPlaying) {
                                                  _controller.pause();
                                                } else {
                                                  // If the video is paused, play it.
                                                  _controller.play();
                                                }
                                              });
                                            },
                                            icon: Icon(
                                              _controller.value.isPlaying
                                                  ? Icons.pause
                                                  : Icons.play_arrow,
                                              color: MyColors.ORANGE_COLOR,
                                            ),
                                          ),
                                          ValueListenableBuilder<
                                              VideoPlayerValue>(
                                            valueListenable: _controller,
                                            builder: (c, value, child) {
                                              return Text(
                                                getVideoPosition(_controller
                                                    .value
                                                    .position
                                                    .inMilliseconds),
                                                style: GoogleFonts.montserrat(
                                                  textStyle: TextStyle(
                                                    color: MyColors.WHITE_COLOR,
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                          Text(
                                            " / ${getVideoPosition(_controller.value.duration.inMilliseconds)}",
                                            style: GoogleFonts.montserrat(
                                              textStyle: TextStyle(
                                                color: MyColors.ORANGE_COLOR,
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Expanded(
                                            child: VideoProgressIndicator(
                                              _controller,
                                              allowScrubbing: true,
                                              colors: VideoProgressColors(
                                                backgroundColor:
                                                    MyColors.GREY_COLOR,
                                                bufferedColor:
                                                    MyColors.WHITE_COLOR,
                                                playedColor:
                                                    MyColors.ORANGE_COLOR,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              showUploadButton(),
                            ],
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Stack(
                                children: [
                                  AspectRatio(
                                    aspectRatio: _controller.value.aspectRatio,
                                    child: VideoPlayer(_controller),
                                  ),
                                  Positioned(
                                    bottom: 10.0,
                                    left: 0.0,
                                    right: 0.0,
                                    child: Row(
                                      children: [
                                        IconButton(
                                          onPressed: () {
                                            setState(() {
                                              // If the video is playing, pause it.
                                              if (_controller.value.isPlaying) {
                                                _controller.pause();
                                              } else {
                                                // If the video is paused, play it.
                                                _controller.play();
                                              }
                                            });
                                          },
                                          icon: Icon(
                                            _controller.value.isPlaying
                                                ? Icons.pause
                                                : Icons.play_arrow,
                                            color: MyColors.ORANGE_COLOR,
                                          ),
                                        ),
                                        ValueListenableBuilder<
                                            VideoPlayerValue>(
                                          valueListenable: _controller,
                                          builder: (c, value, child) {
                                            return Text(
                                              getVideoPosition(_controller.value
                                                  .position.inMilliseconds),
                                              style: GoogleFonts.montserrat(
                                                textStyle: TextStyle(
                                                  color: MyColors.WHITE_COLOR,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                        Text(
                                          " / ${getVideoPosition(_controller.value.duration.inMilliseconds)}",
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                              color: MyColors.ORANGE_COLOR,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                        Expanded(
                                          child: VideoProgressIndicator(
                                            _controller,
                                            allowScrubbing: true,
                                            colors: VideoProgressColors(
                                              backgroundColor:
                                                  MyColors.GREY_COLOR,
                                              bufferedColor:
                                                  MyColors.WHITE_COLOR,
                                              playedColor:
                                                  MyColors.ORANGE_COLOR,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              showUploadButton(),
                            ],
                          ),
                  ],
                );
              } else {
                // If the VideoPlayerController is still initializing, show a
                // loading spinner.
                return const Center(
                  child: CircularIndicatorWidget(),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  Widget showUploadButton() => ElevatedButton(
        style: ElevatedButton.styleFrom(
          minimumSize: Size.fromHeight(
            40,
          ),
          // fromHeight use double.infinity as width and 40 is the height
          backgroundColor: MyColors.ORANGE_COLOR,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18),
          ),
        ),
        onPressed: () async {
          HelperClass.show_alert_Dialog(context, "processing");
          ApisServices.uploadVideo(
                  token: SharedPrefs.getAccessToken(),
                  file_path: widget.videoFile.path)
              .then((value) {
            Navigator.pop(context);

            if (value != null) {
              Navigator.pop(context, value);
            } else {
              HelperClass.show_snackbar(context, HelperClass.ERROR_TEXT);
            }
          });
        },
        child: Text(
          "Upload video",
          style: GoogleFonts.workSans(
            textStyle: TextStyle(
              color: MyColors.WHITE_COLOR,
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
}
