import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class UsersImageSliders extends StatefulWidget {
  UsersImageSliders({super.key, required this.usersList});

  List<UserModel> usersList;

  @override
  State<UsersImageSliders> createState() => _UsersImageSlidersState();
}

class _UsersImageSlidersState extends State<UsersImageSliders> {
  var currentIndex = 0;
  double IMAGE_HEIGHT = 300;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider.builder(
          itemCount: widget.usersList.length,
          itemBuilder: (context, index, realIndex) {
            return GestureDetector(
              onTap: () {
                Navigator.pushNamed(
                    context, UserDetailsScreen.USER_DETAILS_ROUTE,
                    arguments: {
                      UserDetailsScreen.USER_ID: widget.usersList[index].id,
                      UserDetailsScreen.IS_ME: false,
                    });
              },
              child: Container(
                decoration: BoxDecoration(
                    color: MyColors.BG_COLOR,
                    borderRadius: BorderRadius.circular(
                      18.0,
                    ),
                    border:
                        Border.all(color: MyColors.ORANGE_COLOR, width: 2.0)),

                child: ClipRRect(
                  borderRadius: BorderRadius.circular(
                    18.0,
                  ),
                  child: widget.usersList[currentIndex].avatar != null &&
                          widget.usersList[currentIndex].avatar != ""
                      ? CachedNetworkImage(
                          imageUrl: widget.usersList[currentIndex].avatar,
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                          height: IMAGE_HEIGHT,
                          alignment: Alignment.topCenter,
                          placeholder: (context, url) => Image.asset(
                            HelperClass.APP_LOGO_PATH,
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width,
                            height: IMAGE_HEIGHT,
                            alignment: Alignment.topCenter,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                            HelperClass.APP_LOGO_PATH,
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width,
                            height: IMAGE_HEIGHT,
                            alignment: Alignment.topCenter,
                          ),
                        )
                      : Image.asset(
                          HelperClass.APP_LOGO_PATH,
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                          height: IMAGE_HEIGHT,
                          alignment: Alignment.topCenter,
                        ),
                ),
              ),
            );
          },
          options: CarouselOptions(
            height: IMAGE_HEIGHT,
            autoPlay: true,
            aspectRatio: 2.0,
            disableCenter: true,
            viewportFraction: 1,
            enlargeCenterPage: false,
            onPageChanged: (i, reason) {
              currentIndex = i;
              setState(() {});
            },
          ),
        ),
        const SizedBox(
          height: 10.0,
        ),
        Center(
          child: dotIndicator(),
        ),
      ],
    );
  }

  Widget dotIndicator() => AnimatedSmoothIndicator(
        activeIndex: currentIndex,
        count: widget.usersList.length,
        effect: ExpandingDotsEffect(
          dotHeight: 10,
          dotWidth: 10,
          dotColor: Colors.white,
          activeDotColor: MyColors.ORANGE_COLOR,
          spacing: 5.0,
        ),
      );
}
