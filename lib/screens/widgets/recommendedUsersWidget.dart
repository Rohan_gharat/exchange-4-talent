import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class RecommendedUsersWidget extends StatelessWidget {
  RecommendedUsersWidget({
    Key? key,
    required this.randomUsersList,
    required this.index,
    required this.margin,
  }) : super(key: key);
  var randomUsersList;
  var index;
  var margin;

  List<Color> colorsList = [
    MyColors.ORANGE_COLOR,
    MyColors.YELLOW_COLOR,
    MyColors.GREY_COLOR
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          UserDetailsScreen.USER_DETAILS_ROUTE,
          arguments: {
            UserDetailsScreen.USER_ID: randomUsersList[index].id,
            UserDetailsScreen.IS_ME: false,
          },
        );
      },
      child: Container(
        width: 150,
        margin: margin,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            18.0,
          ),
          border: Border.all(
            width: 5.0,
            color:
            randomUsersList[index].color_code != null &&
                randomUsersList[index].color_code.trim() != ""
                ? HexColor.fromHex(randomUsersList[index].color_code.trim())
                : Colors.transparent,
          ),
        ),
        child: Container(
          // padding: EdgeInsets.all(
          //   5.0,
          // ),
          decoration: BoxDecoration(
            color: MyColors.BG_COLOR_TEXT_FILED,
            borderRadius: BorderRadius.circular(
              18.0,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(
                  14.0,
                ),
                child: randomUsersList[index].avatar != null &&
                        randomUsersList[index].avatar != ""
                    ? CachedNetworkImage(
                        alignment: Alignment.topCenter,
                        imageUrl: randomUsersList?[index].avatar,
                        height: 96.0,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                        placeholder: (context, url) => Image.asset(
                          HelperClass.APP_LOGO_PATH,
                          height: 96.0,
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover,
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          HelperClass.APP_LOGO_PATH,
                          height: 96.0,
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover,
                        ),
                      )
                    : Image.asset(
                        HelperClass.APP_LOGO_PATH,
                        fit: BoxFit.cover,
                        height: 96.0,
                        width: MediaQuery.of(context).size.width,
                      ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 8.0,
                  left: 8.0,
                  right: 8.0,
                ),
                child: Text(
                  randomUsersList[index].name ?? "",
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.GREY_COLOR1,
                      fontSize: 12.0,
                      fontWeight: FontWeight.bold,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10.0,
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: 8.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.star,
                          color: MyColors.ORANGE_COLOR,
                          size: 16.0,
                        ),
                        Text(
                          "${randomUsersList[index].rating ?? ""}",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      "Hire",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.ORANGE_COLOR,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
