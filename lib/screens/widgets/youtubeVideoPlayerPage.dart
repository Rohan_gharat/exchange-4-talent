
import 'package:e4t/myColors/myColors.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class YoutubeVideoPlayerScreen extends StatefulWidget {
  YoutubeVideoPlayerScreen({super.key, required this.videoUrl});

  String videoUrl;

  @override
  State<YoutubeVideoPlayerScreen> createState() =>
      _YoutubeVideoPlayerScreenState();
}

class _YoutubeVideoPlayerScreenState extends State<YoutubeVideoPlayerScreen> {
  late YoutubePlayerController youtubePlayerController;

  @override
  void initState() {
    super.initState();
    youtubePlayerController = YoutubePlayerController(
      initialVideoId: YoutubePlayer.convertUrlToId(widget.videoUrl)!,
      flags: const YoutubePlayerFlags(
        hideControls: true,
        mute: false,
        autoPlay: false,
        disableDragSeek: true,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: false,
      ),
    );
  }


  @override
  void dispose() {
    youtubePlayerController.dispose();
    super.dispose();
  }

  getVideoPosition(int timeValue) {
    var duration = Duration(milliseconds: timeValue.round());
    return [duration.inMinutes, duration.inSeconds]
        .map((seg) => seg.remainder(60).toString().padLeft(2, '0'))
        .join(':');
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (MediaQuery.of(context).orientation == Orientation.landscape) {
          youtubePlayerController.toggleFullScreenMode();
        } else {
          return true;
        }

        return false;
      },
      child: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              Center(
                child: YoutubePlayer(
                  controller: youtubePlayerController,
                  showVideoProgressIndicator: true,
                  onReady: (){
                    youtubePlayerController.play();
                  },
                  progressColors: const ProgressBarColors(
                    backgroundColor: MyColors.GREY_COLOR,
                    bufferedColor: MyColors.WHITE_COLOR,
                    playedColor: MyColors.ORANGE_COLOR,
                    handleColor: MyColors.ORANGE_COLOR,
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
