import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FullImageWidget extends StatelessWidget {
  FullImageWidget({Key? key, required this.imageUrl, required this.userName})
      : super(key: key);

  String? imageUrl, userName;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            userName!,
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: InteractiveViewer(
            minScale: 1,
            maxScale: 7,
            child: AspectRatio(
              aspectRatio: 1,
              child: Center(
                child: imageUrl != null && imageUrl != ""
                    ? CachedNetworkImage(
                        imageUrl: imageUrl!,
                        placeholder: (context, url) => Image.asset(
                          HelperClass.APP_LOGO_PATH,
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          HelperClass.APP_LOGO_PATH,
                        ),
                      )
                    : Image.asset(
                        HelperClass.APP_LOGO_PATH,
                      ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
