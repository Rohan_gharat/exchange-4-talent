import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';

class FullBannerImageWidget extends StatelessWidget {
  FullBannerImageWidget({
    Key? key,
    required this.imageUrl,
  }) : super(key: key);

  String? imageUrl;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: InteractiveViewer(
            minScale: 1,
            maxScale: 7,
            child: AspectRatio(
              aspectRatio: 1,
              child: Center(
                child: imageUrl != null && imageUrl != ""
                    ? CachedNetworkImage(
                        imageUrl: imageUrl!,
                        placeholder: (context, url) => Image.asset(
                          HelperClass.APP_LOGO_PATH,
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          HelperClass.APP_LOGO_PATH,
                        ),
                      )
                    : Image.asset(
                        HelperClass.APP_LOGO_PATH,
                      ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
