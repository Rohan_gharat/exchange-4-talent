import 'package:e4t/myColors/myColors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomRadioButton extends StatelessWidget {
  CustomRadioButton({
    super.key,
    required this.text,
    required this.radio_button_index,
    required this.selected_index,
    required this.on_tap,
  });

  String text;
  int radio_button_index;
  int selected_index;
  Function() on_tap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(

      onTap: on_tap,
      child: Container(
        padding: EdgeInsets.all(
          10.0,
        ),
        decoration: BoxDecoration(
          color: (selected_index == radio_button_index)
              ? MyColors.WHITE_COLOR
              : Colors.transparent,
          borderRadius: BorderRadius.circular(
            14.0,
          ),
        ),
        alignment: Alignment.center,
        child: Text(
          text,
          style: GoogleFonts.montserrat(
            textStyle: TextStyle(
              color: (selected_index == radio_button_index)
                  ? MyColors.ORANGE_COLOR
                  : MyColors.WHITE_COLOR,
              fontSize: 12.0,
              fontWeight: FontWeight.bold,
            ),
          ),

        ),
      ),
    );
  }
}
