import 'package:e4t/blocks/HomeVideoBannerBloc/home_videos_banner_bloc.dart';
import 'package:e4t/model/homeModel/videosBannerModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/videosSliderVideoPlayerPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:video_player/video_player.dart';

class VideosSlider extends StatelessWidget {
  VideosSlider({super.key, required this.bannersVideosList});

  List<VideosBannerModel> bannersVideosList;

  int currentIndex = 0;
  PageController pageController = PageController(initialPage: 0);
  var tag = "video player";

  @override
  Widget build(BuildContext context) {
    var homeVideosBannerBloc = BlocProvider.of<HomeVideosBannerBloc>(context);
    homeVideosBannerBloc.add(HomeVideosBannerEvent(0, false));
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      child: BlocConsumer<HomeVideosBannerBloc, HomeVideosBannerState>(
        listener: (context, state) {
          if (state.currentIndex > bannersVideosList.length - 1) {
            currentIndex = 0;
          } else {
            currentIndex = state.currentIndex;
          }
          pageController.animateToPage(
            currentIndex,
            duration: Duration(milliseconds: 300),
            curve: Curves.easeIn,
          );
        },
        builder: (context, state) {
          return Stack(
            children: [
              PageView.builder(
                controller: pageController,
                itemCount: bannersVideosList.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: MyColors.ORANGE_COLOR,
                        width: 2.0,
                      ),
                      borderRadius: BorderRadius.circular(
                        18.0,
                      ),
                    ),
                    child: VideoPlayerSlider(
                      url: bannersVideosList[index].video ?? "",
                      index: index,
                      totalLength: bannersVideosList.length - 1,
                      on_tap: () {
                        Navigator.of(context, rootNavigator: true).push(
                          MaterialPageRoute(
                            builder: (ctx) {
                              return VideosSliderVideoPlayerPage(
                                bannersVideosList: bannersVideosList,
                                currentIndex:  currentIndex,
                              );
                            },
                          ),
                        );
                      },
                    ),
                  );
                },
                onPageChanged: (pageIndex) {
                  if (!state.isLastPlayed) {
                    homeVideosBannerBloc
                        .add(HomeVideosBannerEvent(pageIndex, false));
                  }

                },
              ),
              Positioned(
                bottom: 10.0,
                left: 0.0,
                right: 0.0,
                child: Center(
                  child: AnimatedSmoothIndicator(
                    activeIndex: currentIndex,
                    effect: ExpandingDotsEffect(
                      dotHeight: 10,
                      dotWidth: 10,
                      dotColor: Colors.white,
                      activeDotColor: MyColors.ORANGE_COLOR,
                      spacing: 5.0,
                    ),
                    count: bannersVideosList.length,
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

class VideoPlayerSlider extends StatefulWidget {
  VideoPlayerSlider(
      {super.key,
      required this.url,
      required this.index,
      required this.totalLength,
      this.on_tap});

  String url;
  int index, totalLength;
  Function()? on_tap;

  var tag = "video player";

  @override
  State<VideoPlayerSlider> createState() => _VideoPlayerSliderState();
}

class _VideoPlayerSliderState extends State<VideoPlayerSlider> {
  late VideoPlayerController videoPlayerController;
  late Future<void> _initializeVideoPlayerFuture;

  late var homeVideosBannerBloc;
  bool videoEndedFlag = false;

  @override
  void initState() {
    // TODO: implement initState
    homeVideosBannerBloc = BlocProvider.of<HomeVideosBannerBloc>(context);
    super.initState();
    videoPlayerController =
        VideoPlayerController.networkUrl(Uri.parse(widget.url));

    _initializeVideoPlayerFuture =
        videoPlayerController.initialize().then((value) {
      videoPlayerController.setVolume(0.0);
      videoPlayerController.play();
      setState(() {});
    });

    videoPlayerController.addListener(() {
      if (!videoPlayerController.value.isPlaying &&
          videoPlayerController.value.isInitialized &&
          (videoPlayerController.value.duration ==
              videoPlayerController.value.position)) {

        if (videoEndedFlag) {

          bool isLastPlayed = false;
          if (widget.index >= widget.totalLength) {
            isLastPlayed = true;
          }
          homeVideosBannerBloc
              .add(HomeVideosBannerEvent(widget.index + 1, isLastPlayed));
        }
        videoEndedFlag = true;
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    videoPlayerController.pause();
    videoPlayerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(

      onTap: widget.on_tap,
      child: Container(
        height: 200,
        child: FutureBuilder(
          future: _initializeVideoPlayerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return ClipRRect(
                borderRadius: BorderRadius.circular(
                  18.0,
                ),
                child: AspectRatio(
                  aspectRatio: videoPlayerController.value.aspectRatio,
                  child: VideoPlayer(
                    videoPlayerController,
                  ),
                ),
              );
            }
            return Stack(
              alignment: Alignment.center,
              children: [
                const Center(
                  child: CircularIndicatorWidget(),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
