import 'package:e4t/myColors/myColors.dart';
import 'package:flutter/material.dart';

class DrawerIconWidget extends StatelessWidget {
  DrawerIconWidget({Key? key, this.on_tap}) : super(key: key);
  Function()? on_tap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: on_tap,
      child: Padding(
        padding: const EdgeInsets.all(
          8.0,
        ),
        child: Icon(
          Icons.menu,
          color: MyColors.ORANGE_COLOR,
        ),
      ),
    );
  }
}
