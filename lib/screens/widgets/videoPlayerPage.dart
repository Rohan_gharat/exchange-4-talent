import 'dart:async';

import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerScreen extends StatefulWidget {
  VideoPlayerScreen({super.key, required this.videoUrl});

  String videoUrl;

  @override
  State<VideoPlayerScreen> createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;
  int currentDurationInSecond = 0;

  @override
  void initState() {
    super.initState();

    _controller = VideoPlayerController.networkUrl(
      Uri.parse(
        widget.videoUrl,
      ),
    );

    _initializeVideoPlayerFuture = _controller.initialize().then((value) {
      _controller.play();
      setState(() {});
    });

    _controller.addListener(
      () => setState(
          () => currentDurationInSecond = _controller.value.position.inSeconds),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  getVideoPosition(int timeValue) {
    var duration = Duration(milliseconds: timeValue.round());
    return [duration.inMinutes, duration.inSeconds]
        .map((seg) => seg.remainder(60).toString().padLeft(2, '0'))
        .join(':');
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (MediaQuery.of(context).orientation == Orientation.landscape) {
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        } else {
          return true;
        }

        return false;
      },
      child: SafeArea(
        child: Scaffold(
         body: FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
              return Stack(
                  children: [
                    MediaQuery.of(context).orientation == Orientation.landscape
                        ? Stack(
                          children: [
                            VideoPlayer(_controller),
                            Positioned(
                              bottom: 10.0,
                              left: 0.0,
                              right: 0.0,
                              child: Row(
                                children: [
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        // If the video is playing, pause it.
                                        if (_controller.value.isPlaying) {
                                          _controller.pause();
                                        } else {
                                          // If the video is paused, play it.
                                          _controller.play();
                                        }
                                      });
                                    },
                                    icon: Icon(
                                      _controller.value.isPlaying
                                          ? Icons.pause
                                          : Icons.play_arrow,
                                      color: MyColors.ORANGE_COLOR,
                                    ),
                                  ),
                                  ValueListenableBuilder<VideoPlayerValue>(
                                    valueListenable: _controller,
                                    builder: (c, value, child) {
                                      return Text(
                                        getVideoPosition(
                                            _controller.value.position.inMilliseconds),
                                        style: GoogleFonts.montserrat(
                                          textStyle: TextStyle(
                                            color: MyColors.WHITE_COLOR,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                  Text(
                                    " / ${getVideoPosition(_controller.value.duration.inMilliseconds)}",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.ORANGE_COLOR,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Expanded(
                                    child: VideoProgressIndicator(
                                      _controller,
                                      allowScrubbing: true,
                                      colors: VideoProgressColors(
                                        backgroundColor: MyColors.GREY_COLOR,
                                        bufferedColor: MyColors.WHITE_COLOR,
                                        playedColor: MyColors.ORANGE_COLOR,
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      if (MediaQuery.of(context).orientation ==
                                          Orientation.portrait) {
                                        SystemChrome.setPreferredOrientations(
                                            [DeviceOrientation.landscapeLeft]);
                                      } else {
                                        SystemChrome.setPreferredOrientations(
                                            [DeviceOrientation.portraitUp]);
                                      }
                                    },
                                    icon: Icon(
                                      MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                          ? Icons.fullscreen
                                          : Icons.fullscreen_exit,
                                      color: MyColors.ORANGE_COLOR,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )
                        : Column(mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Stack(
                              children: [
                                AspectRatio(
                                    aspectRatio: _controller.value.aspectRatio,
                                    child: VideoPlayer(_controller),
                                  ),
                                Positioned(
                                  bottom: 10.0,
                                  left: 0.0,
                                  right: 0.0,
                                  child: Row(
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          setState(() {
                                            // If the video is playing, pause it.
                                            if (_controller.value.isPlaying) {
                                              _controller.pause();
                                            } else {
                                              // If the video is paused, play it.
                                              _controller.play();
                                            }
                                          });
                                        },
                                        icon: Icon(
                                          _controller.value.isPlaying
                                              ? Icons.pause
                                              : Icons.play_arrow,
                                          color: MyColors.ORANGE_COLOR,
                                        ),
                                      ),
                                      ValueListenableBuilder<VideoPlayerValue>(
                                        valueListenable: _controller,
                                        builder: (c, value, child) {
                                          return Text(
                                            getVideoPosition(
                                                _controller.value.position.inMilliseconds),
                                            style: GoogleFonts.montserrat(
                                              textStyle: TextStyle(
                                                color: MyColors.WHITE_COLOR,
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                      Text(
                                        " / ${getVideoPosition(_controller.value.duration.inMilliseconds)}",
                                        style: GoogleFonts.montserrat(
                                          textStyle: TextStyle(
                                            color: MyColors.ORANGE_COLOR,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Expanded(
                                        child: VideoProgressIndicator(
                                          _controller,
                                          allowScrubbing: true,
                                          colors: VideoProgressColors(
                                            backgroundColor: MyColors.GREY_COLOR,
                                            bufferedColor: MyColors.WHITE_COLOR,
                                            playedColor: MyColors.ORANGE_COLOR,
                                          ),
                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          if (MediaQuery.of(context).orientation ==
                                              Orientation.portrait) {
                                            SystemChrome.setPreferredOrientations(
                                                [DeviceOrientation.landscapeLeft]);
                                          } else {
                                            SystemChrome.setPreferredOrientations(
                                                [DeviceOrientation.portraitUp]);
                                          }
                                        },
                                        icon: Icon(
                                          MediaQuery.of(context).orientation ==
                                              Orientation.portrait
                                              ? Icons.fullscreen
                                              : Icons.fullscreen_exit,
                                          color: MyColors.ORANGE_COLOR,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),

                          ],
                        ),

                    IconButton(
                      onPressed: () {
                        if (MediaQuery.of(context).orientation ==
                            Orientation.landscape) {
                          SystemChrome.setPreferredOrientations(
                              [DeviceOrientation.portraitUp]);
                        } else {
                          Navigator.pop(context);
                        }
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_new,
                        color: MyColors.GREY_COLOR,
                      ),
                    ),
                  ],
                );
              } else {

                return const Center(
                  child: CircularIndicatorWidget(),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
