import 'dart:async';

import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:video_player/video_player.dart';

class FullScreenBannerVideoPlayerScreen extends StatefulWidget {
  FullScreenBannerVideoPlayerScreen({super.key, required this.videoUrl});

  String videoUrl;

  @override
  State<FullScreenBannerVideoPlayerScreen> createState() =>
      _FullScreenBannerVideoPlayerScreenState();
}

class _FullScreenBannerVideoPlayerScreenState
    extends State<FullScreenBannerVideoPlayerScreen> {
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;
  int currentDurationInSecond = 0;
  bool isMute = true;

  @override
  void initState() {
    super.initState();

    _controller = VideoPlayerController.networkUrl(
      Uri.parse(
        widget.videoUrl,
      ),
    );

    _initializeVideoPlayerFuture = _controller.initialize().then((value) {
      _controller.play();
      setState(() {});
    });

    _controller.setLooping(true);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  getVideoPosition(int timeValue) {
    var duration = Duration(milliseconds: timeValue.round());
    return [duration.inMinutes, duration.inSeconds]
        .map((seg) => seg.remainder(60).toString().padLeft(2, '0'))
        .join(':');
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (MediaQuery.of(context).orientation == Orientation.landscape) {
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        } else {
          return true;
        }

        return false;
      },
      child: SafeArea(
        child: Scaffold(
          body: FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Stack(
                  children: [
                    MediaQuery.of(context).orientation == Orientation.landscape
                        ? Stack(
                            children: [
                              VideoPlayer(_controller),
                              Positioned(
                                right: 10.0,
                                child: IconButton(
                                  onPressed: () {
                                    if (isMute) {
                                      isMute = false;
                                      _controller.setVolume(0.0);
                                    } else {
                                      isMute = true;
                                      _controller.setVolume(1.0);
                                    }
                                    setState(() {});
                                  },
                                  icon: Icon(
                                    isMute
                                        ? FontAwesomeIcons.volumeHigh
                                        : FontAwesomeIcons.volumeXmark,
                                    color: MyColors.ORANGE_COLOR,
                                  ),
                                ),
                              ),
                            ],
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Stack(
                                children: [
                                  AspectRatio(
                                    aspectRatio: _controller.value.aspectRatio,
                                    child: VideoPlayer(_controller),
                                  ),
                                  Positioned(
                                    right: 10.0,
                                    child: IconButton(
                                      onPressed: () {
                                        if (isMute) {
                                          isMute = false;
                                          _controller.setVolume(0.0);
                                        } else {
                                          isMute = true;
                                          _controller.setVolume(1.0);
                                        }
                                        setState(() {});
                                      },
                                      icon: Icon(
                                        isMute
                                            ? FontAwesomeIcons.volumeHigh
                                            : FontAwesomeIcons.volumeXmark,
                                        color: MyColors.ORANGE_COLOR,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                  ],
                );
              } else {
                return const Center(
                  child: CircularIndicatorWidget(),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
