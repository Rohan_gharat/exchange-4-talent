import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/main_navigator_screen.dart';
import 'package:e4t/screens/profile/edit_profile_screen.dart';
import 'package:e4t/screens/subscriptions/checkout_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../blocks/subscriptionBloc/subscription_bloc.dart';

class AddSubscriptionsScreen extends StatefulWidget {
  AddSubscriptionsScreen(
      {super.key, required this.showSkip, required this.currentSubscriptionId});

  bool showSkip = false;
  int currentSubscriptionId = 0;

  static const ADD_SUBSCRIPTIONS_SCREEN = "addSubscriptionsScreen";
  static const SHOW_SKIP_KEY = "showSkipKey";
  static const CURRENT_SUBSCRIPTION_key = "currentSubscriptionKey";

  @override
  State<AddSubscriptionsScreen> createState() => _AddSubscriptionsScreenState();
}

class _AddSubscriptionsScreenState extends State<AddSubscriptionsScreen>
    with TickerProviderStateMixin {
  late TabController tabController;
  late SubscriptionBloc subscriptionsBloc;
  late UserBloc userBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(
        "AddSubscriptionsScreen: showSkip: ${widget.showSkip} :: token: ${SharedPrefs.getAccessToken()}");
    subscriptionsBloc = BlocProvider.of<SubscriptionBloc>(context);
    userBloc = BlocProvider.of(context);
    subscriptionsBloc.selectedSubscriptionIndex = 0;
    subscriptionsBloc.subscriptionsList = [];
    subscriptionsBloc
        .add(SubscriptionsLoadingEvent(widget.currentSubscriptionId));

    tabController = TabController(
      length: subscriptionsBloc.subscriptionsList.length,
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext rootContext) {
    return SafeArea(
      child: Scaffold(
        body: BlocConsumer<SubscriptionBloc, SubscriptionState>(
          listener: (context, state) {
            print("AddSubscriptionsScreen: state: $state");
            if (state is SubscriptionLoadedState) {
              tabController = TabController(
                length: subscriptionsBloc.subscriptionsList.length,
                vsync: this,
              );
            }
          },
          builder: (context, state) {
            if (state is SubscriptionLoadingState) {
              return const Center(
                child: CircularIndicatorWidget(),
              );
            } else if (state is SubscriptionErrorState) {
              return CustomErrorWidget(
                on_tap: () {
                  subscriptionsBloc.add(
                      SubscriptionsLoadingEvent(widget.currentSubscriptionId));
                },
              );
            } else if (state is SubscriptionLoadedState) {
              return Container(
                padding: EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                ),
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.05,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Hello ${userBloc.myUserData?.name ?? ""}",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.ORANGE_COLOR,
                          fontSize: 32.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: Text(
                        "Choose a Plan which suits you best",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Expanded(
                      child: DefaultTabController(
                        // initialIndex: 1,
                        length: subscriptionsBloc.subscriptionsList.length,
                        child: Column(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                // height: 30,
                                // width: 150,

                                decoration: BoxDecoration(
                                  color: MyColors.ORANGE_COLOR,
                                  borderRadius: BorderRadius.circular(
                                    14.0,
                                  ),
                                ),
                                padding: const EdgeInsets.all(
                                  4.0,
                                ),
                                width: MediaQuery.of(context).size.width,
                                child: TabBar(
                                  // isScrollable: true,
                                  controller: tabController,
                                  indicator: BoxDecoration(
                                    color: MyColors.WHITE_COLOR,
                                    borderRadius: BorderRadius.circular(
                                      14,
                                    ),
                                  ),
                                  labelColor: MyColors.ORANGE_COLOR,
                                  dividerColor: Colors.black,
                                  // ignore: prefer_const_literals_to_create_immutables
                                  unselectedLabelColor: MyColors.WHITE_COLOR,
                                  labelStyle: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.ORANGE_COLOR,
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  tabs: List.generate(
                                    subscriptionsBloc.subscriptionsList.length,
                                    (index) => Tab(
                                      text: subscriptionsBloc
                                          .subscriptionsList[index].title,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: TabBarView(
                                controller: tabController,
                                children: List.generate(
                                  subscriptionsBloc.subscriptionsList.length,
                                  (index) => SubscriptionDataWidget(
                                    selectedIndex: index,
                                    rootContext: rootContext,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: widget.showSkip
                          ? Container(
                              margin: EdgeInsets.symmetric(
                                vertical: 20.0,
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context, rootNavigator: true)
                                      .pushNamedAndRemoveUntil(
                                          MainNavigationScreen
                                              .MAIN_NAVIGATION_SCREEN_ROUTE,
                                          (route) => false);
                                },
                                child: Text(
                                  "Skip",
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.ORANGE_COLOR,
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class SubscriptionDataWidget extends StatelessWidget {
  SubscriptionDataWidget({
    super.key,
    required this.selectedIndex,
    required this.rootContext,
  });

  int selectedIndex;
  BuildContext rootContext;

  @override
  Widget build(BuildContext context) {
    var subscriptionBloc = BlocProvider.of<SubscriptionBloc>(context);
    var userBloc = BlocProvider.of<UserBloc>(context);
    return Container(
      decoration: BoxDecoration(
        color: MyColors.BG_COLOR_TEXT_FILED,
        borderRadius: BorderRadius.circular(
          18.0,
        ),
      ),
      padding: EdgeInsets.all(
        16.0,
      ),
      margin: EdgeInsets.only(
        top: 16.0,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  subscriptionBloc.subscriptionsList[selectedIndex].title,
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      color: MyColors.ORANGE_COLOR,
                      fontSize: 28.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              subscriptionBloc.subscriptionsList[selectedIndex].subscriptionData
                              .plan !=
                          null &&
                      subscriptionBloc.subscriptionsList[selectedIndex]
                              .subscriptionData.plan !=
                          ""
                  ? Container(
                      decoration: BoxDecoration(
                        color: MyColors.ORANGE_COLOR,
                        borderRadius: BorderRadius.circular(
                          18.0,
                        ),
                      ),
                      padding: EdgeInsets.all(
                        8.0,
                      ),
                      child: Text(
                        subscriptionBloc.subscriptionsList[selectedIndex]
                            .subscriptionData.plan!,
                        style: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Text(
            "For Real Talent",
            style: GoogleFonts.workSans(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 12.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "₹ ${subscriptionBloc.subscriptionsList[selectedIndex].subscriptionData.price}",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.ORANGE_COLOR,
                    fontSize: 32.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              Text(
                "/${subscriptionBloc.subscriptionsList[selectedIndex].subscriptionData.validity}",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.GREY_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Expanded(
            child: ListView.builder(
              // physics: const NeverScrollableScrollPhysics(),
              itemCount: subscriptionBloc.subscriptionsList[selectedIndex]
                  .subscriptionData.descriptionList!.length,
              shrinkWrap: true,
              itemBuilder: (ctx, index) {
                return Container(
                  padding: EdgeInsets.only(
                    top: 8.0,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        FontAwesomeIcons.circleCheck,
                        size: 16,
                        color: MyColors.ORANGE_COLOR,
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Expanded(
                        child: Text(
                          subscriptionBloc.subscriptionsList[selectedIndex]
                              .subscriptionData.descriptionList![index],
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR,
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 20.0,
            ),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: Size.fromHeight(
                  40,
                ),
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                subscriptionBloc.selectedSubscriptionIndex = selectedIndex;
                print(
                    "add subscription screen: user name: ${userBloc.myUserData?.name}");
                if (userBloc.myUserData?.name == null ||
                    userBloc.myUserData?.name == "") {
                  showIncompleteProfileWarningDialog(context);
                } else {
                  Navigator.pushNamed(context, CheckOutScreen.CheckoutRoute);
                }
              },
              child: Text(
                "Select Plan",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  static void showIncompleteProfileWarningDialog(BuildContext context) {
    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(
            18.0,
          ),
        ),
      ),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.25,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: MyColors.WHITE_COLOR,
                borderRadius: BorderRadius.circular(
                  50.0,
                ),
              ),
              padding: EdgeInsets.all(
                8.0,
              ),
              child: Icon(
                Icons.cancel,
                color: MyColors.ORANGE_COLOR,
                size: 24.0,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Oops!",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.ORANGE_COLOR,
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              "Your profile is incomplete please fill it to proceed further",
              style: GoogleFonts.workSans(
                textStyle: TextStyle(
                  color: MyColors.GREY_COLOR,
                  fontSize: 14.0,
                ),
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Cancel",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
                Navigator.pushNamed(
                    context, EditProfileScreen.EDIT_PROFILE_ROUT,
                    arguments: true);
              },
              child: Text(
                "Update profile",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );

    showDialog(
      context: context,
      builder: (ctx) {
        return alert;
      },
    );
  }
}
