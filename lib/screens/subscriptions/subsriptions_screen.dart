import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/SubscriptionHistoryBloc/subscription_history_bloc.dart';
import 'package:e4t/model/subscriptionsModel/subscriptionHistoryDataModel.dart';
import 'package:e4t/screens/subscriptions/add_subscription_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../myColors/myColors.dart';

class SubscriptionsScreen extends StatelessWidget {
  const SubscriptionsScreen({Key? key}) : super(key: key);

  static const SUBSCRIPTIONS_ROUTE = "subscriptionsRoute";

  @override
  Widget build(BuildContext context) {
    var subscriptionHistoryBloc =
        BlocProvider.of<SubscriptionHistoryBloc>(context);

    subscriptionHistoryBloc.add(SubscriptionHistoryLoadingEvent());

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            "Subscriptions",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
          actions: [
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(
                    context, AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN,
                    arguments: {
                      AddSubscriptionsScreen.SHOW_SKIP_KEY: false,
                      AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key: 0
                    });
              },
              child: Padding(
                padding: const EdgeInsets.only(
                  right: 12.0,
                ),
                child: Icon(
                  Icons.add,
                  color: MyColors.ORANGE_COLOR,
                ),
              ),
            ),
          ],
        ),
        body: BlocBuilder<SubscriptionHistoryBloc, SubscriptionHistoryState>(
          builder: (context, state) {
            if (state is SubscriptionHistoryLoadingState) {
              return CircularIndicatorWidget();
            } else if (state is SubscriptionHistoryErrorState) {
              return CustomErrorWidget(
                on_tap: () {
                  subscriptionHistoryBloc
                      .add(SubscriptionHistoryLoadingEvent());
                },
              );
            } else if (state is SubscriptionHistoryLoadedState) {
              List<SubscriptionHistoryDataModel> subscriptionHistoriesList =
                  state.subscriptionHistoriesList;
              if (subscriptionHistoriesList.isEmpty) {
                return NoDataWidget(
                  on_tap: () {
                    subscriptionHistoryBloc
                        .add(SubscriptionHistoryLoadingEvent());
                  },
                );
              } else {
                subscriptionHistoriesList
                    .sort((a, b) => b.isActive.compareTo(a.isActive));

                return SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.only(
                      bottom: 16.0,
                    ),
                    child: ListView.builder(
                      itemCount: subscriptionHistoriesList.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (ctx, index) {
                        var planValidity = "";
                        if (subscriptionHistoriesList[index].buy_date != null &&
                            subscriptionHistoriesList[index].buy_date != "") {
                          planValidity += HelperClass.format_data_dd_mm_yyyy(
                              subscriptionHistoriesList[index].buy_date);
                          if (subscriptionHistoriesList[index].expire_date !=
                                  null &&
                              subscriptionHistoriesList[index].expire_date !=
                                  "") {
                            planValidity +=
                                " - ${HelperClass.format_data_dd_mm_yyyy(subscriptionHistoriesList[index].expire_date)}";
                          }
                        }

                        return GestureDetector(
                          onTap: () {},
                          child: Container(
                            // width: 150,
                            margin: EdgeInsets.only(
                              top: 16.0,
                              left: 16.0,
                              right: 16.0,
                            ),

                            decoration: BoxDecoration(
                              color:
                                  subscriptionHistoriesList[index].isActive == 1
                                      ? MyColors.BG_COLOR_TEXT_FILED
                                      : MyColors.GREY_COLOR1.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(
                                18.0,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(
                                      18.0,
                                    ),
                                  ),
                                  child: subscriptionHistoriesList[index]
                                                  .images !=
                                              null &&
                                          subscriptionHistoriesList[index]
                                                  .images !=
                                              ""
                                      ? CachedNetworkImage(
                                          imageUrl:
                                              subscriptionHistoriesList[index]
                                                  .images,
                                          fit: BoxFit.cover,
                                          height: 80.0,
                                          width: 100.0,
                                          placeholder: (context, url) =>
                                              Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            fit: BoxFit.cover,
                                            height: 80.0,
                                            width: 80.0,
                                          ),
                                          errorWidget: (context, url, error) =>
                                              Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            fit: BoxFit.cover,
                                            height: 80.0,
                                            width: 80.0,
                                          ),
                                        )
                                      : Image.asset(
                                          HelperClass.APP_LOGO_PATH,
                                          fit: BoxFit.cover,
                                          height: 80.0,
                                          width: 80.0,
                                        ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      left: 16.0,
                                      top: 8.0,
                                      right: 8.0,
                                      bottom: 8.0,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          subscriptionHistoriesList[index]
                                                  .title ??
                                              "",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.GREY_COLOR1,
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              "₹ ${subscriptionHistoriesList[index].price}",
                                              style: GoogleFonts.workSans(
                                                textStyle: TextStyle(
                                                  color: MyColors.ORANGE_COLOR,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10.0,
                                            ),
                                            Text(
                                              "/${subscriptionHistoriesList[index].validity}",
                                              style: GoogleFonts.workSans(
                                                textStyle: TextStyle(
                                                  color: MyColors.GREY_COLOR,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          child: Text(
                                            planValidity,
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.GREY_COLOR,
                                                fontSize: 13.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                          margin: EdgeInsets.only(
                                            top: 8.0,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                );
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}
