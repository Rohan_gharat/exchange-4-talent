import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/blocks/ApplyCouponBloc/apply_coupon_bloc.dart';
import 'package:e4t/blocks/availableCouponsBloc/available_coupons_bloc.dart';
import 'package:e4t/blocks/subscriptionBloc/subscription_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/webView/webView.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import '../../blocks/VerifyCouponBloc/verify_coupon_bloc.dart';
import '../../restService/apis_services.dart';
import '../main_navigator_screen.dart';
import 'package:http/http.dart' as http;

class CheckOutScreen extends StatefulWidget {
  CheckOutScreen({super.key});

  static const CheckoutRoute = "checkoutRoute";

  @override
  State<CheckOutScreen> createState() => _CheckOutScreenState();
}

class _CheckOutScreenState extends State<CheckOutScreen> {
  var voucherController = TextEditingController();

  var voucher_code = "", receiptId = "";

  var finalPrice = 0;

  late UserBloc userBloc;
  late var subscriptionBloc;
  late var verifyCouponBloc;
  late var applyCouponBloc;
  late var availableCouponsBloc;

  // late var razorPayCredentialsBloc;

  var TAG = "paymentTag";
  var PURCHASE_TAG = "purchase data";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    subscriptionBloc = BlocProvider.of<SubscriptionBloc>(context);
    verifyCouponBloc = BlocProvider.of<VerifyCouponBloc>(context);
    applyCouponBloc = BlocProvider.of<ApplyCouponBloc>(context);
    availableCouponsBloc = BlocProvider.of<AvailableCouponsBloc>(context);
    userBloc = BlocProvider.of<UserBloc>(context);
    verifyCouponBloc.add(VerifyCouponEvent(subscriptionBloc
        .subscriptionsList[subscriptionBloc.selectedSubscriptionIndex]
        .subscriptionData
        .price));
    if (!Platform.isIOS) {
      applyCouponBloc.add(ApplyCouponEvent(false));
      availableCouponsBloc.add(AvailableCouponsLoadingEvent());
    }

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext rootContext) {
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
              elevation: 0,
              title: Text(
                "Checkout",
                style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              leading: IconButton(
                onPressed: () {
                  FocusManager.instance.primaryFocus?.unfocus();
                  Navigator.pop(rootContext);
                },
                icon: Icon(
                  Icons.arrow_back_ios_new,
                  color: MyColors.ORANGE_COLOR,
                  size: 16,
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(
                  16.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.01,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                        borderRadius: BorderRadius.circular(
                          18.0,
                        ),
                        border: Border.all(
                          color: MyColors.ORANGE_COLOR,
                          width: 1.0,
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(
                              18.0,
                            ),
                            child: subscriptionBloc
                                            .subscriptionsList[subscriptionBloc
                                                .selectedSubscriptionIndex]
                                            .subscriptionData
                                            .images !=
                                        null &&
                                    subscriptionBloc
                                            .subscriptionsList[subscriptionBloc
                                                .selectedSubscriptionIndex]
                                            .subscriptionData
                                            .images !=
                                        ""
                                ? CachedNetworkImage(
                                    imageUrl: subscriptionBloc
                                        .subscriptionsList[subscriptionBloc
                                            .selectedSubscriptionIndex]
                                        .subscriptionData
                                        .images!,
                                    fit: BoxFit.cover,
                                    height: 96,
                                    width: 96,
                                    placeholder: (context, url) => Image.asset(
                                      HelperClass.APP_LOGO_PATH,
                                      fit: BoxFit.cover,
                                      height: 96,
                                      width: 96,
                                    ),
                                    errorWidget: (context, url, error) =>
                                        Image.asset(
                                      HelperClass.APP_LOGO_PATH,
                                      fit: BoxFit.cover,
                                      height: 96,
                                      width: 96,
                                    ),
                                  )
                                : Image.asset(
                                    HelperClass.APP_LOGO_PATH,
                                    fit: BoxFit.cover,
                                    height: 96,
                                    width: 96,
                                  ),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.all(
                                16.0,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${subscriptionBloc.subscriptionsList[subscriptionBloc.selectedSubscriptionIndex].title}",
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                              color: MyColors.WHITE_COLOR,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        child: Icon(
                                          Icons.edit,
                                          color: MyColors.ORANGE_COLOR,
                                          size: 16,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  Text(
                                    "₹ ${subscriptionBloc.subscriptionsList[subscriptionBloc.selectedSubscriptionIndex].subscriptionData.price}",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.GREY_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    !Platform.isIOS
                        ? Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height * 0.1,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Voucher",
                                      style: GoogleFonts.montserrat(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: TextField(
                                            controller: voucherController,
                                            decoration: InputDecoration(
                                              filled: true,
                                              fillColor:
                                                  MyColors.BG_COLOR_TEXT_FILED,
                                              hintText: "Enter voucher code",
                                              hintStyle: GoogleFonts.workSans(
                                                textStyle: TextStyle(
                                                  color: MyColors.WHITE_COLOR,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: MyColors
                                                      .BG_COLOR_TEXT_FILED,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  16.0,
                                                ),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: MyColors
                                                      .BG_COLOR_TEXT_FILED,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  16.0,
                                                ),
                                              ),
                                              disabledBorder:
                                                  OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: MyColors
                                                      .BG_COLOR_TEXT_FILED,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  16.0,
                                                ),
                                              ),
                                              errorBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: MyColors
                                                      .BG_COLOR_TEXT_FILED,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  16.0,
                                                ),
                                              ),
                                              counterStyle:
                                                  GoogleFonts.workSans(
                                                textStyle: TextStyle(
                                                  color: MyColors.WHITE_COLOR,
                                                  fontSize: 10.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              border: InputBorder.none,
                                              counterText: "",
                                            ),
                                            onChanged: (value) {
                                              bool valueCheck = false;
                                              if (value.trim() != "") {
                                                valueCheck = true;
                                              }
                                              applyCouponBloc.add(
                                                  ApplyCouponEvent(valueCheck));
                                            },
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.WHITE_COLOR,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            cursorColor: MyColors.WHITE_COLOR,
                                            textInputAction:
                                                TextInputAction.done,
                                            keyboardType: TextInputType.text,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () async {
                                            if (voucherController.text
                                                .trim()
                                                .isNotEmpty) {
                                              applyCoupon(
                                                  couponCode: voucherController
                                                      .text
                                                      .trim());
                                            } else {
                                              HelperClass.show_snackbar(context,
                                                  "please enter voucher code");
                                            }
                                          },
                                          child: BlocBuilder<ApplyCouponBloc,
                                              ApplyCouponState>(
                                            builder: (context, state) {
                                              double colorOpacity;
                                              if (state
                                                  .appCouponButtonColorCheck) {
                                                colorOpacity = 1.0;
                                              } else {
                                                colorOpacity = 0.4;
                                              }
                                              return Container(
                                                decoration: BoxDecoration(
                                                  color: MyColors.ORANGE_COLOR
                                                      .withOpacity(
                                                          colorOpacity),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    18.0,
                                                  ),
                                                ),
                                                margin: EdgeInsets.only(
                                                  left: 10.0,
                                                ),
                                                padding: EdgeInsets.all(
                                                  16.0,
                                                ),
                                                child: Text(
                                                  "Apply",
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: TextStyle(
                                                      color: state
                                                              .appCouponButtonColorCheck
                                                          ? MyColors.WHITE_COLOR
                                                          : MyColors.GREY_COLOR,
                                                      fontSize: 14.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: 16.0,
                                ),
                                child: BlocConsumer<AvailableCouponsBloc,
                                    AvailableCouponsState>(
                                  listener: (context, availableCouponsState) {
                                    // TODO: implement listener
                                  },
                                  builder: (context, availableCouponsState) {
                                    if (availableCouponsState
                                        is AvailableCouponsLoadingState) {
                                      return CircularIndicatorWidget();
                                    } else if (availableCouponsState
                                        is AvailableCouponsErrorState) {
                                      return CustomErrorWidget(
                                        on_tap: () {
                                          availableCouponsBloc.add(
                                              AvailableCouponsLoadingEvent());
                                        },
                                      );
                                    } else if (availableCouponsState
                                        is AvailableCouponsLoadedState) {
                                      return Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              var alertDialog = AlertDialog(
                                                backgroundColor:
                                                    MyColors.BG_COLOR,
                                                title: Text(
                                                  "Available Coupons",
                                                  style: GoogleFonts.workSans(
                                                    textStyle: TextStyle(
                                                      color:
                                                          MyColors.WHITE_COLOR,
                                                      fontSize: 20.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                                content: Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.8,
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.6,
                                                  child: ListView.builder(
                                                    shrinkWrap: true,
                                                    itemCount:
                                                        availableCouponsState
                                                            .availableCouponsList
                                                            .length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return GestureDetector(
                                                        onTap: () {
                                                          Navigator.of(context,
                                                                  rootNavigator:
                                                                      true)
                                                              .pop();
                                                          applyCoupon(
                                                              couponCode: availableCouponsState
                                                                  .availableCouponsList[
                                                                      index]
                                                                  .coupon_code);
                                                        },
                                                        child: Container(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(
                                                            16.0,
                                                          ),
                                                          child: Text(
                                                            "${availableCouponsState.availableCouponsList[index].coupon_code} (${availableCouponsState.availableCouponsList[index].coupon_discount}% discount)",
                                                             style: GoogleFonts
                                                                .workSans(
                                                              textStyle:
                                                                  TextStyle(
                                                                color: MyColors
                                                                    .ORANGE_COLOR,
                                                                fontSize: 14.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                ),
                                              );

                                              showDialog(
                                                context: context,
                                                builder: (context) =>
                                                    alertDialog,
                                              );
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: MyColors
                                                    .BG_COLOR_TEXT_FILED,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  16.0,
                                                ),
                                              ),
                                              padding: EdgeInsets.all(
                                                12.0,
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      "Available Coupons",
                                                      style:
                                                          GoogleFonts.workSans(
                                                        textStyle: TextStyle(
                                                          color: MyColors
                                                              .WHITE_COLOR,
                                                          fontSize: 16.0,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color:
                                                        MyColors.ORANGE_COLOR,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      );
                                    }

                                    return Container();
                                  },
                                ),
                              ),
                            ],
                          )
                        : Container(),
                    Container(
                      margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.1,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "TOTAL",
                            style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                                color: MyColors.WHITE_COLOR,
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          BlocBuilder<VerifyCouponBloc, VerifyCouponState>(
                            builder: (context, state) {
                              finalPrice = state.price;
                              return Text(
                                "₹ ${state.price}",
                                style: GoogleFonts.montserrat(
                                  textStyle: TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size.fromHeight(
                          40,
                        ),
                        // fromHeight use double.infinity as width and 40 is the height
                        backgroundColor: MyColors.ORANGE_COLOR,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18),
                        ),
                      ),
                      onPressed: () {
                        print("checkout next button: final price: $finalPrice");
                        showTermsAndConditions();

                      },

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Next",
                            style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                                color: MyColors.WHITE_COLOR,
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Icon(
                            Icons.arrow_right_alt,
                            color: MyColors.WHITE_COLOR,
                            // size: 16,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
            ),
      ),
    );
  }

  void applyCoupon({required couponCode}) async {
    FocusManager.instance.primaryFocus?.unfocus();
    HelperClass.show_alert_Dialog(context, "verifying voucher");
    print("token: ${SharedPrefs.getAccessToken()}");
    var response = await ApisServices.verifyCoupon(
      token: SharedPrefs.getAccessToken(),
      subscription_id: subscriptionBloc
          .subscriptionsList[subscriptionBloc.selectedSubscriptionIndex].id,
      coupon_code: couponCode.toString().toLowerCase(),
    );
    Navigator.of(context, rootNavigator: true).pop();
    if (response != null) {
      if (response.status == 200) {
        voucher_code = couponCode;
        verifyCouponBloc.add(VerifyCouponEvent(response.final_price));
        if (response.discount != null) {
          HelperClass.show_snackbar(context,
              "congratulations! you got ${response.discount} rupees discount");
        }
      } else {
        verifyCouponBloc.add(VerifyCouponEvent(subscriptionBloc
            .subscriptionsList[subscriptionBloc.selectedSubscriptionIndex]
            .subscriptionData
            .price));
        if (response.message != null) {
          HelperClass.show_error_snackbar(context, response.message);
        }
      }
    } else {
      HelperClass.show_error_snackbar(context, HelperClass.ERROR_TEXT);
    }
  }

  void showTermsAndConditions() {
    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        "Terms & Conditions",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: Text(
        "Accept the Terms & Conditions to proceed payment",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.ORANGE_COLOR,
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: MyColors.GREY_COLOR1,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Close",
                style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                    color: MyColors.BLACK_COLOR,
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return WebViewScreen(
                        url: HelperClass.TERMS_AND_CONDITIONS_URL,
                      );
                    },
                  ),
                );
              },
              child: Text(
                "View",
                style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
                if (Platform.isIOS) {
                  HelperClass.show_alert_Dialog(context, "processing");
                  try {
                    Offerings offerings = await Purchases.getOfferings();

                    if (offerings.current != null) {
                      var productIndex = 0;
                      for (var element
                          in offerings.current!.availablePackages) {
                        if (element.storeProduct.identifier.toLowerCase() ==
                            subscriptionBloc
                                .subscriptionsList[
                                    subscriptionBloc.selectedSubscriptionIndex]
                                .productId
                                .toString()
                                .toLowerCase()) {
                          productIndex = offerings.current!.availablePackages
                              .indexOf(element);
                        }

                        }
                      print(
                          "$PURCHASE_TAG : product index: $productIndex : product identifier: ${offerings.current!.availablePackages[productIndex].storeProduct.identifier} : product id from subscriptions api: ${subscriptionBloc.subscriptionsList[subscriptionBloc.selectedSubscriptionIndex].productId}");

                      var customerInfo = await Purchases.purchaseStoreProduct(
                          offerings.current!.availablePackages[productIndex]
                              .storeProduct);

                      print(
                          "$PURCHASE_TAG : customerInfo json : ${customerInfo.toJson()}");

                      Navigator.of(context, rootNavigator: true).pop();
                      Checkout_apple(status: customerInfo.toJson());
                    } else {
                      Navigator.of(context, rootNavigator: true).pop();
                    }
                  } catch (error) {
                    Navigator.of(context, rootNavigator: true).pop();
                    print(
                        "$PURCHASE_TAG: purchaseResponse: error: ${error.toString()}");
                  }

                } else {
                  if (finalPrice <= 0) {
                    freeCheckout();
                  } else {
                    gotoPaymentGateway();
                  }
                }
              },
              child: Text(
                "Accept",
                style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );

    showDialog(
      context: context,
      builder: (context) => alert,
    );
  }

  String getEncodedString(String text) {
    print("checkout values: ${text}");
    return base64.encode(utf8.encode(text));
  }

  void freeCheckout() async {
    HelperClass.show_alert_Dialog(context, "processing");
    print("token: ${SharedPrefs.getAccessToken()}");
    var checkoutResponse = await ApisServices.checkout(
      token: SharedPrefs.getAccessToken(),
      subscription_id: subscriptionBloc
          .subscriptionsList[subscriptionBloc.selectedSubscriptionIndex].id,
      coupon_code: voucherController.text.trim(),
      price: finalPrice,
    );
    Navigator.of(context, rootNavigator: true).pop();

    if (checkoutResponse != null) {
      if (checkoutResponse.status == 200) {
        FireBaseHelperClass.updateSubscriptionId(
            firestore: FirebaseFirestore.instance,
            mobileNo: userBloc.myUserData!.mobile,
            subscriptionId: subscriptionBloc
                .subscriptionsList[subscriptionBloc.selectedSubscriptionIndex]
                .id,
            subscriptionValidity: checkoutResponse.expire_date);
       Navigator.of(context, rootNavigator: true).pushNamedAndRemoveUntil(
            MainNavigationScreen.MAIN_NAVIGATION_SCREEN_ROUTE,
            (route) => false);

      }
      HelperClass.show_snackbar(context, checkoutResponse.message);
    } else {
      HelperClass.show_error_snackbar(context, HelperClass.ERROR_TEXT);
    }
  }

  void Checkout_apple({required status}) async {
    HelperClass.show_alert_Dialog(context, "processing");
    print("token: ${SharedPrefs.getAccessToken()}");
    var checkoutResponse = await ApisServices.checkout_apple(
      token: SharedPrefs.getAccessToken(),
      subscription_id: subscriptionBloc
          .subscriptionsList[subscriptionBloc.selectedSubscriptionIndex].id,
      coupon_code: voucherController.text.trim(),
      price: finalPrice,
      paymentType: true,
      status: status,
    );
    Navigator.of(context, rootNavigator: true).pop();

    if (checkoutResponse != null) {
      if (checkoutResponse.status == 200) {
        FireBaseHelperClass.updateSubscriptionId(
            firestore: FirebaseFirestore.instance,
            mobileNo: userBloc.myUserData!.mobile,
            subscriptionId: subscriptionBloc
                .subscriptionsList[subscriptionBloc.selectedSubscriptionIndex]
                .id,
            subscriptionValidity: checkoutResponse.expire_date);

        Navigator.of(context, rootNavigator: true).pushNamedAndRemoveUntil(
            MainNavigationScreen.MAIN_NAVIGATION_SCREEN_ROUTE,
            (route) => false);
      }
      HelperClass.show_snackbar(context, checkoutResponse.message);
    } else {
      HelperClass.show_error_snackbar(context, HelperClass.ERROR_TEXT);
    }
  }

  void gotoPaymentGateway() async {
    var orderId = DateTime.now().millisecondsSinceEpoch;
    var encodedOrderId = getEncodedString("${orderId}");
    var trackingId = DateTime.now().millisecondsSinceEpoch;
    var encodedTrackingId = getEncodedString("${trackingId}");
    var encodedUserId = getEncodedString("${SharedPrefs.getUserId()}");
    var encodedSubscriptionId = getEncodedString(
        "${subscriptionBloc.subscriptionsList[subscriptionBloc.selectedSubscriptionIndex].id}");
    var encodedPrice = getEncodedString("${finalPrice}");
    var encodedPlanName = getEncodedString(
        "${subscriptionBloc.subscriptionsList[subscriptionBloc.selectedSubscriptionIndex].title}");

    print("checkout encodedOrderId: ${encodedOrderId} :: "
        "encodedUserId: $encodedUserId :: encodedSubscriptionId: $encodedSubscriptionId ::"
        "encodedPrice: $encodedPrice :: encodedPlanName: $encodedPlanName :: encodedTrackingId:$encodedTrackingId");


    // String url="https://exchange4talentglobal.com/CCAvenue/CCAvenue/index.php?uid=21&orderId=2342&tid=1233&subId=21&price=1&sub_name=%22asdsadn%22";
    String url =
        "https://exchange4talentglobal.com/CCAvenue/CCAvenue/index.php?uid=$encodedUserId&orderId=$encodedOrderId&tid=$encodedTrackingId&subId=$encodedSubscriptionId&price=$encodedPrice&sub_name=$encodedPlanName";
    // print("url:$url");
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return WebViewScreen(
            url: url,
          );
        },
      ),
    ).then((value) async {

      HelperClass.show_alert_Dialog(context, "processing");
      var response = await ApisServices.checkPayment(
          token: SharedPrefs.getAccessToken(), order_id: orderId);

      Navigator.of(context, rootNavigator: true).pop();
      print("payment response: ${response}");

      if (response != null) {
        if (response.status == 200 && response.paymentStatus) {
          HelperClass.show_snackbar(context, response.message ?? "");
          FireBaseHelperClass.updateSubscriptionId(
              firestore: FirebaseFirestore.instance,
              mobileNo: userBloc.myUserData!.mobile,
              subscriptionId: subscriptionBloc
                  .subscriptionsList[subscriptionBloc.selectedSubscriptionIndex]
                  .id,
              subscriptionValidity: response.expiry_date);
          Navigator.of(context, rootNavigator: true).pushNamedAndRemoveUntil(
              MainNavigationScreen.MAIN_NAVIGATION_SCREEN_ROUTE,
              (route) => false);
        } else {
          HelperClass.show_error_snackbar(context, response.message ?? "");
        }
      } else {
        HelperClass.show_error_snackbar(context, HelperClass.ERROR_TEXT);
      }
    });
  }
}
