import 'package:e4t/blocks/HomeBloc/home_bloc.dart';
import 'package:e4t/blocks/favourite/favourite_bloc.dart';
import 'package:e4t/blocks/mainNavigationBloc/main_navigation_bloc.dart';
import 'package:e4t/blocks/profilePhotoUpdateBloc/profile_photo_update_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/navigators/account_navigator.dart';
import 'package:e4t/navigators/chat_navigator.dart';
import 'package:e4t/navigators/favourite_navigator.dart';
import 'package:e4t/navigators/search_navigator.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/chat/messagesScreen.dart';
import 'package:e4t/screens/recruiters/recruitersPostDetailsScreen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:permission_handler/permission_handler.dart';

import '../navigators/home_navigator.dart';

class MainNavigationScreen extends StatefulWidget {
  const MainNavigationScreen({Key? key}) : super(key: key);
  static const NOTIFICATIONS_TAG = "notifications_tag";

  static const MAIN_NAVIGATION_SCREEN_ROUTE = "main_navigation_screen_route";

  static const HOME_INDEX = 0;
  static const SEARCH_INDEX = 1;
  static const CHAT_INDEX = 2;
  static const FAVOURITE_INDEX = 3;
  static const ACCOUNTS_INDEX = 4;

  @override
  State<MainNavigationScreen> createState() => _MainNavigationScreenState();
}

class _MainNavigationScreenState extends State<MainNavigationScreen> {
  List<GlobalKey<NavigatorState>> navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];
  late UserBloc userBloc;
  late HomeBloc homeBloc;
  late FavouriteBloc favouriteBloc;
  late var mainNavigationBloc;
  late var updateProfilePhotoBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    mainNavigationBloc = BlocProvider.of<MainNavigationBloc>(context);
    mainNavigationBloc
        .add(MainNavigationEvent(MainNavigationScreen.HOME_INDEX, false));
    userBloc = BlocProvider.of<UserBloc>(context);
    userBloc.add(MyUserLoadingEvent(SharedPrefs.getUserId()));

    setupInteractedMessage().then((value) async {
      RemoteMessage? initialMessage =
          await FirebaseMessaging.instance.getInitialMessage();

      if (initialMessage != null) {
        // App received a notification when it was killed
        _handleMessage(initialMessage.data);
      }

      await Permission.notification.isDenied.then(
        (bool value) {
          if (value) {
            Permission.notification.request();
          }
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<UserBloc, UserState>(
        listener: (context, state) {
          // TODO: implement listener
          if (state is MyUserLoadedState) {
            updateProfilePhotoBloc =
                BlocProvider.of<ProfilePhotoUpdateBloc>(context);
            homeBloc = BlocProvider.of<HomeBloc>(context);
            homeBloc.add(HomeLoadingEvent());
            favouriteBloc = BlocProvider.of<FavouriteBloc>(context);
            favouriteBloc.add(FavouriteLoadingEvent());
            updateProfilePhotoBloc.add(ProfilePhotoUpdateEvent(
                userBloc.myUserData?.avatar,
                userBloc.myUserData?.name,
                userBloc.myUserData?.mobile));
          } else if (state is MyUserErrorState) {
            HelperClass.show_snackbar(context, state.errorMsg);
          }
        },
        builder: (context, userState) {
          if (userState is MyUserLoadingState) {
            return CircularIndicatorWidget();
          } else if (userState is MyUserErrorState) {
            return CustomErrorWidget(
              on_tap: () {
                userBloc.add(MyUserLoadingEvent(SharedPrefs.getUserId()));
              },
            );
          } else if (userState is MyUserLoadedState) {
            return BlocConsumer<MainNavigationBloc, MainNavigationState>(
              listener: (context, state) {
                if (state.selectedIndex ==
                        MainNavigationScreen.ACCOUNTS_INDEX &&
                    state.can_pop) {
                  navigatorKeys[state.selectedIndex]
                      .currentState!
                      .popUntil((route) => route.isFirst);
                }
                if (state.selectedIndex == MainNavigationScreen.CHAT_INDEX &&
                    (userState.userDataModel.userModel?.subscriptionValidity ==
                            null ||
                        userState.userDataModel.userModel?.subscriptionValidity
                                ?.toString()
                                .trim() ==
                            "")) {
                  HelperClass.show_snackbar(context,
                      "your not subscribed to any plan buy a subscription to start messaging");
                }
              },
              builder: (context, state) {
                return WillPopScope(
                  onWillPop: () async {
                    _systemBackButtonPressed(state.selectedIndex);
                    return false;
                  },
                  child: SafeArea(
                    child: Scaffold(
                      body: IndexedStack(
                        index: state.selectedIndex,
                        children: [
                          HomeNavigator(
                              navigator_Key: navigatorKeys[
                                  MainNavigationScreen.HOME_INDEX]),
                          SearchNavigator(
                              navigator_Key: navigatorKeys[
                                  MainNavigationScreen.SEARCH_INDEX]),
                          ChatNavigator(
                              navigator_Key: navigatorKeys[
                                  MainNavigationScreen.CHAT_INDEX],
                              subscriptionValidity: userState.userDataModel
                                  .userModel?.subscriptionValidity),
                          FavouriteNavigator(
                              navigator_Key: navigatorKeys[
                                  MainNavigationScreen.FAVOURITE_INDEX]),
                          AccountNavigator(
                              navigator_Key: navigatorKeys[
                                  MainNavigationScreen.ACCOUNTS_INDEX]),
                        ],
                      ),
                      bottomNavigationBar: BottomNavigationBar(
                        items: [
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.home,
                              color: state.selectedIndex ==
                                      MainNavigationScreen.HOME_INDEX
                                  ? MyColors.ORANGE_COLOR
                                  : Colors.white,
                            ),
                            label: "Home",
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.search,
                              color: state.selectedIndex ==
                                      MainNavigationScreen.SEARCH_INDEX
                                  ? MyColors.ORANGE_COLOR
                                  : Colors.white,
                            ),
                            label: "Search",
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.message,
                              color: state.selectedIndex ==
                                      MainNavigationScreen.CHAT_INDEX
                                  ? MyColors.ORANGE_COLOR
                                  : Colors.white,
                            ),
                            label: "Chat",
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.favorite_border,
                              color: state.selectedIndex ==
                                      MainNavigationScreen.FAVOURITE_INDEX
                                  ? MyColors.ORANGE_COLOR
                                  : Colors.white,
                            ),
                            label: "Favourite",
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.person,
                              color: state.selectedIndex ==
                                      MainNavigationScreen.ACCOUNTS_INDEX
                                  ? MyColors.ORANGE_COLOR
                                  : Colors.white,
                            ),
                            label: "Account",
                          ),
                        ],
                        onTap: (index) {
                          FocusManager.instance.primaryFocus?.unfocus();
                          if (index != state.selectedIndex) {
                            if (index == MainNavigationScreen.CHAT_INDEX &&
                                (userState.userDataModel.userModel
                                            ?.subscriptionValidity ==
                                        null ||
                                    userState.userDataModel.userModel
                                            ?.subscriptionValidity ==
                                        "")) {
                              HelperClass.show_snackbar(context,
                                  "your not subscribed to any plan buy a subscription to start messaging");
                            }
                          }

                          selectTabs(index, state.selectedIndex);
                        },
                        currentIndex: state.selectedIndex,
                        showSelectedLabels: false,
                        backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
                        type: BottomNavigationBarType.fixed,
                        showUnselectedLabels: false,
                      ),
                    ),
                  ),
                );
              },
            );
          }
          return Container();
        },
      ),
    );
  }

  selectTabs(int index, previousIndex) {
    if (index == previousIndex) {
      navigatorKeys[index].currentState!.popUntil((route) => route.isFirst);
    } else {
      mainNavigationBloc.add(MainNavigationEvent(index, false));
    }
  }

  Future<bool> _systemBackButtonPressed(int previousIndex) async {
    if (navigatorKeys[previousIndex].currentState!.canPop()) {
      navigatorKeys[previousIndex]
          .currentState!
          .pop(navigatorKeys[previousIndex].currentContext);
    } else {
      if (previousIndex != 0) {
        mainNavigationBloc.add(MainNavigationEvent(0, false));
      } else {
        // Navigator.pop(context);
        SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop');
      }
    }
    return false;
  }

  void addFcmToken({required fcmToken}) async {
    ApisServices.addFcmToken(
      authToken: SharedPrefs.getAccessToken(),
      fcmToken: fcmToken,
    ).then((value) {
      if (!value) {
        addFcmToken(fcmToken: fcmToken);
      }
    });
  }

  Future<void> setupInteractedMessage() async {
    await Firebase.initializeApp();

    final fcmToken = await FirebaseMessaging.instance.getToken();
    print("${MainNavigationScreen.NOTIFICATIONS_TAG}: fcm token: $fcmToken");
    if (fcmToken != null) {
      addFcmToken(fcmToken: fcmToken);
    }

// This function is called when ios app is opened, for android case `onDidReceiveNotificationResponse` function is called

    FirebaseMessaging.onMessageOpenedApp.listen(
      (RemoteMessage message) {
        _handleMessage(message.data);
      },
    );
    enableIOSNotifications();
    await registerNotificationListeners();
  }

  Future<void> registerNotificationListeners() async {
    final AndroidNotificationChannel channel = androidNotificationChannel();
    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);
    const AndroidInitializationSettings androidSettings =
        AndroidInitializationSettings('@drawable/e4t_logo');
    const DarwinInitializationSettings iOSSettings =
        DarwinInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );
    const InitializationSettings initSettings =
        InitializationSettings(android: androidSettings, iOS: iOSSettings);
    flutterLocalNotificationsPlugin.initialize(
      initSettings,
      onDidReceiveNotificationResponse: (NotificationResponse details) {
        // We're receiving the payload as string that looks like this
        // {buttontext: Button Text, subtitle: Subtitle, imageurl: , typevalue: 14, type: course_details}
        // So the code below is used to convert string to map and read whatever property you want
        final List<String> str =
            details.payload!.replaceAll('{', '').replaceAll('}', '').split(',');
        final Map<String, dynamic> result = <String, dynamic>{};
        for (int i = 0; i < str.length; i++) {
          final List<String> s = str[i].split(':');
          result.putIfAbsent(s[0].trim(), () => s[1].trim());
        }

        _handleMessage(result);
      },
    );
// onMessage is called when the app is in foreground and a notification is received
    FirebaseMessaging.onMessage.listen((RemoteMessage? message) {
      if (message != null) {
        // _handleMessage(message.data);
      }
      print(
          "${MainNavigationScreen.NOTIFICATIONS_TAG} : onMessage: remote message: ${message?.toMap()}");

      final RemoteNotification? notification = message!.notification;
      final AndroidNotification? android = message.notification?.android;
// If `onMessage` is triggered with a notification, construct our own
// local notification to show to users using the created channel.
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channelDescription: channel.description,
              icon: android.smallIcon,
            ),
          ),
          payload: message.data.toString(),
        );
      }
    });
  }

  Future<void> enableIOSNotifications() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );
  }

  AndroidNotificationChannel androidNotificationChannel() =>
      const AndroidNotificationChannel(
        'high_importance_channel', // id
        'High Importance Notifications', // title
        description:
            'This channel is used for important notifications.', // description
        importance: Importance.defaultImportance,
      );

  void _handleMessage(Map data) {
    if (data[HelperClass.NOTIFICATIONS_TYPE_KEY] ==
        HelperClass.NOTIFICATIONS_JOB_TYPE_KEY) {
      if (data[HelperClass.NOTIFICATIONS_JOB_ID_KEY] != null) {
        Navigator.pushNamed(context,
            RecruitersPostDetailsScreen.RECRUITERS_POST_DETAILS_SCREEN_ROUTE,
            arguments: int.parse(data[HelperClass.NOTIFICATIONS_JOB_ID_KEY]));
      }
    } else if (data[HelperClass.NOTIFICATIONS_TYPE_KEY] ==
        HelperClass.NOTIFICATIONS_CHAT_TYPE_KEY) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return MessagesScreen(
              name: data[HelperClass.NOTIFICATION_CHAT_NAME_KEY] ?? "",
              chat_receiver_uid: data[HelperClass.NOTIFICATION_CHAT_ID_KEY],
              isGroupChat:
                  data[HelperClass.NOTIFICATIONS_IS_GROUP_CHAT_KEY] == "1"
                      ? true
                      : false,
              profilePic: data[HelperClass.NOTIFICATIONS_PROFILE_PIC_KEY] ?? "",
              currentUserUid: data[HelperClass.NOTIFICATIONS_SENDER_ID_KEY],
              isSecretService:
                  data[HelperClass.NOTIFICATION_IS_SECRET_SERVICE_KEY] == "1"
                      ? true
                      : false,
            );
          },
        ),
      );
    }
  }
}
