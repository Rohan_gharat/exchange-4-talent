import 'package:e4t/blocks/AddReviewBloc/add_review_bloc.dart';
import 'package:e4t/blocks/RatingBloc/rating_bloc.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../myColors/myColors.dart';

class ReviewDialog extends StatelessWidget {
  ReviewDialog({Key? key, required this.rating,required this.talent_id}) : super(key: key);

  var formKey = GlobalKey<FormState>();
  var reviewController = TextEditingController();
  var rating = 0.0;
  var talent_id;

  @override
  Widget build(BuildContext context) {
    var reviewsBloc = BlocProvider.of<AddReviewBloc>(context);
    var ratingBloc = BlocProvider.of<RatingBloc>(context);
    ratingBloc.add(RatingEvent(5));


    return AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(
            18.0,
          ),
        ),
      ),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.35,
        child: Form(
          key: formKey,
          child: Column(
            children: [
              BlocConsumer<RatingBloc, RatingState>(
                listener: (c,state){
                  rating = state.rating;
                },
                builder: (context, state) {
                  return RatingBar.builder(
                    initialRating: state.rating,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemSize: 30,
                    unratedColor: MyColors.GREY_COLOR1,
                    itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: MyColors.ORANGE_COLOR,
                    ),
                    onRatingUpdate: (rating) {

                      ratingBloc.add(RatingEvent(rating));
                    },
                  );
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10.0,
                ),
                child: TextFormField(
                  controller: reviewController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(
                      8.0,
                    ),
                    filled: true,
                    fillColor: MyColors.BG_COLOR,
                    hintText: "Add Review",
                    hintStyle: GoogleFonts.workSans(
                      textStyle: TextStyle(
                        color: MyColors.GREY_COLOR,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    counterStyle: GoogleFonts.workSans(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    counterText: "",
                  ),

                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  cursorColor: MyColors.WHITE_COLOR,
                  // textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.multiline,
                  maxLines: 5,
                  validator: (value) {
                    if (value == null || value.trim() == "") {
                      return 'Please add your Review';
                    }
                    return null;
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      actions: [
        BlocConsumer<AddReviewBloc, AddReviewState>(
          listener: (c,state){
            if(state is ReviewPostedState){
              reviewController.clear();
              FocusManager.instance.primaryFocus?.unfocus();
              HelperClass.show_snackbar(context, state.msg);
            }else if (state is ReviewErrorState){
              HelperClass.show_snackbar(context, state.errorMsg);
            }
          },
          builder: (context, state) {
            if(state is ReviewPostingState){
              return CircularIndicatorWidget();
            }
            return Padding(
              padding: const EdgeInsets.only(
                right: 16.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      // fromHeight use double.infinity as width and 40 is the height
                      backgroundColor: MyColors.ORANGE_COLOR,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18),
                      ),
                    ),
                    onPressed: () {
                      // Navigator.pop(context);
                      Navigator.of(context, rootNavigator: true).pop();
                    },
                    child: Text(
                      "Cancel",
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20.0,
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      // fromHeight use double.infinity as width and 40 is the height
                      backgroundColor: MyColors.ORANGE_COLOR,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18),
                      ),
                    ),
                    onPressed: () async {

                      reviewsBloc.add(ReviewPostingEvent(reviewController.text.trim(), rating, talent_id));

                    },
                    child: Text(
                      "Add",
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ],
    );
  }
}
