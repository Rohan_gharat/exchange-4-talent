import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/ReviewsBloc/reviews_bloc.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../myColors/myColors.dart';

class ReviewsScreen extends StatelessWidget {
  ReviewsScreen({Key? key, required this.talentId}) : super(key: key);
  static const ALL_REVIEWS_ROUTE = "allReviewsRoute";
  int talentId;

  @override
  Widget build(BuildContext context) {
    var reviewsBloc = BlocProvider.of<ReviewsBloc>(context);

    reviewsBloc.add(ReviewsLoadingEvent(talentId));

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            "Reviews",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
        ),
        body: BlocListener<ReviewsBloc, ReviewsState>(
          listener: (context, state) {
            if (state is ReviewsErrorState) {
              HelperClass.show_snackbar(context, state.errrorMsg);
            }
          },
          child: BlocBuilder<ReviewsBloc, ReviewsState>(
            builder: (context, state) {
              if (state is ReviewsLoadingState) {
                return CircularIndicatorWidget();
              } else if (state is ReviewsErrorState) {
                return CustomErrorWidget(
                  on_tap: () {
                    reviewsBloc.add(ReviewsLoadingEvent(talentId));
                  },
                );
              } else if (state is ReviewsLoadedState) {
                return state.reviewsList.isNotEmpty
                    ? SingleChildScrollView(
                        child: Container(
                          margin: EdgeInsets.only(
                            bottom: 16.0,
                            left: 16.0,
                            right: 16.0,
                          ),
                          child: ListView.builder(
                            itemCount: state.reviewsList.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (ctx, index) {
                              return GestureDetector(
                                onTap: () {},
                                child: Container(
                                  // width: 150,
                                  margin: EdgeInsets.only(
                                    top: 16.0,
                                  ),
                                  padding: EdgeInsets.all(
                                    10.0,
                                  ),
                                  decoration: BoxDecoration(
                                    color: MyColors.BG_COLOR_TEXT_FILED,
                                    borderRadius: BorderRadius.circular(
                                      18.0,
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(
                                            18.0,
                                          ),
                                        ),
                                        child: state.reviewsList[index]
                                                        .avatar !=
                                                    null &&
                                                state.reviewsList[index]
                                                        .avatar !=
                                                    ""
                                            ? CachedNetworkImage(
                                                imageUrl: state
                                                    .reviewsList[index].avatar!,
                                                fit: BoxFit.cover,
                                                height: 70.0,
                                                width: 70.0,
                                                placeholder: (context, url) =>
                                                    Image.asset(
                                                  HelperClass.APP_LOGO_PATH,
                                                  fit: BoxFit.cover,
                                                  height: 70.0,
                                                  width: 70.0,
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Image.asset(
                                                  HelperClass.APP_LOGO_PATH,
                                                  fit: BoxFit.cover,
                                                  height: 70.0,
                                                  width: 70.0,
                                                ),
                                              )
                                            : Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                fit: BoxFit.cover,
                                                height: 70.0,
                                                width: 70.0,
                                              ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                            left: 16.0,
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                state.reviewsList[index]
                                                        .username ??
                                                    "",
                                                style: GoogleFonts.workSans(
                                                  textStyle: TextStyle(
                                                    color: MyColors.GREY_COLOR1,
                                                    fontSize: 16.0,
                                                    fontWeight: FontWeight.bold,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              RatingBar.builder(
                                                initialRating: double.parse(
                                                    state.reviewsList[index]
                                                            .rating ??
                                                        "0.0"),
                                                minRating: 1,
                                                direction: Axis.horizontal,
                                                allowHalfRating: true,
                                                itemCount: 5,
                                                itemSize: 18,
                                                unratedColor:
                                                    MyColors.GREY_COLOR1,
                                                itemPadding:
                                                    EdgeInsets.symmetric(
                                                        horizontal: 1.0),
                                                itemBuilder: (context, _) =>
                                                    const Icon(
                                                  Icons.star,
                                                  color: MyColors.ORANGE_COLOR,
                                                ),
                                                onRatingUpdate: (rating) {
                                                },
                                                ignoreGestures: true,
                                              ),
                                              Text(
                                                state.reviewsList[index]
                                                        .comment ??
                                                    "",
                                                style: GoogleFonts.workSans(
                                                  textStyle: TextStyle(
                                                    color:
                                                        MyColors.ORANGE_COLOR,
                                                    fontSize: 16.0,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    : NoDataWidget(
                        on_tap: () {
                          reviewsBloc.add(ReviewsLoadingEvent(talentId));
                        },
                      );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }
}
