import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/RecruitersApplyBloc/recruiters_apply_bloc.dart';
import 'package:e4t/blocks/RecruitersBloc/recruiters_bloc.dart';
import 'package:e4t/blocks/RecruitersJobsActivePostsBloc/recruiters_jobs_active_posts_bloc.dart';
import 'package:e4t/blocks/RecruitersPostDetailsBloc/recruiters_post_details_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/RecruitersJobs/AppliedJobsScreen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class RecruitersPostDetailsScreen extends StatelessWidget {
  RecruitersPostDetailsScreen({super.key, required this.postId});

  int postId;
  static const RECRUITERS_POST_DETAILS_SCREEN_ROUTE =
      "RecruitersPostDetailsScreenRoute";

  @override
  Widget build(BuildContext context) {
    var recruitersBloc = BlocProvider.of<RecruitersBloc>(context);

    var recruitersApplyBloc = BlocProvider.of<RecruitersApplyBloc>(context);
    var recruitersJobsActivePostsBloc =
        BlocProvider.of<RecruitersJobsActivePostsBloc>(context);
    recruitersApplyBloc
        .add(RecruitersApplyEvent(HelperClass.NOT_APPLIED_JOB_ID));

    var recruitersPostDetailsBloc =
        BlocProvider.of<RecruitersPostDetailsBloc>(context);
    recruitersPostDetailsBloc.add(RecruitersPostByIdEvent(postId));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
        elevation: 0,
        title: Text(
          "Job",
          style: GoogleFonts.montserrat(
            textStyle: const TextStyle(
              color: MyColors.WHITE_COLOR,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios_new,
            color: MyColors.ORANGE_COLOR,
            size: 16,
          ),
        ),
      ),
      body: BlocConsumer<RecruitersPostDetailsBloc, RecruitersPostDetailsState>(
        listener: (context, state) {
          // TODO: implement listener
          if (state is RecruitersPostDetailsLoadedState) {
            recruitersApplyBloc.add(RecruitersApplyEvent(
                state.recruitersPostModel.jobStatus ??
                    HelperClass.NOT_APPLIED_JOB_ID));
          }
        },
        builder: (context, state) {
          if (state is RecruitersPostDetailsLoadingState) {
            return const CircularIndicatorWidget();
          } else if (state is RecruitersPostDetailsErrorState) {
            return CustomErrorWidget(
              on_tap: () {
                recruitersPostDetailsBloc.add(RecruitersPostByIdEvent(postId));
              },
            );
          } else if (state is RecruitersPostDetailsLoadedState) {
            return Column(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                      ),
                      SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(
                                16.0,
                              ),
                              child: state.recruitersPostModel
                                              .recruitersModel !=
                                          null &&
                                      state.recruitersPostModel.recruitersModel!
                                              .logo !=
                                          null &&
                                      state.recruitersPostModel.recruitersModel!
                                              .logo
                                              .toString()
                                              .trim() !=
                                          ""
                                  ? CachedNetworkImage(
                                      imageUrl: state.recruitersPostModel
                                          .recruitersModel!.logo,
                                      width: MediaQuery.of(context).size.width,
                                      height: 250.0,
                                      // fit: BoxFit.cover,
                                      placeholder: (context, url) =>
                                          Image.asset(
                                        HelperClass.APP_LOGO_PATH,
                                        height: 250.0,
                                        width:
                                            MediaQuery.of(context).size.width,
                                      ),
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                        HelperClass.APP_LOGO_PATH,
                                        height: 250.0,
                                        width:
                                            MediaQuery.of(context).size.width,
                                      ),
                                    )
                                  : Image.asset(
                                      HelperClass.APP_LOGO_PATH,
                                      height: 300.0,
                                      width: MediaQuery.of(context).size.width,
                                    ),
                            ),
                            Container(
                              padding: const EdgeInsets.all(
                                8.0,
                              ),
                              margin: const EdgeInsets.only(
                                top: 16.0,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    state.recruitersPostModel
                                                    .recruitersModel !=
                                                null &&
                                            state.recruitersPostModel
                                                    .recruitersModel !=
                                                null &&
                                            state.recruitersPostModel
                                                    .recruitersModel!.name
                                                    .toString()
                                                    .trim() !=
                                                ""
                                        ? "${state.recruitersPostModel.recruitersModel!.name} is hiring for ${state.recruitersPostModel.title}"
                                        : "${state.recruitersPostModel.title}",
                                    style: GoogleFonts.montserrat(
                                      textStyle: const TextStyle(
                                        color: MyColors.ORANGE_COLOR,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  state.recruitersPostModel.location != null &&
                                          state.recruitersPostModel.location
                                                  .toString()
                                                  .trim() !=
                                              ""
                                      ? Container(
                                          margin: const EdgeInsets.only(
                                            top: 10.0,
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              const Icon(
                                                Icons.location_on,
                                                color: MyColors.ORANGE_COLOR,
                                                size: 20.0,
                                              ),
                                              const SizedBox(
                                                width: 2.0,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  "${state.recruitersPostModel.location}",
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: const TextStyle(
                                                      color:
                                                          MyColors.WHITE_COLOR,
                                                      fontSize: 16.0,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      : Container(),
                                  state.recruitersPostModel.end_date != null &&
                                          state.recruitersPostModel.end_date
                                                  .toString()
                                                  .trim() !=
                                              "" &&
                                          HelperClass
                                                  .format_recruiter_post_date_dd_mm_yyyy(
                                                      state.recruitersPostModel
                                                          .end_date) !=
                                              null &&
                                          HelperClass.daysBetween(
                                                  DateTime.now(),
                                                  DateFormat(HelperClass
                                                          .DATE_TIME_STAMP_FORMAT)
                                                      .parse(state
                                                          .recruitersPostModel
                                                          .end_date)) >
                                              0
                                      ? Text(
                                          // "Campaign End Date: ${HelperClass.format_recruiter_post_date_dd_mm_yyyy(state.recruitersPostsList[index].end_date)}",
                                          "${HelperClass.daysBetween(DateTime.now(), DateFormat(HelperClass.DATE_TIME_STAMP_FORMAT).parse(state.recruitersPostModel.end_date))} days left for apply",
                                          style: GoogleFonts.montserrat(
                                            textStyle: const TextStyle(
                                                color: MyColors.WHITE_COLOR,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        )
                                      : Container(),
                                  state.recruitersPostModel.audition_date !=
                                              null &&
                                          state.recruitersPostModel
                                                  .audition_date
                                                  .toString()
                                                  .trim() !=
                                              "" &&
                                          HelperClass
                                                  .format_recruiter_post_date_dd_mm_yyyy(
                                                      state.recruitersPostModel
                                                          .audition_date) !=
                                              null
                                      ? Text(
                                          "Audition Date: ${HelperClass.format_recruiter_post_date_dd_mm_yyyy(state.recruitersPostModel.audition_date)}",
                                          style: GoogleFonts.montserrat(
                                            textStyle: const TextStyle(
                                              color: MyColors.WHITE_COLOR,
                                              fontSize: 16.0,
                                            ),
                                          ),
                                        )
                                      : Container(),
                                  Container(
                                    margin: const EdgeInsets.only(
                                      top: 16.0,
                                    ),
                                    child: Text(
                                      "Description",
                                      style: GoogleFonts.montserrat(
                                        textStyle: const TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 16.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "${state.recruitersPostModel.description}",
                                    style: GoogleFonts.montserrat(
                                      textStyle: const TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    top: 4.0,
                    bottom: 10.0,
                    left: 8.0,
                    right: 8.0,
                  ),
                  child: BlocBuilder<RecruitersApplyBloc, RecruitersApplyState>(
                    builder: (context, state) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: MyColors.ORANGE_COLOR,
                                minimumSize: const Size.fromHeight(
                                  40.0,
                                ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: state.jobStatusId ==
                                      HelperClass.NOT_APPLIED_JOB_ID
                                  ? () async {
                                      HelperClass.show_alert_Dialog(
                                          context, "processing");
                                      var response =
                                          await ApisServices.applyJob(
                                              userId: SharedPrefs.getUserId(),
                                              postId: postId,
                                              token:
                                                  SharedPrefs.getAccessToken());
                                      Navigator.of(context, rootNavigator: true)
                                          .pop();

                                      if (response != null) {
                                        if (response.status == 200) {
                                          recruitersApplyBloc.add(
                                              RecruitersApplyEvent(
                                                  HelperClass.APPLIED_JOB_ID));
                                          if (response.recruitersPostModel !=
                                              null) {
                                            recruitersJobsActivePostsBloc.add(
                                                RecruitersJobsActivePostsApplyRemoveJobEvent(
                                              recruitersPostModel:
                                                  response.recruitersPostModel!,
                                            ));

                                            recruitersBloc.url =
                                            "${ApisServices.BASE_URL}${AppliedJobsScreen.APPLIED_JOBS_API}";
                                            recruitersBloc
                                                .recruitersPostsListAll
                                                .clear();
                                            recruitersBloc.add(
                                                RecruitersLoadingEvent());
                                          }
                                          HelperClass.show_snackbar(
                                              context,
                                              response.message ??
                                                  "Thank you for your interest");
                                        } else {
                                          HelperClass.show_snackbar(
                                              context,
                                              response.message ??
                                                  HelperClass.ERROR_TEXT);
                                        }
                                      } else {
                                        HelperClass.show_snackbar(
                                            context, HelperClass.ERROR_TEXT);
                                      }
                                    }
                                  : state.jobStatusId ==
                                          HelperClass.APPLIED_JOB_ID
                                      ? () {
                                          ScaffoldMessenger.of(context)
                                              .hideCurrentSnackBar();
                                          HelperClass.show_snackbar(context,
                                              "you already applied for this job");
                                        }
                                      : () {},
                              child: Text(
                                state.jobStatusId ==
                                        HelperClass.NOT_APPLIED_JOB_ID
                                    ? HelperClass.NOT_APPLIED_JOB_key
                                    : state.jobStatusId ==
                                            HelperClass.APPLIED_JOB_ID
                                        ? HelperClass.APPLIED_JOB_key
                                        : state.jobStatusId ==
                                                HelperClass.SHORTELISTED_JOB_ID
                                            ? HelperClass.SHORTELISTED_JOB_key
                                            : state.jobStatusId ==
                                                    HelperClass.REJECTED_JOB_ID
                                                ? HelperClass.REJECTED_JOB_key
                                                : state.jobStatusId ==
                                                        HelperClass.HIRED_JOB_ID
                                                    ? HelperClass.HIRED_JOB_key
                                                    : "",
                                style: GoogleFonts.montserrat(
                                  textStyle: const TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 16.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          state.jobStatusId == 1
                              ? Container(
                                  margin: const EdgeInsets.only(
                                    left: 8.0,
                                  ),
                                  child: GestureDetector(
                                    onTap: () {
                                      var alert = AlertDialog(
                                        backgroundColor: MyColors.BG_COLOR,
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(
                                              32.0,
                                            ),
                                          ),
                                        ),
                                        title: Text(
                                          "Delete Application",
                                          style: GoogleFonts.montserrat(
                                            textStyle: const TextStyle(
                                              color: MyColors.RED_COLOR,
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        content: SizedBox(
                                          height: 100.0,
                                          child: Column(
                                            children: [
                                              Text(
                                                "Are You Sure You Want To Delete your Application for this job",
                                                style: GoogleFonts.montserrat(
                                                  textStyle: const TextStyle(
                                                    color: MyColors.RED_COLOR,
                                                    fontSize: 16.0,
                                                    // fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        actions: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                  // fromHeight use double.infinity as width and 40 is the height
                                                  backgroundColor:
                                                      MyColors.ORANGE_COLOR,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            18),
                                                  ),
                                                ),
                                                onPressed: () {
                                                  // Navigator.pop(context);
                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop();
                                                },
                                                child: Text(
                                                  "Cancel",
                                                  style: GoogleFonts.workSans(
                                                    textStyle: TextStyle(
                                                      color:
                                                          MyColors.WHITE_COLOR,
                                                      fontSize: 14.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 20.0,
                                              ),
                                              ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                  // fromHeight use double.infinity as width and 40 is the height
                                                  backgroundColor:
                                                      MyColors.ORANGE_COLOR,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            18),
                                                  ),
                                                ),
                                                onPressed: () async {
                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop();
                                                  HelperClass.show_alert_Dialog(
                                                      context, "processing");
                                                  var response =
                                                      await ApisServices.removeJob(
                                                          postId: postId,
                                                          token: SharedPrefs
                                                              .getAccessToken());
                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop();

                                                  if (response != null) {
                                                    if (response.status ==
                                                        200) {
                                                      recruitersApplyBloc.add(
                                                          RecruitersApplyEvent(
                                                              HelperClass
                                                                  .NOT_APPLIED_JOB_ID));
                                                      if (response
                                                              .recruitersPostModel !=
                                                          null) {
                                                        recruitersJobsActivePostsBloc
                                                            .add(
                                                                RecruitersJobsActivePostsApplyRemoveJobEvent(
                                                          recruitersPostModel:
                                                              response
                                                                  .recruitersPostModel!,
                                                        ));

                                                        recruitersBloc.url =
                                                            "${ApisServices.BASE_URL}${AppliedJobsScreen.APPLIED_JOBS_API}";
                                                        recruitersBloc
                                                            .recruitersPostsListAll
                                                            .clear();
                                                        recruitersBloc.add(
                                                            RecruitersLoadingEvent());
                                                      }
                                                      HelperClass.show_snackbar(
                                                          context,
                                                          response.message ??
                                                              "Application deleted for this job");
                                                    } else {
                                                      HelperClass.show_snackbar(
                                                          context,
                                                          response.message ??
                                                              HelperClass
                                                                  .ERROR_TEXT);
                                                    }
                                                  } else {
                                                    HelperClass.show_snackbar(
                                                        context,
                                                        HelperClass.ERROR_TEXT);
                                                  }
                                                },
                                                child: Text(
                                                  "Delete",
                                                  style: GoogleFonts.workSans(
                                                    textStyle: TextStyle(
                                                      color:
                                                          MyColors.WHITE_COLOR,
                                                      fontSize: 14.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      );
                                      showDialog(
                                        context: context,
                                        builder: (context) => alert,
                                      );
                                    },
                                    child: const Icon(
                                      FontAwesomeIcons.trash,
                                      color: MyColors.RED_COLOR,
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink(),
                        ],
                      );
                    },
                  ),
                ),
              ],
            );
          }
          return Container();
        },
      ),
    );
  }
}
