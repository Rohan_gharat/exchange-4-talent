import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';

import '../profile/user_details_screen.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  static const SearchRoute = "searchRoute";

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  bool isError = false, isLoading = false;
  List<UserModel> searchUsersList = [];

  String? url, token;

  var searchController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    token = SharedPrefs.getAccessToken();
    url = "${ApisServices.BASE_URL}v2/search";

    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        if (!isLoading && url != null) {
          loadData();
        }
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    scrollController.dispose();
  }

  void loadData() async {

    isError = false;
    isLoading = true;
    setState(() {});
    var response = await ApisServices.searchUserPagination(
        url: url, token: token, searchText: searchController.text.trim());
    if (response != null) {
      if (response.tradeBarterDataModel != null &&
          response.tradeBarterDataModel!.tradeBartersList != null) {
        isLoading = false;
        searchUsersList
            .addAll(response.tradeBarterDataModel!.tradeBartersList ?? []);
        url = response.tradeBarterDataModel!.nextPageUrl;
        setState(() {});
      }
    } else {
      isLoading = false;
      setState(() {});
      if (searchUsersList.isEmpty) {
        HelperClass.show_snackbar(context, HelperClass.ERROR_TEXT);
        isError = true;
        setState(() {});
      }
    }
  }

  void validateSearchField() {
    if (searchController.text.trim().isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("please type something to search"),
        duration: Duration(
          seconds: 1,
        ),
      ));
    } else {
      FocusManager.instance.primaryFocus?.unfocus();
      url = "${ApisServices.BASE_URL}v2/search";
      searchUsersList.clear();
      loadData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
            elevation: 0,
            title: Text(
              "Search",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.WHITE_COLOR,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            actions: [
              DrawerIconWidget(
                on_tap: () {
                  _scaffoldKey.currentState!.openEndDrawer();
                },
              ),
            ],
          ),
          endDrawer: DrawerMenu(),
          body: Padding(
            padding: const EdgeInsets.all(
              16.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyColors.BG_COLOR_TEXT_FILED,
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        margin: EdgeInsets.only(
                          top: 10.0,
                        ),
                        // padding: EdgeInsets.all(
                        //   10.0,
                        // ),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                // autofocus: true,
                                controller: searchController,
                                decoration: InputDecoration(
                                  // isDense: true,
                                  // Added this
                                  contentPadding: EdgeInsets.all(10.0),
                                  filled: true,
                                  fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                  hintText: "Search",
                                  hintStyle: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.GREY_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                    ),
                                    borderRadius: BorderRadius.circular(
                                      16.0,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                    ),
                                    borderRadius: BorderRadius.circular(
                                      16.0,
                                    ),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                    ),
                                    borderRadius: BorderRadius.circular(
                                      16.0,
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                    ),
                                    borderRadius: BorderRadius.circular(
                                      16.0,
                                    ),
                                  ),
                                  counterStyle: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 10.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  border: InputBorder.none,
                                  counterText: "",
                                ),
                                style: GoogleFonts.workSans(
                                  textStyle: TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                cursorColor: MyColors.WHITE_COLOR,
                                textInputAction: TextInputAction.done,
                                keyboardType: TextInputType.text,
                                onEditingComplete: () {
                                  validateSearchField();
                                },
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                validateSearchField();
                              },
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  right: 8.0,
                                ),
                                child: Icon(
                                  Icons.search,
                                  color: MyColors.ORANGE_COLOR,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: SingleChildScrollView(
                    controller: scrollController,
                    child: Column(
                      children: [
                        searchUsersList.isNotEmpty
                            ? SingleChildScrollView(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    top: 16.0,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      GridView.builder(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount: searchUsersList.length,
                                        gridDelegate:
                                            SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          crossAxisSpacing: 15.0,
                                          mainAxisSpacing: 15.0,
                                          childAspectRatio: 0.7,
                                        ),
                                        itemBuilder: (ctx, index) {
                                          return GestureDetector(
                                            onTap: () {
                                              FocusManager.instance.primaryFocus
                                                  ?.unfocus();
                                              // Navigator.pushNamed(context,
                                              //     UserDetailsScreen.USER_DETAILS_ROUTE);
                                              Navigator.pushNamed(
                                                  context,
                                                  UserDetailsScreen
                                                      .USER_DETAILS_ROUTE,
                                                  arguments: {
                                                    UserDetailsScreen.USER_ID:
                                                        searchUsersList[index]
                                                            .id,
                                                    UserDetailsScreen.IS_ME:
                                                        false,
                                                  });
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  18.0,
                                                ),
                                                border: Border.all(
                                                  width: 5.0,
                                                  color: searchUsersList[index]
                                                                  .color_code !=
                                                              null &&
                                                          searchUsersList[index]
                                                                  .color_code
                                                                  .trim() !=
                                                              ""
                                                      ? HexColor.fromHex(
                                                          searchUsersList[index]
                                                              .color_code
                                                              .trim())
                                                      : Colors.transparent,
                                                ),
                                              ),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: MyColors
                                                      .BG_COLOR_TEXT_FILED,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    18.0,
                                                  ),
                                                ),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        9.0,
                                                      ),
                                                      child: searchUsersList[
                                                                          index]
                                                                      .avatar !=
                                                                  null &&
                                                              searchUsersList[
                                                                          index]
                                                                      .avatar !=
                                                                  ""
                                                          ? CachedNetworkImage(
                                                              alignment:
                                                                  Alignment
                                                                      .topCenter,
                                                              imageUrl:
                                                                  searchUsersList[
                                                                          index]
                                                                      .avatar,
                                                              fit: BoxFit.cover,
                                                              height: 120.0,
                                                              width:
                                                                  MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                              placeholder:
                                                                  (context,
                                                                          url) =>
                                                                      Image
                                                                          .asset(
                                                                HelperClass
                                                                    .APP_LOGO_PATH,
                                                                fit: BoxFit
                                                                    .cover,
                                                                height: 120.0,
                                                                width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                              ),
                                                              errorWidget: (context,
                                                                      url,
                                                                      error) =>
                                                                  Image.asset(
                                                                HelperClass
                                                                    .APP_LOGO_PATH,
                                                                fit: BoxFit
                                                                    .cover,
                                                                height: 120.0,
                                                                width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                              ),
                                                            )
                                                          : Image.asset(
                                                              HelperClass
                                                                  .APP_LOGO_PATH,
                                                              fit: BoxFit.cover,
                                                              height: 120.0,
                                                              width:
                                                                  MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                            ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        top: 8.0,
                                                      ),
                                                      child: Text(
                                                        searchUsersList[index]
                                                                .name ??
                                                            "",
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          textStyle: const TextStyle(
                                                            color: MyColors
                                                                .GREY_COLOR1,
                                                            fontSize: 14.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                        maxLines: 2,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ),
                                                    RatingBar.builder(
                                                      initialRating:
                                                          double.parse(
                                                              searchUsersList[
                                                                          index]
                                                                      .rating ??
                                                                  "0.0"),
                                                      minRating: 1,
                                                      direction:
                                                          Axis.horizontal,
                                                      allowHalfRating: true,
                                                      itemCount: 5,
                                                      itemSize: 18,
                                                      unratedColor:
                                                          MyColors.GREY_COLOR1,
                                                      itemPadding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 1.0),
                                                      itemBuilder:
                                                          (context, _) =>
                                                              const Icon(
                                                        Icons.star,
                                                        color: MyColors
                                                            .ORANGE_COLOR,
                                                      ),
                                                      onRatingUpdate: (rating) {
                                                      },
                                                      ignoreGestures: true,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : Container(
                                margin: const EdgeInsets.only(
                                  top: 100.0,
                                ),
                                child: !isLoading
                                    ? isError
                                        ? CustomErrorWidget(
                                            on_tap: () {
                                              loadData();
                                            },
                                          )
                                        : NoDataWidget(
                                            on_tap: () {
                                              loadData();
                                            },
                                          )
                                    : Container(),
                              ),
                        Center(
                          child: isLoading
                              ? const CircularIndicatorWidget()
                              : Container(),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
