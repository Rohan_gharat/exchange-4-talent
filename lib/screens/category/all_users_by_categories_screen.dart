
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/screens/category/subcategories_screen.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/screens/widgets/usersListWidget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../myColors/myColors.dart';
import '../../restService/apis_services.dart';
import '../../utils/sharePrefs.dart';
import '../widgets/circular_indicator_widget.dart';
import '../widgets/error_widget.dart';

class AllUsersByCategoriesScreen extends StatefulWidget {
  AllUsersByCategoriesScreen({Key? key, this.mapData}) : super(key: key);
  static const ALL_USERS_BY_CATEGORIES_ROUTE = "allUsersBycategoriesRoute";

  Map? mapData;

  @override
  State<AllUsersByCategoriesScreen> createState() =>
      _AllUsersByCategoriesScreenState();
}

class _AllUsersByCategoriesScreenState
    extends State<AllUsersByCategoriesScreen> {
  bool isError = false, isLoading = true;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  List<UserModel> usersList = [];
  String? url = "", token;
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("AllUsersByCategoriesScreen: map data: ${widget.mapData}");

    token = SharedPrefs.getAccessToken();
    url = "${ApisServices.BASE_URL}v3/getUsersByCategory";

    loadData();

    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        if (!isLoading && url != null) {
          loadData();
        }
      }
    });
  }

  void loadData() async {

    isError = false;
    isLoading = true;
    setState(() {});
    var response = await ApisServices.getAllUsersByCategory(
      url: url,
      token: token,
      category_id: widget.mapData![SubCategoriesScreen.CATEGORY_ID],
    );
    if (response != null) {
      if (response.tradeBarterDataModel != null &&
          response.tradeBarterDataModel!.tradeBartersList != null) {
        isLoading = false;
        usersList.addAll(response.tradeBarterDataModel!.tradeBartersList ?? []);
        url = response.tradeBarterDataModel!.nextPageUrl;
        setState(() {});
      }
    } else {
      isLoading = false;
      setState(() {});
      if (usersList.isEmpty) {
        HelperClass.show_snackbar(context, HelperClass.ERROR_TEXT);
        isError = true;
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            widget.mapData![SubCategoriesScreen.TITLE] ?? "",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
          actions: [
            DrawerIconWidget(
              on_tap: () {
                _scaffoldKey.currentState!.openEndDrawer();
              },
            ),
          ],
        ),
        endDrawer: DrawerMenu(),
        body: SingleChildScrollView(
          controller: scrollController,
          child: Padding(
            padding: EdgeInsets.all(
              16.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                usersList.isNotEmpty
                    ? UsersListWidget(usersList: usersList)
                    : Container(
                        margin: const EdgeInsets.only(
                          top: 100.0,
                        ),
                        child: !isLoading
                            ? isError
                                ? CustomErrorWidget(
                                    on_tap: () {
                                      loadData();
                                    },
                                  )
                                : NoDataWidget(
                                    on_tap: () {
                                      loadData();
                                    },
                                  )
                            : Container(),
                      ),

                Center(
                  child:
                      isLoading ? const CircularIndicatorWidget() : Container(),
                ),
                SizedBox(
                  height: 30.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
