import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';

import '../search/search_screen.dart';

class AllUsersScreen extends StatefulWidget {
  AllUsersScreen({
    super.key,
    required this.mapData,
  });

  Map? mapData;

  static const All_USERS_ROUTE = "SubCategoriesScreenRoute";

  static const ID_KEY = "Id";
  static const TITLE_KEY = "title";
  static const IS_ALL_USERS_KEY = "isAllUsers";

  @override
  State<AllUsersScreen> createState() => _AllUsersScreenState();
}

class _AllUsersScreenState extends State<AllUsersScreen> {
  bool isError = false, isLoading = true;
  List<UserModel> usersList = [];
  String? url = "", token;
  int id = -1;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    id = widget.mapData![AllUsersScreen.ID_KEY] ?? -1;
    token = SharedPrefs.getAccessToken();
    url = widget.mapData![AllUsersScreen.IS_ALL_USERS_KEY]
        ? "${ApisServices.BASE_URL}v2/users"
        : "${ApisServices.BASE_URL}v2/subscriptionByusers";

    loadData();

    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        if (!isLoading && url != null) {
          loadData();
        }
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    scrollController.dispose();
  }

  loadData() async {
    print("all users screen url: $url");

    isError = false;
    isLoading = true;
    setState(() {});
    var response = await ApisServices.getAllUsersPagination(
        url: url,
        token: token,
        isAllUsers: widget.mapData![AllUsersScreen.IS_ALL_USERS_KEY],
        subscriptionId: id);
    if (response != null) {
      if (response.tradeBarterDataModel != null &&
          response.tradeBarterDataModel!.tradeBartersList != null) {
        isLoading = false;
        usersList.addAll(response.tradeBarterDataModel!.tradeBartersList ?? []);
        url = response.tradeBarterDataModel!.nextPageUrl;
        setState(() {});
      }
    } else {
      isLoading = false;
      setState(() {});
      if (usersList.isEmpty) {
        HelperClass.show_snackbar(context, HelperClass.ERROR_TEXT);
        isError = true;
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            widget.mapData![AllUsersScreen.TITLE_KEY],
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
          actions: [
            // ProfileIconWidget(),
            DrawerIconWidget(
              on_tap: () {
                _scaffoldKey.currentState!.openEndDrawer();
              },
            ),
          ],
        ),
        endDrawer: DrawerMenu(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, SearchScreen.SearchRoute);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyColors.BG_COLOR_TEXT_FILED,
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        padding: EdgeInsets.all(
                          10.0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Search",
                              style: GoogleFonts.workSans(
                                textStyle: TextStyle(
                                  color: MyColors.GREY_COLOR,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Icon(
                              Icons.search,
                              color: MyColors.ORANGE_COLOR,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Expanded(
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  children: [
                    Container(
                      child: usersList.isNotEmpty
                          ? GridView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: usersList.length,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                crossAxisSpacing: 15.0,
                                mainAxisSpacing: 15.0,
                                childAspectRatio: 0.8,
                              ),
                              itemBuilder: (ctx, index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.pushNamed(context,
                                        UserDetailsScreen.USER_DETAILS_ROUTE,
                                        arguments: {
                                          UserDetailsScreen.USER_ID:
                                              usersList[index].id,
                                          UserDetailsScreen.IS_ME: false,
                                        });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                        18.0,
                                      ),
                                      border: Border.all(
                                        width: 5.0,
                                        color: usersList[index].color_code !=
                                                    null &&
                                                usersList[index]
                                                        .color_code
                                                        .trim() !=
                                                    ""
                                            ? HexColor.fromHex(usersList[index]
                                                .color_code
                                                .trim())
                                            : Colors.transparent,
                                      ),
                                    ),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                        borderRadius: BorderRadius.circular(
                                          18.0,
                                        ),
                                      ),
                                      // padding: EdgeInsets.all(5.0,),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          ClipRRect(
                                            borderRadius: BorderRadius.circular(
                                              14.0,
                                            ),
                                            child: usersList[index].avatar !=
                                                        null &&
                                                    usersList[index].avatar !=
                                                        ""
                                                ? CachedNetworkImage(
                                                    alignment:
                                                        Alignment.topCenter,
                                                    imageUrl:
                                                        usersList[index].avatar,
                                                    fit: BoxFit.cover,
                                                    height: 120.0,
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    placeholder:
                                                        (context, url) =>
                                                            Image.asset(
                                                      HelperClass.APP_LOGO_PATH,
                                                      fit: BoxFit.cover,
                                                      height: 120.0,
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                    ),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Image.asset(
                                                      HelperClass.APP_LOGO_PATH,
                                                      fit: BoxFit.cover,
                                                      height: 120.0,
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                    ),
                                                  )
                                                : Image.asset(
                                                    HelperClass.APP_LOGO_PATH,
                                                    fit: BoxFit.cover,
                                                    height: 120.0,
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                  ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 8.0,
                                              left: 4.0,
                                              right: 4.0,
                                            ),
                                            child: Text(
                                              usersList[index].name ?? "",
                                              style: GoogleFonts.montserrat(
                                                textStyle: const TextStyle(
                                                  color: MyColors.GREY_COLOR1,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.bold,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),
                                              textAlign: TextAlign.center,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          RatingBar.builder(
                                            initialRating: double.parse(
                                                usersList[index].rating ??
                                                    "0.0"),
                                            minRating: 1,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemSize: 18,
                                            unratedColor: MyColors.GREY_COLOR1,
                                            itemPadding: EdgeInsets.symmetric(
                                                horizontal: 1.0),
                                            itemBuilder: (context, _) =>
                                                const Icon(
                                              Icons.star,
                                              color: MyColors.ORANGE_COLOR,
                                            ),
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                            },
                                            ignoreGestures: true,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            )
                          : Container(
                              margin: const EdgeInsets.only(
                                top: 100.0,
                              ),
                              child: !isLoading
                                  ? isError
                                      ? CustomErrorWidget(
                                          on_tap: () {
                                            loadData();
                                          },
                                        )
                                      : NoDataWidget(
                                          on_tap: () {
                                            loadData();
                                          },
                                        )
                                  : Container(),
                            ),
                    ),
                    Center(
                      child: isLoading
                          ? const CircularIndicatorWidget()
                          : Container(),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
