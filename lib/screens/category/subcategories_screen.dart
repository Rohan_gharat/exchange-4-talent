import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/model/categoriesModel/subcategory_by_category_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/category/all_categories_screen.dart';
import 'package:e4t/screens/category/all_users_by_categories_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/screens/widgets/usersImageSliders.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../search/search_screen.dart';
import '../widgets/subCategoriesWidget.dart';

class SubCategoriesScreen extends StatefulWidget {
  SubCategoriesScreen({super.key, this.mapData});

  Map? mapData;

  static const SubCategoriesRoute = "categoriesRoute";
  static const TITLE = "title";
  static const CATEGORY_ID = "categoryId";

  @override
  State<SubCategoriesScreen> createState() => _SubCategoriesScreenState();
}

class _SubCategoriesScreenState extends State<SubCategoriesScreen> {
  SubcategoryByCategoryModel? subcategoryByCategoryModel;
  bool isError = false, isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print("SubCategoriesScreen: map data: ${widget.mapData}");
    loadData();
  }

  void loadData() {
    isError = false;
    isLoading = true;
    setState(() {});
    ApisServices.getSubCategoriesByCategory(
            token: SharedPrefs.getAccessToken(),
            categoryId: widget.mapData![SubCategoriesScreen.CATEGORY_ID])
        .then((value) {
      isLoading = false;
      if (value != null) {
        subcategoryByCategoryModel = value;
        isError = false;
        setState(() {});
      } else {
        isError = true;

        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // var categoryBloc = BlocProvider.of<CategoryBloc>(context);
    // categoryBloc
    //     .add(SubcategoryLoadingEvent(widget.mapData![SubCategoriesScreen.CATEGORY_ID], widget.mapData![SubCategoriesScreen.TITLE]));
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            widget.mapData![SubCategoriesScreen.TITLE] ?? "",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
          actions: [
            // ProfileIconWidget(),
          ],
        ),
        body: isError
            ? CustomErrorWidget(
                on_tap: () {
                  loadData();
                },
              )
            : isLoading
                ? CircularIndicatorWidget()
                : SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(
                        16.0,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, SearchScreen.SearchRoute);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    padding: EdgeInsets.all(
                                      10.0,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Search",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.GREY_COLOR,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                        Icon(
                                          Icons.search,
                                          color: MyColors.ORANGE_COLOR,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                // SizedBox(
                                //   width: 10.0,
                                // ),
                                // Container(
                                //   decoration: BoxDecoration(
                                //     color: MyColors.WHITE_COLOR,
                                //     borderRadius: BorderRadius.circular(
                                //       16.0,
                                //     ),
                                //   ),
                                //   padding: EdgeInsets.all(
                                //     10.0,
                                //   ),
                                //   child: Icon(
                                //     Icons.sort,
                                //     color: MyColors.ORANGE_COLOR,
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 16.0,
                          ),
                          subcategoryByCategoryModel != null &&
                                  subcategoryByCategoryModel?.usersList !=
                                      null &&
                                  subcategoryByCategoryModel!
                                      .usersList.isNotEmpty
                              ? UsersImageSliders(
                                  usersList:
                                      subcategoryByCategoryModel!.usersList)
                              : Container(),
                          // subcategoryByCategoryModel?.subcategoriesList?.isNotEmpty?
                          subcategoryByCategoryModel != null &&
                                  subcategoryByCategoryModel!
                                          .subcategoriesList !=
                                      null &&
                                  subcategoryByCategoryModel!
                                      .subcategoriesList!.isNotEmpty
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // Container(
                                    //   margin: EdgeInsets.only(
                                    //     top: 16.0,
                                    //   ),
                                    //   child: GridView.builder(
                                    //     shrinkWrap: true,
                                    //     physics: NeverScrollableScrollPhysics(),
                                    //     itemCount: subcategoryByCategoryModel
                                    //         ?.subcategoriesList?.length,
                                    //     gridDelegate:
                                    //         SliverGridDelegateWithFixedCrossAxisCount(
                                    //       crossAxisCount: 2,
                                    //       crossAxisSpacing: 15.0,
                                    //       mainAxisSpacing: 15.0,
                                    //       childAspectRatio: 0.8,
                                    //     ),
                                    //     itemBuilder: (ctx, index) {
                                    //       return GestureDetector(
                                    //         onTap: () {
                                    //           Navigator.pushNamed(
                                    //               context,
                                    //               AllUsersScreen
                                    //                   .All_USERS_ROUTE,
                                    //               arguments: {
                                    //                 AllUsersScreen
                                    //                         .IS_ALL_USERS_KEY:
                                    //                     false,
                                    //                 AllUsersScreen.TITLE_KEY:
                                    //                     subcategoryByCategoryModel
                                    //                             ?.subcategoriesList?[
                                    //                                 index]
                                    //                             .title ??
                                    //                         "",
                                    //                 AllUsersScreen
                                    //                         .SUBCATEGORY_ID_KEY:
                                    //                     subcategoryByCategoryModel
                                    //                         ?.subcategoriesList?[
                                    //                             index]
                                    //                         .id
                                    //               });
                                    //         },
                                    //         child: Container(
                                    //           decoration: BoxDecoration(
                                    //             color: MyColors
                                    //                 .BG_COLOR_TEXT_FILED,
                                    //             borderRadius:
                                    //                 BorderRadius.circular(
                                    //               18.0,
                                    //             ),
                                    //           ),
                                    //           child: Column(
                                    //             mainAxisAlignment:
                                    //                 MainAxisAlignment.start,
                                    //             crossAxisAlignment:
                                    //                 CrossAxisAlignment.center,
                                    //             children: [
                                    //               ClipRRect(
                                    //                 borderRadius:
                                    //                     BorderRadius.circular(
                                    //                   9.0,
                                    //                 ),
                                    //                 child: subcategoryByCategoryModel
                                    //                                 ?.subcategoriesList?[
                                    //                                     index]
                                    //                                 .image !=
                                    //                             null &&
                                    //                         subcategoryByCategoryModel
                                    //                                 ?.subcategoriesList?[
                                    //                                     index]
                                    //                                 .image !=
                                    //                             ""
                                    //                     ? CachedNetworkImage(
                                    //                         imageUrl:
                                    //                             subcategoryByCategoryModel
                                    //                                 ?.subcategoriesList?[
                                    //                                     index]
                                    //                                 .image,
                                    //                         fit: BoxFit.cover,
                                    //                         height: 120.0,
                                    //                         width:
                                    //                             MediaQuery.of(
                                    //                                     context)
                                    //                                 .size
                                    //                                 .width,
                                    //                         placeholder:
                                    //                             (context,
                                    //                                     url) =>
                                    //                                 Image.asset(
                                    //                           "images/circular_image_placeholder.jpg",
                                    //                           fit: BoxFit.cover,
                                    //                           height: 120.0,
                                    //                           width:
                                    //                               MediaQuery.of(
                                    //                                       context)
                                    //                                   .size
                                    //                                   .width,
                                    //                         ),
                                    //                         errorWidget:
                                    //                             (context, url,
                                    //                                     error) =>
                                    //                                 Image.asset(
                                    //                           "images/circular_image_placeholder.jpg",
                                    //                           fit: BoxFit.cover,
                                    //                           height: 120.0,
                                    //                           width:
                                    //                               MediaQuery.of(
                                    //                                       context)
                                    //                                   .size
                                    //                                   .width,
                                    //                         ),
                                    //                       )
                                    //                     : Image.asset(
                                    //                         "images/circular_image_placeholder.jpg",
                                    //                         fit: BoxFit.cover,
                                    //                         height: 120.0,
                                    //                         width:
                                    //                             MediaQuery.of(
                                    //                                     context)
                                    //                                 .size
                                    //                                 .width,
                                    //                       ),
                                    //               ),
                                    //               Padding(
                                    //                 padding:
                                    //                     const EdgeInsets.only(
                                    //                   top: 8.0,
                                    //                 ),
                                    //                 child: Text(
                                    //                   subcategoryByCategoryModel
                                    //                           ?.subcategoriesList?[
                                    //                               index]
                                    //                           .title ??
                                    //                       "",
                                    //                   style: GoogleFonts
                                    //                       .montserrat(
                                    //                     textStyle: TextStyle(
                                    //                       color: MyColors
                                    //                           .GREY_COLOR1,
                                    //                       fontSize: 14.0,
                                    //                       fontWeight:
                                    //                           FontWeight.bold,
                                    //                       overflow: TextOverflow
                                    //                           .ellipsis,
                                    //                     ),
                                    //                   ),
                                    //                   maxLines: 2,
                                    //                   overflow:
                                    //                       TextOverflow.ellipsis,
                                    //                 ),
                                    //               ),
                                    //             ],
                                    //           ),
                                    //         ),
                                    //       );
                                    //     },
                                    //   ),
                                    // ),
                                    SubCategoriesWidget(
                                        subcategoryByCategoryModel:
                                            subcategoryByCategoryModel!),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 16.0,
                                      ),
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          minimumSize: Size.fromHeight(
                                            40,
                                          ),
                                          // fromHeight use double.infinity as width and 40 is the height
                                          backgroundColor:
                                              MyColors.ORANGE_COLOR,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18),
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                            context,
                                            AllUsersByCategoriesScreen
                                                .ALL_USERS_BY_CATEGORIES_ROUTE,
                                            arguments: widget.mapData,
                                          );
                                        },
                                        child: Text(
                                          "Show more",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.WHITE_COLOR,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : Padding(
                                  padding: const EdgeInsets.all(
                                    16.0,
                                  ),
                                  child: NoDataWidget(
                                    on_tap: () {
                                      loadData();
                                    },
                                  ),
                                ),

                          Container(
                            // color: Colors.red,

                            margin: EdgeInsets.only(
                              top: 16.0,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Relevant Categories",
                                      style: GoogleFonts.montserrat(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR1,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.pushNamed(
                                            context,
                                            AllCategoriesScreen
                                                .ALL_CATEGORIES_ROUTE);
                                      },
                                      child: Text(
                                        "View all",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.ORANGE_COLOR,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                ListView.builder(
                                  itemCount: subcategoryByCategoryModel
                                      ?.randomCategoriesList?.length,
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (ctx, index) {
                                    return GestureDetector(
                                      onTap: () {
                                        Navigator.pushNamed(
                                            context,
                                            SubCategoriesScreen
                                                .SubCategoriesRoute,
                                            arguments: {
                                              SubCategoriesScreen.CATEGORY_ID:
                                                  subcategoryByCategoryModel
                                                      ?.randomCategoriesList?[
                                                          index]
                                                      .id,
                                              SubCategoriesScreen.TITLE:
                                                  subcategoryByCategoryModel
                                                      ?.randomCategoriesList?[
                                                          index]
                                                      .title
                                            });
                                      },
                                      child: Container(
                                        // width: 150,
                                        margin: EdgeInsets.only(
                                          top: 16.0,
                                        ),
                                        padding: EdgeInsets.all(
                                          10.0,
                                        ),
                                        decoration: BoxDecoration(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                          borderRadius: BorderRadius.circular(
                                            18.0,
                                          ),
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            ClipRRect(
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(
                                                  9.0,
                                                ),
                                                bottomLeft: Radius.circular(
                                                  9.0,
                                                ),
                                              ),
                                              child: subcategoryByCategoryModel
                                                              ?.randomCategoriesList?[
                                                                  index]
                                                              .image !=
                                                          null &&
                                                      subcategoryByCategoryModel
                                                              ?.randomCategoriesList?[
                                                                  index]
                                                              .image !=
                                                          ""
                                                  ? CachedNetworkImage(
                                                      imageUrl:
                                                          subcategoryByCategoryModel
                                                              ?.randomCategoriesList?[
                                                                  index]
                                                              .image,
                                                      fit: BoxFit.cover,
                                                      height: 60.0,
                                                      width: 60.0,
                                                      placeholder:
                                                          (context, url) =>
                                                              Image.asset(
                                                        HelperClass
                                                            .APP_LOGO_PATH,
                                                        fit: BoxFit.cover,
                                                        height: 60.0,
                                                        width: 60.0,
                                                      ),
                                                      errorWidget: (context,
                                                              url, error) =>
                                                          Image.asset(
                                                        HelperClass
                                                            .APP_LOGO_PATH,
                                                        fit: BoxFit.cover,
                                                        height: 60.0,
                                                        width: 60.0,
                                                      ),
                                                    )
                                                  : Image.asset(
                                                      HelperClass.APP_LOGO_PATH,
                                                      fit: BoxFit.cover,
                                                      height: 60.0,
                                                      width: 60.0,
                                                    ),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 8.0,
                                                ),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      subcategoryByCategoryModel
                                                              ?.randomCategoriesList?[
                                                                  index]
                                                              .title ??
                                                          "",
                                                      style:
                                                          GoogleFonts.workSans(
                                                        textStyle: TextStyle(
                                                          color: MyColors
                                                              .GREY_COLOR1,
                                                          fontSize: 16.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ),
                                                      maxLines: 2,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 5.0,
                                                      ),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          // RatingBar.builder(
                                                          //   initialRating: 3,
                                                          //   minRating: 1,
                                                          //   direction:
                                                          //       Axis.horizontal,
                                                          //   allowHalfRating:
                                                          //       true,
                                                          //   itemCount: 5,
                                                          //   itemSize: 18,
                                                          //   unratedColor:
                                                          //       MyColors
                                                          //           .GREY_COLOR1,
                                                          //   itemPadding: EdgeInsets
                                                          //       .symmetric(
                                                          //           horizontal:
                                                          //               1.0),
                                                          //   itemBuilder:
                                                          //       (context, _) =>
                                                          //           const Icon(
                                                          //     Icons.star,
                                                          //     color: MyColors
                                                          //         .ORANGE_COLOR,
                                                          //   ),
                                                          //   onRatingUpdate:
                                                          //       (rating) {
                                                          //     print(rating);
                                                          //   },
                                                          //   ignoreGestures:
                                                          //       true,
                                                          // ),
                                                          Text(
                                                            "View",
                                                            style: GoogleFonts
                                                                .montserrat(
                                                              textStyle:
                                                                  TextStyle(
                                                                color: MyColors
                                                                    .ORANGE_COLOR,
                                                                fontSize: 12.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
      ),
    );
  }
}

// class SubcategoryBlcUI extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return BlocConsumer<CategoryBloc, CategoryState>(
//       listener: (context, state) {
//         // TODO: implement listener
//       },
//       builder: (context, state) {
//         if (state is SubCategoriesLoadingState) {
//           return const CircularIndicatorWidget();
//         } else if (state is SubCategoriesErrorState) {
//           return CustomErrorWidget(
//             on_tap: () {
//               categoryBloc.add(SubcategoryLoadingEvent(
//                   widget.mapData![SubCategoriesScreen.CATEGORY_ID],
//                   widget.mapData![SubCategoriesScreen.TITLE]));
//             },
//           );
//         } else if (state is SubCategoriesLoadedState) {
//           return SingleChildScrollView(
//             child: Padding(
//               padding: const EdgeInsets.all(
//                 16.0,
//               ),
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   GestureDetector(
//                     onTap: () {
//                       Navigator.pushNamed(context, SearchScreen.SearchRoute);
//                     },
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Expanded(
//                           child: Container(
//                             decoration: BoxDecoration(
//                               color: MyColors.BG_COLOR_TEXT_FILED,
//                               borderRadius: BorderRadius.circular(
//                                 16.0,
//                               ),
//                             ),
//                             padding: EdgeInsets.all(
//                               10.0,
//                             ),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 Text(
//                                   "Search",
//                                   style: GoogleFonts.workSans(
//                                     textStyle: TextStyle(
//                                       color: MyColors.GREY_COLOR,
//                                       fontSize: 16.0,
//                                       fontWeight: FontWeight.bold,
//                                     ),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   width: 10.0,
//                                 ),
//                                 Icon(
//                                   Icons.search,
//                                   color: MyColors.ORANGE_COLOR,
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                         // SizedBox(
//                         //   width: 10.0,
//                         // ),
//                         // Container(
//                         //   decoration: BoxDecoration(
//                         //     color: MyColors.WHITE_COLOR,
//                         //     borderRadius: BorderRadius.circular(
//                         //       16.0,
//                         //     ),
//                         //   ),
//                         //   padding: EdgeInsets.all(
//                         //     10.0,
//                         //   ),
//                         //   child: Icon(
//                         //     Icons.sort,
//                         //     color: MyColors.ORANGE_COLOR,
//                         //   ),
//                         // ),
//                       ],
//                     ),
//                   ),
//                   SizedBox(
//                     height: 16.0,
//                   ),
//                   ImageSliders(),
//                   Container(
//                     margin: EdgeInsets.only(
//                       top: 16.0,
//                     ),
//                     child: GridView.builder(
//                       shrinkWrap: true,
//                       physics: NeverScrollableScrollPhysics(),
//                       itemCount: state
//                           .subcategoryByCategoryModel.subcategoriesList?.length,
//                       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                         crossAxisCount: 2,
//                         crossAxisSpacing: 15.0,
//                         mainAxisSpacing: 15.0,
//                         childAspectRatio: 0.8,
//                       ),
//                       itemBuilder: (ctx, index) {
//                         return GestureDetector(
//                           onTap: () {
//                             Navigator.pushNamed(
//                                 context, AllUsersScreen.All_USERS_ROUTE);
//                           },
//                           child: Container(
//                             decoration: BoxDecoration(
//                               color: MyColors.BG_COLOR_TEXT_FILED,
//                               borderRadius: BorderRadius.circular(
//                                 18.0,
//                               ),
//                             ),
//                             child: Column(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: [
//                                 ClipRRect(
//                                   borderRadius: BorderRadius.circular(
//                                     9.0,
//                                   ),
//                                   child: state
//                                                   .subcategoryByCategoryModel
//                                                   .subcategoriesList?[index]
//                                                   .image !=
//                                               null &&
//                                           state
//                                                   .subcategoryByCategoryModel
//                                                   .subcategoriesList?[index]
//                                                   .image !=
//                                               ""
//                                       ? CachedNetworkImage(
//                                           imageUrl: state
//                                               .subcategoryByCategoryModel
//                                               .subcategoriesList?[index]
//                                               .image,
//                                           fit: BoxFit.cover,
//                                           height: 120.0,
//                                           width:
//                                               MediaQuery.of(context).size.width,
//                                           placeholder: (context, url) =>
//                                               Image.asset(
//                                             "images/circular_image_placeholder.jpg",
//                                             fit: BoxFit.cover,
//                                             height: 120.0,
//                                             width: MediaQuery.of(context)
//                                                 .size
//                                                 .width,
//                                           ),
//                                           errorWidget: (context, url, error) =>
//                                               Image.asset(
//                                             "images/circular_image_placeholder.jpg",
//                                             fit: BoxFit.cover,
//                                             height: 120.0,
//                                             width: MediaQuery.of(context)
//                                                 .size
//                                                 .width,
//                                           ),
//                                         )
//                                       : Image.asset(
//                                           "images/circular_image_placeholder.jpg",
//                                           fit: BoxFit.cover,
//                                           height: 120.0,
//                                           width:
//                                               MediaQuery.of(context).size.width,
//                                         ),
//                                 ),
//                                 Padding(
//                                   padding: const EdgeInsets.only(
//                                     top: 8.0,
//                                   ),
//                                   child: Text(
//                                     state.subcategoryByCategoryModel
//                                             .subcategoriesList?[index].title ??
//                                         "",
//                                     style: GoogleFonts.montserrat(
//                                       textStyle: TextStyle(
//                                         color: MyColors.GREY_COLOR1,
//                                         fontSize: 14.0,
//                                         fontWeight: FontWeight.bold,
//                                         overflow: TextOverflow.ellipsis,
//                                       ),
//                                     ),
//                                     maxLines: 2,
//                                     overflow: TextOverflow.ellipsis,
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         );
//                       },
//                     ),
//                   ),
//                   Container(
//                     margin: EdgeInsets.only(
//                       top: 16.0,
//                     ),
//                     child: ElevatedButton(
//                       style: ElevatedButton.styleFrom(
//                         minimumSize: Size.fromHeight(
//                           40,
//                         ),
//                         // fromHeight use double.infinity as width and 40 is the height
//                         backgroundColor: MyColors.ORANGE_COLOR,
//                         shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(18),
//                         ),
//                       ),
//                       onPressed: () {
//                         Navigator.pushNamed(
//                           context,
//                           AllSubCategoriesScreen.ALL_SUB_CATEGORIES_ROUTE,
//                         );
//                       },
//                       child: Text(
//                         "Show more",
//                         style: GoogleFonts.workSans(
//                           textStyle: TextStyle(
//                             color: MyColors.WHITE_COLOR,
//                             fontSize: 14.0,
//                             fontWeight: FontWeight.bold,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                   Container(
//                     // color: Colors.red,
//
//                     margin: EdgeInsets.only(
//                       top: 16.0,
//                     ),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Text(
//                               "Relevant Categories",
//                               style: GoogleFonts.montserrat(
//                                 textStyle: TextStyle(
//                                   color: MyColors.GREY_COLOR1,
//                                   fontSize: 16.0,
//                                   fontWeight: FontWeight.bold,
//                                 ),
//                               ),
//                             ),
//                             GestureDetector(
//                               onTap: () {
//                                 Navigator.pushNamed(context,
//                                     AllCategoriesScreen.ALL_CATEGORIES_ROUTE);
//                               },
//                               child: Text(
//                                 "View all",
//                                 style: GoogleFonts.workSans(
//                                   textStyle: TextStyle(
//                                     color: MyColors.ORANGE_COLOR,
//                                     fontSize: 14.0,
//                                     fontWeight: FontWeight.bold,
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ],
//                         ),
//                         ListView.builder(
//                           itemCount: state.subcategoryByCategoryModel
//                               .randomCategoriesList?.length,
//                           shrinkWrap: true,
//                           physics: NeverScrollableScrollPhysics(),
//                           itemBuilder: (ctx, index) {
//                             return GestureDetector(
//                               onTap: () {
//                                 Navigator.pushNamed(context,
//                                     SubCategoriesScreen.SubCategoriesRoute,
//                                     arguments: {
//                                       SubCategoriesScreen
//                                               .SubCategoriesScreen.CATEGORY_ID:
//                                           state.subcategoryByCategoryModel
//                                               .randomCategoriesList?[index].id,
//                                       SubCategoriesScreen
//                                               .SubCategoriesScreen.TITLE:
//                                           state
//                                               .subcategoryByCategoryModel
//                                               .randomCategoriesList?[index]
//                                               .title
//                                     });
//                               },
//                               child: Container(
//                                 // width: 150,
//                                 margin: EdgeInsets.only(
//                                   top: 16.0,
//                                 ),
//                                 padding: EdgeInsets.all(
//                                   10.0,
//                                 ),
//                                 decoration: BoxDecoration(
//                                   color: MyColors.BG_COLOR_TEXT_FILED,
//                                   borderRadius: BorderRadius.circular(
//                                     18.0,
//                                   ),
//                                 ),
//                                 child: Row(
//                                   mainAxisAlignment: MainAxisAlignment.start,
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: [
//                                     ClipRRect(
//                                       borderRadius: BorderRadius.only(
//                                         topLeft: Radius.circular(
//                                           9.0,
//                                         ),
//                                         bottomLeft: Radius.circular(
//                                           9.0,
//                                         ),
//                                       ),
//                                       child: Image.asset(
//                                         "images/circular_image_placeholder.jpg",
//                                         fit: BoxFit.cover,
//                                         height: 60.0,
//                                         width: 60.0,
//                                       ),
//                                     ),
//                                     Expanded(
//                                       child: Padding(
//                                         padding: const EdgeInsets.only(
//                                           left: 8.0,
//                                         ),
//                                         child: Column(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.start,
//                                           crossAxisAlignment:
//                                               CrossAxisAlignment.start,
//                                           children: [
//                                             Text(
//                                               state
//                                                   .subcategoryByCategoryModel
//                                                   .randomCategoriesList?[index]
//                                                   .title,
//                                               style: GoogleFonts.workSans(
//                                                 textStyle: TextStyle(
//                                                   color: MyColors.GREY_COLOR1,
//                                                   fontSize: 16.0,
//                                                   fontWeight: FontWeight.bold,
//                                                   overflow:
//                                                       TextOverflow.ellipsis,
//                                                 ),
//                                               ),
//                                               maxLines: 2,
//                                               overflow: TextOverflow.ellipsis,
//                                             ),
//                                             Container(
//                                               margin: EdgeInsets.only(
//                                                 top: 5.0,
//                                               ),
//                                               child: Row(
//                                                 mainAxisAlignment:
//                                                     MainAxisAlignment
//                                                         .spaceBetween,
//                                                 crossAxisAlignment:
//                                                     CrossAxisAlignment.start,
//                                                 children: [
//                                                   RatingBar.builder(
//                                                     initialRating: 3,
//                                                     minRating: 1,
//                                                     direction: Axis.horizontal,
//                                                     allowHalfRating: true,
//                                                     itemCount: 5,
//                                                     itemSize: 18,
//                                                     unratedColor:
//                                                         MyColors.GREY_COLOR1,
//                                                     itemPadding:
//                                                         EdgeInsets.symmetric(
//                                                             horizontal: 1.0),
//                                                     itemBuilder: (context, _) =>
//                                                         const Icon(
//                                                       Icons.star,
//                                                       color:
//                                                           MyColors.ORANGE_COLOR,
//                                                     ),
//                                                     onRatingUpdate: (rating) {
//                                                       print(rating);
//                                                     },
//                                                     ignoreGestures: true,
//                                                   ),
//                                                   Text(
//                                                     "View",
//                                                     style:
//                                                         GoogleFonts.montserrat(
//                                                       textStyle: TextStyle(
//                                                         color: MyColors
//                                                             .ORANGE_COLOR,
//                                                         fontSize: 16.0,
//                                                         fontWeight:
//                                                             FontWeight.bold,
//                                                       ),
//                                                     ),
//                                                   ),
//                                                 ],
//                                               ),
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             );
//                           },
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           );
//         }
//         return Scaffold();
//       },
//     );
//   }
// }
