import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/model/categoriesModel/users_by_category_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/category/all_categories_screen.dart';
import 'package:e4t/screens/category/all_users_by_categories_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/screens/widgets/usersImageSliders.dart';
import 'package:e4t/screens/widgets/usersListWidget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../search/search_screen.dart';

class UsersByCategoriesScreen extends StatefulWidget {
  UsersByCategoriesScreen({super.key, this.mapData});

  Map? mapData;

  static const USER_BY_CATEGORY_SCREEN_Route = "userByCategoryScreenRoute";
  static const TITLE = "title";
  static const CATEGORY_ID = "categoryId";

  @override
  State<UsersByCategoriesScreen> createState() =>
      _UsersByCategoriesScreenState();
}

class _UsersByCategoriesScreenState extends State<UsersByCategoriesScreen> {
  UsersByCategoryModel? usersByCategoryModel;
  bool isError = false, isLoading = true;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print("SubCategoriesScreen: map data: ${widget.mapData}");
    loadData();
  }

  void loadData() {
    isError = false;
    isLoading = true;
    setState(() {});
    ApisServices.getUsersByCategory(
            token: SharedPrefs.getAccessToken(),
            categoryId: widget.mapData![UsersByCategoriesScreen.CATEGORY_ID])
        .then((value) {
      isLoading = false;
      if (value != null) {
        usersByCategoryModel = value;
        isError = false;
        setState(() {});
      } else {
        isError = true;

        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
     return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            widget.mapData![UsersByCategoriesScreen.TITLE] ?? "",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
          actions: [
            // ProfileIconWidget(),
            DrawerIconWidget(
              on_tap: () {
                _scaffoldKey.currentState!.openEndDrawer();
              },
            ),
          ],
        ),
        endDrawer: DrawerMenu(),
        body: isError
            ? CustomErrorWidget(
                on_tap: () {
                  loadData();
                },
              )
            : isLoading
                ? CircularIndicatorWidget()
                : SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(
                        16.0,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, SearchScreen.SearchRoute);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    padding: EdgeInsets.all(
                                      10.0,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Search",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.GREY_COLOR,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                        Icon(
                                          Icons.search,
                                          color: MyColors.ORANGE_COLOR,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 16.0,
                          ),
                          usersByCategoryModel != null &&
                                  usersByCategoryModel?.usersList != null &&
                                  usersByCategoryModel!.usersList.isNotEmpty
                              ? UsersImageSliders(
                                  usersList: usersByCategoryModel!.usersList)
                              : Container(),
                          usersByCategoryModel != null &&
                                  usersByCategoryModel!.usersList.isNotEmpty
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 16.0,),
                                      child: UsersListWidget(
                                          usersList:
                                              usersByCategoryModel!.usersList),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 16.0,
                                      ),
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          minimumSize: Size.fromHeight(
                                            40,
                                          ),
                                          // fromHeight use double.infinity as width and 40 is the height
                                          backgroundColor:
                                              MyColors.ORANGE_COLOR,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18),
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                            context,
                                            AllUsersByCategoriesScreen
                                                .ALL_USERS_BY_CATEGORIES_ROUTE,
                                            arguments: widget.mapData,
                                          );
                                        },
                                        child: Text(
                                          "Show more",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.WHITE_COLOR,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : Padding(
                                  padding: const EdgeInsets.all(
                                    16.0,
                                  ),
                                  child: NoDataWidget(
                                    on_tap: () {
                                      loadData();
                                    },
                                  ),
                                ),

                          Container(
                            // color: Colors.red,

                            margin: EdgeInsets.only(
                              top: 16.0,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Relevant Categories",
                                      style: GoogleFonts.montserrat(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR1,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.pushNamed(
                                            context,
                                            AllCategoriesScreen
                                                .ALL_CATEGORIES_ROUTE);
                                      },
                                      child: Text(
                                        "View all",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.ORANGE_COLOR,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                ListView.builder(
                                  itemCount: usersByCategoryModel
                                      ?.randomCategoriesList?.length,
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (ctx, index) {
                                    return GestureDetector(
                                      onTap: () {
                                        Navigator.pushNamed(
                                            context,
                                            UsersByCategoriesScreen
                                                .USER_BY_CATEGORY_SCREEN_Route,
                                            arguments: {
                                              UsersByCategoriesScreen
                                                      .CATEGORY_ID:
                                                  usersByCategoryModel
                                                      ?.randomCategoriesList?[
                                                          index]
                                                      .id,
                                              UsersByCategoriesScreen.TITLE:
                                                  usersByCategoryModel
                                                      ?.randomCategoriesList?[
                                                          index]
                                                      .title
                                            });
                                      },
                                      child: Container(
                                        // width: 150,
                                        margin: EdgeInsets.only(
                                          top: 16.0,
                                        ),
                                        padding: EdgeInsets.all(
                                          10.0,
                                        ),
                                        decoration: BoxDecoration(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                          borderRadius: BorderRadius.circular(
                                            18.0,
                                          ),
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            ClipRRect(
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(
                                                  9.0,
                                                ),
                                                bottomLeft: Radius.circular(
                                                  9.0,
                                                ),
                                              ),
                                              child: usersByCategoryModel
                                                              ?.randomCategoriesList?[
                                                                  index]
                                                              .image !=
                                                          null &&
                                                      usersByCategoryModel
                                                              ?.randomCategoriesList?[
                                                                  index]
                                                              .image !=
                                                          ""
                                                  ? CachedNetworkImage(
                                                      imageUrl: usersByCategoryModel
                                                          ?.randomCategoriesList?[
                                                              index]
                                                          .image,
                                                      fit: BoxFit.cover,
                                                      height: 60.0,
                                                      width: 60.0,
                                                      placeholder:
                                                          (context, url) =>
                                                              Image.asset(
                                                        HelperClass
                                                            .APP_LOGO_PATH,
                                                        fit: BoxFit.cover,
                                                        height: 60.0,
                                                        width: 60.0,
                                                      ),
                                                      errorWidget: (context,
                                                              url, error) =>
                                                          Image.asset(
                                                        HelperClass
                                                            .APP_LOGO_PATH,
                                                        fit: BoxFit.cover,
                                                        height: 60.0,
                                                        width: 60.0,
                                                      ),
                                                    )
                                                  : Image.asset(
                                                      HelperClass.APP_LOGO_PATH,
                                                      fit: BoxFit.cover,
                                                      height: 60.0,
                                                      width: 60.0,
                                                    ),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 8.0,
                                                ),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      usersByCategoryModel
                                                              ?.randomCategoriesList?[
                                                                  index]
                                                              .title ??
                                                          "",
                                                      style:
                                                          GoogleFonts.workSans(
                                                        textStyle: TextStyle(
                                                          color: MyColors
                                                              .GREY_COLOR1,
                                                          fontSize: 16.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ),
                                                      maxLines: 2,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                        top: 5.0,
                                                      ),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            "View",
                                                            style: GoogleFonts
                                                                .montserrat(
                                                              textStyle:
                                                                  const TextStyle(
                                                                color: MyColors
                                                                    .ORANGE_COLOR,
                                                                fontSize: 12.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
      ),
    );
  }
}

