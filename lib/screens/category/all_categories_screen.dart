import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/screens/category/usersByCategories_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../blocks/allCategoryBloc/all_category_bloc.dart';
import '../../myColors/myColors.dart';

class AllCategoriesScreen extends StatelessWidget {
  AllCategoriesScreen({Key? key}) : super(key: key);
  static const ALL_CATEGORIES_ROUTE = "allCategoriesRoute";
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var allCategoriesBloc = BlocProvider.of<AllCategoryBloc>(context);
    allCategoriesBloc.add(AllCategoriesLoadingEvent());
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            "Categories",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
          actions: [
            // ProfileIconWidget(),
            DrawerIconWidget(
              on_tap: () {
                _scaffoldKey.currentState!.openEndDrawer();
              },
            ),
          ],
        ),
        endDrawer: DrawerMenu(),
        body: BlocConsumer<AllCategoryBloc, AllCategoryState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            if (state is AllCategoriesLoadingState) {
              return CircularIndicatorWidget();
            } else if (state is AllCategoriesErrorState) {
              return CustomErrorWidget(on_tap: () {
                allCategoriesBloc.add(AllCategoriesLoadingEvent());
              });
            } else if (state is AllCategoriesLoadedState) {
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(
                    16.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GridView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: state.categoriesList.length,
                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 15.0,
                          mainAxisSpacing: 15.0,
                          childAspectRatio: 0.8,
                        ),
                        itemBuilder: (ctx, index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context,
                                  UsersByCategoriesScreen
                                      .USER_BY_CATEGORY_SCREEN_Route,
                                  arguments: {
                                    UsersByCategoriesScreen.CATEGORY_ID:
                                        state.categoriesList[index].id,
                                    UsersByCategoriesScreen.TITLE:
                                        state.categoriesList[index].title ?? ""
                                  });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: MyColors.BG_COLOR_TEXT_FILED,
                                borderRadius: BorderRadius.circular(
                                  18.0,
                                ),
                                border: Border.all(
                                  width: 5.0,
                                  color:
                                      state.categoriesList[index].color_code !=
                                              null
                                          ? HexColor.fromHex(state
                                              .categoriesList[index].color_code)
                                          : MyColors.CATEGORY_BG_COLOR,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(
                                      9.0,
                                    ),
                                    child: state.categoriesList[index].image !=
                                                null &&
                                            state.categoriesList[index].image !=
                                                ""
                                        ? CachedNetworkImage(
                                            imageUrl: state
                                                .categoriesList[index].image,
                                            fit: BoxFit.cover,
                                            height: 120.0,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            placeholder: (context, url) =>
                                                Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              fit: BoxFit.cover,
                                              height: 120.0,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                            ),
                                            errorWidget:
                                                (context, url, error) =>
                                                    Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              fit: BoxFit.cover,
                                              height: 120.0,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                            ),
                                          )
                                        : Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            fit: BoxFit.cover,
                                            height: 120.0,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                          ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(
                                      8.0,
                                    ),
                                    child: Text(
                                      state.categoriesList[index].title ?? "",
                                      style: GoogleFonts.montserrat(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR1,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              );
            }
            return Scaffold();
          },
        ),
      ),
    );
  }
}
