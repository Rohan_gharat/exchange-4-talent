import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/blocks/ToggleBarterBloc/toggle_barter_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/auth/loginScreen.dart';
import 'package:e4t/screens/profile/edit_profile_screen.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../blocks/profilePhotoUpdateBloc/profile_photo_update_bloc.dart';
import '../../myColors/myColors.dart';
import '../subscriptions/subsriptions_screen.dart';

class AccountsScreen extends StatelessWidget {
  AccountsScreen({Key? key}) : super(key: key);

  static const ACCOUNTS_ROUTE = "accounts_route";
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var userBloc = BlocProvider.of<UserBloc>(context);
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            "Account",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          actions: [
            DrawerIconWidget(
              on_tap: () {
                _scaffoldKey.currentState!.openEndDrawer();
              },
            ),
          ],
        ),
        endDrawer: DrawerMenu(),
        body: SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
              color: MyColors.BG_COLOR_TEXT_FILED,
              borderRadius: BorderRadius.circular(
                18.0,
              ),
            ),
            margin: EdgeInsets.only(
              top: 16.0,
              left: 16.0,
              right: 16.0,
              bottom: 16.0,
            ),
            padding: EdgeInsets.all(
              16.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BlocBuilder<ProfilePhotoUpdateBloc, ProfilePhotoUpdateState>(
                  builder: (context, state) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                            context, UserDetailsScreen.USER_DETAILS_ROUTE,
                            arguments: {
                              UserDetailsScreen.USER_ID:
                                  SharedPrefs.getUserId(),
                              UserDetailsScreen.IS_ME: true
                            });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                100.0,
                              ),
                              border: Border.all(
                                width: 2.0,
                                color: userBloc.myUserData?.color_code !=
                                            null &&
                                        userBloc.myUserData?.color_code
                                                .trim() !=
                                            ""
                                    ? HexColor.fromHex(
                                        userBloc.myUserData?.color_code.trim())
                                    : Colors.transparent,
                              ),
                            ),
                            child: ClipRRect(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(
                                  100.0,
                                ),
                              ),
                              child: state.profilePhotoUrl != null &&
                                      state.profilePhotoUrl != ""
                                  ? CachedNetworkImage(
                                      alignment: Alignment.topCenter,
                                      imageUrl: state.profilePhotoUrl!,
                                      fit: BoxFit.cover,
                                      height: 70.0,
                                      width: 70.0,
                                      placeholder: (context, url) =>
                                          Image.asset(
                                        HelperClass.APP_LOGO_PATH,
                                        fit: BoxFit.cover,
                                        height: 70.0,
                                        width: 70.0,
                                      ),
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                        HelperClass.APP_LOGO_PATH,
                                        fit: BoxFit.cover,
                                        height: 70.0,
                                        width: 70.0,
                                      ),
                                    )
                                  : Image.asset(
                                      HelperClass.APP_LOGO_PATH,
                                      fit: BoxFit.cover,
                                      height: 70.0,
                                      width: 70.0,
                                    ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                left: 8.0,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    state.name ?? "",
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.GREY_COLOR1,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      userBloc.myUserData!.phone_code != null
                                          ? Text(
                                              "+${userBloc.myUserData!.phone_code}",
                                              style: GoogleFonts.workSans(
                                                textStyle: TextStyle(
                                                  color: MyColors.ORANGE_COLOR,
                                                  fontSize: 16.0,
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      Container(
                                        margin: EdgeInsets.only(
                                          left: 5.0,
                                        ),
                                        child: Text(
                                          state.mobile ?? "",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.ORANGE_COLOR,
                                              fontSize: 16.0,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
                GestureDetector(
                  onTap: () {
                    goToProfile(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 24.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.person,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Edit Profile",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(
                        context, SubscriptionsScreen.SUBSCRIPTIONS_ROUTE);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 16.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.subscriptions,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Subscription",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    HelperClass.openUrl(
                        url: HelperClass.PRIVACY_POLICY_URL, context: context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 24.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.privacy_tip_outlined,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Privacy Policy",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    HelperClass.openUrl(
                        url: HelperClass.CONTACT_US_URL, context: context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 24.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          FontAwesomeIcons.addressCard,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Contact Us",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    HelperClass.openUrl(
                        url: HelperClass.TERMS_AND_CONDITIONS_URL,
                        context: context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 24.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          FontAwesomeIcons.book,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Terms & Conditions",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    HelperClass.openUrl(
                        url: HelperClass.SHIPPING_URL, context: context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 24.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          FontAwesomeIcons.truck,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Shipping & Delivery Policy",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    HelperClass.openUrl(
                        url: HelperClass.REFUND_URL, context: context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 24.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          FontAwesomeIcons.exclamation,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Cancellation & Refund Policy",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    var alertDialog = AlertDialog(
                      backgroundColor: MyColors.BG_COLOR,
                      title: Text(
                        "Delete Account",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.RED_COLOR,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      content: Container(
                        height: 100.0,
                        child: Column(
                          children: [
                            Text(
                              "Are You Sure You Want To Delete Your Account",
                              style: GoogleFonts.montserrat(
                                textStyle: TextStyle(
                                  color: MyColors.RED_COLOR,
                                  fontSize: 14.0,
                                  // fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      actions: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                // fromHeight use double.infinity as width and 40 is the height
                                backgroundColor: MyColors.ORANGE_COLOR,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () {
                                // Navigator.pop(context);
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                              },
                              child: Text(
                                "No",
                                style: GoogleFonts.workSans(
                                  textStyle: TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20.0,
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                // fromHeight use double.infinity as width and 40 is the height
                                backgroundColor: MyColors.ORANGE_COLOR,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () async {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();

                                HelperClass.show_alert_Dialog(
                                    context, "deleting data");
                                var response =
                                    await ApisServices.deleteUserData(
                                        token: SharedPrefs.getAccessToken());

                                if (response != null &&
                                    response.status == 200) {
                                  await FireBaseHelperClass
                                      .deleteUserFromFirebase(
                                          firestore: FirebaseFirestore.instance,
                                          uid: userBloc.myUserData!.mobile);
                                  await SharedPrefs.clearSharedPref();

                                  userBloc.myUserData = null;
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                  Navigator.of(context, rootNavigator: true)
                                      .pushNamedAndRemoveUntil(
                                          LoginScreen.LOGIN_ROUTE,
                                          (route) => false);
                                } else {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                  HelperClass.show_error_snackbar(
                                      context, HelperClass.ERROR_TEXT);
                                }
                              },
                              child: Text(
                                "Yes",
                                style: GoogleFonts.workSans(
                                  textStyle: TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    );
                    showDialog(
                        context: context,
                        builder: (ctx) {
                          return alertDialog;
                        });
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 24.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          FontAwesomeIcons.deleteLeft,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Delete Account",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    var alertDialog = AlertDialog(
                      backgroundColor: MyColors.BG_COLOR,
                      title: Text(
                        "Logout",
                        style: GoogleFonts.montserrat(
                          textStyle: const TextStyle(
                            color: MyColors.ORANGE_COLOR,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      content: SizedBox(
                        height: 100.0,
                        child: Column(
                          children: [
                            Text(
                              "Are You Sure You Want To Logout",
                              style: GoogleFonts.montserrat(
                                textStyle: const TextStyle(
                                  color: MyColors.ORANGE_COLOR,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      actions: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                // fromHeight use double.infinity as width and 40 is the height
                                backgroundColor: MyColors.ORANGE_COLOR,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () {
                                // Navigator.pop(context);
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                              },
                              child: Text(
                                "No",
                                style: GoogleFonts.workSans(
                                  textStyle: const TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20.0,
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                // fromHeight use double.infinity as width and 40 is the height
                                backgroundColor: MyColors.ORANGE_COLOR,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () async {
                                Navigator.of(context, rootNavigator: true)
                                    .pop();

                                HelperClass.show_alert_Dialog(
                                    context, "processing");

                                final fcmToken =
                                    await FirebaseMessaging.instance.getToken();
                                print("accounts screen: fcm token: $fcmToken");

                                var response = await ApisServices.logout(
                                    authToken: SharedPrefs.getAccessToken()!,
                                    fcmToken: fcmToken);
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                                print("logout response accounts: ${response}");
                                if (response != null) {
                                  if (response.status == 200) {
                                    userBloc.myUserData = null;
                                    await SharedPrefs.clearSharedPref();
                                    Navigator.of(context, rootNavigator: true)
                                        .pushNamedAndRemoveUntil(
                                            LoginScreen.LOGIN_ROUTE,
                                            (route) => false);
                                  } else {
                                    HelperClass.show_snackbar(
                                        context,
                                        response.message ??
                                            HelperClass.ERROR_TEXT);
                                  }
                                } else {
                                  HelperClass.show_snackbar(
                                      context, HelperClass.ERROR_TEXT);
                                }
                              },
                              child: Text(
                                "Yes",
                                style: GoogleFonts.workSans(
                                  textStyle: TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    );
                    showDialog(
                        context: context,
                        builder: (ctx) {
                          return alertDialog;
                        });
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 24.0,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.login,
                          color: MyColors.ORANGE_COLOR,
                          size: 24.0,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Sign out",
                          style: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR1,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void goToProfile(BuildContext context) {
    Navigator.pushNamed(context, EditProfileScreen.EDIT_PROFILE_ROUT);
  }
}
