import 'package:e4t/blocks/FeedbackBloc/feedback_bloc.dart';
import 'package:e4t/blocks/FeedbackBloc/feedback_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class FeedbackFormScreen extends StatefulWidget {
  FeedbackFormScreen({super.key});

  static const FEEDBACK_FORM_SCREEN_ROUTE = "feedback_form_route";

  @override
  State<FeedbackFormScreen> createState() => _FeedbackFormScreenState();
}

class _FeedbackFormScreenState extends State<FeedbackFormScreen> {
  var feedbackController = TextEditingController();

  var formKey = GlobalKey<FormState>();
  var feedbackBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    feedbackBloc = BlocProvider.of<FeedbackBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
        elevation: 0,
        title: Text(
          "Feedback Form",
          style: GoogleFonts.montserrat(
            textStyle: TextStyle(
              color: MyColors.WHITE_COLOR,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: MyColors.ORANGE_COLOR,
            size: 16,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.all(
                  16.0,
                ),
                child: TextFormField(
                  // autofocus: true,
                  controller: feedbackController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.zero,
                    filled: true,
                    fillColor: MyColors.BG_COLOR_TEXT_FILED,
                    hintText: "Enter Your Feedback",
                    hintStyle: GoogleFonts.workSans(
                      textStyle: TextStyle(
                        color: MyColors.GREY_COLOR,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    counterStyle: GoogleFonts.workSans(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    counterText: "",
                    prefixIcon: Align(
                      alignment: Alignment.topLeft,
                      widthFactor: 1.0,
                      heightFactor: 7.0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 4.0, left: 8),
                        child: Icon(
                          Icons.description,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                    ),
                  ),
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  cursorColor: MyColors.WHITE_COLOR,
                  keyboardType: TextInputType.multiline,
                  maxLines: 10,
                  validator: (value) {
                    if (value == null || value.trim() == "") {
                      return 'Please enter your feedback';
                    }
                    return null;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 30.0,
                  bottom: 30.0,
                  left: 16.0,
                  right: 16.0,
                ),
                child: BlocConsumer<FeedbackBloc, FeedbackState>(
                  listener: (context, state) {
                    if (state is FeedbackAddedState) {
                      feedbackController.clear();
                      HelperClass.show_error_snackbar(context, state.message);
                    } else if (state is FeedbackErrorState) {
                      HelperClass.show_snackbar(context, state.message);
                    }
                  },
                  builder: (context, state) {
                    if (state is FeedbackLoadingState) {
                      return const CircularIndicatorWidget();
                    }
                    return ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0.0,
                        minimumSize: Size.fromHeight(
                          40,
                        ),
                        // fromHeight use double.infinity as width and 40 is the height
                        backgroundColor: MyColors.ORANGE_COLOR,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18),
                        ),
                      ),
                      onPressed: () {
                        if (!formKey.currentState!.validate()) {
                          // return;
                          HelperClass.show_error_snackbar(
                              context, "please enter your feedback");
                        } else {
                          FocusManager.instance.primaryFocus?.unfocus();
                          feedbackBloc.add(
                              AddFeedbackEvent(feedbackController.text.trim()));
                        }
                      },
                      child: Text(
                        "Submit",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
