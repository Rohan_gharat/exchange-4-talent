import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:e4t/blocks/favourite/favourite_bloc.dart';
import 'package:e4t/blocks/mainNavigationBloc/main_navigation_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/model/ReviewsModel/ratingDetailsModel.dart';
import 'package:e4t/model/userModel/user_data_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/chat/messagesScreen.dart';
import 'package:e4t/screens/main_navigator_screen.dart';
import 'package:e4t/screens/reviews/review_dialog.dart';
import 'package:e4t/screens/reviews/reviews_screen.dart';
import 'package:e4t/screens/subscriptions/add_subscription_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/imageSliders.dart';
import 'package:e4t/screens/widgets/recommendedUsersWidget.dart';
import 'package:e4t/screens/widgets/videoPlayerPageFile.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../blocks/AddReviewBloc/add_review_bloc.dart';
import '../category/all_users_screen.dart';

class UserDetailsScreen extends StatefulWidget {
  UserDetailsScreen({super.key, this.mapData});

  Map? mapData;

  static const USER_DETAILS_ROUTE = "userDetailsRoute";

  static const USER_ID = "userId";
  static const IS_ME = "isMe";

  @override
  State<UserDetailsScreen> createState() => _UserDetailsScreenState();
}

class _UserDetailsScreenState extends State<UserDetailsScreen> {
  UserDataModel? userDataModel;
  bool isError = false, isLoading = true;
  late UserBloc userBloc;
  late var favouriteBloc;
  late var addReviewBloc;
  late var mainNavigationBloc;
  List<RatingDetailsModel> ratingDetailsList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBloc = BlocProvider.of<UserBloc>(context);
    favouriteBloc = BlocProvider.of<FavouriteBloc>(context);
    addReviewBloc = BlocProvider.of<AddReviewBloc>(context);
    mainNavigationBloc = BlocProvider.of<MainNavigationBloc>(context);

    loadData();
  }

  loadData() {
    isError = false;
    isLoading = true;
    setState(() {});
    ApisServices.getUserDataById(
            userId: widget.mapData![UserDetailsScreen.USER_ID],
            token: SharedPrefs.getAccessToken()!)
        .then((value) {
      isLoading = false;
      if (value != null) {
        if (value.status == 200) {
          userDataModel = value;
          ratingDetailsList = userDataModel!.ratingDetailsList ?? [];
          addReviewBloc.add(ReviewInitialEvent(
              double.parse(userDataModel?.userModel!.rating ?? "0.0")));
          socialMediaChips();
          isError = false;
          setState(() {});
        } else {
          HelperClass.show_snackbar(
              context, value.message ?? HelperClass.ERROR_TEXT);
          isError = true;
          setState(() {});
        }
      } else {
        HelperClass.show_snackbar(context, HelperClass.ERROR_TEXT);
        isError = true;

        setState(() {});
      }
    });
  }

  List<Widget> socialMediaChips() {
    List<Widget> chipsList = [];
    if (userBloc.myUserData != null &&
        userBloc.myUserData!.subscriptionId > 0) {
      if (userDataModel?.userModel?.fb_link != null &&
          userDataModel?.userModel?.fb_link != "") {
        chipsList.add(addChipWidget(
          title: userDataModel?.userModel?.subscriptionId == 24
              ? "Facebook"
              : "${userDataModel?.userModel?.fb_link}",
          icon: FontAwesomeIcons.facebook,
          url: userDataModel?.userModel?.fb_link,
          canClick: userBloc.myUserData!.subscriptionId >= 22 ? true : false,
        ));
      }
      if (userDataModel?.userModel?.insta_link != null &&
          userDataModel?.userModel?.insta_link != "") {
        chipsList.add(addChipWidget(
          title: userDataModel?.userModel?.subscriptionId == 24
              ? "Instagram"
              : "${userDataModel?.userModel?.insta_link}",
          icon: FontAwesomeIcons.instagram,
          url: userDataModel?.userModel?.insta_link,
          canClick: userBloc.myUserData!.subscriptionId >= 22 ? true : false,
        ));
      }
      if (userDataModel?.userModel?.linkedin_link != null &&
          userDataModel?.userModel?.linkedin_link != "") {
        chipsList.add(addChipWidget(
          title: userDataModel?.userModel?.subscriptionId == 24
              ? "LinkedIn"
              : "${userDataModel?.userModel?.linkedin_link}",
          icon: FontAwesomeIcons.linkedin,
          url: userDataModel?.userModel?.linkedin_link,
          canClick: userBloc.myUserData!.subscriptionId >= 22 ? true : false,
        ));
      }
    }

    if (userDataModel?.userModel?.wikipedia_link != null &&
        userDataModel?.userModel?.wikipedia_link != "") {
      chipsList.add(addChipWidget(
        title: "Wikipedia",
        icon: FontAwesomeIcons.wikipediaW,
        url: userDataModel?.userModel?.wikipedia_link,
        canClick: true,
      ));
    }
    if (userDataModel?.userModel?.imdb_link != null &&
        userDataModel?.userModel?.imdb_link != "") {
      chipsList.add(addChipWidget(
        title: "IMDB",
        icon: FontAwesomeIcons.imdb,
        url: userDataModel?.userModel?.imdb_link,
        canClick: true,
      ));
    }

    return chipsList;
  }

  Widget addChipWidget(
          {required title, required icon, required url, required canClick}) =>
      InputChip(
        label: Text(
          title,
          style: GoogleFonts.workSans(
            textStyle: TextStyle(
              color: MyColors.ORANGE_COLOR,
              fontSize: 14.0,
            ),
          ),
        ),
        avatar: Icon(
          icon,
          color: MyColors.ORANGE_COLOR,
        ),
        backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
        padding: EdgeInsets.only(
          top: 5.0,
          bottom: 5.0,
          left: 7.0,
          right: 0.0,
        ),
        onPressed: () {
          canClick ? openUrl(url) : null;
        },
      );

  void openUrl(var url) async {
    if (await canLaunchUrl(Uri.parse(url))) {
      launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
    } else {
      HelperClass.show_snackbar(
          context, "this page link is not found or moved to new link");
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: isError
          ? Scaffold(
              body: CustomErrorWidget(
                on_tap: () {
                  loadData();
                },
              ),
            )
          : isLoading
              ? Scaffold(
                  body: CircularIndicatorWidget(),
                )
              : Scaffold(
                  appBar: AppBar(
                    backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
                    elevation: 0,
                    title: Text(
                      userDataModel?.userModel?.name ?? "",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    leading: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_new,
                        color: MyColors.ORANGE_COLOR,
                        size: 16,
                      ),
                    ),
                    actions: [
                      !widget.mapData![UserDetailsScreen.IS_ME]
                          ? GestureDetector(
                              onTap: () {
                                updateFavouriteStatus(
                                    talent_id: userDataModel?.userModel?.id);
                              },
                              child: Padding(
                                padding: EdgeInsets.only(
                                  right: 12.0,
                                ),
                                child: Icon(
                                  userDataModel?.userModel?.isfavourite == 1
                                      ? Icons.favorite
                                      : Icons.favorite_border,
                                  color: MyColors.ORANGE_COLOR,
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                  body: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(
                        16.0,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          userDataModel != null &&
                                  userDataModel?.userBannersList != null &&
                                  userDataModel!.userBannersList!.isNotEmpty
                              ? ImageSliders(
                                  userBannersList:
                                      userDataModel?.userBannersList,
                                  userName:
                                      userDataModel!.userModel!.name ?? "",
                                  color_code: userDataModel
                                              ?.userModel?.subscriptionId ==
                                          24
                                      ? userDataModel?.userModel?.color_code !=
                                                  null &&
                                              userDataModel
                                                      ?.userModel?.color_code !=
                                                  ""
                                          ? HexColor.fromHex(userDataModel
                                              ?.userModel?.color_code)
                                          : MyColors.STARRY_EYED_COLOR
                                      : MyColors.ORANGE_COLOR,
                                )
                              : Container(),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              !widget.mapData![UserDetailsScreen.IS_ME]
                                  ? userDataModel!.userModel!.subscriptionId !=
                                          24
                                      ? GestureDetector(
                                          onTap: () {
                                            // showHireDialog(context);
                                            HelperClass.checkSubscription(
                                                userBloc,
                                                context,
                                                userDataModel?.userModel);
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: MyColors.ORANGE_COLOR,
                                              borderRadius:
                                                  BorderRadius.circular(
                                                18.0,
                                              ),
                                            ),
                                            padding: EdgeInsets.symmetric(
                                              vertical: 12.0,
                                              horizontal: 24.0,
                                            ),
                                            child: Text(
                                              "Hire Now",
                                              style: GoogleFonts.workSans(
                                                textStyle: TextStyle(
                                                  color: MyColors.GREY_COLOR1,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Container()
                                  : Container(),
                              !widget.mapData![UserDetailsScreen.IS_ME]
                                  ? GestureDetector(
                                      onTap: () async {
                                        if (userBloc.myUserData!
                                                    .subscriptionValidity !=
                                                null &&
                                            userBloc.myUserData!
                                                    .subscriptionValidity !=
                                                "") {
                                          if (userDataModel!
                                                      .userModel!.mobile !=
                                                  null &&
                                              userDataModel!
                                                      .userModel!.mobile !=
                                                  "") {
                                            HelperClass.show_alert_Dialog(
                                                context, "loading");
                                            var response = await FireBaseHelperClass
                                                .createUserInFireBaseIfNotExists(
                                                    fireStore: FirebaseFirestore
                                                        .instance,
                                                    usermodel: userDataModel!
                                                        .userModel!);
                                            Navigator.of(context,
                                                    rootNavigator: true)
                                                .pop();
                                            if (response) {
                                              Navigator.of(context,
                                                      rootNavigator: true)
                                                  .push(
                                                MaterialPageRoute(
                                                  builder: (context) {
                                                    return MessagesScreen(
                                                      name: userDataModel!
                                                              .userModel!
                                                              .name ??
                                                          "",
                                                      chat_receiver_uid:
                                                          userDataModel!
                                                              .userModel!
                                                              .mobile,
                                                      isGroupChat: false,
                                                      profilePic: userDataModel!
                                                              .userModel!
                                                              .avatar ??
                                                          "",
                                                      currentUserUid: userBloc
                                                          .myUserData!.mobile,
                                                      isSecretService: false,
                                                    );
                                                  },
                                                ),
                                              );
                                            } else {
                                              HelperClass.show_snackbar(context,
                                                  HelperClass.ERROR_TEXT);
                                            }
                                          } else {
                                            HelperClass.show_snackbar(context,
                                                "cant chat with this user");
                                          }
                                        } else {
                                          mainNavigationBloc.add(
                                              MainNavigationEvent(
                                                  MainNavigationScreen
                                                      .CHAT_INDEX,
                                                  false));
                                        }
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          left: 10.0,
                                        ),
                                        decoration: BoxDecoration(
                                          color: MyColors.ORANGE_COLOR,
                                          borderRadius: BorderRadius.circular(
                                            18.0,
                                          ),
                                        ),
                                        padding: EdgeInsets.symmetric(
                                          vertical: 12.0,
                                          horizontal: 24.0,
                                        ),
                                        child: Text(
                                          "Chat",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.GREY_COLOR1,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  : Container(),
                              Expanded(
                                child: Container(),
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.star,
                                    color: MyColors.ORANGE_COLOR,
                                    size: 16.0,
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  BlocBuilder<AddReviewBloc, AddReviewState>(
                                    builder: (context, state) {
                                      double rating = 0.0;
                                      if (state is ReviewPostedState) {
                                        rating = state.rating;
                                      }
                                      return Text(
                                        "${rating ?? 0.0}",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.GREY_COLOR1,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                          widget.mapData![UserDetailsScreen.IS_ME]
                              ? Container(
                                  margin: EdgeInsets.only(
                                    top: 16.0,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            minimumSize: Size.fromHeight(
                                              40,
                                            ),
                                            // fromHeight use double.infinity as width and 40 is the height
                                            backgroundColor:
                                                MyColors.ORANGE_COLOR,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(18),
                                            ),
                                          ),
                                          onPressed: () {
                                            if (userDataModel!.userModel!
                                                        .subscriptionId ==
                                                    null ||
                                                userDataModel!.userModel!
                                                        .subscriptionId ==
                                                    0) {
                                              HelperClass.show_snackbar(context,
                                                  "your not subscribed to any plan buy a subscription to upload image");
                                              Navigator.pushNamed(
                                                  context,
                                                  AddSubscriptionsScreen
                                                      .ADD_SUBSCRIPTIONS_SCREEN,
                                                  arguments: {
                                                    AddSubscriptionsScreen
                                                        .SHOW_SKIP_KEY: false,
                                                    AddSubscriptionsScreen
                                                        .CURRENT_SUBSCRIPTION_key: 0
                                                  });
                                            } else {
                                              check_for_permissions(
                                                  isPhoto: true);
                                            }
                                          },
                                          child: Text(
                                            "Add Photo",
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.WHITE_COLOR,
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Expanded(
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            minimumSize: Size.fromHeight(
                                              40,
                                            ),
                                            // fromHeight use double.infinity as width and 40 is the height
                                            backgroundColor:
                                                MyColors.ORANGE_COLOR,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(18),
                                            ),
                                          ),
                                          onPressed: () {
                                            if (userDataModel!.userModel!
                                                        .subscriptionId ==
                                                    null ||
                                                userDataModel!.userModel!
                                                        .subscriptionId ==
                                                    0) {
                                              HelperClass.show_snackbar(context,
                                                  "your not subscribed to any plan buy a subscription to upload image");
                                              Navigator.pushNamed(
                                                  context,
                                                  AddSubscriptionsScreen
                                                      .ADD_SUBSCRIPTIONS_SCREEN,
                                                  arguments: {
                                                    AddSubscriptionsScreen
                                                        .SHOW_SKIP_KEY: false,
                                                    AddSubscriptionsScreen
                                                        .CURRENT_SUBSCRIPTION_key: 0
                                                  });
                                            } else {
                                              check_for_permissions(
                                                  isPhoto: false);
                                            }
                                          },
                                          child: Text(
                                            "Add Video",
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.WHITE_COLOR,
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Container(),
                          Wrap(
                            spacing: 10,
                            children: socialMediaChips(),
                          ),
                          userDataModel?.userModel?.description != null &&
                                  userDataModel?.userModel?.description != ""
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 16.0,
                                      ),
                                      child: Text(
                                        "Description",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.ORANGE_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 4.0,
                                      ),
                                      child: Text(
                                        userDataModel?.userModel?.description ??
                                            "",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.GREY_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox.shrink(),
                          userDataModel?.userModel?.tags != null &&
                                  userDataModel?.userModel?.tags != ""
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 16.0,
                                      ),
                                      child: Text(
                                        "Tags",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.ORANGE_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 4.0,
                                      ),
                                      child: Text(
                                        userDataModel?.userModel?.tags ?? "",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.GREY_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox.shrink(),
                          userDataModel?.userModel?.category_title != null &&
                                  userDataModel?.userModel?.category_title != ""
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 16.0,
                                      ),
                                      child: Text(
                                        "Categories",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.ORANGE_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 4.0,
                                      ),
                                      child: Text(
                                        userDataModel
                                                ?.userModel?.category_title ??
                                            "",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.GREY_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox.shrink(),
                          userDataModel?.userModel?.barter_description !=
                                      null &&
                                  userDataModel
                                          ?.userModel?.barter_description !=
                                      ""
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 16.0,
                                      ),
                                      child: Text(
                                        "Trading For",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.ORANGE_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 4.0,
                                      ),
                                      child: Text(
                                        userDataModel?.userModel
                                                ?.barter_description ??
                                            "",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.GREY_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox.shrink(),
                          SizedBox(
                            height: 20.0,
                          ),
                          Divider(
                            height: 10.0,
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          userDataModel!.userModel!.subscriptionId != 24
                              ? Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Reviews",
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                              color: MyColors.GREY_COLOR1,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            Navigator.pushNamed(context,
                                                ReviewsScreen.ALL_REVIEWS_ROUTE,
                                                arguments: userDataModel
                                                    ?.userModel?.id);
                                          },
                                          child: Text(
                                            "See all",
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.ORANGE_COLOR,
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                        borderRadius: BorderRadius.circular(
                                          18.0,
                                        ),
                                      ),
                                      padding: EdgeInsets.all(
                                        16.0,
                                      ),
                                      margin: EdgeInsets.only(
                                        top: 8.0,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Expanded(
                                            flex: 4,
                                            child: BlocBuilder<AddReviewBloc,
                                                AddReviewState>(
                                              builder: (context, state) {
                                                double rating = 0.0;
                                                if (state
                                                    is ReviewPostedState) {
                                                  rating = state.rating;
                                                }

                                                return Column(
                                                  children: [
                                                    Text(
                                                      "${rating}/5",
                                                      style:
                                                          GoogleFonts.workSans(
                                                        textStyle: TextStyle(
                                                          color: MyColors
                                                              .GREY_COLOR,
                                                          fontSize: 20.0,
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 10.0,
                                                    ),
                                                    RatingBar.builder(
                                                      initialRating: rating,
                                                      minRating: 1,
                                                      direction:
                                                          Axis.horizontal,
                                                      allowHalfRating: true,
                                                      itemCount: 5,
                                                      itemSize: 18,
                                                      unratedColor:
                                                          MyColors.GREY_COLOR1,
                                                      itemPadding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 1.0),
                                                      itemBuilder:
                                                          (context, _) =>
                                                              const Icon(
                                                        Icons.star,
                                                        color: MyColors
                                                            .ORANGE_COLOR,
                                                      ),
                                                      onRatingUpdate: (rating) {
                                                      },
                                                      ignoreGestures: true,
                                                    ),
                                                  ],
                                                );
                                              },
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Expanded(
                                            flex: 5,
                                            child: ListView.builder(
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemCount:
                                                  ratingDetailsList.length,
                                              shrinkWrap: true,
                                              itemBuilder: (ctx, index) {
                                                return Container(
                                                  padding: EdgeInsets.only(
                                                    top: 8.0,
                                                  ),
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        "${ratingDetailsList[index].star}",
                                                        style: GoogleFonts
                                                            .workSans(
                                                          textStyle: TextStyle(
                                                            color: MyColors
                                                                .GREY_COLOR,
                                                            fontSize: 12.0,
                                                          ),
                                                        ),
                                                      ),
                                                      Icon(
                                                        Icons.star,
                                                        color: MyColors
                                                            .ORANGE_COLOR,
                                                        size: 12.0,
                                                      ),
                                                      SizedBox(
                                                        width: 5.0,
                                                      ),
                                                      Expanded(
                                                          child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(
                                                            16,
                                                          ),
                                                        ),
                                                        child:
                                                            LinearProgressIndicator(
                                                          value: double.parse(
                                                              ratingDetailsList[
                                                                          index]
                                                                      .average ??
                                                                  "0.0"),
                                                          backgroundColor:
                                                              MyColors
                                                                  .GREY_COLOR,
                                                          color: MyColors
                                                              .YELLOW_COLOR,
                                                          minHeight: 5.0,
                                                        ),
                                                      )),
                                                      SizedBox(
                                                        width: 5.0,
                                                      ),
                                                      Text(
                                                        "${ratingDetailsList[index].total}",
                                                        style: GoogleFonts
                                                            .workSans(
                                                          textStyle: TextStyle(
                                                            color: MyColors
                                                                .GREY_COLOR,
                                                            fontSize: 12.0,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    !widget.mapData![UserDetailsScreen.IS_ME]
                                        ? Container(
                                            margin: EdgeInsets.only(
                                              top: 16.0,
                                            ),
                                            child: ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                minimumSize: Size.fromHeight(
                                                  40,
                                                ),
                                                // fromHeight use double.infinity as width and 40 is the height
                                                backgroundColor:
                                                    MyColors.ORANGE_COLOR,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(18),
                                                ),
                                              ),
                                              onPressed: () {
                                                addReviewDialog(
                                                    context: context,
                                                    rating: double.parse(
                                                        userDataModel?.userModel
                                                                ?.rating ??
                                                            "0.0"),
                                                    talent_id: userDataModel
                                                        ?.userModel?.id);
                                              },
                                              child: Text(
                                                "Add Review",
                                                style: GoogleFonts.workSans(
                                                  textStyle: TextStyle(
                                                    color: MyColors.WHITE_COLOR,
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        : Container(),
                                  ],
                                )
                              : Container(),
                          userDataModel != null &&
                                  userDataModel!.randomUsersList!.isNotEmpty &&
                                  userDataModel!.userModel!.subscriptionId != 24
                              ? Container(
                                  // color: Colors.red,
                                  height: 240.0,
                                  margin: EdgeInsets.only(
                                    top: 16.0,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                          vertical: 10.0,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Similar Talent",
                                              style: GoogleFonts.montserrat(
                                                textStyle: TextStyle(
                                                  color: MyColors.GREY_COLOR1,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.pushNamed(
                                                    context,
                                                    AllUsersScreen
                                                        .All_USERS_ROUTE,
                                                    arguments: {
                                                      AllUsersScreen
                                                              .IS_ALL_USERS_KEY:
                                                          true,
                                                      AllUsersScreen.TITLE_KEY:
                                                          "Similar Talent"
                                                    });
                                              },
                                              child: Text(
                                                "See All",
                                                style: GoogleFonts.workSans(
                                                  textStyle: TextStyle(
                                                    color:
                                                        MyColors.ORANGE_COLOR,
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount: userDataModel
                                              ?.randomUsersList!.length,
                                          shrinkWrap: true,
                                          itemBuilder: (ctx, index) {
                                            return RecommendedUsersWidget(
                                              randomUsersList: userDataModel
                                                  ?.randomUsersList!,
                                              index: index,
                                              margin: EdgeInsets.only(
                                                right: 16.0,
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Container(),
                          !widget.mapData![UserDetailsScreen.IS_ME]
                              ? Container(
                                  margin: EdgeInsets.only(
                                    top: 16.0,
                                  ),
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      minimumSize: Size.fromHeight(
                                        40,
                                      ),
                                      // fromHeight use double.infinity as width and 40 is the height
                                      backgroundColor: MyColors.ORANGE_COLOR,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                      ),
                                    ),
                                    onPressed: () {
                                      HelperClass.checkSubscription(userBloc,
                                          context, userDataModel?.userModel);
                                    },
                                    child: Text(
                                      "Hire Now",
                                      style: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                  ),
                ),
    );
  }

  void updateFavouriteStatus({required talent_id}) {
    HelperClass.show_alert_Dialog(context, "processing");
    ApisServices.updateFavouriteStatus(
            token: SharedPrefs.getAccessToken(), talent_id: talent_id)
        .then((value) {
      Navigator.of(context, rootNavigator: true).pop();
      String msg = "";
      if (value != null) {
        msg = value.message;
        if (value.status == 200) {
          if (value.data) {
            userDataModel?.userModel?.isfavourite = 1;
            setState(() {});
          } else {
            userDataModel?.userModel?.isfavourite = 0;
            setState(() {});
          }
          favouriteBloc.add(FavouriteLoadingEvent());
        }
      } else {
        msg = HelperClass.ERROR_TEXT;
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            msg,
          ),
          duration: Duration(seconds: 1),
        ),
      );
    });
  }

  void addReviewDialog(
      {required BuildContext context,
      required double rating,
      required int talent_id}) {
    showDialog(
      context: context,
      builder: (ctx) {
        return ReviewDialog(
          rating: rating,
          talent_id: talent_id,
        );
      },
    );
  }

  void check_for_permissions({required isPhoto}) async {
    var permission = Permission.photos;
    if (Platform.isAndroid) {
      var deviceInfo = DeviceInfoPlugin();
      final androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.version.sdkInt <= 32) {
        /// use [Permissions.storage.status]
        permission = Permission.storage;
      } else {
        /// use [Permissions.photos.status]
        permission = Permission.photos;
      }
    }

    Map<Permission, PermissionStatus> perms =
        await [permission, Permission.camera].request();

    if (perms[permission]!.isGranted && perms[Permission.camera]!.isGranted) {
      showAlertDialog(isPhoto: isPhoto);
    } else
    // if (perms[Permission.storage]!.isDenied ||
    //   perms[Permission.camera]!.isDenied)
    {
      String msg = "";
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        msg =
            "Storage, camera Permission is need to get the photo, please enable it in settings";
      } else {
        msg = "Storage, camera Permission is need to get the photo";
      }

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
        duration: Duration(seconds: 1),
      ));
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        Future.delayed(Duration(milliseconds: 1500), () {
          openAppSettings();
        });
      }
    }
  }

  void showAlertDialog({required isPhoto}) {
    AlertDialog alertDialog = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        isPhoto ? "Add Photo" : "Add video",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        textAlign: TextAlign.center,
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          isPhoto
              ? GestureDetector(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true).pop();
                    if (isPhoto) {
                      _getFromGallery();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Gallery",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              : Container(),
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              if (isPhoto) {
                _getFromCamera();
              } else {
                getVideo(ImageSource.camera);
              }
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Camera",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return alertDialog;
      },
    );
  }

  _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      uploadPhoto(imageFile: File(pickedFile.path));
    }
  }

  _getFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      uploadPhoto(imageFile: File(pickedFile.path));
    }
  }

  void uploadPhoto({required imageFile}) {
    HelperClass.show_alert_Dialog(context, "processing");
    ApisServices.uploadPhoto(
            token: SharedPrefs.getAccessToken(), file_path: imageFile!.path)
        .then((value) {
      String msg = "";
      Navigator.of(context, rootNavigator: true).pop();
      if (value != null) {
        msg = value.message ?? "";
        if (value.status == 200) {
          // setState(() {});
          loadData();
        } else if (value.status == 204) {
          Navigator.pushNamed(
              context, AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN,
              arguments: {
                AddSubscriptionsScreen.SHOW_SKIP_KEY: false,
                AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key:
                    value.subscription_id ?? 0
              });
        }
      } else {
        msg = HelperClass.ERROR_TEXT;
      }
      HelperClass.show_snackbar(context, msg);
    });
  }

  Future getVideo(
    ImageSource img,
  ) async {
    final pickedFile = await ImagePicker().pickVideo(
        source: img,
        preferredCameraDevice: CameraDevice.front,
        maxDuration: Duration(seconds: userDataModel?.video_duration ?? 60));

    XFile? xfilePick = pickedFile;
    if (xfilePick != null) {
      final result = await Navigator.of(context, rootNavigator: true)
          .push(MaterialPageRoute(
        builder: (context) {
          return VideoPlayerFileScreen(videoFile: File(xfilePick.path));
        },
      ));

      if (!mounted) return;

      if (result != null) {
        HelperClass.show_snackbar(context, result.message ?? "");
        if (result.status == 200) {
          loadData();
        } else if (result.status == 204) {
          Navigator.pushNamed(
              context, AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN,
              arguments: {
                AddSubscriptionsScreen.SHOW_SKIP_KEY: false,
                AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key:
                    result.subscription_id ?? 0
              });
        }
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(// is this context <<<
          const SnackBar(content: Text('Nothing is selected')));
    }
  }
}
