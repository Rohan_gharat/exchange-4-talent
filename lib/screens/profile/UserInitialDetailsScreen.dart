import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:e4t/blocks/CategoriesBlocNew/categories_new_bloc.dart';
import 'package:e4t/blocks/ChoosePhotoBloc/choose_photo_bloc.dart';
import 'package:e4t/blocks/CitiesBloc/cities_bloc.dart';
import 'package:e4t/blocks/CountriesBloc/countries_bloc.dart';
import 'package:e4t/blocks/GenderBloc/gender_bloc.dart';
import 'package:e4t/blocks/HireDataShowBloc/hire_data_show_bloc.dart';
import 'package:e4t/blocks/StatesBloc/states_bloc.dart';
import 'package:e4t/blocks/SubCategoryBloc/sub_category_drop_down_bloc.dart';
import 'package:e4t/blocks/TalentTypeBloc/talent_type_bloc.dart';
import 'package:e4t/blocks/categoriesDropDownBloc/categories_drop_down_bloc.dart';
import 'package:e4t/blocks/dobBloc/dob_bloc.dart';
import 'package:e4t/blocks/talentBarterBloc/talent_barter_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/model/addressModel/addressModel.dart';
import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:e4t/model/userModel/user_data_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/subscriptions/add_subscription_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/customRadioButton.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:http/http.dart' as http;

class UserInitialDetailsScreen extends StatefulWidget {
  UserInitialDetailsScreen({super.key});

  static const USER_INITIAL_DETAILS_ROUTE = "/userInitialDetailsRoute";

  @override
  State<UserInitialDetailsScreen> createState() =>
      _UserInitialDetailsScreenState();
}

class _UserInitialDetailsScreenState extends State<UserInitialDetailsScreen> {
  // int RadioValue = 1;
  bool talentBarterSwitchState = true, talentType = true;

  var formKey = GlobalKey<FormState>();

  late var dobBloc;

  late var genderBloc;

  late var talentTypeBloc;
  late var talentBarterBloc;
  late var choosePhotoBloc;
  late var countriesBloc;
  late var statesBloc;
  late var citiesBloc;
  late var categoriesNewBloc;
  late var hireDataShowBloc;

  int selectedCategoryIndex = -1;
  int selectedSubCategoryIndex = -1;

  String? dob = "";
  var gender = 1;

  var categoryId = -1;
  var subCategoryId = -1;

  int countryId = -1;
  int stateId = -1;
  int cityId = -1;

  int hireDataFlag = 1;

  String countryName = "";
  String stateName = "";
  String cityName = "";

  var name_controller = TextEditingController();
  var email_controller = TextEditingController();
  var tags_controller = TextEditingController();
  var fb_link_controller = TextEditingController();
  var insta_link_controller = TextEditingController();
  var linkedIn_link_controller = TextEditingController();
  var wikipedialink_controller = TextEditingController();
  var imdb_link_controller = TextEditingController();
  var bioController = TextEditingController();
  var barterDescriptionController = TextEditingController();
  File? image_File;
  List<CategoriesModel> selectedCategoriesList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    dobBloc = BlocProvider.of<DobBloc>(context);
    genderBloc = BlocProvider.of<GenderBloc>(context);
    genderBloc.add(GenderEvent(HelperClass.MALE_RADIO_BUTTON_INDEX));

    talentTypeBloc = BlocProvider.of<TalentTypeBloc>(context);
    talentTypeBloc.add(TalentTypeEvent(true));

    categoriesNewBloc = BlocProvider.of<CategoriesNewBloc>(context);
    categoriesNewBloc.add(CategoriesNewLoadingEvent(null));

    countriesBloc = BlocProvider.of<CountriesBloc>(context);
    countriesBloc.add(CountriesLoadingEvent(null, false));

    statesBloc = BlocProvider.of<StatesBloc>(context);
    citiesBloc = BlocProvider.of<CitiesBloc>(context);

    talentBarterBloc = BlocProvider.of<TalentBarterBloc>(context);
    talentBarterBloc.add(TalentBarterEvent(true));

    choosePhotoBloc = BlocProvider.of<ChoosePhotoBloc>(context);
    choosePhotoBloc.add(ChoosePhotoEvent(image_File));

    hireDataShowBloc = BlocProvider.of<HireDataShowBloc>(context);

    hireDataFlag = HelperClass.BOTH_DATA_RADIO_BUTTON_INDEX;

    hireDataShowBloc.add(HireDataShowEvent(hireDataFlag));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.1,
              ),
              padding: EdgeInsets.only(
                left: 20.0,
                right: 20.0,
                bottom: 20.0,
              ),
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Hello",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.ORANGE_COLOR,
                          fontSize: 32.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: Text(
                        "Please fill the details below to get started",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        check_for_permissions();
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                          top: 30.0,
                        ),
                        child: Center(
                          child: Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: MyColors.ORANGE_COLOR,
                                    width: 5.0,
                                  ),
                                  borderRadius: BorderRadius.circular(
                                    100.0,
                                  ),
                                ),
                                child: BlocConsumer<ChoosePhotoBloc,
                                    ChoosePhotoState>(
                                  listener: (context, choosePhotoState) {
                                    image_File = choosePhotoState.image_File;
                                  },
                                  builder: (context, choosePhotoState) {
                                    return ClipRRect(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(
                                          100.0,
                                        ),
                                      ),
                                      child: choosePhotoState.image_File != null
                                          ? Image.file(
                                              choosePhotoState.image_File!,
                                              fit: BoxFit.cover,
                                              height: 100.0,
                                              width: 100.0,
                                            )
                                          : Image.asset(
                                              "images/circular_image_placeholder.jpg",
                                              fit: BoxFit.cover,
                                              height: 100.0,
                                              width: 100.0,
                                            ),
                                    );
                                  },
                                ),
                              ),
                              Positioned(
                                right: 0.0,
                                top: -20.0,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                        100.0,
                                      ),
                                      color: MyColors.ORANGE_COLOR),
                                  padding: EdgeInsets.all(
                                    12.0,
                                  ),
                                  child: Icon(
                                    Icons.edit,
                                    color: MyColors.WHITE_COLOR,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10.0,
                        bottom: 4.0,
                      ),
                      child: Text(
                        "Name",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: name_controller,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: MyColors.BG_COLOR_TEXT_FILED,
                        hintText: "Enter Name",
                        hintStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.GREY_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        counterStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        counterText: "",
                        prefixIcon: Icon(
                          Icons.person,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp("[a-zA-Z ]")),
                      ],
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      cursorColor: MyColors.WHITE_COLOR,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value == null || value.trim() == "") {
                          return 'Please enter a valid name';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Text(
                      "Email",
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: 10.0,
                      ),
                      child: TextFormField(
                        controller: email_controller,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.zero,
                          filled: true,
                          fillColor: MyColors.BG_COLOR_TEXT_FILED,
                          hintText: "Enter Email",
                          hintStyle: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            borderRadius: BorderRadius.circular(
                              16.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            borderRadius: BorderRadius.circular(
                              16.0,
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            borderRadius: BorderRadius.circular(
                              16.0,
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            borderRadius: BorderRadius.circular(
                              16.0,
                            ),
                          ),
                          counterStyle: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.WHITE_COLOR,
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          counterText: "",
                          prefixIcon: Icon(
                            Icons.email,
                            color: MyColors.ORANGE_COLOR,
                          ),
                        ),
                        style: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        cursorColor: MyColors.WHITE_COLOR,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value == null) {
                            return "please enter email address";
                          }
                          if (!EmailValidator.validate(value)) {
                            return "please enter valid email address";
                          }

                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Text(
                      "Bio",
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: 10.0,
                      ),
                      child: TextFormField(
                        controller: bioController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.zero,
                          filled: true,
                          fillColor: MyColors.BG_COLOR_TEXT_FILED,
                          hintText: "Enter bio",
                          hintStyle: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.GREY_COLOR,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            borderRadius: BorderRadius.circular(
                              16.0,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            borderRadius: BorderRadius.circular(
                              16.0,
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            borderRadius: BorderRadius.circular(
                              16.0,
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                            ),
                            borderRadius: BorderRadius.circular(
                              16.0,
                            ),
                          ),
                          counterStyle: GoogleFonts.workSans(
                            textStyle: TextStyle(
                              color: MyColors.WHITE_COLOR,
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          counterText: "",
                          prefixIcon: Align(
                            alignment: Alignment.topLeft,
                            widthFactor: 1.0,
                            heightFactor: 7.0,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 4.0, left: 8),
                              child: Icon(
                                Icons.description,
                                color: MyColors.ORANGE_COLOR,
                              ),
                            ),
                          ),
                        ),
                        style: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        cursorColor: MyColors.WHITE_COLOR,
                        keyboardType: TextInputType.multiline,
                        maxLines: 10,
                        validator: (value) {
                          if (value == null || value.trim() == "") {
                            return 'Please enter your bio';
                          }
                          return null;
                        },
                      ),
                    ),
                    /* Padding(
                      padding: const EdgeInsets.only(
                        top: 10.0,
                        bottom: 4.0,
                      ),
                      child: Text(
                        "kind of Talents",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      // autofocus: true,
                      controller: tags_controller,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: MyColors.BG_COLOR_TEXT_FILED,
                        hintText: "Enter kind of Talents",
                        hintStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.GREY_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        counterStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        counterText: "",
                        prefixIcon: Icon(
                          Icons.person,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                      // inputFormatters: [
                      //   FilteringTextInputFormatter.allow(RegExp("[a-zA-Z ]")),
                      // ],
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      cursorColor: MyColors.WHITE_COLOR,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                      // validator: (value) {
                      //   if (value == null || value.trim() == "") {
                      //     return 'Please enter a valid name';
                      //   }
                      //   return null;
                      // },
                    ),*/
                    SizedBox(
                      height: 16.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10.0,
                        bottom: 4.0,
                      ),
                      child: Text(
                        "Facebook Handle",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      controller: fb_link_controller,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: MyColors.BG_COLOR_TEXT_FILED,
                        hintText: "Enter Your Facebook Handle",
                        hintStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.GREY_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        counterStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        counterText: "",
                        prefixIcon: Icon(
                          FontAwesomeIcons.facebook,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      cursorColor: MyColors.WHITE_COLOR,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 16.0,
                        bottom: 4.0,
                      ),
                      child: Text(
                        "Instagram Handle",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      controller: insta_link_controller,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: MyColors.BG_COLOR_TEXT_FILED,
                        hintText: "Enter Your Instagram Handle",
                        hintStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.GREY_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        counterStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        counterText: "",
                        prefixIcon: Icon(
                          FontAwesomeIcons.instagram,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      cursorColor: MyColors.WHITE_COLOR,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 16.0,
                        bottom: 4.0,
                      ),
                      child: Text(
                        "LinkedIn Profile Handle",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      controller: linkedIn_link_controller,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: MyColors.BG_COLOR_TEXT_FILED,
                        hintText: "Enter Your LinkedIn Profile Handle",
                        hintStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.GREY_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        counterStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        counterText: "",
                        prefixIcon: Icon(
                          FontAwesomeIcons.linkedin,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      cursorColor: MyColors.WHITE_COLOR,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 16.0,
                        bottom: 4.0,
                      ),
                      child: Text(
                        "WikiPedia Link",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      controller: wikipedialink_controller,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: MyColors.BG_COLOR_TEXT_FILED,
                        hintText: "Enter Your Wikipedia Link",
                        hintStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.GREY_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        counterStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        counterText: "",
                        prefixIcon: Icon(
                          FontAwesomeIcons.wikipediaW,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      cursorColor: MyColors.WHITE_COLOR,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 16.0,
                        bottom: 4.0,
                      ),
                      child: Text(
                        "IMDB Link",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      controller: imdb_link_controller,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: MyColors.BG_COLOR_TEXT_FILED,
                        hintText: "Enter IMDB Link",
                        hintStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.GREY_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.BG_COLOR_TEXT_FILED,
                          ),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        counterStyle: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        counterText: "",
                        prefixIcon: Icon(
                          FontAwesomeIcons.imdb,
                          color: MyColors.ORANGE_COLOR,
                        ),
                      ),
                      style: GoogleFonts.workSans(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      cursorColor: MyColors.WHITE_COLOR,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 30.0,
                        bottom: 4.0,
                      ),
                      child: Text(
                        "Date of Birth (optional)",
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    BlocConsumer<DobBloc, DobState>(
                      listener: (c, dobState) {
                        dob = dobState.dob!;
                      },
                      builder: (c, dobState) {
                        return GestureDetector(
                          onTap: () async {
                            FocusManager.instance.primaryFocus?.unfocus();
                            DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: dobState.dob == null ||
                                      dobState.dob?.trim() == ""
                                  ? DateTime.now()
                                  : DateFormat(HelperClass.DOB_FORMAT)
                                      .parse(dobState.dob!),
                              firstDate: DateTime(1950),
                              //DateTime.now() - not to allow to choose before today.
                              lastDate: DateTime.now(),
                            );

                            if (pickedDate != null) {
                              var pick_date_formated =
                                  DateFormat(HelperClass.DOB_FORMAT)
                                      .format(pickedDate);

                              dobBloc.add(DobEvent(pick_date_formated));
                            }
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                              borderRadius: BorderRadius.circular(
                                16.0,
                              ),
                            ),
                            padding: EdgeInsets.all(
                              10.0,
                            ),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.calendar_today,
                                  color: MyColors.ORANGE_COLOR,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  dobState.dob != null && dobState.dob != ""
                                      ? dobState.dob ?? ""
                                      : "Date of Birth",
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                    BlocBuilder<GenderBloc, GenderState>(
                      builder: (context, state) {
                        gender = state.index;
                        return Container(
                          decoration: BoxDecoration(
                            color: MyColors.ORANGE_COLOR,
                            borderRadius: BorderRadius.circular(
                              14.0,
                            ),
                          ),
                          padding: EdgeInsets.all(
                            4.0,
                          ),
                          margin: EdgeInsets.only(
                            top: 16.0,
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomRadioButton(
                                  text: HelperClass.MALE_VALUE,
                                  radio_button_index:
                                      HelperClass.MALE_RADIO_BUTTON_INDEX,
                                  selected_index: state.index,
                                  on_tap: () {
                                    if (state.index !=
                                        HelperClass.MALE_RADIO_BUTTON_INDEX) {
                                      genderBloc.add(
                                        GenderEvent(
                                          HelperClass.MALE_RADIO_BUTTON_INDEX,
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Expanded(
                                child: CustomRadioButton(
                                    text: HelperClass.FEMALE_VALUE,
                                    radio_button_index:
                                        HelperClass.FEMALE_RADIO_BUTTON_INDEX,
                                    selected_index: state.index,
                                    on_tap: () {
                                      if (state.index !=
                                          HelperClass
                                              .FEMALE_RADIO_BUTTON_INDEX) {
                                        genderBloc.add(
                                          GenderEvent(
                                            HelperClass
                                                .FEMALE_RADIO_BUTTON_INDEX,
                                          ),
                                        );
                                      }
                                    }),
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Expanded(
                                child: CustomRadioButton(
                                    text: HelperClass.OTHERS_VALUE,
                                    radio_button_index:
                                        HelperClass.OTHERS_RADIO_BUTTON_INDEX,
                                    selected_index: state.index,
                                    on_tap: () {
                                      if (state.index !=
                                          HelperClass
                                              .OTHERS_RADIO_BUTTON_INDEX) {
                                        genderBloc.add(
                                          GenderEvent(
                                            HelperClass
                                                .OTHERS_RADIO_BUTTON_INDEX,
                                          ),
                                        );
                                      }
                                    }),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: BlocConsumer<TalentTypeBloc, TalentTypeState>(
                        listener: (context, talentTypeState) {
                          talentType = talentTypeState.talentType;
                        },
                        builder: (context, talentTypeState) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              IntrinsicHeight(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          FocusManager.instance.primaryFocus
                                              ?.unfocus();
                                          talentTypeBloc
                                              .add(TalentTypeEvent(true));
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: talentTypeState.talentType
                                                  ? MyColors.ORANGE_COLOR
                                                  : MyColors.WHITE_COLOR,
                                              borderRadius:
                                                  BorderRadius.circular(
                                                18.0,
                                              ),
                                              border: talentTypeState.talentType
                                                  ? Border.all(
                                                      color:
                                                          MyColors.WHITE_COLOR,
                                                      width: 2.0)
                                                  : null),
                                          padding: EdgeInsets.all(
                                            8.0,
                                          ),
                                          child: Row(
                                            children: [
                                              Icon(
                                                Icons.person,
                                                color:
                                                    talentTypeState.talentType
                                                        ? MyColors.WHITE_COLOR
                                                        : MyColors.ORANGE_COLOR,
                                              ),
                                              SizedBox(
                                                width: 5.0,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  "I am a Talent",
                                                  style: GoogleFonts.workSans(
                                                    textStyle: TextStyle(
                                                      color: talentTypeState
                                                              .talentType
                                                          ? MyColors.WHITE_COLOR
                                                          : MyColors
                                                              .ORANGE_COLOR,
                                                      fontSize: 16.0,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          FocusManager.instance.primaryFocus
                                              ?.unfocus();
                                          talentTypeBloc
                                              .add(TalentTypeEvent(false));
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: !talentTypeState.talentType
                                                  ? MyColors.ORANGE_COLOR
                                                  : MyColors.WHITE_COLOR,
                                              borderRadius:
                                                  BorderRadius.circular(
                                                18.0,
                                              ),
                                              border: !talentTypeState
                                                      .talentType
                                                  ? Border.all(
                                                      color:
                                                          MyColors.WHITE_COLOR,
                                                      width: 2.0)
                                                  : null),
                                          padding: EdgeInsets.all(
                                            8.0,
                                          ),
                                          child: Row(
                                            children: [
                                              Icon(
                                                Icons.person,
                                                color:
                                                    !talentTypeState.talentType
                                                        ? MyColors.WHITE_COLOR
                                                        : MyColors.ORANGE_COLOR,
                                              ),
                                              SizedBox(
                                                width: 5.0,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  "I am a Talent Seeker",
                                                  style: GoogleFonts.workSans(
                                                    textStyle: TextStyle(
                                                      color: !talentTypeState
                                                              .talentType
                                                          ? MyColors.WHITE_COLOR
                                                          : MyColors
                                                              .ORANGE_COLOR,
                                                      fontSize: 16.0,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              talentTypeState.talentType
                                  ? Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            top: 16.0,
                                          ),
                                          child: BlocConsumer<TalentBarterBloc,
                                              TalentBarterState>(
                                            listener: (c, talentBarterState) {
                                              talentBarterSwitchState =
                                                  talentBarterState
                                                      .isTalentBarter;
                                            },
                                            builder: (c, talentBarterState) {
                                              return Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      Switch(
                                                        value: talentBarterState
                                                            .isTalentBarter,
                                                        onChanged:
                                                            (bool value) {
                                                          talentBarterBloc.add(
                                                              TalentBarterEvent(
                                                                  value));
                                                        },
                                                        activeColor: MyColors
                                                            .WHITE_COLOR,
                                                        activeTrackColor:
                                                            MyColors
                                                                .ORANGE_COLOR,
                                                        inactiveThumbColor:
                                                            MyColors
                                                                .WHITE_COLOR,
                                                        inactiveTrackColor:
                                                            MyColors
                                                                .ORANGE_COLOR,
                                                      ),
                                                      SizedBox(
                                                        width: 10.0,
                                                      ),
                                                      Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          color: MyColors
                                                              .WHITE_COLOR,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                            18.0,
                                                          ),
                                                        ),
                                                        padding: EdgeInsets.all(
                                                          8.0,
                                                        ),
                                                        child: Text(
                                                          "Open for Talent Barter",
                                                          style: GoogleFonts
                                                              .workSans(
                                                            textStyle:
                                                                TextStyle(
                                                              color: MyColors
                                                                  .BLACK_COLOR,
                                                              fontSize: 12.0,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 10.0,
                                                      ),
                                                      GestureDetector(
                                                        onTap: () {
                                                          var alertDialog =
                                                              AlertDialog(
                                                            backgroundColor:
                                                                MyColors
                                                                    .BG_COLOR,
                                                            content: Container(
                                                              // height: 100.0,
                                                              child: Text(
                                                                HelperClass
                                                                    .TALENT_BARTER_TEXT,
                                                                style: GoogleFonts
                                                                    .montserrat(
                                                                  textStyle:
                                                                      const TextStyle(
                                                                    color: MyColors
                                                                        .WHITE_COLOR,
                                                                    fontSize:
                                                                        14.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          );
                                                          showDialog(
                                                              context: context,
                                                              // barrierDismissible: false,
                                                              builder: (ctx) {
                                                                return alertDialog;
                                                              });
                                                        },
                                                        child: const Padding(
                                                          padding:
                                                              EdgeInsets.all(
                                                            8.0,
                                                          ),
                                                          child: Icon(
                                                            Icons.help,
                                                            color: MyColors
                                                                .ORANGE_COLOR,
                                                            size: 16,
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  talentBarterState
                                                          .isTalentBarter
                                                      ? Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .only(
                                                                top: 16.0,
                                                                bottom: 4.0,
                                                              ),
                                                              child: Text(
                                                                "Trading For",
                                                                style: GoogleFonts
                                                                    .montserrat(
                                                                  textStyle:
                                                                      const TextStyle(
                                                                    color: MyColors
                                                                        .WHITE_COLOR,
                                                                    fontSize:
                                                                        16.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            TextField(
                                                              // autofocus: true,
                                                              controller:
                                                                  barterDescriptionController,
                                                              decoration:
                                                                  InputDecoration(
                                                                filled: true,
                                                                fillColor: MyColors
                                                                    .BG_COLOR_TEXT_FILED,
                                                                hintText:
                                                                    "Enter Trading For Description",
                                                                hintStyle:
                                                                    GoogleFonts
                                                                        .workSans(
                                                                  textStyle:
                                                                      const TextStyle(
                                                                    color: MyColors
                                                                        .GREY_COLOR,
                                                                    fontSize:
                                                                        16.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                                focusedBorder:
                                                                    OutlineInputBorder(
                                                                  borderSide:
                                                                      const BorderSide(
                                                                    color: MyColors
                                                                        .BG_COLOR_TEXT_FILED,
                                                                  ),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                    16.0,
                                                                  ),
                                                                ),
                                                                enabledBorder:
                                                                    OutlineInputBorder(
                                                                  borderSide:
                                                                      const BorderSide(
                                                                    color: MyColors
                                                                        .BG_COLOR_TEXT_FILED,
                                                                  ),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                    16.0,
                                                                  ),
                                                                ),
                                                                disabledBorder:
                                                                    OutlineInputBorder(
                                                                  borderSide:
                                                                      const BorderSide(
                                                                    color: MyColors
                                                                        .BG_COLOR_TEXT_FILED,
                                                                  ),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                    16.0,
                                                                  ),
                                                                ),
                                                                errorBorder:
                                                                    OutlineInputBorder(
                                                                  borderSide:
                                                                      const BorderSide(
                                                                    color: MyColors
                                                                        .BG_COLOR_TEXT_FILED,
                                                                  ),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                    16.0,
                                                                  ),
                                                                ),
                                                                counterStyle:
                                                                    GoogleFonts
                                                                        .workSans(
                                                                  textStyle:
                                                                      const TextStyle(
                                                                    color: MyColors
                                                                        .WHITE_COLOR,
                                                                    fontSize:
                                                                        10.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                                counterText: "",
                                                              ),
                                                              style: GoogleFonts
                                                                  .workSans(
                                                                textStyle:
                                                                    TextStyle(
                                                                  color: MyColors
                                                                      .WHITE_COLOR,
                                                                  fontSize:
                                                                      16.0,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                ),
                                                              ),
                                                              cursorColor: MyColors
                                                                  .WHITE_COLOR,
                                                              textInputAction:
                                                                  TextInputAction
                                                                      .done,
                                                              keyboardType:
                                                                  TextInputType
                                                                      .text,
                                                            ),
                                                          ],
                                                        )
                                                      : SizedBox.shrink(),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 10.0,
                                                bottom: 4.0,
                                              ),
                                              child: Text(
                                                "Visible Data For Hire",
                                                style: GoogleFonts.montserrat(
                                                  textStyle: TextStyle(
                                                    color: MyColors.WHITE_COLOR,
                                                    fontSize: 16.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            BlocBuilder<HireDataShowBloc,
                                                HireDataShowState>(
                                              builder: (context, state) {
                                                hireDataFlag = state.index;
                                                return Container(
                                                  decoration: BoxDecoration(
                                                    color:
                                                        MyColors.ORANGE_COLOR,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      14.0,
                                                    ),
                                                  ),
                                                  padding: EdgeInsets.all(
                                                    4.0,
                                                  ),
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        child:
                                                            CustomRadioButton(
                                                          text: HelperClass
                                                              .BOTH_DATA_VALUE,
                                                          radio_button_index:
                                                              HelperClass
                                                                  .BOTH_DATA_RADIO_BUTTON_INDEX,
                                                          selected_index:
                                                              state.index,
                                                          on_tap: () {
                                                            if (state.index !=
                                                                HelperClass
                                                                    .BOTH_DATA_RADIO_BUTTON_INDEX) {
                                                              hireDataShowBloc
                                                                  .add(
                                                                HireDataShowEvent(
                                                                  HelperClass
                                                                      .BOTH_DATA_RADIO_BUTTON_INDEX,
                                                                ),
                                                              );
                                                            }
                                                          },
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 10.0,
                                                      ),
                                                      Expanded(
                                                        child:
                                                            CustomRadioButton(
                                                          text: HelperClass
                                                              .MOBILE_VALUE,
                                                          radio_button_index:
                                                              HelperClass
                                                                  .MOBILE_RADIO_BUTTON_INDEX,
                                                          selected_index:
                                                              state.index,
                                                          on_tap: () {
                                                            if (state.index !=
                                                                HelperClass
                                                                    .MOBILE_RADIO_BUTTON_INDEX) {
                                                              hireDataShowBloc
                                                                  .add(
                                                                HireDataShowEvent(
                                                                  HelperClass
                                                                      .MOBILE_RADIO_BUTTON_INDEX,
                                                                ),
                                                              );
                                                            }
                                                          },
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 10.0,
                                                      ),
                                                      Expanded(
                                                        child:
                                                            CustomRadioButton(
                                                          text: HelperClass
                                                              .EMAIL_VALUE,
                                                          radio_button_index:
                                                              HelperClass
                                                                  .EMAIL_RADIO_BUTTON_INDEX,
                                                          selected_index:
                                                              state.index,
                                                          on_tap: () {
                                                            if (state.index !=
                                                                HelperClass
                                                                    .EMAIL_RADIO_BUTTON_INDEX) {
                                                              hireDataShowBloc
                                                                  .add(
                                                                HireDataShowEvent(
                                                                  HelperClass
                                                                      .EMAIL_RADIO_BUTTON_INDEX,
                                                                ),
                                                              );
                                                            }
                                                          },
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                        BlocConsumer<CategoriesNewBloc,
                                            CategoriesNewState>(
                                          listener:
                                              (context, categoriesNewState) {
                                            if (categoriesNewState
                                                is CategoriesNewLoadedState) {
                                              selectedCategoriesList =
                                                  categoriesNewState
                                                      .selectedCategoriesList;
                                            }
                                          },
                                          builder:
                                              (context, categoriesNewState) {
                                            if (categoriesNewState
                                                is CategoriesNewLoadingState) {
                                              return CircularIndicatorWidget();
                                            } else if (categoriesNewState
                                                is CategoriesNewErrorState) {
                                              return CustomErrorWidget(
                                                on_tap: () {
                                                  categoriesNewBloc.add(
                                                      CategoriesNewLoadingEvent(
                                                          categoriesNewState
                                                              .categoryTitle));
                                                },
                                              );
                                            } else if (categoriesNewState
                                                is CategoriesNewLoadedState) {
                                              return Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                      top: 10.0,
                                                      bottom: 4.0,
                                                    ),
                                                    child: Text(
                                                      "Category",
                                                      style: GoogleFonts
                                                          .montserrat(
                                                        textStyle:
                                                            const TextStyle(
                                                          color: MyColors
                                                              .WHITE_COLOR,
                                                          fontSize: 16.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      FocusManager
                                                          .instance.primaryFocus
                                                          ?.unfocus();
                                                      showMultipleCategoryDialog(
                                                          context,
                                                          categoriesNewState
                                                              .categoriesList);
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        color: MyColors
                                                            .BG_COLOR_TEXT_FILED,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                          16.0,
                                                        ),
                                                      ),
                                                      padding: EdgeInsets.all(
                                                        12.0,
                                                      ),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Expanded(
                                                            child: Text(
                                                              categoriesNewState
                                                                      .selectedCategoriesList
                                                                      .isNotEmpty
                                                                  ? HelperClass.getTextsFromSelectedCategories(
                                                                      selectedCategoriesList:
                                                                          categoriesNewState
                                                                              .selectedCategoriesList)
                                                                  : "Select Category",
                                                              style: GoogleFonts
                                                                  .workSans(
                                                                textStyle:
                                                                    const TextStyle(
                                                                  color: MyColors
                                                                      .WHITE_COLOR,
                                                                  fontSize:
                                                                      16.0,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          const Icon(
                                                            Icons
                                                                .keyboard_arrow_down,
                                                            color: MyColors
                                                                .ORANGE_COLOR,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              );
                                            }
                                            return Container();
                                          },
                                        ),
                                      ],
                                    )
                                  : Container(),
                            ],
                          );
                        },
                      ),
                    ),
                    BlocBuilder<CountriesBloc, CountriesState>(
                      builder: (c, countriesState) {
                        countryId = -1;
                        countryName = "";
                        stateId = -1;
                        stateName = "";
                        cityId = -1;
                        cityName = "";

                        if (countriesState is CountriesLoadingState) {
                          return Padding(
                            padding: const EdgeInsets.all(
                              16.0,
                            ),
                            child: CircularIndicatorWidget(),
                          );
                        } else if (countriesState
                            is CountriesDropDownErrorState) {
                          return Container(
                            margin: const EdgeInsets.only(
                              top: 16.0,
                            ),
                            child: CustomErrorWidget(
                              on_tap: () {
                                countriesBloc.add(CountriesLoadingEvent(
                                    countriesState.currentCountryTitle,
                                    countriesState.isStatePreviouslyAvailable));
                              },
                            ),
                          );
                        } else if (countriesState
                            is CountriesDropDownLoadedState) {
                          if (countriesState.selected_index != -1) {
                            /// load states based on country id

                            countryName = countriesState
                                .countriesList[countriesState.selected_index]
                                .name!;
                            countryId = countriesState
                                .countriesList[countriesState.selected_index]
                                .id!;

                            statesBloc.add(StatesDropDownLoadingEvent(
                                countryId, null, false));
                          }
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 10.0,
                                  bottom: 4.0,
                                ),
                                child: Text(
                                  "Country",
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  FocusManager.instance.primaryFocus?.unfocus();
                                  showAddressDialog(
                                      context,
                                      AddressEnum.countryE,
                                      countriesState.countriesList,
                                      countriesState.selected_index);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: MyColors.BG_COLOR_TEXT_FILED,
                                    borderRadius: BorderRadius.circular(
                                      16.0,
                                    ),
                                  ),
                                  padding: EdgeInsets.all(
                                    12.0,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        countriesState.selected_index == -1
                                            ? "Select Country"
                                            : countriesState
                                                .countriesList[countriesState
                                                    .selected_index]
                                                .name!,
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.WHITE_COLOR,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                      Icon(
                                        Icons.keyboard_arrow_down,
                                        color: MyColors.ORANGE_COLOR,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              BlocBuilder<StatesBloc, StatesState>(
                                  builder: (context, statesState) {
                                stateId = -1;
                                stateName = "";
                                cityId = -1;
                                cityName = "";
                                if (statesState is StatesLoadingState) {
                                  return Padding(
                                    padding: const EdgeInsets.all(
                                      16.0,
                                    ),
                                    child: CircularIndicatorWidget(),
                                  );
                                } else if (statesState
                                    is StatesDropDownErrorState) {
                                  return Padding(
                                    padding: const EdgeInsets.all(
                                      16.0,
                                    ),
                                    child: CustomErrorWidget(
                                      on_tap: () {
                                        statesBloc.add(
                                            StatesDropDownLoadingEvent(
                                                statesState.countryId,
                                                statesState.availableStateName,
                                                false));
                                      },
                                    ),
                                  );
                                } else if (statesState
                                    is StatesDropDownLoadedState) {
                                  if (statesState.selected_index != -1) {
                                    stateName = statesState
                                        .statesList[statesState.selected_index]
                                        .name!;
                                    stateId = statesState
                                        .statesList[statesState.selected_index]
                                        .id!;
                                    citiesBloc.add(CitiesDropDownLoadingEvent(
                                        stateId, null));
                                  }
                                  return Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 10.0,
                                          bottom: 4.0,
                                        ),
                                        child: Text(
                                          "State",
                                          style: GoogleFonts.montserrat(
                                            textStyle: const TextStyle(
                                              color: MyColors.WHITE_COLOR,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          FocusManager.instance.primaryFocus
                                              ?.unfocus();
                                          showAddressDialog(
                                              context,
                                              AddressEnum.stateE,
                                              statesState.statesList,
                                              statesState.selected_index);
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: MyColors.BG_COLOR_TEXT_FILED,
                                            borderRadius: BorderRadius.circular(
                                              16.0,
                                            ),
                                          ),
                                          padding: EdgeInsets.all(
                                            12.0,
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                statesState.statesList
                                                            .isEmpty ||
                                                        statesState
                                                                .selected_index ==
                                                            -1
                                                    ? "Select State"
                                                    : statesState
                                                        .statesList[statesState
                                                            .selected_index]
                                                        .name!,
                                                style: GoogleFonts.workSans(
                                                  textStyle: const TextStyle(
                                                    color: MyColors.WHITE_COLOR,
                                                    fontSize: 16.0,
                                                  ),
                                                ),
                                              ),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: MyColors.ORANGE_COLOR,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      stateId != -1
                                          ? BlocBuilder<CitiesBloc,
                                                  CitiesState>(
                                              builder: (c, citiesState) {
                                              cityId = -1;
                                              cityName = "";
                                              if (citiesState
                                                  is CitiesLoadingState) {
                                                return const Padding(
                                                  padding: EdgeInsets.all(
                                                    16.0,
                                                  ),
                                                  child:
                                                      CircularIndicatorWidget(),
                                                );
                                              } else if (citiesState
                                                  is CitiesDropDownErrorState) {
                                                return Padding(
                                                  padding: const EdgeInsets.all(
                                                    16.0,
                                                  ),
                                                  child: CustomErrorWidget(
                                                    on_tap: () {
                                                      citiesBloc.add(
                                                          CitiesDropDownLoadingEvent(
                                                              citiesState
                                                                  .StateId,
                                                              citiesState
                                                                  .availableCityName));
                                                    },
                                                  ),
                                                );
                                              } else if (citiesState
                                                  is CitiesDropDownLoadedState) {
                                                if (citiesState
                                                        .selected_index !=
                                                    -1) {
                                                  cityId = citiesState
                                                      .citiesList[citiesState
                                                          .selected_index]
                                                      .id!;
                                                  cityName = citiesState
                                                      .citiesList[citiesState
                                                          .selected_index]
                                                      .name!;
                                                }

                                                return Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        top: 10.0,
                                                        bottom: 4.0,
                                                      ),
                                                      child: Text(
                                                        "City",
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          textStyle:
                                                              const TextStyle(
                                                            color: MyColors
                                                                .WHITE_COLOR,
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {
                                                        FocusManager.instance
                                                            .primaryFocus
                                                            ?.unfocus();
                                                        showAddressDialog(
                                                            context,
                                                            AddressEnum.cityE,
                                                            citiesState
                                                                .citiesList,
                                                            citiesState
                                                                .selected_index);
                                                      },
                                                      child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          color: MyColors
                                                              .BG_COLOR_TEXT_FILED,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                            16.0,
                                                          ),
                                                        ),
                                                        // margin: EdgeInsets.only(
                                                        //   top: 16.0,
                                                        // ),
                                                        padding: EdgeInsets.all(
                                                          12.0,
                                                        ),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Text(
                                                              citiesState.citiesList
                                                                          .isEmpty ||
                                                                      citiesState
                                                                              .selected_index ==
                                                                          -1
                                                                  ? "Select City"
                                                                  : citiesState
                                                                      .citiesList[
                                                                          citiesState
                                                                              .selected_index]
                                                                      .name!,
                                                              style: GoogleFonts
                                                                  .workSans(
                                                                textStyle:
                                                                    const TextStyle(
                                                                  color: MyColors
                                                                      .WHITE_COLOR,
                                                                  fontSize:
                                                                      16.0,
                                                                ),
                                                              ),
                                                            ),
                                                            const Icon(
                                                              Icons
                                                                  .keyboard_arrow_down,
                                                              color: MyColors
                                                                  .ORANGE_COLOR,
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                );
                                              }

                                              return Container();
                                            })
                                          : Container(),
                                    ],
                                  );
                                }

                                return Container();
                              })
                            ],
                          );
                        }
                        return Container();
                      },
                    ),
                    Container(
                      alignment: Alignment.centerRight,
                      margin: const EdgeInsets.only(
                        top: 50.0,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              // fromHeight use double.infinity as width and 40 is the height
                              backgroundColor: MyColors.GREY_COLOR,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18),
                              ),
                            ),
                            onPressed: () async {
                              FocusManager.instance.primaryFocus?.unfocus();
                              await SharedPrefs.setBool(
                                  SharedPrefs.isUserDataCompletedKey, true);

                              Navigator.pushNamedAndRemoveUntil(
                                  context,
                                  AddSubscriptionsScreen
                                      .ADD_SUBSCRIPTIONS_SCREEN,
                                  arguments: {
                                    AddSubscriptionsScreen.SHOW_SKIP_KEY: true,
                                    AddSubscriptionsScreen
                                        .CURRENT_SUBSCRIPTION_key: 0
                                  },
                                  (route) => false);
                            },
                            child: Text(
                              "Skip",
                              style: GoogleFonts.workSans(
                                textStyle: const TextStyle(
                                  color: MyColors.WHITE_COLOR,
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              // fromHeight use double.infinity as width and 40 is the height
                              backgroundColor: MyColors.ORANGE_COLOR,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18),
                              ),
                            ),
                            onPressed: () async {
                              FocusManager.instance.primaryFocus?.unfocus();

                              String msg = "";

                              int userId = SharedPrefs.getUserId()!;
                              var url =
                                  "${ApisServices.BASE_URL}v2/updateusers/$userId/update";
                              var request =
                                  http.MultipartRequest("POST", Uri.parse(url));

                              if (!formKey.currentState!.validate()) {
                                // return;
                              }

                              if (image_File == null) {
                                msg += "please select your photo\n";
                              }

                              if (name_controller.text.trim() == "") {
                                msg += "please enter name\n";
                              } else {
                                request.fields[HelperClass.NAME_KEY] =
                                    name_controller.text;
                              }
                              if (email_controller.text.trim() == "") {
                                msg += "please enter email address\n";
                              } else if (!EmailValidator.validate(
                                  email_controller.text.trim())) {
                                msg += "please enter valid email address\n";
                              } else {
                                request.fields[HelperClass.EMAIL_KEY] =
                                    email_controller.text;
                              }
                              if (bioController.text.trim() == "") {
                                msg += "please enter your bio\n";
                              } else {
                                request.fields[HelperClass.DESCRIPTION_KEY] =
                                    bioController.text;
                              }
                              if (countryName.trim() != "") {
                                request.fields[HelperClass.COUNTRY_KEY] =
                                    countryName.trim();
                              } else {
                                msg += "please select country\n";
                              }
                              if (stateName.trim() != "") {
                                request.fields[HelperClass.STATE_KEY] =
                                    stateName.trim();
                              } else {
                                msg += "please select state\n";
                              }
                              if (cityName.trim() != "") {
                                request.fields[HelperClass.CITY_KEY] =
                                    cityName.trim();
                              } else {
                                msg += "please select city\n";
                              }

                              if (talentType) {
                                /// i am a talent

                                request.fields[HelperClass.TALENT_TYPE_KEY] =
                                    "1";

                                request.fields[HelperClass.HIRE_DATA_FLAG_KEY] =
                                    "$hireDataFlag";

                                if (selectedCategoriesList.isEmpty) {
                                  msg += "please select category\n";
                                } else {
                                  List<int> selectedCategoriesIdList = [];
                                  for (CategoriesModel category
                                      in selectedCategoriesList) {
                                    selectedCategoriesIdList.add(category.id);
                                  }

                                  request.fields[
                                          HelperClass.CATEGORY_IDS_NEW_KEY] =
                                      selectedCategoriesIdList.join(",");
                                }

                                if (talentBarterSwitchState) {
                                  request.fields[HelperClass.TRADE_BARTER_KEY] =
                                      "1";
                                  if (barterDescriptionController.text.trim() !=
                                      "") {
                                    request.fields[HelperClass
                                            .BARTER_DESCRIPTION_KEY] =
                                        barterDescriptionController.text.trim();
                                  } else {
                                    msg += "enter Trading For description\n";
                                  }
                                } else {
                                  request.fields[HelperClass.TRADE_BARTER_KEY] =
                                      "0";
                                }
                              } else {
                                request.fields[HelperClass.TALENT_TYPE_KEY] =
                                    "0";
                              }

                              if (msg != "") {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(
                                      msg,
                                    ),
                                    duration: const Duration(
                                      seconds: 1,
                                    ),
                                  ),
                                );
                              } else {
                                request.fields[HelperClass.DOB_KEY] = dob!;
                                if (tags_controller.text.trim() != "") {
                                  request.fields[HelperClass.TAGS_KEY] =
                                      tags_controller.text;
                                }
                                if (fb_link_controller.text.trim() != "") {
                                  request.fields[HelperClass.FB_LINK_KEY] =
                                      fb_link_controller.text;
                                }
                                if (insta_link_controller.text.trim() != "") {
                                  request.fields[HelperClass.INSTA_LINK_KEY] =
                                      insta_link_controller.text;
                                }
                                if (linkedIn_link_controller.text.trim() !=
                                    "") {
                                  request.fields[
                                          HelperClass.LINKED_IN_LINK_KEY] =
                                      linkedIn_link_controller.text;
                                }
                                if (wikipedialink_controller.text.trim() !=
                                    "") {
                                  request.fields[
                                          HelperClass.WIKIPEDIA_LINK_KEY] =
                                      wikipedialink_controller.text;
                                }
                                if (imdb_link_controller.text.trim() != "") {
                                  request.fields[HelperClass.IMDB_LINK_KEY] =
                                      imdb_link_controller.text;
                                }
                                if (gender ==
                                    HelperClass.MALE_RADIO_BUTTON_INDEX) {
                                  request.fields[HelperClass.GENDER_KEY] =
                                      HelperClass.MALE_VALUE;
                                } else if (gender ==
                                    HelperClass.FEMALE_RADIO_BUTTON_INDEX) {
                                  request.fields[HelperClass.GENDER_KEY] =
                                      HelperClass.FEMALE_VALUE;
                                } else if (gender ==
                                    HelperClass.OTHERS_RADIO_BUTTON_INDEX) {
                                  request.fields[HelperClass.GENDER_KEY] =
                                      HelperClass.OTHERS_VALUE;
                                }

                                HelperClass.show_alert_Dialog(
                                    context, "updating profile");

                                request.headers["Content-Type"] =
                                    'application/json; charset=UTF-8';
                                request.headers["Accept"] = 'application/json';
                                request.headers["Authorization"] =
                                    SharedPrefs.getAccessToken()!;
                                http.MultipartFile multipartFile =
                                    await http.MultipartFile.fromPath(
                                        "avatar", image_File!.path);
                                request.files.add(multipartFile);

                                var response = await request.send();
                                var streamRes =
                                    await http.Response.fromStream(response);

                                Navigator.of(context).pop();

                                if (streamRes.statusCode == 200) {
                                  var model = UserDataModel.fromJson(
                                      jsonDecode(streamRes.body));
                                  if (model.status == 200) {
                                    FireBaseHelperClass
                                        .updateUserProfileInFirebase(
                                            firestore:
                                                FirebaseFirestore.instance,
                                            userModel: model.userModel!);
                                    HelperClass.show_snackbar(
                                        context, "profile updated");

                                    await SharedPrefs.setBool(
                                        SharedPrefs.isUserDataCompletedKey,
                                        true);

                                    Navigator.pushNamedAndRemoveUntil(
                                        context,
                                        AddSubscriptionsScreen
                                            .ADD_SUBSCRIPTIONS_SCREEN,
                                        arguments: {
                                          AddSubscriptionsScreen.SHOW_SKIP_KEY:
                                              true,
                                          AddSubscriptionsScreen
                                              .CURRENT_SUBSCRIPTION_key: 0
                                        },
                                        (route) => false);
                                  } else {
                                    HelperClass.show_snackbar(
                                        context,
                                        model.message ??
                                            HelperClass.ERROR_TEXT);
                                  }
                                } else {
                                  print(HelperClass.ERROR_TEXT);
                                  HelperClass.show_snackbar(
                                      context, HelperClass.ERROR_TEXT);
                                }
                              }
                            },
                            child: Text(
                              "Save & Continue",
                              style: GoogleFonts.workSans(
                                textStyle: TextStyle(
                                  color: MyColors.WHITE_COLOR,
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showMultipleCategoryDialog(
      BuildContext context, List<CategoriesModel> categoriesList) {
    List<CategoriesModel> selectedCategoriesList = [];

    List<CategoriesModel> previousCategoriesList = [];
    categoriesList.forEach((element) {
      CategoriesModel category = CategoriesModel(
        id: element.id,
        description: element.description,
        image: element.image,
        isSelected: element.isSelected,
        title: element.title,
        subcategoriesList: element.subcategoriesList,
      );
      previousCategoriesList.add(category);
    });

    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        "Select Category",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: BlocBuilder<CategoriesNewBloc, CategoriesNewState>(
        builder: (context, state) {
          return Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: MediaQuery.of(context).size.height * 0.6,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: categoriesList.length,
              itemBuilder: (ctx, index) {
                return GestureDetector(
                  onTap: () {
                    categoriesList[index].isSelected =
                        !categoriesList[index].isSelected;
                    categoriesNewBloc.add(CategoriesNewChangeEvent(
                        categoriesList, selectedCategoriesList));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(
                            16.0,
                          ),
                          child: Text(
                            categoriesList[index].title,
                            style: GoogleFonts.workSans(
                              textStyle: TextStyle(
                                color: MyColors.ORANGE_COLOR,
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Theme(
                        data: ThemeData(
                          unselectedWidgetColor: MyColors.ORANGE_COLOR,
                        ),
                        child: Checkbox(
                          activeColor: MyColors.ORANGE_COLOR,
                          value: categoriesList[index].isSelected,
                          onChanged: (value) {
                            categoriesList[index].isSelected =
                                !categoriesList[index].isSelected;
                            categoriesNewBloc.add(CategoriesNewChangeEvent(
                                categoriesList, selectedCategoriesList));
                          },
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        },
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                // Navigator.pop(context);
                List<CategoriesModel> selectedCategoriesList1 = [];
                for (CategoriesModel category in previousCategoriesList) {
                  if (category.isSelected) {
                    selectedCategoriesList1.add(category);
                  }
                }
                categoriesNewBloc.add(CategoriesNewChangeEvent(
                    previousCategoriesList, selectedCategoriesList1));
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Cancel",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                for (CategoriesModel category in categoriesList) {
                  if (category.isSelected) {
                    selectedCategoriesList.add(category);
                  }
                }

                categoriesNewBloc.add(CategoriesNewChangeEvent(
                    categoriesList, selectedCategoriesList));
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Save",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
    showDialog(
      context: context,
      builder: (ctx) {
        return WillPopScope(
          child: alert,
          onWillPop: () async => false,
        );
      },
    );
  }

  void showAddressDialog(BuildContext context, AddressEnum addressEnum,
      List<AddressModel> addressList, int previousIndex) {
    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        addressEnum == AddressEnum.countryE
            ? "Select Country"
            : addressEnum == AddressEnum.stateE
                ? "Select State"
                : "Select City",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.8,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: addressList.length,
          itemBuilder: (ctx, index) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context, rootNavigator: true).pop();

                if (previousIndex != index) {
                  addressEnum == AddressEnum.countryE
                      ? countriesBloc
                          .add(CountriesDropDownChangeEvent(addressList, index))
                      : addressEnum == AddressEnum.stateE
                          ? statesBloc.add(
                              StatesDropDownChangeEvent(addressList, index))
                          : citiesBloc.add(
                              CitiesDropDownChangeEvent(addressList, index));
                }
              },
              child: Container(
                padding: EdgeInsets.all(
                  16.0,
                ),
                child: Text(
                  addressList[index].name!,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.ORANGE_COLOR,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
    showDialog(
      context: context,
      builder: (ctx) {
        return alert;
      },
    );
  }

  void check_for_permissions() async {
    var permission = Permission.photos;
    if (Platform.isAndroid) {
      var deviceInfo = DeviceInfoPlugin();
      final androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.version.sdkInt <= 32) {
        /// use [Permissions.storage.status]
        permission = Permission.storage;
      } else {
        /// use [Permissions.photos.status]
        permission = Permission.photos;
      }
    }

    Map<Permission, PermissionStatus> perms =
        await [permission, Permission.camera].request();

    if (perms[permission]!.isGranted && perms[Permission.camera]!.isGranted) {
      showAlertDialog();
    } else
    // if (perms[Permission.storage]!.isDenied ||
    //   perms[Permission.camera]!.isDenied)
    {
      String msg = "";
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        msg =
            "Storage, camera Permission is need to get the photo, please enable it in settings";
      } else {
        msg = "Storage, camera Permission is need to get the photo";
      }

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
        duration: Duration(seconds: 1),
      ));
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        Future.delayed(Duration(milliseconds: 1500), () {
          openAppSettings();
        });
      }
    }
  }

  void showAlertDialog() {
    AlertDialog alertDialog = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              _getFromGallery();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Gallery",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              _getFromCamera();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Camera",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return alertDialog;
      },
    );
  }

  _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      choosePhotoBloc.add(ChoosePhotoEvent(File(pickedFile.path)));
    }
  }

  _getFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      choosePhotoBloc.add(ChoosePhotoEvent(File(pickedFile.path)));
    }
  }
}
