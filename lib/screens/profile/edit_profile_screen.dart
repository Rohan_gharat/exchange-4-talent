import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:e4t/blocks/CategoriesBlocNew/categories_new_bloc.dart';
import 'package:e4t/blocks/HireDataShowBloc/hire_data_show_bloc.dart';
import 'package:e4t/blocks/SubCategoryBloc/sub_category_drop_down_bloc.dart';
import 'package:e4t/blocks/categoriesDropDownBloc/categories_drop_down_bloc.dart';
import 'package:e4t/blocks/talentBarterBloc/talent_barter_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:e4t/model/userModel/user_data_model.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/subscriptions/checkout_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/customRadioButton.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../blocks/CitiesBloc/cities_bloc.dart';
import '../../blocks/CountriesBloc/countries_bloc.dart';
import '../../blocks/GenderBloc/gender_bloc.dart';
import '../../blocks/StatesBloc/states_bloc.dart';
import '../../blocks/dobBloc/dob_bloc.dart';
import '../../blocks/profilePhotoUpdateBloc/profile_photo_update_bloc.dart';
import '../../model/addressModel/addressModel.dart';
import 'package:http/http.dart' as http;

class EditProfileScreen extends StatefulWidget {
  EditProfileScreen({super.key, this.popScreenCheck = false});

  static const EDIT_PROFILE_ROUT = "editProfileRoute";
  bool popScreenCheck = false;

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  var formKey = GlobalKey<FormState>();

  File? image_File;

  late UserBloc userBloc;
  late var updateProfilePhotoBloc;
  late var dobBloc;
  late var genderBloc;
  late var categoriesDropDownBloc;
  late var subCategoryDropDownBloc;
  late var talentBarterBloc;
  late var hireDataShowBloc;

  late var countriesBloc;
  late var statesBloc;
  late var citiesBloc;

  late var categoriesNewBloc;

  List<CategoriesModel> selectedCategoriesList = [];

  int selectedCategoryIndex = -1;
  int selectedSubCategoryIndex = -1;
  String? dob = "";
  var gender = 1;
  var categoryId = -1;
  var subCategoryId = -1;

  var talent_type = 0;
  int countryId = -1;
  int stateId = -1;
  int cityId = -1;
  int hireDataFlag = 1;

  String countryName = "";
  String stateName = "";
  String cityName = "";
  bool talentBarterSwitchState = true;

  var name_controller = TextEditingController();
  var email_controller = TextEditingController();
  var bio_controller = TextEditingController();
  var fb_link_controller = TextEditingController();
  var insta_link_controller = TextEditingController();
  var linkedIn_link_controller = TextEditingController();
  var wikipedialink_controller = TextEditingController();
  var imdb_link_controller = TextEditingController();
  var tags_controller = TextEditingController();
  var barterDescriptionController = TextEditingController();

  UserModel? userModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBloc = BlocProvider.of<UserBloc>(context);
    dobBloc = BlocProvider.of<DobBloc>(context);
    genderBloc = BlocProvider.of<GenderBloc>(context);
    categoriesDropDownBloc = BlocProvider.of<CategoriesDropDownBloc>(context);
    subCategoryDropDownBloc = BlocProvider.of<SubCategoryDropDownBloc>(context);

    countriesBloc = BlocProvider.of<CountriesBloc>(context);

    statesBloc = BlocProvider.of<StatesBloc>(context);
    citiesBloc = BlocProvider.of<CitiesBloc>(context);

    updateProfilePhotoBloc = BlocProvider.of<ProfilePhotoUpdateBloc>(context);

    talentBarterBloc = BlocProvider.of<TalentBarterBloc>(context);

    categoriesNewBloc = BlocProvider.of<CategoriesNewBloc>(context);
    hireDataShowBloc = BlocProvider.of<HireDataShowBloc>(context);

    userModel = userBloc.myUserData;
    if (userModel != null) {
      talent_type = userModel!.talentType ?? 1;
      name_controller.text = userModel?.name ?? "";
      if (userModel!.email != null) {
        email_controller.text = userModel!.email;
      }
      bio_controller.text = userModel!.description ?? "";
      fb_link_controller.text = userModel!.fb_link ?? "";
      insta_link_controller.text = userModel!.insta_link ?? "";
      linkedIn_link_controller.text = userModel!.linkedin_link ?? "";
      wikipedialink_controller.text = userModel!.wikipedia_link ?? "";
      imdb_link_controller.text = userModel!.imdb_link ?? "";
      tags_controller.text = userModel!.tags ?? "";
      barterDescriptionController.text = userModel!.barter_description ?? "";

      dob = userModel!.dob;
      dobBloc.add(DobEvent(userModel!.dob));
      if (userModel!.gender == null) {
        gender = HelperClass.MALE_RADIO_BUTTON_INDEX;
        genderBloc.add(GenderEvent(HelperClass.MALE_RADIO_BUTTON_INDEX));
      } else if (userModel!.gender.toString().trim() ==
          HelperClass.MALE_VALUE) {
        gender = HelperClass.MALE_RADIO_BUTTON_INDEX;
        genderBloc.add(GenderEvent(HelperClass.MALE_RADIO_BUTTON_INDEX));
      } else if (userModel!.gender.toString().trim() ==
          HelperClass.FEMALE_VALUE) {
        gender = HelperClass.FEMALE_RADIO_BUTTON_INDEX;
        genderBloc.add(GenderEvent(HelperClass.FEMALE_RADIO_BUTTON_INDEX));
      } else if (userModel!.gender.toString().trim() ==
          HelperClass.OTHERS_VALUE) {
        gender = HelperClass.OTHERS_RADIO_BUTTON_INDEX;
        genderBloc.add(GenderEvent(HelperClass.OTHERS_RADIO_BUTTON_INDEX));
      }

      if (userModel!.flag == null) {
        hireDataFlag = HelperClass.BOTH_DATA_RADIO_BUTTON_INDEX;
      } else if (userModel!.flag == 1) {
        hireDataFlag = HelperClass.BOTH_DATA_RADIO_BUTTON_INDEX;
      } else if (userModel!.flag == 2) {
        hireDataFlag = HelperClass.MOBILE_RADIO_BUTTON_INDEX;
      } else if (userModel!.flag == 3) {
        hireDataFlag = HelperClass.EMAIL_RADIO_BUTTON_INDEX;
      }
      hireDataShowBloc.add(HireDataShowEvent(hireDataFlag));

      if (talent_type == 1) {
        if (userModel!.categoryId != null && userModel!.categoryId != -1) {
          bool subCategoryAvailability = false;
          if (userModel!.subcategoryId != null &&
              userModel!.subcategoryId != -1) {
            subCategoryAvailability = true;
          }
          categoriesDropDownBloc.add(CategoriesDropDownLoadingEvent(
              userModel!.categoryId, subCategoryAvailability));
        } else {
          categoriesDropDownBloc.add(CategoriesDropDownLoadingEvent(-1, false));
        }

        categoriesNewBloc
            .add(CategoriesNewLoadingEvent(userModel!.category_title));
      }

      if (userModel!.country != null && userModel!.country != "") {
        bool stateAvailability = false;

        if (userModel!.state != null && userModel!.state != "") {
          stateAvailability = true;
        }

        countriesBloc
            .add(CountriesLoadingEvent(userModel!.country, stateAvailability));
      } else {
        countriesBloc.add(CountriesLoadingEvent(null, false));
      }
      if (userModel!.tradeBarter == 1) {
        talentBarterBloc.add(TalentBarterEvent(true));
      } else {
        talentBarterBloc.add(TalentBarterEvent(false));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
            elevation: 0,
            title: Text(
              "My Profile",
              style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                  color: MyColors.WHITE_COLOR,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back_ios_new,
                color: MyColors.ORANGE_COLOR,
                size: 16,
              ),
            ),
          ),
          body: BlocConsumer<UserBloc, UserState>(
            listener: (context, userState) {},
            builder: (c, userState) {
              if (userState is MyUserLoadingState) {
                return CircularIndicatorWidget();
              } else if (userState is MyUserErrorState) {
                return CustomErrorWidget(
                  on_tap: () {
                    userBloc.add(MyUserLoadingEvent(SharedPrefs.getUserId()));
                  },
                );
              } else if (userState is MyUserLoadedState) {
                return Container(
                  padding: EdgeInsets.all(
                    16.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          child: Form(
                            key: formKey,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BlocConsumer<ProfilePhotoUpdateBloc,
                                    ProfilePhotoUpdateState>(
                                  listener: (context, state) {
                                    // TODO: implement listener
                                  },
                                  builder: (context, state) {
                                    return GestureDetector(
                                      onTap: () {
                                        check_for_permissions();
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          top: 20.0,
                                        ),
                                        child: Center(
                                          child: Stack(
                                            clipBehavior: Clip.none,
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color:
                                                        MyColors.ORANGE_COLOR,
                                                    width: 5.0,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    100.0,
                                                  ),
                                                ),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(
                                                      100.0,
                                                    ),
                                                  ),
                                                  child: state.profilePhotoUrl !=
                                                              null &&
                                                          state.profilePhotoUrl !=
                                                              ""
                                                      ? CachedNetworkImage(
                                                          alignment: Alignment
                                                              .topCenter,
                                                          imageUrl: state
                                                              .profilePhotoUrl!,
                                                          fit: BoxFit.cover,
                                                          height: 100.0,
                                                          width: 100.0,
                                                          placeholder:
                                                              (ctx, url) =>
                                                                  Image.asset(
                                                            "images/circular_image_placeholder.jpg",
                                                            fit: BoxFit.cover,
                                                            height: 100.0,
                                                            width: 100.0,
                                                          ),
                                                          errorWidget: (context,
                                                                  url, error) =>
                                                              Image.asset(
                                                            "images/circular_image_placeholder.jpg",
                                                            fit: BoxFit.cover,
                                                            height: 100.0,
                                                            width: 100.0,
                                                          ),
                                                        )
                                                      : Image.asset(
                                                          "images/circular_image_placeholder.jpg",
                                                          fit: BoxFit.cover,
                                                          height: 100.0,
                                                          width: 100.0,
                                                        ),
                                                ),
                                              ),
                                              Positioned(
                                                right: 0.0,
                                                top: -20.0,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        100.0,
                                                      ),
                                                      color: MyColors
                                                          .ORANGE_COLOR),
                                                  padding: EdgeInsets.all(
                                                    12.0,
                                                  ),
                                                  child: Icon(
                                                    Icons.edit,
                                                    color: MyColors.WHITE_COLOR,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                                SizedBox(
                                  height: 30.0,
                                ),
                                Text(
                                  "Name",
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 4.0,
                                  ),
                                  child: TextFormField(
                                    controller: name_controller,
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.zero,
                                      filled: true,
                                      fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                      hintText: "Enter Name",
                                      hintStyle: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      counterStyle: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 10.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      counterText: "",
                                      prefixIcon: Icon(
                                        Icons.person,
                                        color: MyColors.ORANGE_COLOR,
                                      ),
                                    ),
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp("[a-zA-Z ]")),
                                    ],
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    cursorColor: MyColors.WHITE_COLOR,
                                    textInputAction: TextInputAction.done,
                                    keyboardType: TextInputType.text,
                                    validator: (value) {
                                      if (value == null || value.trim() == "") {
                                        return 'Please enter a valid name';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                                SizedBox(
                                  height: 16.0,
                                ),
                                Text(
                                  "Email",
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 4.0,
                                  ),
                                  child: TextFormField(
                                    // autofocus: true,
                                    controller: email_controller,
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.zero,
                                      filled: true,
                                      fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                      hintText: "Enter Email",
                                      hintStyle: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      counterStyle: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 10.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      counterText: "",
                                      prefixIcon: Icon(
                                        Icons.email,
                                        color: MyColors.ORANGE_COLOR,
                                      ),
                                    ),
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    cursorColor: MyColors.WHITE_COLOR,
                                    textInputAction: TextInputAction.done,
                                    keyboardType: TextInputType.emailAddress,
                                    validator: (value) {
                                      if (value == null || value.trim() == "") {
                                        return 'Please enter a valid email address';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                                SizedBox(
                                  height: 16.0,
                                ),
                                Text(
                                  "Bio",
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 4.0,
                                  ),
                                  child: TextFormField(
                                    controller: bio_controller,
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.zero,
                                      filled: true,
                                      fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                      hintText: "Enter bio",
                                      hintStyle: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      counterStyle: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 10.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      counterText: "",
                                      prefixIcon: const Align(
                                        alignment: Alignment.topLeft,
                                        widthFactor: 1.0,
                                        heightFactor: 7.0,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              top: 4.0, left: 8),
                                          child: Icon(
                                            Icons.description,
                                            color: MyColors.ORANGE_COLOR,
                                          ),
                                        ),
                                      ),
                                    ),
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    cursorColor: MyColors.WHITE_COLOR,
                                    // textInputAction: TextInputAction.done,
                                    keyboardType: TextInputType.multiline,
                                    maxLines: 10,
                                    validator: (value) {
                                      if (value == null || value.trim() == "") {
                                        return 'Please enter your bio';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                                /* Padding(
                                  padding: const EdgeInsets.only(
                                    top: 20.0,
                                    bottom: 4.0,
                                  ),
                                  child: Text(
                                    "kind of Talents",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: TextField(
                                    // autofocus: true,
                                    controller: tags_controller,
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                      hintText: "Enter kind of Talents",
                                      hintStyle: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          16.0,
                                        ),
                                      ),
                                      counterStyle: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 10.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      counterText: "",
                                      prefixIcon: Icon(
                                        Icons.person,
                                        color: MyColors.ORANGE_COLOR,
                                      ),
                                    ),
                                    // inputFormatters: [
                                    //   FilteringTextInputFormatter.allow(
                                    //       RegExp("[a-zA-Z ]")),
                                    // ],
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    cursorColor: MyColors.WHITE_COLOR,
                                    textInputAction: TextInputAction.done,
                                    keyboardType: TextInputType.text,
                                    // validator: (value) {
                                    //   if (value == null || value.trim() == "") {
                                    //     return 'Please enter a valid name';
                                    //   }
                                    //   return null;
                                    // },
                                  ),
                                ),*/

                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 16.0,
                                    bottom: 4.0,
                                  ),
                                  child: Text(
                                    "Facebook Handle",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                TextField(
                                  controller: fb_link_controller,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                    hintText: "Enter Your facebook Handle",
                                    hintStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.GREY_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    counterStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 10.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    counterText: "",
                                    prefixIcon: Icon(
                                      FontAwesomeIcons.facebook,
                                      color: MyColors.ORANGE_COLOR,
                                    ),
                                  ),
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  cursorColor: MyColors.WHITE_COLOR,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 16.0,
                                    bottom: 4.0,
                                  ),
                                  child: Text(
                                    "Instagram Handle",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                TextField(
                                  controller: insta_link_controller,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                    hintText: "Enter Your Instagram Handle",
                                    hintStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.GREY_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    counterStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 10.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    counterText: "",
                                    prefixIcon: Icon(
                                      FontAwesomeIcons.instagram,
                                      color: MyColors.ORANGE_COLOR,
                                    ),
                                  ),
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  cursorColor: MyColors.WHITE_COLOR,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 16.0,
                                    bottom: 4.0,
                                  ),
                                  child: Text(
                                    "LinkedIn Profile Handle",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                TextField(
                                  controller: linkedIn_link_controller,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                    hintText:
                                        "Enter Your LinkedIn Profile Handle",
                                    hintStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.GREY_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    counterStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 10.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    counterText: "",
                                    prefixIcon: Icon(
                                      FontAwesomeIcons.linkedin,
                                      color: MyColors.ORANGE_COLOR,
                                    ),
                                  ),
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  cursorColor: MyColors.WHITE_COLOR,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 16.0,
                                    bottom: 4.0,
                                  ),
                                  child: Text(
                                    "WikiPedia Link",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                TextField(
                                  controller: wikipedialink_controller,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                    hintText: "Enter Your Wikipedia Link",
                                    hintStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.GREY_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    counterStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 10.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    counterText: "",
                                    prefixIcon: Icon(
                                      FontAwesomeIcons.wikipediaW,
                                      color: MyColors.ORANGE_COLOR,
                                    ),
                                  ),
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  cursorColor: MyColors.WHITE_COLOR,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 16.0,
                                    bottom: 4.0,
                                  ),
                                  child: Text(
                                    "IMDB Link",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                TextField(
                                  controller: imdb_link_controller,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: MyColors.BG_COLOR_TEXT_FILED,
                                    hintText: "Enter IMDB Link",
                                    hintStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.GREY_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                    ),
                                    counterStyle: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 10.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    counterText: "",
                                    prefixIcon: Icon(
                                      FontAwesomeIcons.imdb,
                                      color: MyColors.ORANGE_COLOR,
                                    ),
                                  ),
                                  style: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  cursorColor: MyColors.WHITE_COLOR,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 16.0,
                                    bottom: 4.0,
                                  ),
                                  child: Text(
                                    "Date of Birth (optional)",
                                    style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                BlocConsumer<DobBloc, DobState>(
                                  listener: (context, state) {
                                    dob = state.dob;
                                  },
                                  builder: (context, state) {
                                    return GestureDetector(
                                      onTap: () async {
                                        FocusManager.instance.primaryFocus
                                            ?.unfocus();
                                        DateTime? pickedDate =
                                            await showDatePicker(
                                          context: context,
                                          initialDate: state.dob == null ||
                                                  state.dob?.trim() == ""
                                              ? DateTime.now()
                                              : DateFormat(
                                                      HelperClass.DOB_FORMAT)
                                                  .parse(state.dob!),
                                          firstDate: DateTime(1950),
                                          //DateTime.now() - not to allow to choose before today.
                                          lastDate: DateTime.now(),
                                        );

                                        if (pickedDate != null) {
                                          var pick_date_formated =
                                              DateFormat(HelperClass.DOB_FORMAT)
                                                  .format(pickedDate);

                                          dobBloc.add(
                                              DobEvent(pick_date_formated));
                                        }
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: MyColors.BG_COLOR_TEXT_FILED,
                                          borderRadius: BorderRadius.circular(
                                            16.0,
                                          ),
                                        ),
                                        padding: EdgeInsets.all(
                                          10.0,
                                        ),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.calendar_today,
                                              color: MyColors.ORANGE_COLOR,
                                            ),
                                            SizedBox(
                                              width: 10.0,
                                            ),
                                            Text(
                                              state.dob != null &&
                                                      state.dob != ""
                                                  ? state.dob ?? ""
                                                  : "Date of Birth",
                                              style: GoogleFonts.montserrat(
                                                textStyle: TextStyle(
                                                  color: MyColors.WHITE_COLOR,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                                BlocBuilder<GenderBloc, GenderState>(
                                  builder: (context, state) {
                                    gender = state.index;
                                    return Container(
                                      decoration: BoxDecoration(
                                        color: MyColors.ORANGE_COLOR,
                                        borderRadius: BorderRadius.circular(
                                          14.0,
                                        ),
                                      ),
                                      padding: EdgeInsets.all(
                                        4.0,
                                      ),
                                      margin: EdgeInsets.only(
                                        top: 16.0,
                                      ),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: CustomRadioButton(
                                                text: HelperClass.MALE_VALUE,
                                                radio_button_index: HelperClass
                                                    .MALE_RADIO_BUTTON_INDEX,
                                                selected_index: state.index,
                                                on_tap: () {
                                                  if (state.index !=
                                                      HelperClass
                                                          .MALE_RADIO_BUTTON_INDEX) {
                                                    genderBloc.add(GenderEvent(
                                                        HelperClass
                                                            .MALE_RADIO_BUTTON_INDEX));
                                                  }
                                                }),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Expanded(
                                            child: CustomRadioButton(
                                              text: HelperClass.FEMALE_VALUE,
                                              radio_button_index: HelperClass
                                                  .FEMALE_RADIO_BUTTON_INDEX,
                                              selected_index: state.index,
                                              on_tap: () {
                                                if (state.index !=
                                                    HelperClass
                                                        .FEMALE_RADIO_BUTTON_INDEX) {
                                                  genderBloc.add(GenderEvent(
                                                      HelperClass
                                                          .FEMALE_RADIO_BUTTON_INDEX));
                                                }
                                              },
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Expanded(
                                            child: CustomRadioButton(
                                              text: HelperClass.OTHERS_VALUE,
                                              radio_button_index: HelperClass
                                                  .OTHERS_RADIO_BUTTON_INDEX,
                                              selected_index: state.index,
                                              on_tap: () {
                                                if (state.index !=
                                                    HelperClass
                                                        .OTHERS_RADIO_BUTTON_INDEX) {
                                                  genderBloc.add(GenderEvent(
                                                      HelperClass
                                                          .OTHERS_RADIO_BUTTON_INDEX));
                                                }
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                ),
                                talent_type == 1
                                    ? Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                              top: 16.0,
                                            ),
                                            child: BlocConsumer<
                                                TalentBarterBloc,
                                                TalentBarterState>(
                                              listener: (c, talentBarterState) {
                                                talentBarterSwitchState =
                                                    talentBarterState
                                                        .isTalentBarter;
                                              },
                                              builder: (c, talentBarterState) {
                                                return Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Switch(
                                                          value: talentBarterState
                                                              .isTalentBarter,
                                                          onChanged:
                                                              (bool value) {
                                                            talentBarterBloc.add(
                                                                TalentBarterEvent(
                                                                    value));
                                                          },
                                                          activeColor: MyColors
                                                              .WHITE_COLOR,
                                                          activeTrackColor:
                                                              MyColors
                                                                  .ORANGE_COLOR,
                                                          inactiveThumbColor:
                                                              MyColors
                                                                  .WHITE_COLOR,
                                                          inactiveTrackColor:
                                                              MyColors
                                                                  .ORANGE_COLOR,
                                                        ),
                                                        SizedBox(
                                                          width: 10.0,
                                                        ),
                                                        Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            color: MyColors
                                                                .WHITE_COLOR,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                              18.0,
                                                            ),
                                                          ),
                                                          padding:
                                                              EdgeInsets.all(
                                                            8.0,
                                                          ),
                                                          child: Text(
                                                            "Open for Talent Barter",
                                                            style: GoogleFonts
                                                                .workSans(
                                                              textStyle:
                                                                  TextStyle(
                                                                color: MyColors
                                                                    .BLACK_COLOR,
                                                                fontSize: 12.0,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 10.0,
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            var alertDialog =
                                                                AlertDialog(
                                                              backgroundColor:
                                                                  MyColors
                                                                      .BG_COLOR,
                                                              content:
                                                                  Container(
                                                                // height: 100.0,
                                                                child: Text(
                                                                  HelperClass
                                                                      .TALENT_BARTER_TEXT,
                                                                  style: GoogleFonts
                                                                      .montserrat(
                                                                    textStyle:
                                                                        const TextStyle(
                                                                      color: MyColors
                                                                          .WHITE_COLOR,
                                                                      fontSize:
                                                                          14.0,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            );
                                                            showDialog(
                                                                context:
                                                                    context,
                                                                // barrierDismissible: false,
                                                                builder: (ctx) {
                                                                  return alertDialog;
                                                                });
                                                          },
                                                          child: const Padding(
                                                            padding:
                                                                EdgeInsets.all(
                                                              8.0,
                                                            ),
                                                            child: Icon(
                                                              Icons.help,
                                                              color: MyColors
                                                                  .ORANGE_COLOR,
                                                              size: 16,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                    talentBarterState
                                                            .isTalentBarter
                                                        ? Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .only(
                                                                  top: 16.0,
                                                                  bottom: 4.0,
                                                                ),
                                                                child: Text(
                                                                  "Trading For",
                                                                  style: GoogleFonts
                                                                      .montserrat(
                                                                    textStyle:
                                                                        TextStyle(
                                                                      color: MyColors
                                                                          .WHITE_COLOR,
                                                                      fontSize:
                                                                          16.0,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              TextField(
                                                                // autofocus: true,
                                                                controller:
                                                                    barterDescriptionController,
                                                                decoration:
                                                                    InputDecoration(
                                                                  filled: true,
                                                                  fillColor:
                                                                      MyColors
                                                                          .BG_COLOR_TEXT_FILED,
                                                                  hintText:
                                                                      "Enter Trading For Description",
                                                                  hintStyle:
                                                                      GoogleFonts
                                                                          .workSans(
                                                                    textStyle:
                                                                        const TextStyle(
                                                                      color: MyColors
                                                                          .GREY_COLOR,
                                                                      fontSize:
                                                                          16.0,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                  ),
                                                                  focusedBorder:
                                                                      OutlineInputBorder(
                                                                    borderSide:
                                                                        BorderSide(
                                                                      color: MyColors
                                                                          .BG_COLOR_TEXT_FILED,
                                                                    ),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                      16.0,
                                                                    ),
                                                                  ),
                                                                  enabledBorder:
                                                                      OutlineInputBorder(
                                                                    borderSide:
                                                                        BorderSide(
                                                                      color: MyColors
                                                                          .BG_COLOR_TEXT_FILED,
                                                                    ),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                      16.0,
                                                                    ),
                                                                  ),
                                                                  disabledBorder:
                                                                      OutlineInputBorder(
                                                                    borderSide:
                                                                        BorderSide(
                                                                      color: MyColors
                                                                          .BG_COLOR_TEXT_FILED,
                                                                    ),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                      16.0,
                                                                    ),
                                                                  ),
                                                                  errorBorder:
                                                                      OutlineInputBorder(
                                                                    borderSide:
                                                                        BorderSide(
                                                                      color: MyColors
                                                                          .BG_COLOR_TEXT_FILED,
                                                                    ),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                      16.0,
                                                                    ),
                                                                  ),
                                                                  counterStyle:
                                                                      GoogleFonts
                                                                          .workSans(
                                                                    textStyle:
                                                                        TextStyle(
                                                                      color: MyColors
                                                                          .WHITE_COLOR,
                                                                      fontSize:
                                                                          10.0,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                  ),
                                                                  counterText:
                                                                      "",
                                                                ),

                                                                style: GoogleFonts
                                                                    .workSans(
                                                                  textStyle:
                                                                      const TextStyle(
                                                                    color: MyColors
                                                                        .WHITE_COLOR,
                                                                    fontSize:
                                                                        16.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                                cursorColor:
                                                                    MyColors
                                                                        .WHITE_COLOR,
                                                                textInputAction:
                                                                    TextInputAction
                                                                        .done,
                                                                keyboardType:
                                                                    TextInputType
                                                                        .text,
                                                              ),
                                                            ],
                                                          )
                                                        : SizedBox.shrink(),
                                                  ],
                                                );
                                              },
                                            ),
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  top: 10.0,
                                                  bottom: 4.0,
                                                ),
                                                child: Text(
                                                  "Visible Data For Hire",
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: TextStyle(
                                                      color:
                                                          MyColors.WHITE_COLOR,
                                                      fontSize: 16.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              BlocBuilder<HireDataShowBloc,
                                                  HireDataShowState>(
                                                builder: (context, state) {
                                                  hireDataFlag = state.index;
                                                  return Container(
                                                    decoration: BoxDecoration(
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        14.0,
                                                      ),
                                                    ),
                                                    padding: EdgeInsets.all(
                                                      4.0,
                                                    ),
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child:
                                                              CustomRadioButton(
                                                            text: HelperClass
                                                                .BOTH_DATA_VALUE,
                                                            radio_button_index:
                                                                HelperClass
                                                                    .BOTH_DATA_RADIO_BUTTON_INDEX,
                                                            selected_index:
                                                                state.index,
                                                            on_tap: () {
                                                              if (state.index !=
                                                                  HelperClass
                                                                      .BOTH_DATA_RADIO_BUTTON_INDEX) {
                                                                hireDataShowBloc
                                                                    .add(
                                                                  HireDataShowEvent(
                                                                    HelperClass
                                                                        .BOTH_DATA_RADIO_BUTTON_INDEX,
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 10.0,
                                                        ),
                                                        Expanded(
                                                          child:
                                                              CustomRadioButton(
                                                            text: HelperClass
                                                                .MOBILE_VALUE,
                                                            radio_button_index:
                                                                HelperClass
                                                                    .MOBILE_RADIO_BUTTON_INDEX,
                                                            selected_index:
                                                                state.index,
                                                            on_tap: () {
                                                              if (state.index !=
                                                                  HelperClass
                                                                      .MOBILE_RADIO_BUTTON_INDEX) {
                                                                hireDataShowBloc
                                                                    .add(
                                                                  HireDataShowEvent(
                                                                    HelperClass
                                                                        .MOBILE_RADIO_BUTTON_INDEX,
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 10.0,
                                                        ),
                                                        Expanded(
                                                          child:
                                                              CustomRadioButton(
                                                            text: HelperClass
                                                                .EMAIL_VALUE,
                                                            radio_button_index:
                                                                HelperClass
                                                                    .EMAIL_RADIO_BUTTON_INDEX,
                                                            selected_index:
                                                                state.index,
                                                            on_tap: () {
                                                              if (state.index !=
                                                                  HelperClass
                                                                      .EMAIL_RADIO_BUTTON_INDEX) {
                                                                hireDataShowBloc
                                                                    .add(
                                                                  HireDataShowEvent(
                                                                    HelperClass
                                                                        .EMAIL_RADIO_BUTTON_INDEX,
                                                                  ),
                                                                );
                                                              }
                                                            },
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  );
                                                },
                                              ),
                                            ],
                                          ),
                                          BlocConsumer<CategoriesNewBloc,
                                              CategoriesNewState>(
                                            listener:
                                                (context, categoriesNewState) {
                                              if (categoriesNewState
                                                  is CategoriesNewLoadedState) {
                                                selectedCategoriesList =
                                                    categoriesNewState
                                                        .selectedCategoriesList;
                                              }
                                            },
                                            builder:
                                                (context, categoriesNewState) {
                                              if (categoriesNewState
                                                  is CategoriesNewLoadingState) {
                                                return CircularIndicatorWidget();
                                              } else if (categoriesNewState
                                                  is CategoriesNewErrorState) {
                                                return CustomErrorWidget(
                                                  on_tap: () {
                                                    categoriesNewBloc.add(
                                                        CategoriesNewLoadingEvent(
                                                            categoriesNewState
                                                                .categoryTitle));
                                                  },
                                                );
                                              } else if (categoriesNewState
                                                  is CategoriesNewLoadedState) {
                                                return Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        top: 10.0,
                                                        bottom: 4.0,
                                                      ),
                                                      child: Text(
                                                        "Category",
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          textStyle: TextStyle(
                                                            color: MyColors
                                                                .WHITE_COLOR,
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {
                                                        FocusManager.instance
                                                            .primaryFocus
                                                            ?.unfocus();
                                                        showMultipleCategoryDialog(
                                                            context,
                                                            categoriesNewState
                                                                .categoriesList);
                                                      },
                                                      child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          color: MyColors
                                                              .BG_COLOR_TEXT_FILED,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                            16.0,
                                                          ),
                                                        ),
                                                        padding: EdgeInsets.all(
                                                          12.0,
                                                        ),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Expanded(
                                                              child: Text(
                                                                categoriesNewState
                                                                        .selectedCategoriesList
                                                                        .isNotEmpty
                                                                    ? HelperClass.getTextsFromSelectedCategories(
                                                                        selectedCategoriesList:
                                                                            categoriesNewState.selectedCategoriesList)
                                                                    : "Select Category",
                                                                style: GoogleFonts
                                                                    .workSans(
                                                                  textStyle:
                                                                      TextStyle(
                                                                    color: MyColors
                                                                        .WHITE_COLOR,
                                                                    fontSize:
                                                                        16.0,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons
                                                                  .keyboard_arrow_down,
                                                              color: MyColors
                                                                  .ORANGE_COLOR,
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                );
                                              }
                                              return Container();
                                            },
                                          ),
                                        ],
                                      )
                                    : Container(),
                                BlocBuilder<CountriesBloc, CountriesState>(
                                  builder: (c, countriesState) {
                                    countryId = -1;
                                    countryName = "";
                                    stateId = -1;
                                    stateName = "";
                                    cityId = -1;
                                    cityName = "";

                                    if (countriesState
                                        is CountriesLoadingState) {
                                      return Padding(
                                        padding: const EdgeInsets.all(
                                          16.0,
                                        ),
                                        child: CircularIndicatorWidget(),
                                      );
                                    } else if (countriesState
                                        is CountriesDropDownErrorState) {
                                      return Container(
                                        margin: const EdgeInsets.only(
                                          top: 16.0,
                                        ),
                                        child: CustomErrorWidget(
                                          on_tap: () {
                                            countriesBloc.add(CountriesLoadingEvent(
                                                countriesState
                                                    .currentCountryTitle,
                                                countriesState
                                                    .isStatePreviouslyAvailable));
                                          },
                                        ),
                                      );
                                    } else if (countriesState
                                        is CountriesDropDownLoadedState) {
                                      if (countriesState.selected_index != -1) {
                                        /// load states based on country id

                                        countryName = countriesState
                                            .countriesList[
                                                countriesState.selected_index]
                                            .name!;
                                        countryId = countriesState
                                            .countriesList[
                                                countriesState.selected_index]
                                            .id!;
                                        String availableStateName = "";

                                        bool cityAvailability = false;

                                        if (countriesState
                                            .isStatePreviouslyAvailable) {
                                          if (userModel?.state != null &&
                                              userModel?.state != "") {
                                            availableStateName =
                                                userModel?.state;
                                          }

                                          if (userModel!.city != null &&
                                              userModel!.city != "") {
                                            cityAvailability = true;
                                          }
                                        }

                                        statesBloc.add(
                                            StatesDropDownLoadingEvent(
                                                countryId,
                                                availableStateName,
                                                cityAvailability));
                                      }
                                      return Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 16.0,
                                              bottom: 4.0,
                                            ),
                                            child: Text(
                                              "Country",
                                              style: GoogleFonts.montserrat(
                                                textStyle: TextStyle(
                                                  color: MyColors.WHITE_COLOR,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              FocusManager.instance.primaryFocus
                                                  ?.unfocus();
                                              showAddressDialog(
                                                  context,
                                                  AddressEnum.countryE,
                                                  countriesState.countriesList,
                                                  countriesState
                                                      .selected_index);
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: MyColors
                                                    .BG_COLOR_TEXT_FILED,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  16.0,
                                                ),
                                              ),
                                              padding: EdgeInsets.all(
                                                12.0,
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    countriesState
                                                                .selected_index ==
                                                            -1
                                                        ? "Select Country"
                                                        : countriesState
                                                            .countriesList[
                                                                countriesState
                                                                    .selected_index]
                                                            .name!,
                                                    style: GoogleFonts.workSans(
                                                      textStyle: TextStyle(
                                                        color: MyColors
                                                            .WHITE_COLOR,
                                                        fontSize: 16.0,
                                                      ),
                                                    ),
                                                  ),
                                                  Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color:
                                                        MyColors.ORANGE_COLOR,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          BlocBuilder<StatesBloc, StatesState>(
                                              builder: (context, statesState) {
                                            stateId = -1;
                                            stateName = "";
                                            cityId = -1;
                                            cityName = "";
                                            if (statesState
                                                is StatesLoadingState) {
                                              return Padding(
                                                padding: const EdgeInsets.all(
                                                  16.0,
                                                ),
                                                child:
                                                    CircularIndicatorWidget(),
                                              );
                                            } else if (statesState
                                                is StatesDropDownErrorState) {
                                              return Padding(
                                                padding: const EdgeInsets.all(
                                                  16.0,
                                                ),
                                                child: CustomErrorWidget(
                                                  on_tap: () {
                                                    FocusManager
                                                        .instance.primaryFocus
                                                        ?.unfocus();
                                                    statesBloc.add(
                                                        StatesDropDownLoadingEvent(
                                                            statesState
                                                                .countryId,
                                                            statesState
                                                                .availableStateName,
                                                            statesState
                                                                .isCityPreviouslyAvailable));
                                                  },
                                                ),
                                              );
                                            } else if (statesState
                                                is StatesDropDownLoadedState) {
                                              if (statesState.selected_index !=
                                                  -1) {
                                                stateName = statesState
                                                    .statesList[statesState
                                                        .selected_index]
                                                    .name!;
                                                stateId = statesState
                                                    .statesList[statesState
                                                        .selected_index]
                                                    .id!;

                                                String availableCityName = "";
                                                if (statesState
                                                    .isCityPreviouslyAvailable) {
                                                  if (userModel?.city != null &&
                                                      userModel?.city != "") {
                                                    availableCityName =
                                                        userModel?.city;
                                                  }
                                                }

                                                citiesBloc.add(
                                                    CitiesDropDownLoadingEvent(
                                                        stateId,
                                                        availableCityName));
                                              }
                                              return Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                      top: 16.0,
                                                      bottom: 4.0,
                                                    ),
                                                    child: Text(
                                                      "State",
                                                      style: GoogleFonts
                                                          .montserrat(
                                                        textStyle: TextStyle(
                                                          color: MyColors
                                                              .WHITE_COLOR,
                                                          fontSize: 16.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      FocusManager
                                                          .instance.primaryFocus
                                                          ?.unfocus();
                                                      showAddressDialog(
                                                          context,
                                                          AddressEnum.stateE,
                                                          statesState
                                                              .statesList,
                                                          statesState
                                                              .selected_index);
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        color: MyColors
                                                            .BG_COLOR_TEXT_FILED,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                          16.0,
                                                        ),
                                                      ),
                                                      padding: EdgeInsets.all(
                                                        12.0,
                                                      ),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text(
                                                            statesState.statesList
                                                                        .isEmpty ||
                                                                    statesState
                                                                            .selected_index ==
                                                                        -1
                                                                ? "Select State"
                                                                : statesState
                                                                    .statesList[
                                                                        statesState
                                                                            .selected_index]
                                                                    .name!,
                                                            style: GoogleFonts
                                                                .workSans(
                                                              textStyle:
                                                                  TextStyle(
                                                                color: MyColors
                                                                    .WHITE_COLOR,
                                                                fontSize: 16.0,
                                                              ),
                                                            ),
                                                          ),
                                                          Icon(
                                                            Icons
                                                                .keyboard_arrow_down,
                                                            color: MyColors
                                                                .ORANGE_COLOR,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  stateId != -1
                                                      ? BlocBuilder<CitiesBloc,
                                                              CitiesState>(
                                                          builder:
                                                              (c, citiesState) {
                                                          cityId = -1;
                                                          cityName = "";
                                                          if (citiesState
                                                              is CitiesLoadingState) {
                                                            return const Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(
                                                                16.0,
                                                              ),
                                                              child:
                                                                  CircularIndicatorWidget(),
                                                            );
                                                          } else if (citiesState
                                                              is CitiesDropDownErrorState) {
                                                            return Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(
                                                                16.0,
                                                              ),
                                                              child:
                                                                  CustomErrorWidget(
                                                                on_tap: () {
                                                                  citiesBloc.add(CitiesDropDownLoadingEvent(
                                                                      citiesState
                                                                          .StateId,
                                                                      citiesState
                                                                          .availableCityName));
                                                                },
                                                              ),
                                                            );
                                                          } else if (citiesState
                                                              is CitiesDropDownLoadedState) {
                                                            if (citiesState
                                                                    .selected_index !=
                                                                -1) {
                                                              cityId = citiesState
                                                                  .citiesList[
                                                                      citiesState
                                                                          .selected_index]
                                                                  .id!;
                                                              cityName = citiesState
                                                                  .citiesList[
                                                                      citiesState
                                                                          .selected_index]
                                                                  .name!;
                                                            }

                                                            return Column(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                          .only(
                                                                    top: 16.0,
                                                                    bottom: 4.0,
                                                                  ),
                                                                  child: Text(
                                                                    "City",
                                                                    style: GoogleFonts
                                                                        .montserrat(
                                                                      textStyle:
                                                                          const TextStyle(
                                                                        color: MyColors
                                                                            .WHITE_COLOR,
                                                                        fontSize:
                                                                            16.0,
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                GestureDetector(
                                                                  onTap: () {
                                                                    FocusManager
                                                                        .instance
                                                                        .primaryFocus
                                                                        ?.unfocus();
                                                                    showAddressDialog(
                                                                        context,
                                                                        AddressEnum
                                                                            .cityE,
                                                                        citiesState
                                                                            .citiesList,
                                                                        citiesState
                                                                            .selected_index);
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      color: MyColors
                                                                          .BG_COLOR_TEXT_FILED,
                                                                      borderRadius:
                                                                          BorderRadius
                                                                              .circular(
                                                                        16.0,
                                                                      ),
                                                                    ),
                                                                    padding:
                                                                        EdgeInsets
                                                                            .all(
                                                                      12.0,
                                                                    ),
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      children: [
                                                                        Text(
                                                                          citiesState.citiesList.isEmpty || citiesState.selected_index == -1
                                                                              ? "Select City"
                                                                              : citiesState.citiesList[citiesState.selected_index].name!,
                                                                          style:
                                                                              GoogleFonts.workSans(
                                                                            textStyle:
                                                                                TextStyle(
                                                                              color: MyColors.WHITE_COLOR,
                                                                              fontSize: 16.0,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .keyboard_arrow_down,
                                                                          color:
                                                                              MyColors.ORANGE_COLOR,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            );
                                                          }

                                                          return Container();
                                                        })
                                                      : Container(),
                                                ],
                                              );
                                            }

                                            return Container();
                                          })
                                        ],
                                      );
                                    }
                                    return Container();
                                  },
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(top: 50.0, bottom: 30.0),
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      elevation: 0.0,
                                      minimumSize: Size.fromHeight(
                                        40,
                                      ),
                                      // fromHeight use double.infinity as width and 40 is the height
                                      backgroundColor: MyColors.ORANGE_COLOR,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                      ),
                                    ),
                                    onPressed: () async {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      if (!formKey.currentState!.validate()) {
                                        // return;
                                      }

                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();

                                      var userId = SharedPrefs.getUserId()!;
                                      var url =
                                          "${ApisServices.BASE_URL}v2/updateusers/$userId/update";
                                      var request = http.MultipartRequest(
                                          "POST", Uri.parse(url));

                                      String msg = "";

                                      if (name_controller.text.trim() == "") {
                                        msg += "please enter name\n";
                                      } else {
                                        request.fields[HelperClass.NAME_KEY] =
                                            name_controller.text;
                                      }
                                      if (email_controller.text.trim() == "") {
                                        msg += "please enter email\n";
                                      } else {
                                        request.fields[HelperClass.EMAIL_KEY] =
                                            email_controller.text;
                                      }
                                      if (bio_controller.text.trim() == "") {
                                        msg += "please enter your bio\n";
                                      } else {
                                        request.fields[
                                                HelperClass.DESCRIPTION_KEY] =
                                            bio_controller.text;
                                      }
                                      if (countryName.trim() != "") {
                                        request.fields[HelperClass
                                            .COUNTRY_KEY] = countryName.trim();
                                      } else {
                                        msg += "please select country\n";
                                      }
                                      if (stateName.trim() != "") {
                                        request.fields[HelperClass.STATE_KEY] =
                                            stateName.trim();
                                      } else {
                                        msg += "please select state\n";
                                      }
                                      if (cityName.trim() != "") {
                                        request.fields[HelperClass.CITY_KEY] =
                                            cityName.trim();
                                      } else {
                                        msg += "please select city\n";
                                      }

                                      if (talent_type == 1) {
                                        request.fields[HelperClass
                                                .HIRE_DATA_FLAG_KEY] =
                                            "$hireDataFlag";

                                        if (selectedCategoriesList.isEmpty) {
                                          msg += "please select category\n";
                                        } else {
                                          List<int> selectedCategoriesIdList =
                                              [];
                                          for (CategoriesModel category
                                              in selectedCategoriesList) {
                                            selectedCategoriesIdList
                                                .add(category.id);
                                          }

                                          request.fields[HelperClass
                                                  .CATEGORY_IDS_NEW_KEY] =
                                              selectedCategoriesIdList
                                                  .join(",");
                                        }

                                        if (talentBarterSwitchState) {
                                          request.fields[HelperClass
                                              .TRADE_BARTER_KEY] = "1";
                                          if (barterDescriptionController.text
                                                  .trim() !=
                                              "") {
                                            request.fields[HelperClass
                                                    .BARTER_DESCRIPTION_KEY] =
                                                barterDescriptionController.text
                                                    .trim();
                                          } else {
                                            msg +=
                                                "enter Trading For description\n";
                                          }
                                        } else {
                                          request.fields[HelperClass
                                              .TRADE_BARTER_KEY] = "0";
                                        }
                                      }
                                      if (msg != "") {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          SnackBar(
                                            content: Text(
                                              msg,
                                            ),
                                            duration:
                                                const Duration(seconds: 1),
                                          ),
                                        );
                                      } else {
                                        if (gender ==
                                            HelperClass
                                                .MALE_RADIO_BUTTON_INDEX) {
                                          request.fields[
                                                  HelperClass.GENDER_KEY] =
                                              HelperClass.MALE_VALUE;
                                        } else if (gender ==
                                            HelperClass
                                                .FEMALE_RADIO_BUTTON_INDEX) {
                                          request.fields[
                                                  HelperClass.GENDER_KEY] =
                                              HelperClass.FEMALE_VALUE;
                                        } else if (gender ==
                                            HelperClass
                                                .OTHERS_RADIO_BUTTON_INDEX) {
                                          request.fields[
                                                  HelperClass.GENDER_KEY] =
                                              HelperClass.OTHERS_VALUE;
                                        }
                                        request.fields[HelperClass.DOB_KEY] = dob!;
                                        if (tags_controller.text.trim() != "") {
                                          request.fields[HelperClass.TAGS_KEY] =
                                              tags_controller.text;
                                        }
                                        if (fb_link_controller.text.trim() !=
                                            "") {
                                          request.fields[
                                                  HelperClass.FB_LINK_KEY] =
                                              fb_link_controller.text;
                                        }
                                        if (insta_link_controller.text.trim() !=
                                            "") {
                                          request.fields[
                                                  HelperClass.INSTA_LINK_KEY] =
                                              insta_link_controller.text;
                                        }
                                        if (linkedIn_link_controller.text
                                                .trim() !=
                                            "") {
                                          request.fields[HelperClass
                                                  .LINKED_IN_LINK_KEY] =
                                              linkedIn_link_controller.text;
                                        }
                                        if (wikipedialink_controller.text
                                                .trim() !=
                                            "") {
                                          request.fields[HelperClass
                                                  .WIKIPEDIA_LINK_KEY] =
                                              wikipedialink_controller.text;
                                        }
                                        if (imdb_link_controller.text.trim() !=
                                            "") {
                                          request.fields[
                                                  HelperClass.IMDB_LINK_KEY] =
                                              imdb_link_controller.text;
                                        }

                                        HelperClass.show_alert_Dialog(
                                            context, "updating profile");

                                        request.headers["Content-Type"] =
                                            'application/json; charset=UTF-8';
                                        request.headers["Accept"] =
                                            'application/json';
                                        request.headers["Authorization"] =
                                            SharedPrefs.getAccessToken()!;

                                        var response = await request.send();
                                        var streamRes =
                                            await http.Response.fromStream(
                                                response);

                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .pop();
                                        // String updateMsg = "";
                                        if (streamRes.statusCode == 200) {
                                          var model = UserDataModel.fromJson(
                                              jsonDecode(streamRes.body));
                                          if (model.status == 200) {
                                            print("profile updated");
                                            HelperClass.show_snackbar(
                                                context, "profile updated");

                                            userBloc.myUserData =
                                                model.userModel;

                                            updateProfilePhotoBloc.add(
                                                ProfilePhotoUpdateEvent(
                                                    model.userModel?.avatar,
                                                    model.userModel?.name,
                                                    model.userModel?.mobile));
                                            FireBaseHelperClass
                                                .updateUserProfileInFirebase(
                                                    firestore: FirebaseFirestore
                                                        .instance,
                                                    userModel:
                                                        model.userModel!);

                                            if (widget.popScreenCheck) {
                                              Navigator.of(context).pop();
                                              Navigator.pushNamed(context,
                                                  CheckOutScreen.CheckoutRoute);
                                            }
                                          } else {
                                            HelperClass.show_snackbar(
                                                context,
                                                model.message ??
                                                    HelperClass.ERROR_TEXT);
                                          }
                                        } else {
                                          print(HelperClass.ERROR_TEXT);
                                          HelperClass.show_snackbar(
                                              context, HelperClass.ERROR_TEXT);
                                        }
                                      }
                                    },
                                    child: Text(
                                      "Save Changes",
                                      style: GoogleFonts.montserrat(
                                        textStyle: TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  void showAddressDialog(BuildContext context, AddressEnum addressEnum,
      List<AddressModel> addressList, int previousIndex) {
    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        addressEnum == AddressEnum.countryE
            ? "Select Country"
            : addressEnum == AddressEnum.stateE
                ? "Select State"
                : "Select City",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.6,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: addressList.length,
          itemBuilder: (ctx, index) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context, rootNavigator: true).pop();

                if (previousIndex != index) {
                  addressEnum == AddressEnum.countryE
                      ? countriesBloc
                          .add(CountriesDropDownChangeEvent(addressList, index))
                      : addressEnum == AddressEnum.stateE
                          ? statesBloc.add(
                              StatesDropDownChangeEvent(addressList, index))
                          : citiesBloc.add(
                              CitiesDropDownChangeEvent(addressList, index));
                }
              },
              child: Container(
                padding: EdgeInsets.all(
                  16.0,
                ),
                child: Text(
                  addressList[index].name!,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.ORANGE_COLOR,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
    showDialog(
      context: context,
      builder: (ctx) {
        return alert;
      },
    );
  }

  void showMultipleCategoryDialog(
      BuildContext context, List<CategoriesModel> categoriesList) {
    List<CategoriesModel> selectedCategoriesList = [];

    List<CategoriesModel> previousCategoriesList = [];
    categoriesList.forEach((element) {
      CategoriesModel category = CategoriesModel(
        id: element.id,
        description: element.description,
        image: element.image,
        isSelected: element.isSelected,
        title: element.title,
        subcategoriesList: element.subcategoriesList,
      );
      previousCategoriesList.add(category);
    });

    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        "Select Category",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: BlocBuilder<CategoriesNewBloc, CategoriesNewState>(
        builder: (context, state) {
          return Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: MediaQuery.of(context).size.height * 0.8,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: categoriesList.length,
              itemBuilder: (ctx, index) {
                return GestureDetector(
                  onTap: () {
                    categoriesList[index].isSelected =
                        !categoriesList[index].isSelected;
                    categoriesNewBloc.add(CategoriesNewChangeEvent(
                        categoriesList, selectedCategoriesList));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(
                            16.0,
                          ),
                          child: Text(
                            categoriesList[index].title,
                            style: GoogleFonts.workSans(
                              textStyle: TextStyle(
                                color: MyColors.ORANGE_COLOR,
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                      // Expanded(
                      //   child: Container(),
                      // ),
                      Theme(
                        data: ThemeData(
                          unselectedWidgetColor: MyColors.ORANGE_COLOR,
                        ),
                        child: Checkbox(
                          activeColor: MyColors.ORANGE_COLOR,
                          value: categoriesList[index].isSelected,
                          onChanged: (value) {
                            categoriesList[index].isSelected =
                                !categoriesList[index].isSelected;
                            categoriesNewBloc.add(CategoriesNewChangeEvent(
                                categoriesList, selectedCategoriesList));
                          },
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        },
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                // Navigator.pop(context);
                List<CategoriesModel> selectedCategoriesList1 = [];
                for (CategoriesModel category in previousCategoriesList) {
                  if (category.isSelected) {
                    selectedCategoriesList1.add(category);
                  }
                }

                categoriesNewBloc.add(CategoriesNewChangeEvent(
                    previousCategoriesList, selectedCategoriesList1));
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Cancel",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                for (CategoriesModel category in categoriesList) {
                  if (category.isSelected) {
                    selectedCategoriesList.add(category);
                  }
                }

                categoriesNewBloc.add(CategoriesNewChangeEvent(
                    categoriesList, selectedCategoriesList));
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Save",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
    showDialog(
      context: context,
      builder: (ctx) {
        return WillPopScope(
          child: alert,
          onWillPop: () async => false,
        );
      },
    );
  }

  void showCategoryDialog(BuildContext context, bool isCategory, var list) {
    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        isCategory ? "Select Category" : "Select Subcategory",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.6,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: list.length,
          itemBuilder: (ctx, index) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context, rootNavigator: true).pop();
                if (isCategory) {
                  categoriesDropDownBloc
                      .add(CategoriesDropDownChangeEvent(list, index));
                } else {
                  subCategoryDropDownBloc
                      .add(SubCategoriesDropDownChangeEvent(list, index));
                }
              },
              child: Container(
                padding: EdgeInsets.all(
                  16.0,
                ),
                child: Text(
                  list[index].title,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.ORANGE_COLOR,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
    showDialog(
      context: context,
      builder: (ctx) {
        return alert;
      },
    );
  }

  void check_for_permissions() async {
    var permission = Permission.photos;
    if (Platform.isAndroid) {
      var deviceInfo = DeviceInfoPlugin();
      final androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.version.sdkInt <= 32) {
        /// use [Permissions.storage.status]
        permission = Permission.storage;
      } else {
        /// use [Permissions.photos.status]
        permission = Permission.photos;
      }
    }

    Map<Permission, PermissionStatus> perms =
        await [permission, Permission.camera].request();

    if (perms[permission]!.isGranted && perms[Permission.camera]!.isGranted) {
      showAlertDialog();
    } else
    // if (perms[Permission.storage]!.isDenied ||
    //   perms[Permission.camera]!.isDenied)
    {
      String msg = "";
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        msg =
            "Storage, camera Permission is need to get the photo, please enable it in settings";
      } else {
        msg = "Storage, camera Permission is need to get the photo";
      }

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
        duration: Duration(seconds: 1),
      ));
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        Future.delayed(Duration(milliseconds: 1500), () {
          openAppSettings();
        });
      }
    }
  }

  void showAlertDialog() {
    AlertDialog alertDialog = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        "Profile Photo",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        textAlign: TextAlign.center,
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              _getFromGallery();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Gallery",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              _getFromCamera();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Camera",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return alertDialog;
      },
    );
  }

  _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      image_File = File(pickedFile.path);
      uploadPhoto();
    }
  }

  _getFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      image_File = File(pickedFile.path);
      uploadPhoto();
    }
  }

  void uploadPhoto() {
    HelperClass.show_alert_Dialog(context, "updating profile photo");
    ApisServices.updateProfilePhoto(
            token: SharedPrefs.getAccessToken(), file_path: image_File!.path)
        .then((value) {
      String msg = "";
      Navigator.of(context, rootNavigator: true).pop();
      if (value != null) {
        msg = value.message;
        if (value.status == 200) {
          FireBaseHelperClass.updateUserProfilePhotoInFirebase(
              firestore: FirebaseFirestore.instance,
              photoUrl: value.path,
              mobileNo: userModel!.mobile.toString());
          userBloc.myUserData?.avatar = value.path;
          updateProfilePhotoBloc.add(ProfilePhotoUpdateEvent(value.path,
              userBloc.myUserData?.name, userBloc.myUserData?.mobile));
        } else {
          image_File = null;
        }
      } else {
        image_File = null;
        msg = HelperClass.ERROR_TEXT;
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            msg,
          ),
          duration: const Duration(seconds: 1),
        ),
      );
    });
  }
}
