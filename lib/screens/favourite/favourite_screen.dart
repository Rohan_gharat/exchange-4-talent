import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/favourite/favourite_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class FavouriteScreen extends StatelessWidget {
  FavouriteScreen({super.key});

  static const FAVOURITE_ROUTE = "FavouriteRoute";
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var favouriteBloc = BlocProvider.of<FavouriteBloc>(context);
    var userBloc = BlocProvider.of<UserBloc>(context);

    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            "Favourite",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          actions: [
            DrawerIconWidget(
              on_tap: () {
                _scaffoldKey.currentState!.openEndDrawer();
              },
            ),
          ],
        ),
        endDrawer: DrawerMenu(),
        body: BlocConsumer<FavouriteBloc, FavouriteState>(
          listener: (c, state) {
            // TODO: implement listener
            print("favourite screen: $state");
            if (state is RemoveFavouriteState) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                    state.updateFavouriteModel.message,
                  ),
                  duration: const Duration(seconds: 1),
                ),
              );
            }
          },
          builder: (context, state) {
            if (state is FavouriteLoadingState) {
              return CircularIndicatorWidget();
            } else if (state is FavouriteErrorState) {
              return CustomErrorWidget(
                on_tap: () {
                  favouriteBloc.add(FavouriteLoadingEvent());
                },
              );
            }

            return favouriteBloc.favouritesList.isNotEmpty
                ? SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          ListView.builder(
                            itemCount: favouriteBloc.favouritesList.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (ctx, index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(context,
                                      UserDetailsScreen.USER_DETAILS_ROUTE,
                                      arguments: {
                                        UserDetailsScreen.USER_ID: favouriteBloc
                                            .favouritesList[index].id,
                                        UserDetailsScreen.IS_ME: false,
                                      });
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                    top: 8.0,
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                      18.0,
                                    ),
                                    border: Border.all(
                                      width: 5.0,
                                      color: favouriteBloc.favouritesList[index]
                                                      .color_code !=
                                                  null &&
                                              favouriteBloc
                                                      .favouritesList[index]
                                                      .color_code
                                                      .trim() !=
                                                  ""
                                          ? HexColor.fromHex(favouriteBloc
                                              .favouritesList[index].color_code
                                              .trim())
                                          : Colors.transparent,
                                    ),
                                  ),
                                  child: Container(
                                    // width: 150,

                                    padding: EdgeInsets.all(
                                      10.0,
                                    ),
                                    decoration: BoxDecoration(
                                      color: index % 2 == 0
                                          ? MyColors.BG_COLOR_TEXT_FILED
                                          : MyColors.BLACK_COLOR,
                                      borderRadius: BorderRadius.circular(
                                        18.0,
                                      ),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        ClipRRect(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(
                                              18.0,
                                            ),
                                          ),
                                          child: favouriteBloc
                                                          .favouritesList[index]
                                                          .avatar !=
                                                      null &&
                                                  favouriteBloc
                                                          .favouritesList[index]
                                                          .avatar !=
                                                      ""
                                              ? CachedNetworkImage(
                                                  alignment:
                                                      Alignment.topCenter,
                                                  imageUrl: favouriteBloc
                                                      .favouritesList[index]
                                                      .avatar,
                                                  placeholder: (context, url) =>
                                                      Image.asset(
                                                    HelperClass.APP_LOGO_PATH,
                                                    fit: BoxFit.cover,
                                                    height: 70.0,
                                                    width: 70.0,
                                                  ),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Image.asset(
                                                    HelperClass.APP_LOGO_PATH,
                                                    fit: BoxFit.cover,
                                                    height: 70.0,
                                                    width: 70.0,
                                                  ),
                                                  fit: BoxFit.cover,
                                                  height: 70.0,
                                                  width: 70.0,
                                                )
                                              : Image.asset(
                                                  HelperClass.APP_LOGO_PATH,
                                                  fit: BoxFit.cover,
                                                  height: 70.0,
                                                  width: 70.0,
                                                ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 16.0,
                                            ),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "${favouriteBloc.favouritesList[index].name ?? ""}",
                                                  style: GoogleFonts.workSans(
                                                    textStyle: const TextStyle(
                                                      color:
                                                          MyColors.GREY_COLOR1,
                                                      fontSize: 16.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  ),
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                SizedBox(
                                                  height: 8.0,
                                                ),
                                                Text(
                                                  favouriteBloc
                                                          .favouritesList[index]
                                                          .category_title ??
                                                      "",
                                                  style: GoogleFonts.workSans(
                                                    textStyle: const TextStyle(
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                      fontSize: 16.0,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  ),
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                favouriteBloc.add(
                                                    RemoveFavouriteEvent(
                                                        index,
                                                        favouriteBloc
                                                            .favouritesList[
                                                                index]
                                                            .id));
                                              },
                                              child: const Padding(
                                                padding: EdgeInsets.all(
                                                  4.0,
                                                ),
                                                child: Icon(
                                                  Icons.cancel,
                                                  color: MyColors.ORANGE_COLOR,
                                                ),
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                HelperClass.checkSubscription(
                                                    userBloc,
                                                    context,
                                                    favouriteBloc
                                                        .favouritesList[index]);
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.all(
                                                  4.0,
                                                ),
                                                child: Text(
                                                  "Hire",
                                                  style: GoogleFonts.workSans(
                                                    textStyle: const TextStyle(
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                      fontSize: 14.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  ),
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  )
                : NoDataWidget(
                    on_tap: () {
                      favouriteBloc.add(FavouriteLoadingEvent());
                    },
                  );
          },
        ),
      ),
    );
  }
}
