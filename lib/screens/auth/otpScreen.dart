import 'package:e4t/blocks/auth/auth_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/main_navigator_screen.dart';
import 'package:e4t/screens/profile/UserInitialDetailsScreen.dart';
import 'package:e4t/screens/subscriptions/add_subscription_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class OTPScreen extends StatelessWidget {
  OTPScreen({super.key});

  static const otpRoute = "otpRoute";

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var authBloc = BlocProvider.of<AuthBloc>(context);
    var userBloc = BlocProvider.of<UserBloc>(context);
    return SafeArea(
        child: Scaffold(
      body: Container(
        padding: const EdgeInsets.all(20.0),
        margin: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.1,
        ),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hello",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      color: MyColors.ORANGE_COLOR,
                      fontSize: 32.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Text(
                    "Enter Your OTP",
                    style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    top: 30.0,
                    bottom: 4.0,
                  ),
                  child: Text(
                    "OTP",
                    style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                TextFormField(
                  controller: authBloc.otpController,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: MyColors.BG_COLOR_TEXT_FILED,
                    hintText: "Enter OTP",
                    hintStyle: GoogleFonts.workSans(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    border: InputBorder.none,
                    counterText: "",
                    counterStyle: GoogleFonts.workSans(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    prefixIcon: Icon(
                      Icons.lock_outline,
                      color: MyColors.ORANGE_COLOR,
                    ),
                  ),
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onChanged: (value) {
                    if (value.length == 4) {
                      validateOtp(authBloc, context);
                    }
                  },
                  validator: (value) {
                    if (value?.length != 4) {
                      return "please enter valid otp";
                    }
                    return null;
                  },
                  cursorColor: MyColors.WHITE_COLOR,
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  maxLength: 4,
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 20.0,
                  ),
                  child: BlocConsumer<AuthBloc, AuthState>(
                    listener: (context, state) async {
                      if (state is OTPErrorState) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(
                              state.errorMsg,
                            ),
                          ),
                        );
                      }
                      if (state is VerifyOTPState) {
                        userBloc.myUserData =
                            state.otpModel.otpDataModel!.usermodel;
                        if (state.otpModel.otpDataModel?.category == 0) {
                          Navigator.pushNamedAndRemoveUntil(
                              context,
                              UserInitialDetailsScreen
                                  .USER_INITIAL_DETAILS_ROUTE,
                              (route) => false);
                        } else if (state.otpModel.otpDataModel?.subscription ==
                            0) {
                          await SharedPrefs.setBool(
                              SharedPrefs.isUserDataCompletedKey, true);
                          Navigator.pushNamedAndRemoveUntil(
                              context,
                              AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN,
                              arguments: {
                                AddSubscriptionsScreen.SHOW_SKIP_KEY: true,
                                AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key:
                                    0
                              },
                              (route) => false);
                        } else {
                          await SharedPrefs.setBool(
                              SharedPrefs.isUserDataCompletedKey, true);
                          Navigator.pushNamedAndRemoveUntil(
                              context,
                              MainNavigationScreen.MAIN_NAVIGATION_SCREEN_ROUTE,
                              (route) => false);
                        }
                      }
                    },
                    builder: (context, state) {
                      if (state is OTPLoadingState) {
                        return const Center(
                          child: CircularIndicatorWidget(),
                        );
                      }

                      return Column(
                        children: [
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              minimumSize: Size.fromHeight(
                                40,
                              ),
                              // fromHeight use double.infinity as width and 40 is the height
                              backgroundColor: MyColors.ORANGE_COLOR,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18),
                              ),
                            ),
                            onPressed: () {
                              validateOtp(authBloc, context);
                            },
                            child: Text(
                              "Verify OTP",
                              style: GoogleFonts.workSans(
                                textStyle: TextStyle(
                                  color: MyColors.WHITE_COLOR,
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          BlocConsumer<AuthBloc, AuthState>(
                            listener: (context, state) {
                              // TODO: implement listener
                            },
                            builder: (context, state) {
                              if (state is LoadingState) {
                                return Align(
                                  alignment: Alignment.centerRight,
                                  child: CircularIndicatorWidget(),
                                );
                              }

                              return GestureDetector(
                                onTap: () {
                                  authBloc.add(LogInEvent(
                                      authBloc.mobileController.text, true));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    top: 8.0,
                                  ),
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      "Resend OTP",
                                      style: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.ORANGE_COLOR,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }

  void validateOtp(AuthBloc authBloc, BuildContext context) {
    if (!_formKey.currentState!.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Please enter a valid OTP.',
          ),
        ),
      );
    } else {
      FocusManager.instance.primaryFocus?.unfocus();

      print(
          "login otp screen: unAuthorisedUserOtp: ${authBloc.unAuthorisedUserOtp}");
      if (authBloc.unAuthorisedUserOtp != null) {
        if (authBloc.unAuthorisedUserOtp ==
            int.parse(authBloc.otpController.text)) {
          authBloc.add(VerifyOtpEvent(
            authBloc.mobileController.text,
            authBloc.otpController.text,
          ));
        } else {
          /// otp not match for unAuthorisedUser
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text(
                'Otp not Matched.',
              ),
            ),
          );
        }
      } else {
        authBloc.add(VerifyOtpEvent(
          authBloc.mobileController.text,
          authBloc.otpController.text,
        ));
      }
    }
  }
}
