import 'package:e4t/blocks/CountriesBloc/countries_bloc.dart';
import 'package:e4t/blocks/auth/auth_bloc.dart';
import 'package:e4t/model/addressModel/addressModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/auth/otpScreen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({super.key});

  static const LOGIN_ROUTE = "loginRoute";

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  var authBloc;
  var countriesBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    countriesBloc = BlocProvider.of<CountriesBloc>(context);
    countriesBloc.add(CountriesLoadingEvent(null, false));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.0,
            ),
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.1,
            ),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Hello",
                    style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                        color: MyColors.ORANGE_COLOR,
                        fontSize: 32.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      "Welcome",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 30.0,
                      bottom: 4.0,
                    ),
                    child: Text(
                      "Mobile",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  IntrinsicHeight(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        BlocBuilder<CountriesBloc, CountriesState>(
                          builder: (context, countriesState) {
                            if (countriesState is CountriesLoadingState) {
                              return const CircularIndicatorWidget();
                            } else if (countriesState
                                is CountriesDropDownErrorState) {
                              return CustomErrorWidget(
                                on_tap: () {
                                  countriesBloc
                                      .add(CountriesLoadingEvent(null, false));
                                },
                              );
                            } else if (countriesState
                                is CountriesDropDownLoadedState) {
                              return GestureDetector(
                                onTap: () {
                                  FocusManager.instance.primaryFocus?.unfocus();
                                  showAddressDialog(
                                      context,
                                      countriesState.countriesList,
                                      countriesState.selected_index);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: MyColors.BG_COLOR_TEXT_FILED,
                                    borderRadius: BorderRadius.circular(
                                      16.0,
                                    ),
                                  ),
                                  padding: EdgeInsets.all(
                                    12.0,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        countriesState.selected_index == -1
                                            ? "+"
                                            : "+${countriesState.countriesList[countriesState.selected_index].phonecode!} ${countriesState.countriesList[countriesState.selected_index].iso3!}",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.WHITE_COLOR,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                      Icon(
                                        Icons.keyboard_arrow_down,
                                        color: MyColors.ORANGE_COLOR,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }
                            return Container();
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: TextFormField(
                            autofocus: true,
                            controller: authBloc.mobileController,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: MyColors.BG_COLOR_TEXT_FILED,
                              hintText: "Enter Mobile",
                              hintStyle: GoogleFonts.workSans(
                                textStyle: TextStyle(
                                  color: MyColors.WHITE_COLOR,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: MyColors.BG_COLOR_TEXT_FILED,
                                ),
                                borderRadius: BorderRadius.circular(
                                  16.0,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: MyColors.BG_COLOR_TEXT_FILED,
                                ),
                                borderRadius: BorderRadius.circular(
                                  16.0,
                                ),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: MyColors.BG_COLOR_TEXT_FILED,
                                ),
                                borderRadius: BorderRadius.circular(
                                  16.0,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: MyColors.BG_COLOR_TEXT_FILED,
                                ),
                                borderRadius: BorderRadius.circular(
                                  16.0,
                                ),
                              ),
                              counterStyle: GoogleFonts.workSans(
                                textStyle: TextStyle(
                                  color: MyColors.WHITE_COLOR,
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              border: InputBorder.none,
                              counterText: "",
                              prefixIcon: Icon(
                                Icons.phone,
                                color: MyColors.ORANGE_COLOR,
                              ),
                            ),
                            style: GoogleFonts.workSans(
                              textStyle: TextStyle(
                                color: MyColors.WHITE_COLOR,
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            cursorColor: MyColors.WHITE_COLOR,
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            maxLength: 10,
                            validator: (value) {
                              if (
                                  // !RegExp(r'^[\+]?[(]?[6-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')
                                  !RegExp(r'^[6-9][0-9]{9}').hasMatch(value!)
                                  // | RegExp(r'^(\d)\1{9}$').hasMatch(value)
                                  ) {
                                return 'Please enter a valid phone number';
                              }
                              return null;
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 20.0,
                    ),
                    child: BlocConsumer<AuthBloc, AuthState>(
                      listener: (context, state) {
                        print("auth state: $state");
                        if (state is LoggedInState) {
                          if (!state.isFromResend) {
                            Navigator.pushNamed(context, OTPScreen.otpRoute);
                          }
                        }else if (state is AuthErrorState){
                          HelperClass.show_snackbar(context, state.message);
                        }
                      },
                      builder: (context, state) {
                        if (state is LoadingState) {
                          return const Center(
                            child: CircularIndicatorWidget(),
                          );
                        }

                        return Column(
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                minimumSize: Size.fromHeight(
                                  40,
                                ),
                                // fromHeight use double.infinity as width and 40 is the height
                                backgroundColor: MyColors.ORANGE_COLOR,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () {
                                ScaffoldMessenger.of(context).clearSnackBars();
                                if (!_formKey.currentState!.validate()) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text(
                                        'Please enter a valid phone number.',
                                      ),
                                    ),
                                  );
                                  return;
                                }
                                FocusManager.instance.primaryFocus?.unfocus();
                                if (authBloc.phoneCode == null) {
                                  HelperClass.show_snackbar(
                                      context, "please select country code");
                                  return;
                                }
                                authBloc.add(LogInEvent(
                                    authBloc.mobileController.text, false));
                              },
                              child: Text(
                                "Sign In",
                                style: GoogleFonts.workSans(
                                  textStyle: TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showAddressDialog(
      BuildContext context, List<AddressModel> addressList, int previousIndex) {
    var alert = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        "Select Country",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.8,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: addressList.length,
          itemBuilder: (ctx, index) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context, rootNavigator: true).pop();
                print("showAddressDialog index: $index");
                if (previousIndex != index) {
                  countriesBloc
                      .add(CountriesDropDownChangeEvent(addressList, index));
                  authBloc.phoneCode = addressList[index].phonecode;
                }
              },
              child: Container(
                padding: EdgeInsets.all(
                  16.0,
                ),
                child: Text(
                  addressList[index].name!,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.ORANGE_COLOR,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
    showDialog(
      context: context,
      builder: (ctx) {
        return alert;
      },
    );
  }
}
