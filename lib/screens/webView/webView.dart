import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreen extends StatefulWidget {
  WebViewScreen({super.key, required this.url});

  String url;

  @override
  State<WebViewScreen> createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  late final WebViewController controller;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    controller = WebViewController()
      ..loadRequest(
        Uri.parse(widget.url),
      );
    controller.setJavaScriptMode(JavaScriptMode.unrestricted);

  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (await controller.canGoBack()) {
          controller.goBack();
        } else {
          return true;
        }
        return false;
      },
      child: SafeArea(
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            Scaffold(
              body: WebViewWidget(
                controller: controller,
              ),
              resizeToAvoidBottomInset: false,
            ),
            isLoading ? CircularIndicatorWidget() : SizedBox.shrink(),
            GestureDetector(
              onTap: () async {

                Navigator.pop(context);
              },
              child: Container(
                margin: EdgeInsets.only(
                  left: 8.0,
                  top: 8.0,
                ),
                decoration: BoxDecoration(
                  color: MyColors.BG_COLOR,
                  borderRadius: BorderRadius.circular(
                    100.0,
                  ),
                ),
                padding: EdgeInsets.all(
                  8.0,
                ),
                child: Icon(
                  Icons.close,
                  color: MyColors.ORANGE_COLOR,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
