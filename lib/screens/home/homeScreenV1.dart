import 'package:e4t/blocks/HomeBloc/home_bloc.dart';
import 'package:e4t/blocks/SubscriptionsDrawerMenuBloc/subscriptions_drawer_menu_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/model/subscriptionsModel/subscriptionsDrawerMenuModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/RecruitersJobs/RecruitersTabsScreen.dart';
import 'package:e4t/screens/category/all_categories_screen.dart';
import 'package:e4t/screens/category/usersByCategories_screen.dart';
import 'package:e4t/screens/main_navigator_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/videosSlider.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../blocks/ToggleBarterBloc/toggle_barter_bloc.dart';
import '../../blocks/mainNavigationBloc/main_navigation_bloc.dart';
import '../../blocks/profilePhotoUpdateBloc/profile_photo_update_bloc.dart';
import '../category/all_users_screen.dart';
import '../widgets/recommendedUsersWidget.dart';

class HomeScreenV1 extends StatelessWidget {
  HomeScreenV1({super.key});

  static const HomeRouteV1 = "HomeRouteV1";
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    print("home screen");
    var homeBloc = BlocProvider.of<HomeBloc>(context);
    var userBloc = BlocProvider.of<UserBloc>(context);
    var mainNavigationBloc = BlocProvider.of<MainNavigationBloc>(context);
    var subscriptionsDrawerMenuBloc =
        BlocProvider.of<SubscriptionsDrawerMenuBloc>(context);

    print("home :${userBloc.myUserData?.name}");
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: BlocBuilder<ProfilePhotoUpdateBloc, ProfilePhotoUpdateState>(
            builder: (context, state) {
              print("home screen v1 user state: $state");
              return Text(
                "Hello ${state.name ?? ""}",
                style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              );
            },
          ),
          actions: [
            // ProfileIconWidget(),
            DrawerIconWidget(
              on_tap: () {
                _scaffoldKey.currentState!.openEndDrawer();
              },
            ),
          ],
        ),
        endDrawer: DrawerMenu(),
        body: BlocConsumer<HomeBloc, HomeState>(
          listener: (context, state) {
            if (state is HomeLoadedState) {
              List<SubscriptionsDrawerMenuModel> menuList = [];
              if (state.homeModel.verticalSubscriptionsList != null &&
                  state.homeModel.verticalSubscriptionsList!.isNotEmpty) {
                for (var menu in state.homeModel.verticalSubscriptionsList!) {
                  menuList.add(SubscriptionsDrawerMenuModel(
                      id: menu.id, title: menu.title));
                }
              }
              if (state.homeModel.horizontalSubscriptionsList != null &&
                  state.homeModel.horizontalSubscriptionsList!.isNotEmpty) {
                for (var menu
                    in state.homeModel.horizontalSubscriptionsList!) {
                  menuList.add(SubscriptionsDrawerMenuModel(
                      id: menu.id, title: menu.title));
                }
              }
              print("menu list: ${menuList}");
              subscriptionsDrawerMenuBloc
                  .add(SubscriptionsDrawerMenuEvent(menuList));
            }
          },
          builder: (context, state) {
            if (state is HomeLoadingState) {
              return CircularIndicatorWidget();
            } else if (state is HomeErrorState) {
              return CustomErrorWidget(
                on_tap: () {
                  homeBloc.add(HomeLoadingEvent());
                },
              );
            } else if (state is HomeLoadedState) {
              return SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(
                        16.0,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                mainNavigationBloc.add(MainNavigationEvent(
                                    MainNavigationScreen.SEARCH_INDEX, false));
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: MyColors.BG_COLOR_TEXT_FILED,
                                  borderRadius: BorderRadius.circular(
                                    16.0,
                                  ),
                                ),
                                padding: EdgeInsets.all(
                                  10.0,
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Search",
                                      style: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.GREY_COLOR,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Icon(
                                      Icons.search,
                                      color: MyColors.ORANGE_COLOR,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    state.homeModel.bannersVideosList != null &&
                            state.homeModel.bannersVideosList!.isNotEmpty
                        ? VideosSlider(
                            bannersVideosList:
                                state.homeModel.bannersVideosList!)
                        : Container(),
                    Container(
                      height: 170.0,
                      margin: EdgeInsets.only(top: 16.0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 16.0,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Categories",
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                      color: MyColors.GREY_COLOR1,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context,
                                        AllCategoriesScreen
                                            .ALL_CATEGORIES_ROUTE);
                                  },
                                  child: Text(
                                    "View all",
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.ORANGE_COLOR,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(
                                top: 16.0,
                              ),
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: state
                                    .homeModel.homeCategoriesList!.length,
                                shrinkWrap: true,
                                itemBuilder: (ctx, index) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.pushNamed(
                                          context,
                                          UsersByCategoriesScreen
                                              .USER_BY_CATEGORY_SCREEN_Route,
                                          arguments: {
                                            UsersByCategoriesScreen.CATEGORY_ID:
                                                state
                                                    .homeModel
                                                    .homeCategoriesList![index]
                                                    .id,
                                            UsersByCategoriesScreen.TITLE: state
                                                .homeModel
                                                .homeCategoriesList![index]
                                                .title
                                          });

                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: MyColors.BG_COLOR_TEXT_FILED,
                                        borderRadius: BorderRadius.circular(
                                          18.0,
                                        ),
                                        border: Border.all(
                                          width: 5.0,
                                          color: state
                                                      .homeModel
                                                      .homeCategoriesList![
                                                          index]
                                                      .color_code !=
                                                  null
                                              ? HexColor.fromHex(state
                                                  .homeModel
                                                  .homeCategoriesList![index]
                                                  .color_code)
                                              : MyColors.CATEGORY_BG_COLOR,
                                        ),
                                      ),
                                      width: 100,
                                      margin: EdgeInsets.only(
                                        left: 16.0,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          ClipRRect(
                                            borderRadius: BorderRadius.circular(
                                              18.0,
                                            ),
                                            child: state
                                                        .homeModel
                                                        .homeCategoriesList?[
                                                            index]
                                                        .image !=
                                                    null
                                                ? CachedNetworkImage(
                                                    alignment:
                                                        Alignment.topCenter,
                                                    imageUrl: state
                                                        .homeModel
                                                        .homeCategoriesList?[
                                                            index]
                                                        .image,
                                                    fit: BoxFit.cover,
                                                    height: 80,
                                                    width: 100,
                                                    placeholder:
                                                        (context, url) =>
                                                            Image.asset(
                                                      HelperClass.APP_LOGO_PATH,
                                                      fit: BoxFit.cover,
                                                      height: 80,
                                                      width: 100,
                                                    ),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Image.asset(
                                                      HelperClass.APP_LOGO_PATH,
                                                      fit: BoxFit.cover,
                                                      height: 80,
                                                      width: 100,
                                                    ),
                                                  )
                                                : Image.asset(
                                                    HelperClass.APP_LOGO_PATH,
                                                    fit: BoxFit.cover,
                                                    height: 80,
                                                    width: 100,
                                                  ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 8.0,
                                            ),
                                            child: Text(
                                              state
                                                      .homeModel
                                                      .homeCategoriesList?[
                                                          index]
                                                      .title ??
                                                  "",
                                              style: GoogleFonts.workSans(
                                                textStyle: TextStyle(
                                                  color: MyColors.GREY_COLOR1,
                                                  fontSize: 11.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              textAlign: TextAlign.center,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 2,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    state.homeModel.recruitersList != null &&
                            state.homeModel.recruitersList!.isNotEmpty
                        ? Container(
                            height: 210.0,
                            margin: const EdgeInsets.only(
                              top: 8.0,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(
                                    16.0,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Recruiters",
                                        style: GoogleFonts.montserrat(
                                          textStyle: TextStyle(
                                            color: MyColors.GREY_COLOR1,
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {

                                          Navigator.pushNamed(
                                              context, RecruitersTabsScreen.RECRUITERS_TABS_ROUTE,
                                              arguments: false);
                                        },
                                        child: Text(
                                          "View all",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.ORANGE_COLOR,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: state
                                        .homeModel.recruitersList!.length,
                                    shrinkWrap: true,
                                    itemBuilder: (ctx, index) {
                                      return Container(
                                        width: 120,
                                        margin: EdgeInsets.only(
                                          left: 16.0,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                            18.0,
                                          ),
                                          border: Border.all(
                                            width: 5.0,
                                            color: state
                                                        .homeModel
                                                        .recruitersList![
                                                            index]
                                                        .color_code !=
                                                    null
                                                ? HexColor.fromHex(state
                                                    .homeModel
                                                    .recruitersList![index]
                                                    .color_code)
                                                : MyColors.CATEGORY_BG_COLOR,
                                          ),
                                        ),
                                        child: Container(
                                          // padding: EdgeInsets.all(
                                          //   5.0,
                                          // ),
                                          decoration: BoxDecoration(
                                            color: MyColors.BG_COLOR_TEXT_FILED,
                                            borderRadius: BorderRadius.circular(
                                              18.0,
                                            ),
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  14.0,
                                                ),
                                                child: state
                                                                .homeModel
                                                                .recruitersList![
                                                                    index]
                                                                .logo !=
                                                            null &&
                                                        state
                                                                .homeModel
                                                                .recruitersList![
                                                                    index]
                                                                .logo !=
                                                            ""
                                                    ? CachedNetworkImage(
                                                        alignment:
                                                            Alignment.topCenter,
                                                        imageUrl: state
                                                            .homeModel
                                                            .recruitersList![
                                                                index]
                                                            .logo,
                                                        height: 96.0,
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                        // fit: BoxFit.cover,
                                                        placeholder:
                                                            (context, url) =>
                                                                Image.asset(
                                                          HelperClass
                                                              .APP_LOGO_PATH,
                                                          height: 96.0,
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          fit: BoxFit.cover,
                                                        ),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Image.asset(
                                                          HelperClass
                                                              .APP_LOGO_PATH,
                                                          height: 96.0,
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          fit: BoxFit.cover,
                                                        ),
                                                      )
                                                    : Image.asset(
                                                        HelperClass
                                                            .APP_LOGO_PATH,
                                                        fit: BoxFit.cover,
                                                        height: 96.0,
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                      ),
                                              ),
                                              Center(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    top: 8.0,
                                                    left: 8.0,
                                                    right: 8.0,
                                                  ),
                                                  child: Text(
                                                    state
                                                            .homeModel
                                                            .recruitersList![
                                                                index]
                                                            .name ??
                                                        "",
                                                    style: GoogleFonts.workSans(
                                                      textStyle: TextStyle(
                                                        color: MyColors
                                                            .GREY_COLOR1,
                                                        fontSize: 12.0,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ),
                                                    textAlign: TextAlign.center,
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container(),
                    state.homeModel.verticalSubscriptionsList != null &&
                            state.homeModel.verticalSubscriptionsList!
                                .isNotEmpty
                        ? ListView.builder(
                            shrinkWrap: true,
                            itemCount: state
                                .homeModel.verticalSubscriptionsList!.length,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, AllUsersScreen.All_USERS_ROUTE,
                                      arguments: {
                                        AllUsersScreen.IS_ALL_USERS_KEY: false,
                                        AllUsersScreen.TITLE_KEY: state
                                                .homeModel
                                                .verticalSubscriptionsList![
                                                    index]
                                                .title ??
                                            "",
                                        AllUsersScreen.ID_KEY: state
                                            .homeModel
                                            .verticalSubscriptionsList![index]
                                            .id
                                      });
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                    top: 16.0,
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                      18.0,
                                    ),
                                  ),
                                  // padding: EdgeInsets.all(5.0),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(
                                      18.0,
                                    ),
                                    child: state
                                                    .homeModel
                                                    .verticalSubscriptionsList![
                                                        index]
                                                    .subscriptionData
                                                    .images !=
                                                null &&
                                            state
                                                    .homeModel
                                                    .verticalSubscriptionsList![
                                                        index]
                                                    .subscriptionData
                                                    .images !=
                                                ""
                                        ? CachedNetworkImage(
                                            imageUrl: state
                                                    .homeModel
                                                    .verticalSubscriptionsList![
                                                        index]
                                                    .subscriptionData
                                                    .images ??
                                                "",
                                            placeholder: (context, url) =>
                                                Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              height: 180.0,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              fit: BoxFit.cover,
                                            ),
                                            errorWidget:
                                                (context, url, error) =>
                                                    Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              height: 180.0,
                                              fit: BoxFit.cover,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                            ),
                                            height: 220.0,
                                            // fit: BoxFit.cover,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                          )
                                        : Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            height: 180.0,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            fit: BoxFit.cover,
                                          ),
                                  ),
                                ),
                              );
                            },
                          )
                        : Container(),
                    state.homeModel.horizontalSubscriptionsList != null &&
                            state.homeModel.horizontalSubscriptionsList!
                                .isNotEmpty
                        ? Container(
                            margin: EdgeInsets.only(
                              top: 16.0,
                              left: 8.0,
                            ),
                            height: 130,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: state.homeModel
                                  .horizontalSubscriptionsList!.length,
                              shrinkWrap: true,
                              itemBuilder: (ctx, index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, AllUsersScreen.All_USERS_ROUTE,
                                        arguments: {
                                          AllUsersScreen.IS_ALL_USERS_KEY:
                                              false,
                                          AllUsersScreen.TITLE_KEY: state
                                                  .homeModel
                                                  .horizontalSubscriptionsList![
                                                      index]
                                                  .title ??
                                              "",
                                          AllUsersScreen.ID_KEY: state
                                              .homeModel
                                              .horizontalSubscriptionsList![
                                                  index]
                                              .id
                                        });
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.45,
                                    margin: EdgeInsets.only(
                                      right: 16.0,
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                        18.0,
                                      ),
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(
                                        18.0,
                                      ),
                                      child: state
                                                      .homeModel
                                                      .horizontalSubscriptionsList![
                                                          index]
                                                      .subscriptionData
                                                      .images !=
                                                  null &&
                                              state
                                                      .homeModel
                                                      .horizontalSubscriptionsList![
                                                          index]
                                                      .subscriptionData
                                                      .images !=
                                                  ""
                                          ? CachedNetworkImage(
                                              imageUrl: state
                                                      .homeModel
                                                      .horizontalSubscriptionsList![
                                                          index]
                                                      .subscriptionData
                                                      .images ??
                                                  "",
                                              placeholder: (context, url) =>
                                                  Image.asset(
                                                HelperClass.APP_LOGO_PATH,

                                                height: 130.0,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                fit: BoxFit.cover,
                                              ),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                  height: 130.0,
                                                fit: BoxFit.cover,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                              ),
                                              height: 130.0,
                                              fit: BoxFit.cover,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                            )
                                          : Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                             height: 130.0,
                                              fit: BoxFit.cover,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                            ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          )
                        : Container(),
                    Container(
                      // color: Colors.red,
                      height: 240.0,
                      margin: EdgeInsets.only(
                        top: 16.0,
                        bottom: 20.0,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(
                              16.0,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Recommended for you",
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                      color: MyColors.GREY_COLOR1,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, AllUsersScreen.All_USERS_ROUTE,
                                        arguments: {
                                          AllUsersScreen.IS_ALL_USERS_KEY: true,
                                          AllUsersScreen.TITLE_KEY:
                                              "Recommended Talent"
                                        });
                                  },
                                  child: Text(
                                    "View all",
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.ORANGE_COLOR,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount:
                                  state.homeModel.randomUsersList!.length,
                              shrinkWrap: true,
                              itemBuilder: (ctx, index) {
                                return RecommendedUsersWidget(
                                  randomUsersList:
                                      state.homeModel.randomUsersList!,
                                  index: index,
                                  margin: EdgeInsets.only(
                                    left: 16.0,
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

}
