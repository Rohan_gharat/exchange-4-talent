import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/model/userModel/user_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/TradeBarter/trade_barter_details_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';

class TradeBarterScreen extends StatefulWidget {
  const TradeBarterScreen({super.key});

  static const TRADE_BARTER_SCREEN_ROUTE = "trade_barter_route";

  @override
  State<TradeBarterScreen> createState() => _TradeBarterScreenState();
}

class _TradeBarterScreenState extends State<TradeBarterScreen> {
  bool isLoading = false, isError = false;
  String? url, token;
  ScrollController scrollController = ScrollController();
  List<UserModel> tradeBartersUsersList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    token = SharedPrefs.getAccessToken();
    url = "${ApisServices.BASE_URL}v2/availableBarterUsers";
    loadData();

    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        if (!isLoading && url != null) {
          loadData();
        }
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            "Trade Barters",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
        ),
        body: SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: 16.0,
                ),
                child: tradeBartersUsersList.isNotEmpty
                    ? GridView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: tradeBartersUsersList.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 15.0,
                          mainAxisSpacing: 15.0,
                          childAspectRatio: 0.8,
                        ),
                        itemBuilder: (ctx, index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context,
                                  TradeBarterDetailsScreen
                                      .TRADE_BARTER_DETAILS_ROUTE,
                                  arguments: {
                                    TradeBarterDetailsScreen.USER_ID:
                                        tradeBartersUsersList[index].id,
                                  });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  18.0,
                                ),
                                border: Border.all(
                                  width: 5.0,
                                  color:
                                      tradeBartersUsersList[index].color_code !=
                                                  null &&
                                              tradeBartersUsersList[index]
                                                      .color_code
                                                      .trim() !=
                                                  ""
                                          ? HexColor.fromHex(
                                              tradeBartersUsersList[index]
                                                  .color_code
                                                  .trim())
                                          : Colors.transparent,
                                ),
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: MyColors.BG_COLOR_TEXT_FILED,
                                  borderRadius: BorderRadius.circular(
                                    18.0,
                                  ),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(
                                        14.0,
                                      ),
                                      child: tradeBartersUsersList[index]
                                                      .avatar !=
                                                  null &&
                                              tradeBartersUsersList[index]
                                                      .avatar !=
                                                  ""
                                          ? CachedNetworkImage(
                                              alignment: Alignment.topCenter,
                                              imageUrl:
                                                  tradeBartersUsersList[index]
                                                      .avatar,
                                              fit: BoxFit.cover,
                                              height: 120.0,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              placeholder: (context, url) =>
                                                  Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                fit: BoxFit.cover,
                                                height: 120.0,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                              ),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                fit: BoxFit.cover,
                                                height: 120.0,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                              ),
                                            )
                                          : Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              fit: BoxFit.cover,
                                              height: 120.0,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                            ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 8.0,
                                      ),
                                      child: Text(
                                        tradeBartersUsersList[index].name ?? "",
                                        style: GoogleFonts.montserrat(
                                          textStyle: TextStyle(
                                            color: MyColors.GREY_COLOR1,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    RatingBar.builder(
                                      initialRating: double.parse(
                                          tradeBartersUsersList[index].rating ??
                                              "0.0"),
                                      minRating: 1,
                                      direction: Axis.horizontal,
                                      allowHalfRating: true,
                                      itemCount: 5,
                                      itemSize: 18,
                                      unratedColor: MyColors.GREY_COLOR1,
                                      itemPadding:
                                          EdgeInsets.symmetric(horizontal: 1.0),
                                      itemBuilder: (context, _) => const Icon(
                                        Icons.star,
                                        color: MyColors.ORANGE_COLOR,
                                      ),
                                      onRatingUpdate: (rating) {},
                                      ignoreGestures: true,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : Container(
                        margin: const EdgeInsets.only(
                          top: 100.0,
                        ),
                        child: !isLoading
                            ? isError
                                ? CustomErrorWidget(
                                    on_tap: () {
                                      loadData();
                                    },
                                  )
                                : NoDataWidget(
                                    on_tap: () {
                                      loadData();
                                    },
                                  )
                            : Container(),
                      ),
              ),
              Center(
                child:
                    isLoading ? const CircularIndicatorWidget() : Container(),
              ),
              SizedBox(
                height: 30.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void loadData() async {
    isError = false;
    isLoading = true;
    setState(() {});
    var response =
        await ApisServices.getTradeBarterUsers(url: url, token: token);
    if (response != null) {
      if (response.tradeBarterDataModel != null &&
          response.tradeBarterDataModel!.tradeBartersList != null) {
        isLoading = false;
        tradeBartersUsersList
            .addAll(response.tradeBarterDataModel!.tradeBartersList ?? []);
        url = response.tradeBarterDataModel!.nextPageUrl;
        setState(() {});
      }
    } else {
      isLoading = false;
      setState(() {});
      if (tradeBartersUsersList.isEmpty) {
        HelperClass.show_snackbar(context, HelperClass.ERROR_TEXT);
        isError = true;
        setState(() {});
      }
    }
  }
}
