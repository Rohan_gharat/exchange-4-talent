import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:e4t/blocks/favourite/favourite_bloc.dart';
import 'package:e4t/blocks/mainNavigationBloc/main_navigation_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/model/ReviewsModel/ratingDetailsModel.dart';
import 'package:e4t/model/userModel/user_data_model.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/screens/reviews/review_dialog.dart';
import 'package:e4t/screens/subscriptions/add_subscription_screen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/imageSliders.dart';
import 'package:e4t/screens/widgets/videoPlayerPageFile.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../blocks/AddReviewBloc/add_review_bloc.dart';

class TradeBarterDetailsScreen extends StatefulWidget {
  TradeBarterDetailsScreen({super.key, this.mapData});

  Map? mapData;

  static const TRADE_BARTER_DETAILS_ROUTE = "tradeBarterDetailsRoute";

  static const USER_ID = "userId";

  @override
  State<TradeBarterDetailsScreen> createState() =>
      _TradeBarterDetailsScreenState();
}

class _TradeBarterDetailsScreenState extends State<TradeBarterDetailsScreen> {
  UserDataModel? userDataModel;
  bool isError = false, isLoading = true;
  late UserBloc userBloc;
  late var favouriteBloc;
  late var addReviewBloc;
  late var mainNavigationBloc;
  List<RatingDetailsModel> ratingDetailsList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBloc = BlocProvider.of<UserBloc>(context);
    favouriteBloc = BlocProvider.of<FavouriteBloc>(context);
    addReviewBloc = BlocProvider.of<AddReviewBloc>(context);
    mainNavigationBloc = BlocProvider.of<MainNavigationBloc>(context);

    loadData();
  }

  loadData() {
    isError = false;
    isLoading = true;
    setState(() {});
    ApisServices.getUserDataById(
            userId: widget.mapData![TradeBarterDetailsScreen.USER_ID],
            token: SharedPrefs.getAccessToken()!)
        .then((value) {
      isLoading = false;
      if (value != null) {
        if (value.status == 200) {
          userDataModel = value;
          ratingDetailsList = userDataModel!.ratingDetailsList ?? [];
          addReviewBloc.add(ReviewInitialEvent(
              double.parse(userDataModel?.userModel!.rating ?? "0.0")));

          isError = false;
          setState(() {});
        } else {
          HelperClass.show_snackbar(
              context, value.message ?? HelperClass.ERROR_TEXT);
          isError = true;
          setState(() {});
        }
      } else {
        HelperClass.show_snackbar(context, HelperClass.ERROR_TEXT);
        isError = true;

        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: isError
          ? Scaffold(
              body: CustomErrorWidget(
                on_tap: () {
                  loadData();
                },
              ),
            )
          : isLoading
              ? Scaffold(
                  body: CircularIndicatorWidget(),
                )
              : Scaffold(
                  appBar: AppBar(
                    backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
                    elevation: 0,
                    title: Text(
                      userDataModel?.userModel?.name ?? "",
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    leading: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_new,
                        color: MyColors.ORANGE_COLOR,
                        size: 16,
                      ),
                    ),
                  ),
                  body: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(
                        16.0,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          userDataModel != null &&
                                  userDataModel?.userBannersList != null &&
                                  userDataModel!.userBannersList!.isNotEmpty
                              ? ImageSliders(
                                  userBannersList:
                                      userDataModel?.userBannersList,
                                  userName:
                                      userDataModel!.userModel!.name ?? "",
                                  // subscriptionId:
                                  //     userDataModel?.userModel?.subscriptionId,
                                  color_code: userDataModel
                                              ?.userModel?.subscriptionId ==
                                          24
                                      ? userDataModel?.userModel?.color_code !=
                                                  null &&
                                              userDataModel
                                                      ?.userModel?.color_code !=
                                                  ""
                                          ? HexColor.fromHex(userDataModel
                                              ?.userModel?.color_code)
                                          : MyColors.STARRY_EYED_COLOR
                                      : MyColors.ORANGE_COLOR,
                                )
                              : Container(),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 16.0,
                            ),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                minimumSize: Size.fromHeight(
                                  40,
                                ),
                                // fromHeight use double.infinity as width and 40 is the height
                                backgroundColor: MyColors.ORANGE_COLOR,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () {
                                Navigator.pushNamed(context,
                                    UserDetailsScreen.USER_DETAILS_ROUTE,
                                    arguments: {
                                      UserDetailsScreen.USER_ID:
                                          widget.mapData![
                                              TradeBarterDetailsScreen.USER_ID],
                                      UserDetailsScreen.IS_ME: false,
                                    });
                              },
                              child: Text(
                                "View Profile",
                                style: GoogleFonts.workSans(
                                  textStyle: TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          userDataModel?.userModel?.barter_description !=
                                      null &&
                                  userDataModel
                                          ?.userModel?.barter_description !=
                                      ""
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 16.0,
                                      ),
                                      child: Text(
                                        userDataModel!.userModel!
                                                    .subscriptionId ==
                                                24
                                            ? "Trading For"
                                            : "Barter For",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.ORANGE_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 4.0,
                                      ),
                                      child: Text(
                                        userDataModel?.userModel
                                                ?.barter_description ??
                                            "",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.GREY_COLOR,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox.shrink(),
                          SizedBox(
                            height: 20.0,
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 16.0,
                            ),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                minimumSize: Size.fromHeight(
                                  40,
                                ),
                                // fromHeight use double.infinity as width and 40 is the height
                                backgroundColor: MyColors.ORANGE_COLOR,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () {
                                // showWarningDialog(context);
                                HelperClass.checkSubscription(userBloc, context,
                                    userDataModel?.userModel);
                              },
                              child: Text(
                                "Hire Now",
                                style: GoogleFonts.workSans(
                                  textStyle: TextStyle(
                                    color: MyColors.WHITE_COLOR,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
    );
  }

  void updateFavouriteStatus({required talent_id}) {
    HelperClass.show_alert_Dialog(context, "processing");
    ApisServices.updateFavouriteStatus(
            token: SharedPrefs.getAccessToken(), talent_id: talent_id)
        .then((value) {
      Navigator.of(context, rootNavigator: true).pop();
      String msg = "";
      if (value != null) {
        msg = value.message;
        if (value.status == 200) {
          if (value.data) {
            userDataModel?.userModel?.isfavourite = 1;
            setState(() {});
          } else {
            userDataModel?.userModel?.isfavourite = 0;
            setState(() {});
          }
          favouriteBloc.add(FavouriteLoadingEvent());
        }
      } else {
        msg = HelperClass.ERROR_TEXT;
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            msg,
          ),
          duration: Duration(seconds: 1),
        ),
      );
    });
  }

  void addReviewDialog(
      {required BuildContext context,
      required double rating,
      required int talent_id}) {
    showDialog(
      context: context,
      builder: (ctx) {
        return ReviewDialog(
          rating: rating,
          talent_id: talent_id,
        );
      },
    );
  }

  void check_for_permissions({required isPhoto}) async {
    var permission = Permission.photos;
    if (Platform.isAndroid) {
      var deviceInfo = DeviceInfoPlugin();
      final androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.version.sdkInt <= 32) {
        /// use [Permissions.storage.status]
        permission = Permission.storage;
      } else {
        /// use [Permissions.photos.status]
        permission = Permission.photos;
      }
    }

    Map<Permission, PermissionStatus> perms =
        await [permission, Permission.camera].request();

    if (perms[permission]!.isGranted && perms[Permission.camera]!.isGranted) {
      showAlertDialog(isPhoto: isPhoto);
    } else
    // if (perms[Permission.storage]!.isDenied ||
    //   perms[Permission.camera]!.isDenied)
    {
      String msg = "";
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        msg =
            "Storage, camera Permission is need to get the photo, please enable it in settings";
      } else {
        msg = "Storage, camera Permission is need to get the photo";
      }

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
        duration: Duration(seconds: 1),
      ));
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        Future.delayed(Duration(milliseconds: 1500), () {
          openAppSettings();
        });
      }
    }
  }

  void showAlertDialog({required isPhoto}) {
    AlertDialog alertDialog = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        isPhoto ? "Add Photo" : "Add video",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        textAlign: TextAlign.center,
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          isPhoto
              ? GestureDetector(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true).pop();
                    if (isPhoto) {
                      _getFromGallery();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Gallery",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.workSans(
                          textStyle: TextStyle(
                            color: MyColors.WHITE_COLOR,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              : Container(),
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              if (isPhoto) {
                _getFromCamera();
              } else {
                getVideo(ImageSource.camera);
              }
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Camera",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return alertDialog;
      },
    );
  }

  _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      uploadPhoto(imageFile: File(pickedFile.path));
    }
  }

  _getFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      uploadPhoto(imageFile: File(pickedFile.path));
    }
  }

  void uploadPhoto({required imageFile}) {
    HelperClass.show_alert_Dialog(context, "processing");
    ApisServices.uploadPhoto(
            token: SharedPrefs.getAccessToken(), file_path: imageFile!.path)
        .then((value) {
      String msg = "";
      Navigator.of(context, rootNavigator: true).pop();
      if (value != null) {
        msg = value.message ?? "";
        if (value.status == 200) {
          // setState(() {});
          loadData();
        } else if (value.status == 204) {
          Navigator.pushNamed(
              context, AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN,
              arguments: {
                AddSubscriptionsScreen.SHOW_SKIP_KEY: false,
                AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key:
                    value.subscription_id ?? 0
              });
        }
      } else {
        msg = HelperClass.ERROR_TEXT;
      }
      HelperClass.show_snackbar(context, msg);
    });
  }

  Future getVideo(
    ImageSource img,
  ) async {
    final pickedFile = await ImagePicker().pickVideo(
        source: img,
        preferredCameraDevice: CameraDevice.front,
        maxDuration: const Duration(seconds: 60));

    XFile? xfilePick = pickedFile;
    if (xfilePick != null) {
      final result = await Navigator.of(context, rootNavigator: true)
          .push(MaterialPageRoute(
        builder: (context) {
          return VideoPlayerFileScreen(videoFile: File(xfilePick.path));
        },
      ));

      if (!mounted) return;

      if (result != null) {
        HelperClass.show_snackbar(context, result.feedback ?? "");
        if (result.status == 200) {
          // setState(() {});
          loadData();
        } else if (result.status == 204) {
          Navigator.pushNamed(
              context, AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN,
              arguments: {
                AddSubscriptionsScreen.SHOW_SKIP_KEY: false,
                AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key:
                    result.subscription_id ?? 0
              });
        }
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(// is this context <<<
          const SnackBar(content: Text('Nothing is selected')));
    }
  }
}
