import 'package:e4t/blocks/AdsBloc/ads_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/auth/loginScreen.dart';
import 'package:e4t/screens/main_navigator_screen.dart';
import 'package:e4t/screens/profile/UserInitialDetailsScreen.dart';
import 'package:e4t/screens/widgets/adsVideosSlider.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class AdsScreen extends StatefulWidget {
  AdsScreen({super.key, required this.isShowLoginButton});

  static const ADS_SCREEN_ROUTE = "ads_route";

  bool isShowLoginButton;

  @override
  State<AdsScreen> createState() => _AdsScreenState();
}

class _AdsScreenState extends State<AdsScreen> {
  bool isVideo = true, showSkip = false;

  late var adsBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    adsBloc = BlocProvider.of<AdsBloc>(context);
    adsBloc.add(AdsLoadingEvent());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (MediaQuery.of(context).orientation == Orientation.landscape) {
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        } else {
          return true;
        }

        return false;
      },
      child: SafeArea(
        child: Scaffold(
          appBar: !widget.isShowLoginButton
              ? AppBar(
                  backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
                  elevation: 0,
                  title: Text(
                    "Talent Introductions",
                    style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  leading: IconButton(
                    onPressed: () {
                      if (MediaQuery.of(context).orientation ==
                          Orientation.landscape) {
                        SystemChrome.setPreferredOrientations(
                            [DeviceOrientation.portraitUp]);
                      } else {
                        Navigator.pop(context);
                      }
                    },
                    icon: Icon(
                      Icons.arrow_back_ios_new,
                      color: MyColors.ORANGE_COLOR,
                      size: 16,
                    ),
                  ),
                )
              : null,
          body: BlocConsumer<AdsBloc, AdsState>(
            listener: (context, state) {
              if (state is AdsErrorState) {

                if (widget.isShowLoginButton) {
                  checkLoggedIn(isFromError: true);
                }
              } else if (state is AdsLoadedState) {
                if (widget.isShowLoginButton) {
                  Future.delayed(
                      Duration(
                        seconds: 4,
                      ), () {
                    showSkip = true;
                    setState(() {});
                  });
                }
              }
            },
            builder: (context, state) {
              if (state is AdsLoadingState) {
                return CircularIndicatorWidget();
              } else if (state is AdsErrorState) {
                return !widget.isShowLoginButton
                    ? CustomErrorWidget(
                        on_tap: () {
                          adsBloc.add(AdsLoadingEvent());
                        },
                      )
                    : Container();
              } else if (state is AdsLoadedState) {
                return Stack(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                    ),
                    Positioned(
                      top: 0.0,
                      bottom: 0.0,
                      child: AdsVideosSlider(
                        adsDataList: state.adsDataList,
                        on_tap: () {
                          print("click on reel");

                          if (!widget.isShowLoginButton) {
                            checkLoggedIn(isFromError: false);
                          }
                        },
                      ),
                    ),
                    showSkip
                        ? Positioned(
                            left: 20.0,
                            bottom: MediaQuery.of(context).size.height * 0.1,
                            child: GestureDetector(
                              onTap: () {

                                checkLoggedIn(isFromError: false);
                              },
                              child: Icon(
                                FontAwesomeIcons.xmark,
                                color: MyColors.ORANGE_COLOR,
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                    widget.isShowLoginButton
                        ? Positioned(
                            bottom: 10.0,
                            right: 0.0,
                            left: 0.0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    // fromHeight use double.infinity as width and 40 is the height
                                    backgroundColor: MyColors.GREY_COLOR,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18),
                                    ),
                                  ),
                                  onPressed: () {
                                    // Navigator.pop(context);
                                    // Navigator.of(context, rootNavigator: true).pop();
                                    checkLoggedIn(isFromError: false);
                                  },
                                  child: Text(
                                    "Skip",
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 20.0,
                                ),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    // fromHeight use double.infinity as width and 40 is the height
                                    backgroundColor: MyColors.ORANGE_COLOR,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18),
                                    ),
                                  ),
                                  onPressed: () {

                                    checkLoggedIn(isFromError: false);
                                  },
                                  child: Text(
                                    "Login",
                                    style: GoogleFonts.workSans(
                                      textStyle: TextStyle(
                                        color: MyColors.WHITE_COLOR,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : SizedBox.shrink(),
                  ],
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  void checkLoggedIn({required isFromError}) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    var userDataCheck =
        SharedPrefs.getBool(SharedPrefs.isUserDataCompletedKey) ?? false;
    var otpCompleteCheck =
        SharedPrefs.getBool(SharedPrefs.isOtpCompletedKey) ?? false;
    print(
        "userDataCheck: $userDataCheck :: otpCompleteCheck: $otpCompleteCheck");
    if (userDataCheck) {
      /// user entered data

      if (isFromError) {
        clearAndGoToNextScreen(
            routeName: MainNavigationScreen.MAIN_NAVIGATION_SCREEN_ROUTE);
      } else {
        goToNextScreen(
            routeName: MainNavigationScreen.MAIN_NAVIGATION_SCREEN_ROUTE);
      }
    } else if (otpCompleteCheck) {
      /// otp completed

      if (isFromError) {
        clearAndGoToNextScreen(
            routeName: UserInitialDetailsScreen.USER_INITIAL_DETAILS_ROUTE);
      } else {
        goToNextScreen(
            routeName: UserInitialDetailsScreen.USER_INITIAL_DETAILS_ROUTE);
      }
    } else {

      if (isFromError) {
        clearAndGoToNextScreen(routeName: LoginScreen.LOGIN_ROUTE);
      } else {
        goToNextScreen(routeName: LoginScreen.LOGIN_ROUTE);
      }
    }
  }

  void goToNextScreen({required routeName}) {
    Navigator.pushNamed(
      context, routeName,
      // (route) => false,
    );
  }

  void clearAndGoToNextScreen({required routeName}) {
    Navigator.pushNamedAndRemoveUntil(context, routeName, (route) => false);
  }
}
