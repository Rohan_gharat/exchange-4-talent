import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/screens/chat/all_chat_users_screen.dart';
import 'package:e4t/screens/chat/create_group_screen.dart';
import 'package:e4t/screens/chat/recentChatScreen.dart';
import 'package:e4t/screens/chat/secretServicesScreen.dart';
import 'package:e4t/screens/widgets/drawerIconWidget.dart';
import 'package:e4t/screens/widgets/drawerMenu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../myColors/myColors.dart';

class ChatScreen extends StatefulWidget {
  ChatScreen({Key? key}) : super(key: key);
  static const CHAT_ROUTE = "chat_route";

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen>
    with TickerProviderStateMixin {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  bool groupFoundCheck = false;
  late TabController tabController;
  int currentIndex = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  late var userBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBloc = BlocProvider.of<UserBloc>(context);
    tabController = TabController(length: 2, vsync: this)
      ..addListener(() {
        currentIndex = tabController.index;
        setState(() {});
      });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            "Chat",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          actions: [
            DrawerIconWidget(
              on_tap: () {
                _scaffoldKey.currentState!.openEndDrawer();
              },
            ),
          ],
        ),
        endDrawer: DrawerMenu(),
        body: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: const EdgeInsets.only(
                  top: 4.0,
                ),
                decoration: BoxDecoration(
                  color: MyColors.ORANGE_COLOR,
                  borderRadius: BorderRadius.circular(
                    14.0,
                  ),
                ),
                padding: const EdgeInsets.all(
                  4.0,
                ),
                width: MediaQuery.of(context).size.width,
                child: TabBar(
                    // isScrollable: true,
                    controller: tabController,
                    indicator: BoxDecoration(
                      color: MyColors.WHITE_COLOR,
                      borderRadius: BorderRadius.circular(
                        14,
                      ),
                    ),
                    labelColor: MyColors.ORANGE_COLOR,
                    dividerColor: Colors.black,
                    // ignore: prefer_const_literals_to_create_immutables
                    unselectedLabelColor: MyColors.WHITE_COLOR,
                    tabs: const [
                      Tab(
                        text: "Chat",
                      ),
                      Tab(
                        text: "Secret Service",
                      ),
                    ]
                    ),
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: tabController,
                children: [
                  RecentChatScreen(),
                  SecretServicesScreen(
                    currentIndex: currentIndex,
                  )
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            FloatingActionButton(
              heroTag: "fab1",
              child: Icon(
                Icons.group,
                color: MyColors.WHITE_COLOR,
              ),
              onPressed: () {
               Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return CreateGroupScreen(
                        currentUserUid: userBloc.myUserData!.mobile,
                        isSecretService: currentIndex == 1 ? true : false,
                      );
                    },
                  ),
                );
              },
              backgroundColor: MyColors.ORANGE_COLOR,
            ),
            SizedBox(
              height: 10.0,
            ),
            currentIndex == 0
                ? FloatingActionButton(
                    heroTag: "fab2",
                    child: Icon(
                      Icons.message,
                      color: MyColors.WHITE_COLOR,
                    ),
                    onPressed: () {

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return AllChatUsersScreen(
                                currentUserUid: userBloc.myUserData!.mobile);
                          },
                        ),
                      );
                    },
                    backgroundColor: MyColors.ORANGE_COLOR,
                  )
                : SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
