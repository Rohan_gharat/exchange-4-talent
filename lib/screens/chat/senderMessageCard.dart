import 'package:e4t/firebase/messageEnum.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/chat/displayTextImageGif.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SenderMessageCard extends StatelessWidget {
  const SenderMessageCard({
    Key? key,
    required this.message,
    required this.date,
    required this.type,
    required this.isGroupChat,
    required this.senderUserName,
    required this.isSecretService,
  }) : super(key: key);
  final String message;
  final String date;
  final MessageEnum type;

  final bool isGroupChat;
  final String? senderUserName;
  final bool isSecretService;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width - 45,
        ),
        child: Card(
          elevation: 1,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(
                23.0,
              ),
              bottomRight: Radius.circular(
                23.0,
              ),
              topRight: Radius.circular(
                23.0,
              ),
            ),
          ),
          color: MyColors.LIGHT_ORANGE_COLOR,
          margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          child: Stack(
            children: [
              Padding(
                padding: type == MessageEnum.text
                    ? const EdgeInsets.only(
                        left: 10,
                        right: 30,
                        top: 5,
                        bottom: 20,
                      )
                    : const EdgeInsets.only(
                        left: 5,
                        top: 15,
                        right: 5,
                        bottom: 25,
                      ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    !isSecretService &&
                            isGroupChat &&
                            (senderUserName != null && senderUserName != "")
                        ? Text(
                            senderUserName!,
                            style: GoogleFonts.workSans(
                              textStyle: TextStyle(
                                color: MyColors.ORANGE_COLOR,
                                fontSize: 16.0,
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                    DisplayTextImageGIF(
                      message: message,
                      type: type,
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 8,
                right: 10,
                child: Text(
                  date,
                  style: TextStyle(
                    fontSize: 13,
                    color: Colors.grey[600],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
