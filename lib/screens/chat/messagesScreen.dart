import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:e4t/blocks/MessagesUserDataBloc/messages_user_data_bloc.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/firebase/firebaseUserModel.dart';
import 'package:e4t/firebase/messageModel.dart';
import 'package:e4t/firebase/messageEnum.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/chat/chatDetailsScreen.dart';
import 'package:e4t/screens/chat/myMessageCard.dart';
import 'package:e4t/screens/chat/senderMessageCard.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

class MessagesScreen extends StatefulWidget {
  MessagesScreen({
    Key? key,
    required this.name,
    required this.chat_receiver_uid,
    required this.isGroupChat,
    required this.profilePic,
    required this.currentUserUid,
    required this.isSecretService,
  }) : super(key: key);

  final String name;
  final String chat_receiver_uid;
  final bool isGroupChat;
  final String profilePic;
  String currentUserUid;
  final bool isSecretService;

  @override
  State<MessagesScreen> createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  var msgController = TextEditingController();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  final ScrollController messageController = ScrollController();
  FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  late UserBloc userBloc;
  late MessagesUserDataBloc messagesUserDataBloc;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    messageController.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBloc = BlocProvider.of<UserBloc>(context);
    messagesUserDataBloc = BlocProvider.of<MessagesUserDataBloc>(context);

    if (widget.isGroupChat && userBloc.myUserData?.mobile == null) {
      messagesUserDataBloc
          .add(MessagesUserDataLoadingEvent(SharedPrefs.getUserId()));
    } else {
      messagesUserDataBloc
          .add(MessagesUserDataAvailableEvent(userBloc.myUserData!));
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
        elevation: 0,
        title: Row(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back_ios_new,
                color: MyColors.ORANGE_COLOR,
                size: 16,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(
                4.0,
              ),
              child: GestureDetector(
                onTap: widget.isGroupChat && !widget.isSecretService
                    ? gotoChatDetails
                    : (){
                  Navigator.pop(context);
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(
                    60.0,
                  ),
                  child: widget.profilePic != null && widget.profilePic != ""
                      ? CachedNetworkImage(
                          imageUrl: widget.profilePic,
                          fit: BoxFit.cover,
                          height: 50,
                          width: 50,
                          placeholder: (context, url) => Image.asset(
                            HelperClass.APP_LOGO_PATH,
                            fit: BoxFit.cover,
                            height: 50,
                            width: 50,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                            HelperClass.APP_LOGO_PATH,
                            fit: BoxFit.cover,
                            height: 50,
                            width: 50,
                          ),
                        )
                      : Image.asset(
                          HelperClass.APP_LOGO_PATH,
                          fit: BoxFit.cover,
                          height: 50,
                          width: 50,
                        ),
                ),
              ),
            ),
            Expanded(
              child: GestureDetector(
                onTap: widget.isGroupChat && !widget.isSecretService
                    ? gotoChatDetails
                    : null,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.name,
                      style: GoogleFonts.montserrat(
                        textStyle: TextStyle(
                          color: MyColors.WHITE_COLOR,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: BlocConsumer<MessagesUserDataBloc, MessagesUserDataState>(
        listener: (context, state) {
          if (state is MessagesUserDataLoadedState) {
            widget.currentUserUid = state.userModel.mobile;
          }
        },
        builder: (context, state) {
          if (state is MessagesUserDataLoadingState) {
            return const CircularIndicatorWidget();
          } else if (state is MessagesUserDataErrorState) {
            return CustomErrorWidget(
              on_tap: () {
                messagesUserDataBloc
                    .add(MessagesUserDataLoadingEvent(SharedPrefs.getUserId()));
              },
            );
          } else if (state is MessagesUserDataLoadedState) {
            return StreamBuilder<FirebaseUserModel>(
                stream: FireBaseHelperClass.userData(
                    fireStore: firestore, userId: widget.currentUserUid),
                builder: (context, firebaseUserSnapshot) {
                  if (firebaseUserSnapshot.connectionState ==
                      ConnectionState.waiting) {
                    return const CircularIndicatorWidget();
                  }
                 return Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(
                            8.0,
                          ),
                          child: StreamBuilder<List<MessageModel>>(
                              stream: widget.isGroupChat
                                  ? FireBaseHelperClass.getGroupChatStream(
                                      firestore: firestore,
                                      groupId: widget.chat_receiver_uid,
                                      isSecretService: widget.isSecretService,
                                    )
                                  : FireBaseHelperClass.getChatStream(
                                      fireStore: firestore,
                                      receiverUserId: widget.chat_receiver_uid,
                                      currentUserid: widget.currentUserUid),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return const CircularIndicatorWidget();
                                }
                                if (snapshot.data!.isEmpty) {
                                  return NoDataWidget();
                                } else {
                                  SchedulerBinding.instance
                                      .addPostFrameCallback((_) {
                                    messageController.jumpTo(messageController
                                        .position.maxScrollExtent);
                                  });

                                  return ListView.builder(
                                    controller: messageController,
                                    itemCount: snapshot.data!.length,
                                    shrinkWrap: true,
                                    itemBuilder: (ctx, index) {
                                      final messageData = snapshot.data![index];
                                      var timeSent = DateFormat.Hm()
                                          .format(messageData.timeSent);

                                      if (messageData.senderId ==
                                          widget.currentUserUid) {
                                        return MyMessageCard(
                                          message: messageData.text ?? "",
                                          date: timeSent,
                                          type: messageData.type,
                                          isSeen: messageData.isSeen,
                                          isGroupChat: widget.isGroupChat,
                                          senderUserName:
                                              messageData.senderName,
                                          isSecretService:
                                              widget.isSecretService,
                                        );
                                      }
                                      return SenderMessageCard(
                                        message: messageData.text ?? "",
                                        date: timeSent,
                                        type: messageData.type,
                                        isGroupChat: widget.isGroupChat,
                                        senderUserName: messageData.senderName,
                                        isSecretService: widget.isSecretService,
                                      );
                                    },
                                  );
                                }
                              }),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: MyColors.BG_COLOR_TEXT_FILED,
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        padding: EdgeInsets.all(
                          8.0,
                        ),
                        margin: EdgeInsets.only(
                          bottom: 10.0,
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextField(
                                // autofocus: true,
                                controller: msgController,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                    left: 8.0,
                                    right: 8.0,
                                  ),
                                  filled: true,
                                  fillColor: MyColors.WHITE_COLOR,
                                  hintText: "type a message",
                                  hintStyle: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.GREY_COLOR,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                    ),
                                    borderRadius: BorderRadius.circular(
                                      32.0,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                    ),
                                    borderRadius: BorderRadius.circular(
                                      32.0,
                                    ),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                    ),
                                    borderRadius: BorderRadius.circular(
                                      32.0,
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: MyColors.BG_COLOR_TEXT_FILED,
                                    ),
                                    borderRadius: BorderRadius.circular(
                                      32.0,
                                    ),
                                  ),
                                  counterStyle: GoogleFonts.workSans(
                                    textStyle: TextStyle(
                                      color: MyColors.WHITE_COLOR,
                                      fontSize: 10.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  border: InputBorder.none,
                                  counterText: "",
                                ),
                                style: GoogleFonts.workSans(
                                  textStyle: TextStyle(
                                    color: MyColors.BLACK_COLOR,
                                    fontSize: 16.0,
                                  ),
                                ),
                                cursorColor: MyColors.BLACK_COLOR,
                                // textInputAction: TextInputAction.done,
                                // keyboardType: TextInputType.text,
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            GestureDetector(
                              onTap: () {
                                check_for_permissions(
                                    senderUserData: firebaseUserSnapshot.data!);
                              },
                              child: Icon(
                                Icons.file_copy,
                                color: MyColors.ORANGE_COLOR,
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            GestureDetector(
                              onTap: () {
                                if (msgController.text.trim().isEmpty) {
                                  HelperClass.show_snackbar(
                                      context, "type a message");
                                } else {
                                  FireBaseHelperClass.sendTextMessage(
                                    firestore: firestore,
                                    context: context,
                                    text: msgController.text.trim(),
                                    receiverUserId: widget.chat_receiver_uid,
                                    senderUser: firebaseUserSnapshot.data!,
                                    isGroupChat: widget.isGroupChat,
                                    isSecretService: widget.isSecretService,
                                    chatProfilePic: widget.profilePic,
                                  );
                                  msgController.clear();
                                }
                              },
                              child: Icon(
                                Icons.send,
                                color: MyColors.ORANGE_COLOR,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                });
          }
          return Container();
        },
      ),
    );
  }

  gotoChatDetails() {
    Navigator.pushNamed(
      context,
      ChatDetailsScreen.CHAT_DETAILS_SCREEN_ROUTE,
      arguments: {
        ChatDetailsScreen.CHAT_DETAILS_CHAT_RECEIVER_ID_KEY:
            widget.chat_receiver_uid,
        ChatDetailsScreen.CHAT_DETAILS_NAME_KEY: widget.name,
        ChatDetailsScreen.CHAT_DETAILS_MY_USER_ID_KEY: widget.currentUserUid,
      },
    );
  }

  void check_for_permissions(
      {required FirebaseUserModel senderUserData}) async {
    var permission = Permission.photos;
    if (Platform.isAndroid) {
      var deviceInfo = DeviceInfoPlugin();
      final androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.version.sdkInt <= 32) {
        /// use [Permissions.storage.status]
        permission = Permission.storage;
      } else {
        /// use [Permissions.photos.status]
        permission = Permission.photos;
      }
    }

    Map<Permission, PermissionStatus> perms =
        await [permission, Permission.camera].request();

    if (perms[permission]!.isGranted && perms[Permission.camera]!.isGranted) {
      print("permission is granted.");
      showAlertDialog(senderUserData: senderUserData);
    } else
    // if (perms[Permission.storage]!.isDenied ||
    //   perms[Permission.camera]!.isDenied)
    {
      String msg = "";
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        print("permission is PermanentlyDenied.");
        msg =
            "Storage, camera Permission is need to get the photo, please enable it in settings";
      } else {
        print("permission is Denied.");
        msg = "Storage, camera Permission is need to get the photo";
      }

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
        duration: Duration(seconds: 1),
      ));
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        Future.delayed(Duration(milliseconds: 1000), () {
          openAppSettings();
        });
      }
    }
  }

  void showAlertDialog({required FirebaseUserModel senderUserData}) {
    print("alert");
    AlertDialog alertDialog = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        "Profile Photo",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        textAlign: TextAlign.center,
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              _getFromGallery(senderUserData: senderUserData);
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Gallery",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              _getFromCamera(senderUserData: senderUserData);
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Camera",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return alertDialog;
      },
    );
  }

  _getFromGallery({required FirebaseUserModel senderUserData}) async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      sendImageFile(
          file: File(pickedFile.path), senderUserData: senderUserData);
    }
  }

  _getFromCamera({required FirebaseUserModel senderUserData}) async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      sendImageFile(
          file: File(pickedFile.path), senderUserData: senderUserData);
    }
  }

  void sendImageFile({
    required File file,
    required FirebaseUserModel senderUserData,
  }) {
    FireBaseHelperClass.sendFileMessage(
      firestore: firestore,
      firebaseStorage: firebaseStorage,
      context: context,
      file: file,
      receiverUserId: widget.chat_receiver_uid,
      senderUserData: senderUserData,
      messageEnum: MessageEnum.image,
      isGroupChat: widget.isGroupChat,
      currentUserUid: widget.currentUserUid,
      isSecretService: widget.isSecretService,
      chatProfilePic: widget.profilePic,
    );
  }
}
