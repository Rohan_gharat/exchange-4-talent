import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/blocks/CreateGroupAllUsersBloc/create_group_all_users_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/firebase/firebaseUserModel.dart';
import 'package:e4t/firebase/groupsModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class ChatDetailsScreen extends StatefulWidget {
  ChatDetailsScreen({
    super.key,
    required this.chat_receiver_uid,
    required this.chatName,
    required this.myUserId,
  });

  static const CHAT_DETAILS_SCREEN_ROUTE = "chatDetailsScreenRoute";

  static const CHAT_DETAILS_NAME_KEY = "chatDetailsNameKey";

  static const CHAT_DETAILS_CHAT_RECEIVER_ID_KEY =
      "chatDetailsChatReceiverIdKey";

  static const CHAT_DETAILS_MY_USER_ID_KEY = "chatDetailsMyUserIdKey";

  var chat_receiver_uid;
  var chatName;
  var myUserId;

  @override
  State<ChatDetailsScreen> createState() => _ChatDetailsScreenState();
}

class _ChatDetailsScreenState extends State<ChatDetailsScreen> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
        elevation: 0,
        title: Text(
          widget.chatName,
          style: GoogleFonts.montserrat(
            textStyle: TextStyle(
              color: MyColors.WHITE_COLOR,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: MyColors.ORANGE_COLOR,
            size: 16,
          ),
        ),
      ),
      body: StreamBuilder<GroupModel>(
        stream: FireBaseHelperClass.getGroupDataStreamFromGroupId(
          firestore: firestore,
          groupId: widget.chat_receiver_uid,
        ),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularIndicatorWidget();
          }

          if (snapshot.hasData && snapshot.data != null) {
            var groupModel = snapshot.data!;

            if (groupModel.membersUid.isEmpty) {
            } else {
              return SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: MyColors.GREY_COLOR1.withOpacity(0.2),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          10.0,
                        ),
                        child: groupModel.groupPic != null &&
                                groupModel.groupPic != ""
                            ? CachedNetworkImage(
                                imageUrl: groupModel.groupPic,
                                // fit: BoxFit.cover,
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                placeholder: (context, url) => Image.asset(
                                  HelperClass.APP_LOGO_PATH,
                                  // fit: BoxFit.cover,
                                  height: 200,
                                  width: MediaQuery.of(context).size.width,
                                ),
                                errorWidget: (context, url, error) =>
                                    Image.asset(
                                  HelperClass.APP_LOGO_PATH,
                                  // fit: BoxFit.cover,
                                  height: 200,
                                  width: MediaQuery.of(context).size.width,
                                ),
                              )
                            : Image.asset(
                                HelperClass.APP_LOGO_PATH,
                                // fit: BoxFit.cover,
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                              ),
                      ),
                    ),
                    StreamBuilder<List<FirebaseUserModel>>(
                      stream: FireBaseHelperClass.userNamesFromUid(
                          firestore: firestore, uids: groupModel.membersUid),
                      builder: (
                        context,
                        userNamesSnapshot,
                      ) {
                        if (userNamesSnapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const CircularIndicatorWidget();
                        }
                        if (userNamesSnapshot.hasData &&
                            userNamesSnapshot.data != null) {
                          List<FirebaseUserModel> userNames =
                              userNamesSnapshot.data!;

                          var adminIndex = userNames.indexWhere(
                              (element) => element.uid == groupModel.senderId);

                          return Padding(
                            padding: const EdgeInsets.all(
                              8.0,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(
                                    top: 20.0,
                                    bottom: 10.0,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "Talents",
                                          style: GoogleFonts.montserrat(
                                            textStyle: const TextStyle(
                                              color: MyColors.ORANGE_COLOR,
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                      groupModel.senderId == widget.myUserId
                                          ? GestureDetector(
                                              onTap: () async {
                                                showDialog(
                                                  context: context,
                                                  builder: (context) =>
                                                      AddTalentInGroupDialog(
                                                    firestore: firestore,
                                                    groupId: widget
                                                        .chat_receiver_uid,
                                                    currentUserUid:
                                                        widget.myUserId,
                                                  ),
                                                );
                                              },
                                              child: const Icon(
                                                FontAwesomeIcons.plus,
                                                color: MyColors.ORANGE_COLOR,
                                              ),
                                            )
                                          : const SizedBox.shrink(),
                                    ],
                                  ),
                                ),
                                ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: userNamesSnapshot.data!.length,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return Container(
                                      margin: EdgeInsets.only(
                                        top: 8.0,
                                        bottom: index >=
                                                userNamesSnapshot.data!.length -
                                                    1
                                            ? 8.0
                                            : 0.0,
                                      ),
                                      padding: const EdgeInsets.all(
                                        16.0,
                                      ),
                                      decoration: BoxDecoration(
                                        color: MyColors.BLACK_COLOR,
                                        borderRadius: BorderRadius.circular(
                                          18.0,
                                        ),
                                      ),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              userNames[index].name ??
                                                  "Anonymous",
                                              style: GoogleFonts.montserrat(
                                                textStyle: const TextStyle(
                                                  color: MyColors.WHITE_COLOR,
                                                  fontSize: 14.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                          groupModel.senderId ==
                                                      widget.myUserId &&
                                                  index != adminIndex
                                              ? GestureDetector(
                                                  onTap: () {
                                                    var alert = AlertDialog(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                          32.0,
                                                        ),
                                                      ),
                                                      backgroundColor:
                                                          MyColors.BG_COLOR,
                                                      title: Text(
                                                        "Remove Talent",
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          textStyle:
                                                              const TextStyle(
                                                            color: MyColors
                                                                .RED_COLOR,
                                                            fontSize: 18.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                      content: Container(
                                                        height: 100.0,
                                                        child: Column(
                                                          children: [
                                                            Text(
                                                              "Are You Sure You Want To Remove ${userNames[index].name ?? "Anonymous"} from this group",
                                                              style: GoogleFonts
                                                                  .montserrat(
                                                                textStyle:
                                                                    const TextStyle(
                                                                  color: MyColors
                                                                      .RED_COLOR,
                                                                  fontSize:
                                                                      14.0,
                                                                  // fontWeight: FontWeight.bold,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      actions: [
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            ElevatedButton(
                                                              style:
                                                                  ElevatedButton
                                                                      .styleFrom(
                                                                // fromHeight use double.infinity as width and 40 is the height
                                                                backgroundColor:
                                                                    MyColors
                                                                        .ORANGE_COLOR,
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              18),
                                                                ),
                                                              ),
                                                              onPressed: () {
                                                                // Navigator.pop(context);
                                                                Navigator.of(
                                                                        context,
                                                                        rootNavigator:
                                                                            true)
                                                                    .pop();
                                                              },
                                                              child: Text(
                                                                "Cancel",
                                                                style: GoogleFonts
                                                                    .workSans(
                                                                  textStyle:
                                                                      const TextStyle(
                                                                    color: MyColors
                                                                        .WHITE_COLOR,
                                                                    fontSize:
                                                                        14.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 20.0,
                                                            ),
                                                            ElevatedButton(
                                                              style:
                                                                  ElevatedButton
                                                                      .styleFrom(
                                                                // fromHeight use double.infinity as width and 40 is the height
                                                                backgroundColor:
                                                                    MyColors
                                                                        .ORANGE_COLOR,
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              18),
                                                                ),
                                                              ),
                                                              onPressed:
                                                                  () async {
                                                                Navigator.of(
                                                                        context,
                                                                        rootNavigator:
                                                                            true)
                                                                    .pop();

                                                                FireBaseHelperClass.updateMembersInGroup(
                                                                    firestore:
                                                                        firestore,
                                                                    groupId: widget
                                                                        .chat_receiver_uid,
                                                                    isRemove:
                                                                        true,
                                                                    userUid: [
                                                                      userNames[
                                                                              index]
                                                                          .uid
                                                                    ],
                                                                    isSecretService:
                                                                        false);
                                                              },
                                                              child: Text(
                                                                "Remove",
                                                                style: GoogleFonts
                                                                    .workSans(
                                                                  textStyle:
                                                                      const TextStyle(
                                                                    color: MyColors
                                                                        .WHITE_COLOR,
                                                                    fontSize:
                                                                        14.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    );
                                                    showDialog(
                                                      context: context,
                                                      builder: (context) =>
                                                          alert,
                                                    );
                                                  },
                                                  child: const Icon(
                                                    FontAwesomeIcons.trash,
                                                    color: MyColors.RED_COLOR,
                                                  ),
                                                )
                                              : index == adminIndex
                                                  ? Text(
                                                      "Admin",
                                                      style: GoogleFonts
                                                          .montserrat(
                                                        textStyle:
                                                            const TextStyle(
                                                          color: MyColors
                                                              .WHITE_COLOR,
                                                          fontSize: 14.0,
                                                        ),
                                                      ),
                                                    )
                                                  : const SizedBox.shrink(),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          );
                        }
                        return Container();
                      },
                    ),
                  ],
                ),
              );
            }
          }
          return Container();
        },
      ),
    );
  }
}

class AddTalentInGroupDialog extends StatefulWidget {
  AddTalentInGroupDialog({
    super.key,
    required this.firestore,
    required this.groupId,
    required this.currentUserUid,
  });

  var firestore;
  var groupId;
  var currentUserUid;

  @override
  State<AddTalentInGroupDialog> createState() => _AddTalentInGroupDialogState();
}

class _AddTalentInGroupDialogState extends State<AddTalentInGroupDialog> {
  var scrollController = ScrollController();
  var createGroupAllChatUsersBloc;
  List<int> selectedContacts = [];
  List<FirebaseUserModel> selectedUsersList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    createGroupAllChatUsersBloc =
        BlocProvider.of<CreateGroupAllUsersBloc>(context);

    createGroupAllChatUsersBloc.allChatUsers.clear();
    createGroupAllChatUsersBloc.lastDoc = null;
    createGroupAllChatUsersBloc.add(CreateGroupAllChatUsersLoadingEvent(
        widget.currentUserUid, widget.firestore));
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          32.0,
        ),
      ),
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        "Select Talents to create group",
        style: GoogleFonts.montserrat(
          textStyle: const TextStyle(
            color: MyColors.ORANGE_COLOR,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      contentPadding: EdgeInsets.zero,
      content: SizedBox(
        height: MediaQuery.of(context).size.height * 0.9,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: BlocConsumer<CreateGroupAllUsersBloc,
                  CreateGroupAllUsersState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is CreateGroupAllChatUsersLoadingState) {
                    return const CircularIndicatorWidget();
                  } else if (state is CreateGroupAllChatUsersLoadedState) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        selectedUsersList.isNotEmpty
                            ? SizedBox(
                                height: 50,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: selectedUsersList.length,
                                  itemBuilder: (context, chipsIndex) {
                                    return Container(
                                      margin: const EdgeInsets.only(
                                        right: 10.0,
                                      ),
                                      child: InputChip(
                                        label: Text(
                                          selectedUsersList[chipsIndex].name !=
                                                      null &&
                                                  selectedUsersList[chipsIndex]
                                                          .name!
                                                          .trim() !=
                                                      ""
                                              ? selectedUsersList[chipsIndex]
                                                  .name!
                                              : "Anonymous",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.ORANGE_COLOR,
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                        backgroundColor:
                                            MyColors.BG_COLOR_TEXT_FILED,
                                        padding: EdgeInsets.only(
                                          top: 5.0,
                                          bottom: 5.0,
                                          left: 7.0,
                                          right: 0.0,
                                        ),
                                        deleteIconColor: MyColors.RED_COLOR,
                                        deleteIcon: const Icon(
                                          FontAwesomeIcons.circleXmark,
                                          size: 20.0,
                                        ),
                                        onDeleted: () {
                                          selectedUsersList.removeWhere(
                                              (element) =>
                                                  element.uid ==
                                                  state
                                                      .allChatUsersList[
                                                          selectedContacts[
                                                              chipsIndex]]
                                                      .uid);

                                          state
                                              .allChatUsersList[
                                                  selectedContacts[chipsIndex]]
                                              .isSelected = false;
                                          selectedContacts.removeAt(chipsIndex);
                                          createGroupAllChatUsersBloc.add(
                                              CreateGroupAllChatUsersLoadedEvent(
                                                  state.allChatUsersList,
                                                  state.hasMoreData));
                                        },
                                      ),
                                    );
                                  },
                                ),
                              )
                            : const SizedBox.shrink(),
                        Expanded(
                          child: ListView.builder(
                            itemCount: state.hasMoreData
                                ? state.allChatUsersList.length + 1
                                : state.allChatUsersList.length,
                            shrinkWrap: true,
                            controller: scrollController
                              ..addListener(() {
                                if (scrollController.position.maxScrollExtent ==
                                    scrollController.position.pixels) {
                                  if (state.hasMoreData) {
                                    createGroupAllChatUsersBloc.add(
                                        CreateGroupAllChatUsersLoadingEvent(
                                            widget.currentUserUid,
                                            FirebaseFirestore.instance));
                                  }
                                }
                              }),
                            itemBuilder: (ctx, index) {
                              return index >= state.allChatUsersList.length
                                  ? const Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: CircularIndicatorWidget(),
                                    )
                                  : GestureDetector(
                                      onTap: () {
                                        if (selectedContacts.contains(index)) {
                                          selectedContacts.remove(index);
                                          selectedUsersList.removeWhere(
                                              (element) =>
                                                  element.uid ==
                                                  state.allChatUsersList[index]
                                                      .uid);
                                          state.allChatUsersList[index]
                                              .isSelected = false;
                                        } else {
                                          selectedContacts.add(index);
                                          selectedUsersList.add(
                                              state.allChatUsersList[index]);
                                          state.allChatUsersList[index]
                                              .isSelected = true;
                                        }
                                        createGroupAllChatUsersBloc.add(
                                            CreateGroupAllChatUsersLoadedEvent(
                                                state.allChatUsersList,
                                                state.hasMoreData));
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                            18.0,
                                          ),
                                        ),
                                        child: Container(
                                          // width: 150,

                                          padding: EdgeInsets.all(
                                            10.0,
                                          ),
                                          decoration: BoxDecoration(
                                            color: index % 2 == 0
                                                ? MyColors.BG_COLOR_TEXT_FILED
                                                : MyColors.BLACK_COLOR,
                                            borderRadius: BorderRadius.circular(
                                              18.0,
                                            ),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              ClipRRect(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    18.0,
                                                  ),
                                                ),
                                                child: state
                                                                .allChatUsersList[
                                                                    index]
                                                                .profilePic !=
                                                            null &&
                                                        state
                                                                .allChatUsersList[
                                                                    index]
                                                                .profilePic !=
                                                            ""
                                                    ? CachedNetworkImage(
                                                        imageUrl: state
                                                                .allChatUsersList[
                                                                    index]
                                                                .profilePic ??
                                                            "",
                                                        placeholder:
                                                            (context, url) =>
                                                                Image.asset(
                                                          HelperClass
                                                              .APP_LOGO_PATH,
                                                          fit: BoxFit.cover,
                                                          height: 70.0,
                                                          width: 70.0,
                                                        ),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Image.asset(
                                                          HelperClass
                                                              .APP_LOGO_PATH,
                                                          fit: BoxFit.cover,
                                                          height: 70.0,
                                                          width: 70.0,
                                                        ),
                                                        fit: BoxFit.cover,
                                                        height: 70.0,
                                                        width: 70.0,
                                                      )
                                                    : Image.asset(
                                                        HelperClass
                                                            .APP_LOGO_PATH,
                                                        fit: BoxFit.cover,
                                                        height: 70.0,
                                                        width: 70.0,
                                                      ),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 16.0,
                                                  ),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        state
                                                                        .allChatUsersList[
                                                                            index]
                                                                        .name !=
                                                                    null &&
                                                                state
                                                                        .allChatUsersList[
                                                                            index]
                                                                        .name!
                                                                        .trim() !=
                                                                    ""
                                                            ? state
                                                                .allChatUsersList[
                                                                    index]
                                                                .name!
                                                            : "Anonymous",
                                                        style: GoogleFonts
                                                            .workSans(
                                                          textStyle:
                                                              const TextStyle(
                                                            color: MyColors
                                                                .GREY_COLOR1,
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ),
                                                        maxLines: 2,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                      SizedBox(
                                                        height: 8.0,
                                                      ),
                                                      Text(
                                                        state
                                                                .allChatUsersList[
                                                                    index]
                                                                .categoryTitle ??
                                                            "",
                                                        style: GoogleFonts
                                                            .workSans(
                                                          textStyle:
                                                              const TextStyle(
                                                            color: MyColors
                                                                .ORANGE_COLOR,
                                                            fontSize: 16.0,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ),
                                                        maxLines: 2,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              state.allChatUsersList[index]
                                                      .isSelected
                                                  ? Icon(
                                                      Icons.done,
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                    )
                                                  : Container(),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                            },
                          ),
                        ),
                      ],
                    );
                  }
                  return Container();
                  // return CircularIndicatorWidget();
                },
              ),
            ),
          ],
        ),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                "Cancel",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fromHeight use double.infinity as width and 40 is the height
                backgroundColor: MyColors.ORANGE_COLOR,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              onPressed: () async {
                if (selectedUsersList.isEmpty) {
                  ScaffoldMessenger.of(context).removeCurrentSnackBar();
                  HelperClass.show_snackbar(context, "please select talents");
                } else {
                  List<String> uidsList = [];

                  for (var user in selectedUsersList) {
                    uidsList.add(user.uid);
                  }

                  HelperClass.show_alert_Dialog(context, "processing");
                  Future.delayed(const Duration(seconds: 1), () async {
                    var response =
                        await FireBaseHelperClass.updateMembersInGroup(
                      firestore: widget.firestore,
                      groupId: widget.groupId,
                      isRemove: false,
                      userUid: uidsList,
                      isSecretService: false,
                    );
                    Navigator.of(context, rootNavigator: true).pop();
                    if (response) {
                      Navigator.of(context, rootNavigator: true).pop();
                    } else {
                      HelperClass.show_snackbar(
                          context, HelperClass.ERROR_TEXT);
                    }
                  });
                }
              },
              child: Text(
                "Add",
                style: GoogleFonts.workSans(
                  textStyle: TextStyle(
                    color: MyColors.WHITE_COLOR,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
