import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/blocks/AllChatUsersBloc/all_chat_users_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/chat/messagesScreen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class AllChatUsersScreen extends StatelessWidget {
  AllChatUsersScreen({
    Key? key,
    required this.currentUserUid,
  }) : super(key: key);
  var currentUserUid;

  var scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var allChatUsersBloc = BlocProvider.of<AllChatUsersBloc>(context);
    allChatUsersBloc.allChatUsers.clear();
    allChatUsersBloc.lastDoc = null;
    allChatUsersBloc.add(
        AllChatUsersLoadingEvent(currentUserUid, FirebaseFirestore.instance));

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
        elevation: 0,
        title: Text(
          "Available Talents",
          style: GoogleFonts.montserrat(
            textStyle: TextStyle(
              color: MyColors.WHITE_COLOR,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: MyColors.ORANGE_COLOR,
            size: 16,
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(
          8.0,
        ),
        child: BlocConsumer<AllChatUsersBloc, AllChatUsersState>(
          listener: (context, state) {
          },
          builder: (context, state) {
            if (state is AllChatUsersLoadingState) {
              return const CircularIndicatorWidget();
            } else if (state is AllChatUsersLoadedState) {
              return ListView.builder(
                itemCount: state.hasMoreData
                    ? state.allChatUsersList.length + 1
                    : state.allChatUsersList.length,
                shrinkWrap: true,
                controller: scrollController
                  ..addListener(() {
                    if (scrollController.position.maxScrollExtent ==
                        scrollController.position.pixels) {
                      if (state.hasMoreData) {
                        allChatUsersBloc.add(AllChatUsersLoadingEvent(
                            currentUserUid, FirebaseFirestore.instance));
                      }
                    }
                  }),
                itemBuilder: (ctx, index) {
                  return index >= state.allChatUsersList.length
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: const CircularIndicatorWidget(),
                        )
                      : GestureDetector(
                          onTap: () {
                           Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return MessagesScreen(
                                    name: state.allChatUsersList[index].name ??
                                        "",
                                    chat_receiver_uid:
                                        state.allChatUsersList[index].uid,
                                    isGroupChat: false,
                                    profilePic: state.allChatUsersList[index]
                                            .profilePic ??
                                        "",
                                    currentUserUid: currentUserUid,
                                    isSecretService: false,
                                  );
                                },
                              ),
                            );
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              top: 8.0,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                18.0,
                              ),
                            ),
                            child: Container(
                              // width: 150,

                              padding: EdgeInsets.all(
                                10.0,
                              ),
                              decoration: BoxDecoration(
                                color: index % 2 == 0
                                    ? MyColors.BG_COLOR_TEXT_FILED
                                    : MyColors.BLACK_COLOR,
                                borderRadius: BorderRadius.circular(
                                  18.0,
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(
                                        18.0,
                                      ),
                                    ),
                                    child:

                                        state.allChatUsersList[index]
                                                        .profilePic !=
                                                    null &&
                                                state.allChatUsersList[index]
                                                        .profilePic !=
                                                    ""
                                            ? CachedNetworkImage(
                                                imageUrl: state
                                                        .allChatUsersList[index]
                                                        .profilePic ??
                                                    "",
                                                placeholder: (context, url) =>
                                                    Image.asset(
                                                  HelperClass.APP_LOGO_PATH,
                                                  fit: BoxFit.cover,
                                                  height: 70.0,
                                                  width: 70.0,
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Image.asset(
                                                  HelperClass.APP_LOGO_PATH,
                                                  fit: BoxFit.cover,
                                                  height: 70.0,
                                                  width: 70.0,
                                                ),
                                                fit: BoxFit.cover,
                                                height: 70.0,
                                                width: 70.0,
                                              )
                                            : Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                fit: BoxFit.cover,
                                                height: 70.0,
                                                width: 70.0,
                                              ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 16.0,
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${state.allChatUsersList[index].name}",
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.GREY_COLOR1,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          SizedBox(
                                            height: 8.0,
                                          ),
                                          Text(
                                            state.allChatUsersList[index]
                                                    .categoryTitle ??
                                                "",
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.ORANGE_COLOR,
                                                fontSize: 16.0,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                },
              );
            }
            return Container();
            // return CircularIndicatorWidget();
          },
        ),
      ),

    );
  }
}
