import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/firebase/chatContact.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/firebase/groupsModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/chat/messagesScreen.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class RecentChatScreen extends StatefulWidget {
  RecentChatScreen({super.key});

  @override
  State<RecentChatScreen> createState() => _RecentChatScreenState();
}

class _RecentChatScreenState extends State<RecentChatScreen>
    with AutomaticKeepAliveClientMixin {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  bool groupFoundCheck = false;
  var userBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBloc = BlocProvider.of<UserBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.all(
          8.0,
        ),
        child: Column(
          children: [
            StreamBuilder<List<GroupModel>>(
              stream: FireBaseHelperClass.getChatGroups(
                firestore: firestore,
                currentUserUid: userBloc.myUserData!.mobile,
                isSecretService: false,
              ),
              builder: (context, snapshot) {
                if (!snapshot.hasData || snapshot.data!.isEmpty) {
                  /// no groups found
                  groupFoundCheck = false;
                  return Container();
                } else {
                  groupFoundCheck = true;
                  return ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return MessagesScreen(
                                  name: snapshot.data![index].name,
                                  chat_receiver_uid:
                                      snapshot.data![index].groupId,
                                  isGroupChat: true,
                                  profilePic:
                                      snapshot.data![index].groupPic ?? "",
                                  currentUserUid: userBloc.myUserData!.mobile,
                                  isSecretService: false,
                                );
                              },
                            ),
                          );
                        },
                        onLongPress: () {
                          var alert = AlertDialog(
                            backgroundColor: MyColors.BG_COLOR,
                            title: Text(
                              "Exit Group",
                              style: GoogleFonts.montserrat(
                                textStyle: const TextStyle(
                                  color: MyColors.RED_COLOR,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            content: Container(
                              height: 100.0,
                              child: Column(
                                children: [
                                  Text(
                                    "Are You Sure You Want To Exit This Group",
                                    style: GoogleFonts.montserrat(
                                      textStyle: const TextStyle(
                                        color: MyColors.RED_COLOR,
                                        fontSize: 14.0,
                                        // fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            actions: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      // fromHeight use double.infinity as width and 40 is the height
                                      backgroundColor: MyColors.ORANGE_COLOR,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                      ),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context, rootNavigator: true)
                                          .pop();
                                    },
                                    child: Text(
                                      "Cancel",
                                      style: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20.0,
                                  ),
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      // fromHeight use double.infinity as width and 40 is the height
                                      backgroundColor: MyColors.ORANGE_COLOR,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                      ),
                                    ),
                                    onPressed: () async {
                                      Navigator.of(context, rootNavigator: true)
                                          .pop();
                                      // HelperClass.show_alert_Dialog(
                                      //     context, "exiting group");
                                      var response = await FireBaseHelperClass
                                          .exitGroupChat(
                                        firestore: firestore,
                                        groupId: snapshot.data![index].groupId,
                                        userUid: userBloc.myUserData!.mobile,
                                        isSecretService: false,
                                      );
                                      // Navigator.of(context, rootNavigator: true)
                                      //     .pop();
                                      // if (response) {
                                      //   HelperClass.show_snackbar(
                                      //       context, "group existed");
                                      // } else {
                                      //   HelperClass.show_snackbar(
                                      //       context, HelperClass.ERROR_TEXT);
                                      // }
                                    },
                                    child: Text(
                                      "Delete",
                                      style: GoogleFonts.workSans(
                                        textStyle: TextStyle(
                                          color: MyColors.WHITE_COLOR,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          );
                          showDialog(
                            context: context,
                            builder: (context) => alert,
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 8.0,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                              18.0,
                            ),

                          ),
                          child: Container(
                            // width: 150,

                            padding: EdgeInsets.all(
                              10.0,
                            ),
                            decoration: BoxDecoration(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                              borderRadius: BorderRadius.circular(
                                18.0,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(
                                      18.0,
                                    ),
                                  ),
                                  child: snapshot.data![index].groupPic !=
                                              null &&
                                          snapshot.data![index].groupPic != ""
                                      ? CachedNetworkImage(
                                          imageUrl:
                                              snapshot.data![index].groupPic,
                                          placeholder: (context, url) =>
                                              Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            fit: BoxFit.cover,
                                            height: 70.0,
                                            width: 70.0,
                                          ),
                                          errorWidget: (context, url, error) =>
                                              Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            fit: BoxFit.cover,
                                            height: 70.0,
                                            width: 70.0,
                                          ),
                                          fit: BoxFit.cover,
                                          height: 70.0,
                                          width: 70.0,
                                        )
                                      : Image.asset(
                                          HelperClass.APP_LOGO_PATH,
                                          fit: BoxFit.cover,
                                          height: 70.0,
                                          width: 70.0,
                                        ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          snapshot.data![index].name ?? "",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.GREY_COLOR1,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        SizedBox(
                                          height: 8.0,
                                        ),
                                        snapshot.data![index].senderName !=
                                                    null &&
                                                snapshot.data![index]
                                                        .senderName !=
                                                    "" &&
                                                snapshot.data![index]
                                                        .lastMessage !=
                                                    null &&
                                                snapshot.data![index]
                                                        .lastMessage !=
                                                    ""
                                            ? Text(
                                                "${snapshot.data![index].senderName}: ${snapshot.data![index].lastMessage}",
                                                style: GoogleFonts.workSans(
                                                  textStyle: TextStyle(
                                                    color:
                                                        MyColors.ORANGE_COLOR,
                                                    fontSize: 16.0,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              )
                                            : SizedBox.shrink(),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
              },
            ),
            StreamBuilder<List<ChatContact>>(
                stream: FireBaseHelperClass.getChatContacts(
                    firestore: firestore,
                    currentUserUid: userBloc.myUserData!.mobile),
                builder: (context, snapshot) {
                  print("chats list: ${snapshot.data}");

                  if (!snapshot.hasData || snapshot.data!.isEmpty) {
                    return !groupFoundCheck
                        ? Container(
                            margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.1,
                            ),
                            child: NoDataWidget(),
                          )
                        : Container();
                  } else {
                    return ListView.builder(
                      itemCount: snapshot.data!.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (ctx, index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return MessagesScreen(
                                    name: snapshot.data![index].name ?? "",
                                    chat_receiver_uid:
                                        snapshot.data![index].contactId,
                                    isGroupChat: false,
                                    profilePic:
                                        snapshot.data![index].profilePic ?? "",
                                    currentUserUid: userBloc.myUserData!.mobile,
                                    isSecretService: false,
                                  );
                                },
                              ),
                            );
                          },
                          onLongPress: () {
                            var alert = AlertDialog(
                              backgroundColor: MyColors.BG_COLOR,
                              title: Text(
                                "Delete Chat",
                                style: GoogleFonts.montserrat(
                                  textStyle: const TextStyle(
                                    color: MyColors.RED_COLOR,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              content: Container(
                                height: 100.0,
                                child: Column(
                                  children: [
                                    Text(
                                      "Are You Sure You Want To Delete This Chat",
                                      style: GoogleFonts.montserrat(
                                        textStyle: const TextStyle(
                                          color: MyColors.RED_COLOR,
                                          fontSize: 14.0,
                                          // fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              actions: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        // fromHeight use double.infinity as width and 40 is the height
                                        backgroundColor: MyColors.ORANGE_COLOR,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18),
                                        ),
                                      ),
                                      onPressed: () {
                                        // Navigator.pop(context);
                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .pop();
                                      },
                                      child: Text(
                                        "Cancel",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.WHITE_COLOR,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 20.0,
                                    ),
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        // fromHeight use double.infinity as width and 40 is the height
                                        backgroundColor: MyColors.ORANGE_COLOR,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18),
                                        ),
                                      ),
                                      onPressed: () async {
                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .pop();
                                        // HelperClass.show_alert_Dialog(
                                        //     context, "deleting chat");
                                        var response = await FireBaseHelperClass
                                            .deleteChat(
                                                firestore: firestore,
                                                myUserId:
                                                    userBloc.myUserData!.mobile,
                                                contactId: snapshot
                                                    .data![index].contactId);
                                        // Navigator.of(context,
                                        //         rootNavigator: true)
                                        //     .pop();
                                        // if (response) {
                                        //   HelperClass.show_snackbar(
                                        //       context, "chat deleted");
                                        // } else {
                                        //   HelperClass.show_snackbar(
                                        //       context, HelperClass.ERROR_TEXT);
                                        // }
                                      },
                                      child: Text(
                                        "Delete",
                                        style: GoogleFonts.workSans(
                                          textStyle: TextStyle(
                                            color: MyColors.WHITE_COLOR,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            );
                            showDialog(
                              context: context,
                              builder: (context) => alert,
                            );
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              top: 8.0,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                18.0,
                              ),

                            ),
                            child: Container(
                              // width: 150,

                              padding: EdgeInsets.all(
                                10.0,
                              ),
                              decoration: BoxDecoration(
                                color:

                                    MyColors.BLACK_COLOR,
                                borderRadius: BorderRadius.circular(
                                  18.0,
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(
                                        18.0,
                                      ),
                                    ),
                                    child: snapshot.data![index].profilePic !=
                                                null &&
                                            snapshot.data![index].profilePic !=
                                                ""
                                        ? CachedNetworkImage(
                                            imageUrl: snapshot
                                                    .data![index].profilePic ??
                                                "",
                                            placeholder: (context, url) =>
                                                Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              fit: BoxFit.cover,
                                              height: 70.0,
                                              width: 70.0,
                                            ),
                                            errorWidget:
                                                (context, url, error) =>
                                                    Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              fit: BoxFit.cover,
                                              height: 70.0,
                                              width: 70.0,
                                            ),
                                            fit: BoxFit.cover,
                                            height: 70.0,
                                            width: 70.0,
                                          )
                                        : Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            fit: BoxFit.cover,
                                            height: 70.0,
                                            width: 70.0,
                                          ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 16.0,
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            snapshot.data![index].name ?? "",
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.GREY_COLOR1,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          SizedBox(
                                            height: 8.0,
                                          ),
                                          Text(
                                            snapshot.data![index]
                                                    .categoryTitle ??
                                                "",
                                            style: GoogleFonts.workSans(
                                              textStyle: TextStyle(
                                                color: MyColors.ORANGE_COLOR,
                                                fontSize: 16.0,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
