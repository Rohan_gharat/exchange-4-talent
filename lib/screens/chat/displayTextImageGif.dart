import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/firebase/messageEnum.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';

class DisplayTextImageGIF extends StatelessWidget {
  final String message;
  final MessageEnum type;

  const DisplayTextImageGIF({
    Key? key,
    required this.message,
    required this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return type == MessageEnum.text
        ? Text(
            message,
            style: const TextStyle(
              fontSize: 16,
            ),
          )
        : CachedNetworkImage(
            imageUrl: message,
            placeholder: (context, url) =>
                Image.asset(HelperClass.APP_LOGO_PATH),
          );
  }
}
