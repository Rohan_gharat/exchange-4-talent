import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e4t/blocks/userBloc/user_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/firebase/groupsModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/chat/messagesScreen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class SecretServicesScreen extends StatefulWidget {
  SecretServicesScreen({
    super.key,
    required this.currentIndex,
  });

  int currentIndex;

  @override
  State<SecretServicesScreen> createState() => _SecretServicesScreenState();
}

class _SecretServicesScreenState extends State<SecretServicesScreen>
    with AutomaticKeepAliveClientMixin {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  var userBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBloc = BlocProvider.of<UserBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.all(
          8.0,
        ),
        child: Column(
          children: [
            StreamBuilder<List<GroupModel>>(
              stream: FireBaseHelperClass.getChatGroups(
                firestore: firestore,
                currentUserUid: userBloc.myUserData!.mobile,
                isSecretService: widget.currentIndex == 1 ? true : false,
              ),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularIndicatorWidget();
                }

                if (!snapshot.hasData || snapshot.data!.isEmpty) {
                  /// no groups found

                  return Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.1,
                    ),
                    child: NoDataWidget(),
                  );
                } else {
                  return ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return MessagesScreen(
                                  name: snapshot.data![index].name,
                                  chat_receiver_uid:
                                      snapshot.data![index].groupId,
                                  isGroupChat: true,
                                  profilePic:
                                      snapshot.data![index].groupPic ?? "",
                                  currentUserUid: userBloc.myUserData!.mobile,
                                  isSecretService: true,
                                );
                              },
                            ),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 8.0,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                              18.0,
                            ),

                          ),
                          child: Container(
                            // width: 150,

                            padding: EdgeInsets.all(
                              10.0,
                            ),
                            decoration: BoxDecoration(
                              color: MyColors.BG_COLOR_TEXT_FILED,
                              borderRadius: BorderRadius.circular(
                                18.0,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(
                                      18.0,
                                    ),
                                  ),
                                  child: snapshot.data![index].groupPic !=
                                              null &&
                                          snapshot.data![index].groupPic != ""
                                      ? CachedNetworkImage(
                                          imageUrl:
                                              snapshot.data![index].groupPic,
                                          placeholder: (context, url) =>
                                              Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            fit: BoxFit.cover,
                                            height: 70.0,
                                            width: 70.0,
                                          ),
                                          errorWidget: (context, url, error) =>
                                              Image.asset(
                                            HelperClass.APP_LOGO_PATH,
                                            fit: BoxFit.cover,
                                            height: 70.0,
                                            width: 70.0,
                                          ),
                                          fit: BoxFit.cover,
                                          height: 70.0,
                                          width: 70.0,
                                        )
                                      : Image.asset(
                                          HelperClass.APP_LOGO_PATH,
                                          fit: BoxFit.cover,
                                          height: 70.0,
                                          width: 70.0,
                                        ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          snapshot.data![index].name ?? "",
                                          style: GoogleFonts.workSans(
                                            textStyle: TextStyle(
                                              color: MyColors.GREY_COLOR1,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
