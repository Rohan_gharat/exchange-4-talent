import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:e4t/blocks/CreateGroupAllUsersBloc/create_group_all_users_bloc.dart';
import 'package:e4t/blocks/GroupProfilePhotoBloc/group_profile_photo_bloc.dart';
import 'package:e4t/firebase/firebaseHelper.dart';
import 'package:e4t/firebase/firebaseUserModel.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/chat/messagesScreen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class CreateGroupScreen extends StatefulWidget {
  CreateGroupScreen({
    Key? key,
    required this.currentUserUid,
    this.isSecretService = false,
  }) : super(key: key);
  var currentUserUid;
  bool isSecretService = false;

  @override
  State<CreateGroupScreen> createState() => _CreateGroupScreenState();
}

class _CreateGroupScreenState extends State<CreateGroupScreen> {
  List<int> selectedContacts = [];
  List<FirebaseUserModel> selectedUsersList = [];
  File? image_File;

  var groupNameController = TextEditingController();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  var scrollController;
  var createGroupAllChatUsersBloc;
  var groupProfilePhotoBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    scrollController = ScrollController();

    groupProfilePhotoBloc = BlocProvider.of<GroupProfilePhotoBloc>(context);
    groupProfilePhotoBloc.add(GroupProfilePhotoEvent(null));

    createGroupAllChatUsersBloc =
        BlocProvider.of<CreateGroupAllUsersBloc>(context);

    createGroupAllChatUsersBloc.allChatUsers.clear();
    createGroupAllChatUsersBloc.lastDoc = null;
    createGroupAllChatUsersBloc.add(CreateGroupAllChatUsersLoadingEvent(
        widget.currentUserUid, FirebaseFirestore.instance));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    groupNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            widget.isSecretService ? "Secret Group" : "Create Group",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
          actions: [
            IconButton(
              onPressed: () async {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
                if (selectedUsersList.length < 2 ||
                    groupNameController.text.trim().isEmpty ||
                    image_File == null) {
                  String msg = "";
                  if (selectedUsersList.length < 2) {
                    msg += "please select at least 2 talents to create group\n";
                  }
                  if (groupNameController.text.trim().isEmpty) {
                    msg += "please enter group name\n";
                  } else {
                    FocusManager.instance.primaryFocus?.unfocus();
                  }
                  if (image_File == null) {
                    msg += "please select group photo";
                  }

                  HelperClass.show_snackbar(context, msg);
                } else {
                  FocusManager.instance.primaryFocus?.unfocus();
                  HelperClass.show_alert_Dialog(context, "creating group");
                  var group = await FireBaseHelperClass.createGroup(
                    firestore: firestore,
                    firebaseStorage: firebaseStorage,
                    context: context,
                    groupName: groupNameController.text.trim(),
                    imageFile: image_File!,
                    selectedContacts: selectedUsersList,
                    currentUserUid: widget.currentUserUid,
                    isSecretService: widget.isSecretService,
                  );
                  Navigator.of(context, rootNavigator: true).pop();
                  print("create group response: $group");
                  if (group != null) {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return MessagesScreen(
                            name: group.name,
                            chat_receiver_uid: group.groupId,
                            isGroupChat: true,
                            profilePic: group.groupPic ?? "",
                            currentUserUid: widget.currentUserUid,
                            isSecretService: widget.isSecretService,
                          );
                        },
                      ),
                    );
                  } else {
                    HelperClass.show_snackbar(context, HelperClass.ERROR_TEXT);
                  }
                }
              },
              icon: Icon(
                Icons.done,
                color: MyColors.ORANGE_COLOR,
              ),
            ),
          ],
        ),
        body: Container(
          margin: EdgeInsets.all(
            8.0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  check_for_permissions();
                },
                child: Container(
                  margin: EdgeInsets.only(
                    top: 20.0,
                  ),
                  child: Center(
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: MyColors.ORANGE_COLOR,
                              width: 5.0,
                            ),
                            borderRadius: BorderRadius.circular(
                              100.0,
                            ),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                100.0,
                              ),
                            ),
                            child: BlocConsumer<GroupProfilePhotoBloc,
                                GroupProfilePhotoState>(
                              listener: (context, state) {
                                image_File = state.imageFile;
                              },
                              builder: (context, state) {
                                return state.imageFile != null
                                    ? Image.file(
                                        alignment: Alignment.topCenter,
                                        state.imageFile!,
                                        fit: BoxFit.cover,
                                        height: 80.0,
                                        width: 80.0,
                                      )
                                    : Image.asset(
                                        "images/circular_image_placeholder.jpg",
                                        fit: BoxFit.cover,
                                        height: 80.0,
                                        width: 80.0,
                                      );
                              },
                            ),
                          ),
                        ),
                        Positioned(
                          right: 0.0,
                          top: -20.0,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  100.0,
                                ),
                                color: MyColors.ORANGE_COLOR),
                            padding: EdgeInsets.all(
                              10.0,
                            ),
                            child: Icon(
                              Icons.edit,
                              color: MyColors.WHITE_COLOR,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 20.0,
                ),
                child: TextField(
                  controller: groupNameController,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: MyColors.BG_COLOR_TEXT_FILED,
                    hintText: "Enter Group name",
                    hintStyle: GoogleFonts.workSans(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 16.0,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: MyColors.BG_COLOR_TEXT_FILED,
                      ),
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                    ),
                    counterStyle: GoogleFonts.workSans(
                      textStyle: TextStyle(
                        color: MyColors.WHITE_COLOR,
                        fontSize: 10.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    border: InputBorder.none,
                    counterText: "",
                  ),
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                    ),
                  ),
                  cursorColor: MyColors.WHITE_COLOR,
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.text,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 16.0,
                ),
                child: Text(
                  "Select Talents to create group",
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(
                    8.0,
                  ),
                  child: BlocConsumer<CreateGroupAllUsersBloc,
                      CreateGroupAllUsersState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      if (state is CreateGroupAllChatUsersLoadingState) {
                        return const CircularIndicatorWidget();
                      } else if (state is CreateGroupAllChatUsersLoadedState) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            selectedUsersList.isNotEmpty
                                ? SizedBox(
                                    height: 50,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemCount: selectedUsersList.length,
                                      itemBuilder: (context, chipsIndex) {
                                        return Container(
                                          margin: const EdgeInsets.only(
                                            right: 10.0,
                                          ),
                                          child: InputChip(
                                            label: Text(
                                              selectedUsersList[chipsIndex]
                                                              .name !=
                                                          null &&
                                                      selectedUsersList[
                                                                  chipsIndex]
                                                              .name!
                                                              .trim() !=
                                                          ""
                                                  ? selectedUsersList[
                                                          chipsIndex]
                                                      .name!
                                                  : "Anonymous",
                                              style: GoogleFonts.workSans(
                                                textStyle: TextStyle(
                                                  color: MyColors.ORANGE_COLOR,
                                                  fontSize: 14.0,
                                                ),
                                              ),
                                            ),
                                            backgroundColor:
                                                MyColors.BG_COLOR_TEXT_FILED,
                                            padding: EdgeInsets.only(
                                              top: 5.0,
                                              bottom: 5.0,
                                              left: 7.0,
                                              right: 0.0,
                                            ),
                                            deleteIconColor: MyColors.RED_COLOR,
                                            deleteIcon: const Icon(
                                              FontAwesomeIcons.circleXmark,
                                              size: 20.0,
                                            ),
                                            onDeleted: () {
                                              selectedUsersList.removeWhere(
                                                  (element) =>
                                                      element.uid ==
                                                      state
                                                          .allChatUsersList[
                                                              selectedContacts[
                                                                  chipsIndex]]
                                                          .uid);

                                              state
                                                  .allChatUsersList[
                                                      selectedContacts[
                                                          chipsIndex]]
                                                  .isSelected = false;
                                              selectedContacts
                                                  .removeAt(chipsIndex);
                                              createGroupAllChatUsersBloc.add(
                                                  CreateGroupAllChatUsersLoadedEvent(
                                                      state.allChatUsersList,
                                                      state.hasMoreData));
                                            },
                                          ),
                                        );
                                      },
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            Expanded(
                              child: ListView.builder(
                                itemCount: state.hasMoreData
                                    ? state.allChatUsersList.length + 1
                                    : state.allChatUsersList.length,
                                shrinkWrap: true,
                                controller: scrollController
                                  ..addListener(() {
                                    if (scrollController
                                            .position.maxScrollExtent ==
                                        scrollController.position.pixels) {
                                      if (state.hasMoreData) {
                                        createGroupAllChatUsersBloc.add(
                                            CreateGroupAllChatUsersLoadingEvent(
                                                widget.currentUserUid,
                                                FirebaseFirestore.instance));
                                      }
                                    }
                                  }),
                                itemBuilder: (ctx, index) {
                                  return index >= state.allChatUsersList.length
                                      ? const Padding(
                                          padding: EdgeInsets.all(8.0),
                                          child: CircularIndicatorWidget(),
                                        )
                                      : GestureDetector(
                                          onTap: () {
                                            if (selectedContacts
                                                .contains(index)) {
                                              selectedContacts.remove(index);
                                              selectedUsersList.removeWhere(
                                                  (element) =>
                                                      element.uid ==
                                                      state
                                                          .allChatUsersList[
                                                              index]
                                                          .uid);
                                              state.allChatUsersList[index]
                                                  .isSelected = false;
                                            } else {
                                              selectedContacts.add(index);
                                              selectedUsersList.add(state
                                                  .allChatUsersList[index]);
                                              state.allChatUsersList[index]
                                                  .isSelected = true;
                                            }
                                            createGroupAllChatUsersBloc.add(
                                                CreateGroupAllChatUsersLoadedEvent(
                                                    state.allChatUsersList,
                                                    state.hasMoreData));
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                              top: 8.0,
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                18.0,
                                              ),
                                            ),
                                            child: Container(
                                              // width: 150,

                                              padding: EdgeInsets.all(
                                                10.0,
                                              ),
                                              decoration: BoxDecoration(
                                                color: index % 2 == 0
                                                    ? MyColors
                                                        .BG_COLOR_TEXT_FILED
                                                    : MyColors.BLACK_COLOR,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  18.0,
                                                ),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(
                                                        18.0,
                                                      ),
                                                    ),
                                                    child: state
                                                                    .allChatUsersList[
                                                                        index]
                                                                    .profilePic !=
                                                                null &&
                                                            state
                                                                    .allChatUsersList[
                                                                        index]
                                                                    .profilePic !=
                                                                ""
                                                        ? CachedNetworkImage(
                                                            imageUrl: state
                                                                    .allChatUsersList[
                                                                        index]
                                                                    .profilePic ??
                                                                "",
                                                            placeholder:
                                                                (context,
                                                                        url) =>
                                                                    Image.asset(
                                                              HelperClass
                                                                  .APP_LOGO_PATH,
                                                              fit: BoxFit.cover,
                                                              height: 70.0,
                                                              width: 70.0,
                                                            ),
                                                            errorWidget:
                                                                (context, url,
                                                                        error) =>
                                                                    Image.asset(
                                                              HelperClass
                                                                  .APP_LOGO_PATH,
                                                              fit: BoxFit.cover,
                                                              height: 70.0,
                                                              width: 70.0,
                                                            ),
                                                            fit: BoxFit.cover,
                                                            height: 70.0,
                                                            width: 70.0,
                                                          )
                                                        : Image.asset(
                                                            HelperClass
                                                                .APP_LOGO_PATH,
                                                            fit: BoxFit.cover,
                                                            height: 70.0,
                                                            width: 70.0,
                                                          ),
                                                  ),
                                                  Expanded(
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                          .symmetric(
                                                        horizontal: 16.0,
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            "${state.allChatUsersList[index].name}",
                                                            style: GoogleFonts
                                                                .workSans(
                                                              textStyle:
                                                                  const TextStyle(
                                                                color: MyColors
                                                                    .GREY_COLOR1,
                                                                fontSize: 16.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ),
                                                            maxLines: 2,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                          SizedBox(
                                                            height: 8.0,
                                                          ),
                                                          Text(
                                                            state
                                                                    .allChatUsersList[
                                                                        index]
                                                                    .categoryTitle ??
                                                                "",
                                                            style: GoogleFonts
                                                                .workSans(
                                                              textStyle:
                                                                  const TextStyle(
                                                                color: MyColors
                                                                    .ORANGE_COLOR,
                                                                fontSize: 16.0,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ),
                                                            maxLines: 2,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  state.allChatUsersList[index]
                                                          .isSelected
                                                      ? const Icon(
                                                          Icons.done,
                                                          color: MyColors
                                                              .ORANGE_COLOR,
                                                        )
                                                      : Container(),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                },
                              ),
                            ),
                          ],
                        );
                      }
                      return Container();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void check_for_permissions() async {
    var permission = Permission.photos;
    if (Platform.isAndroid) {
      var deviceInfo = DeviceInfoPlugin();
      final androidInfo = await deviceInfo.androidInfo;
      if (androidInfo.version.sdkInt <= 32) {
        /// use [Permissions.storage.status]
        permission = Permission.storage;
      } else {
        /// use [Permissions.photos.status]
        permission = Permission.photos;
      }
    }

    Map<Permission, PermissionStatus> perms =
        await [permission, Permission.camera].request();

    if (perms[permission]!.isGranted && perms[Permission.camera]!.isGranted) {
      print("permission is granted.");
      showAlertDialog();
    } else
    // if (perms[Permission.storage]!.isDenied ||
    //   perms[Permission.camera]!.isDenied)
    {
      String msg = "";
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        print("permission is PermanentlyDenied.");
        msg =
            "Storage, camera Permission is need to get the photo, please enable it in settings";
      } else {
        print("permission is Denied.");
        msg = "Storage, camera Permission is need to get the photo";
      }

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
        duration: Duration(seconds: 1),
      ));
      if (perms[permission]!.isPermanentlyDenied ||
          perms[Permission.camera]!.isPermanentlyDenied) {
        Future.delayed(Duration(milliseconds: 1500), () {
          openAppSettings();
        });
      }
    }
  }

  void showAlertDialog() {
    print("alert");
    AlertDialog alertDialog = AlertDialog(
      backgroundColor: MyColors.BG_COLOR,
      title: Text(
        "Profile Photo",
        style: GoogleFonts.workSans(
          textStyle: TextStyle(
            color: MyColors.WHITE_COLOR,
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        textAlign: TextAlign.center,
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              _getFromGallery();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Gallery",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).pop();
              _getFromCamera();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Camera",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.workSans(
                    textStyle: const TextStyle(
                      color: MyColors.WHITE_COLOR,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return alertDialog;
      },
    );
  }

  _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      groupProfilePhotoBloc.add(GroupProfilePhotoEvent(File(pickedFile.path)));
    }
  }

  _getFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      groupProfilePhotoBloc.add(GroupProfilePhotoEvent(File(pickedFile.path)));
    }
  }
}
