import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/RecruitersJobs/AppliedJobsScreen.dart';
import 'package:e4t/screens/RecruitersJobs/RecruitesJobsScreen.dart';
import 'package:e4t/screens/RecruitersJobs/recruitersJobActivePostsScreen.dart';
import 'package:e4t/screens/RecruitersJobs/recruitersJob_In_Active_PostsScreen.dart';
import 'package:e4t/screens/recruiters/recruitersPostScreen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RecruitersJobPostsTabsScreen extends StatefulWidget {
  RecruitersJobPostsTabsScreen({super.key, required this.dataMap});

  static const RECRUITERS_JOB_POSTS_TABS_ROUTE =
      "RecruitersJobsPostsTabsScreen";

  static const RECRUITERS_ID_KEY = "recruitersIdKey";
  static const RECRUITERS_NAME_KEY = "recruitersNameKey";
  static const RECRUITERS_LOGO_KEY = "recruitersLogoKey";

  Map dataMap;

  @override
  State<RecruitersJobPostsTabsScreen> createState() =>
      _RecruitersJobPostsTabsScreenState();
}

class _RecruitersJobPostsTabsScreenState
    extends State<RecruitersJobPostsTabsScreen> with TickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
          elevation: 0,
          title: Text(
            "${widget.dataMap[RecruitersJobPostsTabsScreen.RECRUITERS_NAME_KEY] ?? "Job"}",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: MyColors.ORANGE_COLOR,
              size: 16,
            ),
          ),
        ),
        body: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: const EdgeInsets.only(
                  top: 4.0,
                ),
                decoration: BoxDecoration(
                  color: MyColors.ORANGE_COLOR,
                  borderRadius: BorderRadius.circular(
                    14.0,
                  ),
                ),
                padding: const EdgeInsets.all(
                  4.0,
                ),
                width: MediaQuery.of(context).size.width,
                child: TabBar(
                  // isScrollable: true,
                  controller: tabController,
                  indicator: BoxDecoration(
                    color: MyColors.WHITE_COLOR,
                    borderRadius: BorderRadius.circular(
                      14,
                    ),
                  ),
                  labelColor: MyColors.ORANGE_COLOR,
                  dividerColor: Colors.black,
                  // ignore: prefer_const_literals_to_create_immutables
                  unselectedLabelColor: MyColors.WHITE_COLOR,
                  tabs: const [
                    Tab(
                      text: "Active Jobs",
                    ),
                    Tab(
                      text: "InActive Jobs",
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: tabController,
                children: [
                  RecruitersJobActivePostsScreen(dataMap: widget.dataMap),
                  RecruitersJobInActivePostsScreen(dataMap: widget.dataMap),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
