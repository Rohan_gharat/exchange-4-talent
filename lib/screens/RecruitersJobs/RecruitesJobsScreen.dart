import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/RecruitersJobsBloc/recruiters_jobs_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/RecruitersJobs/RecruitersJobPostsTabsScreen.dart';
import 'package:e4t/screens/RecruitersJobs/recruitersJobActivePostsScreen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class RecruitersJobsScreen extends StatefulWidget {
  const RecruitersJobsScreen({super.key});

  @override
  State<RecruitersJobsScreen> createState() => _RecruitersJobsScreenState();
}

class _RecruitersJobsScreenState extends State<RecruitersJobsScreen>
    with AutomaticKeepAliveClientMixin {
  var scrollController = ScrollController();
  late RecruitersJobsBloc recruitersJobsBloc;
  static const RECRUITERS_JOBS_API = "getRecruitersWithPostCount";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    recruitersJobsBloc = BlocProvider.of<RecruitersJobsBloc>(context);
    recruitersJobsBloc.url = "${ApisServices.BASE_URL}$RECRUITERS_JOBS_API";
    recruitersJobsBloc.recruitersPostsListAll.clear();
    recruitersJobsBloc.add(RecruitersJobsLoadingEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<RecruitersJobsBloc, RecruitersJobsState>(
        listener: (context, state) {
          if (state is RecruitersJobsErrorState) {
            HelperClass.show_snackbar(context, state.message);
          } else if (state is RecruitersJobsLoadedState) {
             }
        },
        builder: (context, state) {
          if (state is RecruitersJobsLoadingState) {
            return const CircularIndicatorWidget();
          } else if (state is RecruitersJobsErrorState) {
            return CustomErrorWidget(
              on_tap: () {
                recruitersJobsBloc.url =
                    "${ApisServices.BASE_URL}$RECRUITERS_JOBS_API";
                recruitersJobsBloc.add(RecruitersJobsLoadingEvent());
              },
            );
          } else if (state is RecruitersJobsLoadedState) {
            return state.recruitersList.isEmpty
                ? NoDataWidget(
                    on_tap: () {
                      recruitersJobsBloc.url =
                          "${ApisServices.BASE_URL}$RECRUITERS_JOBS_API";
                      recruitersJobsBloc.add(RecruitersJobsLoadingEvent());
                    },
                  )
                : SingleChildScrollView(
                    controller: scrollController
                      ..addListener(() {
                        if (scrollController.position.maxScrollExtent ==
                            scrollController.position.pixels) {
                          if (state.hasMoreData &&
                              !recruitersJobsBloc.isLoading &&
                              recruitersJobsBloc.url != null) {
                            recruitersJobsBloc.isLoading = true;
                            recruitersJobsBloc
                                .add(RecruitersJobsLoadingEvent());
                          }
                        }
                      }),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 20.0,
                        ),
                        GridView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: state.recruitersList.length,
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 20.0,
                            mainAxisSpacing: 20.0,
                            childAspectRatio: 0.7,
                          ),
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(
                                  context,
                                  RecruitersJobPostsTabsScreen
                                      .RECRUITERS_JOB_POSTS_TABS_ROUTE,
                                  arguments: {
                                    RecruitersJobActivePostsScreen.RECRUITERS_ID_KEY:
                                        state.recruitersList[index].id,
                                    RecruitersJobActivePostsScreen
                                            .RECRUITERS_NAME_KEY:
                                        state.recruitersList[index].name,
                                    RecruitersJobActivePostsScreen.RECRUITERS_LOGO_KEY:
                                        state.recruitersList[index].logo,
                                  },
                                );
                              },
                              child: Container(
                               decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    18.0,
                                  ),
                                  color: MyColors.BG_COLOR_TEXT_FILED,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                      child: state.recruitersList[index]
                                                      .logo !=
                                                  null &&
                                              state.recruitersList[index]
                                                      .logo !=
                                                  null &&
                                              state.recruitersList[index].logo
                                                      .toString()
                                                      .trim() !=
                                                  ""
                                          ? CachedNetworkImage(
                                              alignment: Alignment.topCenter,
                                              imageUrl: state
                                                  .recruitersList[index].logo,
                                              height: 150.0,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              fit: BoxFit.cover,
                                              placeholder: (context, url) =>
                                                  Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                height: 150.0,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                              ),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                height: 150.0,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                              ),
                                            )
                                          : Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              height: 150.0,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                            ),
                                    ),
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(
                                        8.0,
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            state.recruitersList[index].name !=
                                                        null &&
                                                    state.recruitersList[index]
                                                            .name !=
                                                        null &&
                                                    state.recruitersList[index]
                                                            .name
                                                            .toString()
                                                            .trim() !=
                                                        ""
                                                ? state
                                                    .recruitersList[index].name
                                                : "NA",

                                            style: GoogleFonts.montserrat(
                                              textStyle: const TextStyle(
                                                  color: MyColors.ORANGE_COLOR,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  overflow:
                                                      TextOverflow.ellipsis),
                                            ),
                                            maxLines: 2,
                                          ),
                                          const SizedBox(
                                            height: 5.0,
                                          ),
                                          state.recruitersList[index]
                                                      .Post_count !=
                                                  null
                                              ? Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    const Icon(
                                                      FontAwesomeIcons
                                                          .briefcase,
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                      size: 20.0,
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    Expanded(
                                                      child: Text(
                                                        "${state.recruitersList[index].Post_count} openings",
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          textStyle:
                                                              const TextStyle(
                                                            color: MyColors
                                                                .WHITE_COLOR,
                                                            fontSize: 16.0,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              : Container(),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                        state.hasMoreData
                            ? const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: CircularIndicatorWidget(),
                              )
                            : SizedBox.shrink()
                      ],
                    ),
                  );
          }
          return Container();
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
