import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/RecruitersJobsInActivePostsBloc/recruiters_jobs_in_active_posts_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class RecruitersJobInActivePostsScreen extends StatefulWidget {
  RecruitersJobInActivePostsScreen({
    super.key,
    required this.dataMap,
  });

  static const RECRUITERS_JOBS_POSTS_SCREEN_ROUTE =
      "RecruitersJobPostsScreenRoute";

  static const RECRUITERS_ID_KEY = "recruitersIdKey";
  static const RECRUITERS_NAME_KEY = "recruitersNameKey";
  static const RECRUITERS_LOGO_KEY = "recruitersLogoKey";

  Map dataMap;

  static const RECRUITERS_JOBS_IN_ACTIVE_POSTS_API =
      "getInActivePostsOfRecruiter";

  @override
  State<RecruitersJobInActivePostsScreen> createState() =>
      _RecruitersJobInActivePostsScreenState();
}

class _RecruitersJobInActivePostsScreenState
    extends State<RecruitersJobInActivePostsScreen>
    with AutomaticKeepAliveClientMixin {
  var scrollController = ScrollController();
  late RecruitersJobsInActivePostsBloc recruitersJobsInActivePostsBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    recruitersJobsInActivePostsBloc =
        BlocProvider.of<RecruitersJobsInActivePostsBloc>(context);

    recruitersJobsInActivePostsBloc.url =
        "${ApisServices.BASE_URL}${RecruitersJobInActivePostsScreen.RECRUITERS_JOBS_IN_ACTIVE_POSTS_API}";
    recruitersJobsInActivePostsBloc.recruitersJobsInActivePostsListAll.clear();
    recruitersJobsInActivePostsBloc.add(RecruitersJobsInActivePostsLoadingEvent(
        widget.dataMap[RecruitersJobInActivePostsScreen.RECRUITERS_ID_KEY]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<RecruitersJobsInActivePostsBloc,
          RecruitersJobsInActivePostsState>(
        listener: (context, state) {
          if (state is RecruitersJobsInActivePostsErrorState) {
            HelperClass.show_snackbar(context, state.message);
          } else if (state is RecruitersJobsInActivePostsLoadedState) {}
        },
        builder: (context, state) {
          if (state is RecruitersJobsInActivePostsLoadingState) {
            return const CircularIndicatorWidget();
          } else if (state is RecruitersJobsInActivePostsErrorState) {
            return CustomErrorWidget(
              on_tap: () {
                recruitersJobsInActivePostsBloc.url =
                    "${ApisServices.BASE_URL}${RecruitersJobInActivePostsScreen.RECRUITERS_JOBS_IN_ACTIVE_POSTS_API}";
                recruitersJobsInActivePostsBloc
                    .recruitersJobsInActivePostsListAll
                    .clear();
                recruitersJobsInActivePostsBloc.add(
                    RecruitersJobsInActivePostsLoadingEvent(widget.dataMap[
                        RecruitersJobInActivePostsScreen.RECRUITERS_ID_KEY]));
              },
            );
          } else if (state is RecruitersJobsInActivePostsLoadedState) {
            return state.recruitersJobsInActivePostsList.isEmpty
                ? NoDataWidget(
                    on_tap: () {
                      recruitersJobsInActivePostsBloc.url =
                          "${ApisServices.BASE_URL}${RecruitersJobInActivePostsScreen.RECRUITERS_JOBS_IN_ACTIVE_POSTS_API}";
                      recruitersJobsInActivePostsBloc
                          .recruitersJobsInActivePostsListAll
                          .clear();
                      recruitersJobsInActivePostsBloc.add(
                          RecruitersJobsInActivePostsLoadingEvent(
                              widget.dataMap[RecruitersJobInActivePostsScreen
                                  .RECRUITERS_ID_KEY]));
                    },
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount: state.hasMoreData
                        ? state.recruitersJobsInActivePostsList.length + 1
                        : state.recruitersJobsInActivePostsList.length,
                    controller: scrollController
                      ..addListener(() {
                        if (scrollController.position.maxScrollExtent ==
                            scrollController.position.pixels) {
                          if (state.hasMoreData &&
                              recruitersJobsInActivePostsBloc.url != null &&
                              !recruitersJobsInActivePostsBloc.isLoading) {
                            recruitersJobsInActivePostsBloc.isLoading = true;
                            recruitersJobsInActivePostsBloc.add(
                                RecruitersJobsInActivePostsLoadingEvent(
                                    widget.dataMap[
                                        RecruitersJobInActivePostsScreen
                                            .RECRUITERS_ID_KEY]));
                          }
                        }
                      }),
                    itemBuilder: (context, index) {
                      return index >=
                              state.recruitersJobsInActivePostsList.length
                          ? const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: CircularIndicatorWidget(),
                            )
                          : GestureDetector(
                              onTap: () {},
                              child: Container(
                                margin: EdgeInsets.only(
                                  top: 8.0,
                                  bottom: index ==
                                          state.recruitersJobsInActivePostsList
                                                  .length -
                                              1
                                      ? 16.0
                                      : 0.0,
                                ),
                                padding: const EdgeInsets.all(
                                  8.0,
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    18.0,
                                  ),
                                  color: MyColors.GREY_COLOR1,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                      child: widget.dataMap[
                                                      RecruitersJobInActivePostsScreen
                                                          .RECRUITERS_LOGO_KEY] !=
                                                  null &&
                                              widget.dataMap[RecruitersJobInActivePostsScreen
                                                          .RECRUITERS_LOGO_KEY]
                                                      .toString()
                                                      .trim() !=
                                                  ""
                                          ? CachedNetworkImage(
                                              alignment: Alignment.topCenter,
                                              imageUrl: widget.dataMap[
                                                  RecruitersJobInActivePostsScreen
                                                      .RECRUITERS_LOGO_KEY],
                                              width: 80.0,
                                              height: 80.0,
                                              fit: BoxFit.cover,
                                              placeholder: (context, url) =>
                                                  Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                height: 80.0,
                                                width: 80,
                                              ),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                height: 80.0,
                                                width: 80,
                                              ),
                                            )
                                          : Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              height: 80.0,
                                              width: 80,
                                            ),
                                    ),
                                    const SizedBox(
                                      width: 10.0,
                                    ),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${state.recruitersJobsInActivePostsList[index].title}",
                                            style: GoogleFonts.montserrat(
                                              textStyle: const TextStyle(
                                                color: MyColors.BLACK_COLOR,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 5.0,
                                          ),
                                          state
                                                          .recruitersJobsInActivePostsList[
                                                              index]
                                                          .location !=
                                                      null &&
                                                  state
                                                          .recruitersJobsInActivePostsList[
                                                              index]
                                                          .location
                                                          .toString()
                                                          .trim() !=
                                                      ""
                                              ? Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    const Icon(
                                                      Icons.location_on,
                                                      color:
                                                          MyColors.BLACK_COLOR,
                                                      size: 20.0,
                                                    ),
                                                    const SizedBox(
                                                      width: 2.0,
                                                    ),
                                                    Expanded(
                                                      child: Text(
                                                        "${state.recruitersJobsInActivePostsList[index].location}",
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          textStyle:
                                                              const TextStyle(
                                                            color: MyColors
                                                                .BLACK_COLOR,
                                                            fontSize: 14.0,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              : Container(),
                                          state
                                                          .recruitersJobsInActivePostsList[
                                                              index]
                                                          .end_date !=
                                                      null &&
                                                  state
                                                          .recruitersJobsInActivePostsList[
                                                              index]
                                                          .end_date
                                                          .toString()
                                                          .trim() !=
                                                      "" &&
                                                  HelperClass.format_recruiter_post_date_dd_mm_yyyy(
                                                          state
                                                              .recruitersJobsInActivePostsList[
                                                                  index]
                                                              .end_date) !=
                                                      null &&
                                                  HelperClass.daysBetween(
                                                          DateTime.now(),
                                                          DateFormat(HelperClass
                                                                  .DATE_TIME_STAMP_FORMAT)
                                                              .parse(state
                                                                  .recruitersJobsInActivePostsList[
                                                                      index]
                                                                  .end_date)) >
                                                      0
                                              ? Text(
                                                  // "Campaign End Date: ${HelperClass.format_recruiter_post_date_dd_mm_yyyy(state.recruitersJobsInActivePostsList[index].end_date)}",
                                                  "${HelperClass.daysBetween(DateTime.now(), DateFormat(HelperClass.DATE_TIME_STAMP_FORMAT).parse(state.recruitersJobsInActivePostsList[index].end_date))} days left for apply",
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: const TextStyle(
                                                        color: MyColors
                                                            .BLACK_COLOR,
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                )
                                              : Container(),
                                          state
                                                          .recruitersJobsInActivePostsList[
                                                              index]
                                                          .audition_date !=
                                                      null &&
                                                  state
                                                          .recruitersJobsInActivePostsList[
                                                              index]
                                                          .audition_date
                                                          .toString()
                                                          .trim() !=
                                                      "" &&
                                                  HelperClass.format_recruiter_post_date_dd_mm_yyyy(
                                                          state
                                                              .recruitersJobsInActivePostsList[
                                                                  index]
                                                              .audition_date) !=
                                                      null
                                              ? Text(
                                                  "Audition Date: ${HelperClass.format_recruiter_post_date_dd_mm_yyyy(state.recruitersJobsInActivePostsList[index].audition_date)}",
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: const TextStyle(
                                                      color:
                                                          MyColors.BLACK_COLOR,
                                                      fontSize: 16.0,
                                                    ),
                                                  ),
                                                )
                                              : Container(),
                                          state.recruitersJobsInActivePostsList[index]
                                                      .jobStatus >
                                                  0
                                              ? Container(
                                                  margin: const EdgeInsets.only(
                                                    top: 4.0,
                                                  ),
                                                  padding: const EdgeInsets.all(
                                                    4.0,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      16.0,
                                                    ),
                                                    color: MyColors.BLACK_COLOR,
                                                  ),
                                                  child: Text(
                                                    state
                                                                .recruitersJobsInActivePostsList[
                                                                    index]
                                                                .jobStatus ==
                                                            1
                                                        ? HelperClass
                                                            .APPLIED_JOB_key
                                                        : state
                                                                    .recruitersJobsInActivePostsList[
                                                                        index]
                                                                    .jobStatus ==
                                                                2
                                                            ? HelperClass
                                                                .SHORTELISTED_JOB_key
                                                            : state
                                                                        .recruitersJobsInActivePostsList[
                                                                            index]
                                                                        .jobStatus ==
                                                                    3
                                                                ? HelperClass
                                                                    .REJECTED_JOB_key
                                                                : state.recruitersJobsInActivePostsList[index].jobStatus ==
                                                                        4
                                                                    ? HelperClass
                                                                        .HIRED_JOB_key
                                                                    : "NA",
                                                    style:
                                                        GoogleFonts.montserrat(
                                                      textStyle:
                                                          const TextStyle(
                                                        color: MyColors
                                                            .WHITE_COLOR,
                                                        fontSize: 14.0,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              : const SizedBox.shrink(),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                    },
                  );
          }
          return Container();
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
