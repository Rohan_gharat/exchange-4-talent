import 'package:cached_network_image/cached_network_image.dart';
import 'package:e4t/blocks/RecruitersBloc/recruiters_bloc.dart';
import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/restService/apis_services.dart';
import 'package:e4t/screens/recruiters/recruitersPostDetailsScreen.dart';
import 'package:e4t/screens/widgets/circular_indicator_widget.dart';
import 'package:e4t/screens/widgets/error_widget.dart';
import 'package:e4t/screens/widgets/no_data_widget.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class AppliedJobsScreen extends StatefulWidget {
  const AppliedJobsScreen({super.key});
  static const APPLIED_JOBS_API = "appliedJobs";
  @override
  State<AppliedJobsScreen> createState() => _AppliedJobsScreenState();
}

class _AppliedJobsScreenState extends State<AppliedJobsScreen>
    with AutomaticKeepAliveClientMixin {
  var scrollController = ScrollController();
  var recruitersBloc;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    recruitersBloc = BlocProvider.of<RecruitersBloc>(context);

    recruitersBloc.url = "${ApisServices.BASE_URL}${AppliedJobsScreen.APPLIED_JOBS_API}";
    recruitersBloc.recruitersPostsListAll.clear();
    recruitersBloc.add(RecruitersLoadingEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<RecruitersBloc, RecruitersState>(
        listener: (context, state) {
          if (state is RecruitersErrorState) {
            HelperClass.show_snackbar(context, state.message);
          }
        },
        builder: (context, state) {
          if (state is RecruitersLoadingState) {
            return const CircularIndicatorWidget();
          } else if (state is RecruitersErrorState) {
            return CustomErrorWidget(
              on_tap: () {
                recruitersBloc.url =
                    "${ApisServices.BASE_URL}${AppliedJobsScreen.APPLIED_JOBS_API}";
                recruitersBloc.recruitersPostsListAll.clear();
                recruitersBloc.add(RecruitersLoadingEvent());
              },
            );
          } else if (state is RecruitersLoadedState) {
            return state.recruitersPostsList.isEmpty
                ? NoDataWidget(
                    on_tap: () {
                      recruitersBloc.url =
                          "${ApisServices.BASE_URL}${AppliedJobsScreen.APPLIED_JOBS_API}";
                      recruitersBloc.add(RecruitersLoadingEvent());
                    },
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount: state.hasMoreData
                        ? state.recruitersPostsList.length + 1
                        : state.recruitersPostsList.length,
                    controller: scrollController
                      ..addListener(() {
                        if (scrollController.position.maxScrollExtent ==
                            scrollController.position.pixels) {
                          if (state.hasMoreData &&
                              recruitersBloc.url != null &&
                              !recruitersBloc.isLoading) {
                            recruitersBloc.isLoading = true;
                            recruitersBloc.add(RecruitersLoadingEvent());
                          }
                        }
                      }),
                    itemBuilder: (context, index) {

                      return index >= state.recruitersPostsList.length
                          ? const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: CircularIndicatorWidget(),
                            )
                          : GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(
                                  context,
                                  RecruitersPostDetailsScreen
                                      .RECRUITERS_POST_DETAILS_SCREEN_ROUTE,
                                  arguments:
                                      state.recruitersPostsList[index].id,
                                );
                              },
                              child: Container(
                                margin: EdgeInsets.only(
                                  top: 8.0,
                                  bottom: index ==
                                          state.recruitersPostsList.length - 1
                                      ? 16.0
                                      : 0.0,
                                ),
                                padding: EdgeInsets.all(
                                  8.0,
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    18.0,
                                  ),
                                  color: MyColors.BG_COLOR_TEXT_FILED,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(
                                        16.0,
                                      ),
                                      child: state.recruitersPostsList[index]
                                                      .recruitersModel !=
                                                  null &&
                                              state.recruitersPostsList[index]
                                                      .recruitersModel!.logo !=
                                                  null &&
                                              state.recruitersPostsList[index]
                                                      .recruitersModel!.logo
                                                      .toString()
                                                      .trim() !=
                                                  ""
                                          ? CachedNetworkImage(
                                              alignment: Alignment.topCenter,
                                              imageUrl: state
                                                  .recruitersPostsList[index]
                                                  .recruitersModel!
                                                  .logo,
                                              width: 80.0,
                                              height: 80.0,
                                              fit: BoxFit.cover,
                                              placeholder: (context, url) =>
                                                  Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                height: 80.0,
                                                width: 80,
                                              ),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Image.asset(
                                                HelperClass.APP_LOGO_PATH,
                                                height: 80.0,
                                                width: 80,
                                              ),
                                            )
                                          : Image.asset(
                                              HelperClass.APP_LOGO_PATH,
                                              height: 80.0,
                                              width: 80,
                                            ),
                                    ),
                                    const SizedBox(
                                      width: 10.0,
                                    ),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          state.recruitersPostsList[index]
                                                          .title !=
                                                      null &&
                                                  state
                                                          .recruitersPostsList[
                                                              index]
                                                          .title !=
                                                      null &&
                                                  state
                                                          .recruitersPostsList[
                                                              index]
                                                          .title
                                                          .toString()
                                                          .trim() !=
                                                      ""
                                              ? Text(
                                                  state
                                                      .recruitersPostsList[
                                                          index]
                                                      .title,
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: const TextStyle(
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                      fontSize: 16.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                )
                                              : const SizedBox.shrink(),
                                          state.recruitersPostsList[index]
                                                          .recruitersModel !=
                                                      null &&
                                                  state
                                                          .recruitersPostsList[
                                                              index]
                                                          .recruitersModel!
                                                          .name !=
                                                      null &&
                                                  state
                                                          .recruitersPostsList[
                                                              index]
                                                          .recruitersModel!
                                                          .name
                                                          .toString()
                                                          .trim() !=
                                                      ""
                                              ? Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    const Icon(
                                                      FontAwesomeIcons.building,
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                      size: 20.0,
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    Text(
                                                      state
                                                          .recruitersPostsList[
                                                              index]
                                                          .recruitersModel!
                                                          .name,
                                                      style: GoogleFonts
                                                          .montserrat(
                                                        textStyle:
                                                            const TextStyle(
                                                          color: MyColors
                                                              .ORANGE_COLOR,
                                                          fontSize: 16.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              : const SizedBox.shrink(),
                                          SizedBox(
                                            height: 5.0,
                                          ),
                                          state.recruitersPostsList[index]
                                                          .location !=
                                                      null &&
                                                  state
                                                          .recruitersPostsList[
                                                              index]
                                                          .location
                                                          .toString()
                                                          .trim() !=
                                                      ""
                                              ? Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    const Icon(
                                                      Icons.location_on,
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                      size: 20.0,
                                                    ),
                                                    const SizedBox(
                                                      width: 2.0,
                                                    ),
                                                    Expanded(
                                                      child: Text(
                                                        "${state.recruitersPostsList[index].location}",
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          textStyle: TextStyle(
                                                            color: MyColors
                                                                .WHITE_COLOR,
                                                            fontSize: 14.0,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              : Container(),
                                          state.recruitersPostsList[index]
                                                          .end_date !=
                                                      null &&
                                                  state
                                                          .recruitersPostsList[
                                                              index]
                                                          .end_date
                                                          .toString()
                                                          .trim() !=
                                                      "" &&
                                                  HelperClass.format_recruiter_post_date_dd_mm_yyyy(
                                                          state
                                                              .recruitersPostsList[
                                                                  index]
                                                              .end_date) !=
                                                      null &&
                                                  HelperClass.daysBetween(
                                                          DateTime.now(),
                                                          DateFormat(HelperClass
                                                                  .DATE_TIME_STAMP_FORMAT)
                                                              .parse(state
                                                                  .recruitersPostsList[
                                                                      index]
                                                                  .end_date)) >
                                                      0
                                              ? Text(
                                                  // "Campaign End Date: ${HelperClass.format_recruiter_post_date_dd_mm_yyyy(state.recruitersPostsList[index].end_date)}",
                                                  "${HelperClass.daysBetween(DateTime.now(), DateFormat(HelperClass.DATE_TIME_STAMP_FORMAT).parse(state.recruitersPostsList[index].end_date))} days left for apply",
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: TextStyle(
                                                        color: MyColors
                                                            .WHITE_COLOR,
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                )
                                              : Container(),
                                          state.recruitersPostsList[index]
                                                          .audition_date !=
                                                      null &&
                                                  state
                                                          .recruitersPostsList[
                                                              index]
                                                          .audition_date
                                                          .toString()
                                                          .trim() !=
                                                      "" &&
                                                  HelperClass.format_recruiter_post_date_dd_mm_yyyy(
                                                          state
                                                              .recruitersPostsList[
                                                                  index]
                                                              .audition_date) !=
                                                      null
                                              ? Text(
                                                  "Audition Date: ${HelperClass.format_recruiter_post_date_dd_mm_yyyy(state.recruitersPostsList[index].audition_date)}",
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: TextStyle(
                                                      color:
                                                          MyColors.ORANGE_COLOR,
                                                      fontSize: 16.0,
                                                    ),
                                                  ),
                                                )
                                              : Container(),
                                          state.recruitersPostsList[index]
                                                      .jobStatus >
                                                  0
                                              ? Container(
                                                  margin: const EdgeInsets.only(
                                                    top: 4.0,
                                                  ),
                                                  padding: const EdgeInsets.all(
                                                    4.0,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      16.0,
                                                    ),
                                                    color:
                                                        MyColors.ORANGE_COLOR,
                                                  ),
                                                  child: Text(
                                                    state
                                                                .recruitersPostsList[
                                                                    index]
                                                                .jobStatus ==
                                                            1
                                                        ? HelperClass
                                                            .APPLIED_JOB_key
                                                        : state
                                                                    .recruitersPostsList[
                                                                        index]
                                                                    .jobStatus ==
                                                                2
                                                            ? HelperClass
                                                                .SHORTELISTED_JOB_key
                                                            : state
                                                                        .recruitersPostsList[
                                                                            index]
                                                                        .jobStatus ==
                                                                    3
                                                                ? HelperClass
                                                                    .REJECTED_JOB_key
                                                                : state.recruitersPostsList[index].jobStatus ==
                                                                        4
                                                                    ? HelperClass
                                                                        .HIRED_JOB_key
                                                                    : "NA",
                                                    style:
                                                        GoogleFonts.montserrat(
                                                      textStyle: const TextStyle(
                                                        color: MyColors
                                                            .WHITE_COLOR,
                                                        fontSize: 14.0,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              : const SizedBox.shrink(),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                    },
                  );
          }
          return Container();
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
