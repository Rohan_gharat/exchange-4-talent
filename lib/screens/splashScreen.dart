import 'package:e4t/myColors/myColors.dart';
import 'package:e4t/screens/auth/loginScreen.dart';
import 'package:e4t/screens/main_navigator_screen.dart';
import 'package:e4t/screens/profile/UserInitialDetailsScreen.dart';
import 'package:e4t/utils/sharePrefs.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 1), () {
      var userDataCheck =
          SharedPrefs.getBool(SharedPrefs.isUserDataCompletedKey) ?? false;
      var otpCompleteCheck =
          SharedPrefs.getBool(SharedPrefs.isOtpCompletedKey) ?? false;
     if (userDataCheck) {
        /// user entered data

        Navigator.pushNamedAndRemoveUntil(
            context,
            MainNavigationScreen.MAIN_NAVIGATION_SCREEN_ROUTE,
            (route) => false);
      } else if (otpCompleteCheck) {
        /// otp completed

        Navigator.pushNamedAndRemoveUntil(
          context,
          UserInitialDetailsScreen.USER_INITIAL_DETAILS_ROUTE,
          (route) => false,
        );
      } else {
        Navigator.pushNamedAndRemoveUntil(
          context,
          LoginScreen.LOGIN_ROUTE,
          (route) => false,
        );
      }

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.BG_COLOR_TEXT_FILED,
      body: Center(
        child: Image.asset(
          "images/splash_logo.png",
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
