import 'package:e4t/screens/Accounts/accounts_screen.dart';
import 'package:e4t/screens/RecruitersJobs/RecruitersJobPostsTabsScreen.dart';
import 'package:e4t/screens/RecruitersJobs/RecruitersTabsScreen.dart';
import 'package:e4t/screens/TradeBarter/tradeBarterScreen.dart';
import 'package:e4t/screens/TradeBarter/trade_barter_details_screen.dart';
import 'package:e4t/screens/ads/adsScreen.dart';
import 'package:e4t/screens/auth/loginScreen.dart';
import 'package:e4t/screens/auth/otpScreen.dart';
import 'package:e4t/screens/category/all_categories_screen.dart';
import 'package:e4t/screens/category/all_users_by_categories_screen.dart';
import 'package:e4t/screens/category/all_users_screen.dart';
import 'package:e4t/screens/category/subcategories_screen.dart';
import 'package:e4t/screens/category/usersByCategories_screen.dart';
import 'package:e4t/screens/chat/chatDetailsScreen.dart';
import 'package:e4t/screens/chat/chat_screen.dart';
import 'package:e4t/screens/favourite/favourite_screen.dart';
import 'package:e4t/screens/feedbackForm/feedbackForm.dart';
import 'package:e4t/screens/home/homeScreenV1.dart';
import 'package:e4t/screens/main_navigator_screen.dart';
import 'package:e4t/screens/profile/UserInitialDetailsScreen.dart';
import 'package:e4t/screens/profile/edit_profile_screen.dart';
import 'package:e4t/screens/profile/user_details_screen.dart';
import 'package:e4t/screens/recruiters/recruitersPostDetailsScreen.dart';
import 'package:e4t/screens/recruiters/recruitersPostScreen.dart';
import 'package:e4t/screens/reviews/reviews_screen.dart';
import 'package:e4t/screens/search/search_screen.dart';
import 'package:e4t/screens/subscriptions/add_subscription_screen.dart';
import 'package:e4t/screens/subscriptions/checkout_screen.dart';
import 'package:e4t/screens/subscriptions/subsriptions_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'myColors/myColors.dart';

class AppRoutes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (ctx) {
      switch (settings.name) {
        case LoginScreen.LOGIN_ROUTE:
          return LoginScreen();

        case UserInitialDetailsScreen.USER_INITIAL_DETAILS_ROUTE:
          return UserInitialDetailsScreen();

        case OTPScreen.otpRoute:
          return OTPScreen();

        case MainNavigationScreen.MAIN_NAVIGATION_SCREEN_ROUTE:
          return MainNavigationScreen();

        case AdsScreen.ADS_SCREEN_ROUTE:
          var data = true;
          if (settings.arguments != null) {
            data = settings.arguments as bool;
          }

          return AdsScreen(
            isShowLoginButton: data,
          );

        case CheckOutScreen.CheckoutRoute:
          return CheckOutScreen();

        case HomeScreenV1.HomeRouteV1:
          return HomeScreenV1();

        case SearchScreen.SearchRoute:
          return SearchScreen();

        case ChatScreen.CHAT_ROUTE:
          return ChatScreen();

        case SubCategoriesScreen.SubCategoriesRoute:
          var data = settings.arguments as Map;
          print("SubCategoriesRoute : data: $data");
          return SubCategoriesScreen(
            mapData: data,
          );

        case UsersByCategoriesScreen.USER_BY_CATEGORY_SCREEN_Route:
          var data = settings.arguments as Map;
          print("USER_BY_CATEGORY_SCREEN_Route : data: $data");
          return UsersByCategoriesScreen(
            mapData: data,
          );

        case UserDetailsScreen.USER_DETAILS_ROUTE:
          var data = settings.arguments as Map;
          print("USER_DETAILS_ROUTE : data: $data");
          return UserDetailsScreen(
            mapData: data,
          );

        case TradeBarterDetailsScreen.TRADE_BARTER_DETAILS_ROUTE:
          var data = settings.arguments as Map;
          print("TRADE_BARTER_DETAILS_ROUTE : data: $data");
          return TradeBarterDetailsScreen(
            mapData: data,
          );

        case AllUsersScreen.All_USERS_ROUTE:
          var data = settings.arguments as Map;
          print("All_USERS_ROUTE : data: $data");
          return AllUsersScreen(
            mapData: data,
          );

        case AccountsScreen.ACCOUNTS_ROUTE:
          return AccountsScreen();

        case FavouriteScreen.FAVOURITE_ROUTE:
          return FavouriteScreen();

        case TradeBarterScreen.TRADE_BARTER_SCREEN_ROUTE:
          return TradeBarterScreen();

        case AllCategoriesScreen.ALL_CATEGORIES_ROUTE:
          return AllCategoriesScreen();

        // case AllSubCategoriesScreen.ALL_SUB_CATEGORIES_ROUTE:
        //   var data = settings.arguments as Map;
        //   print("ALL_SUB_CATEGORIES_ROUTE : data: $data");
        //   return AllSubCategoriesScreen(
        //     mapData: data,
        //   );

        case AllUsersByCategoriesScreen.ALL_USERS_BY_CATEGORIES_ROUTE:
          var data = settings.arguments as Map;
          print("ALL_USERS_BY_CATEGORIES_ROUTE : data: $data");
          return AllUsersByCategoriesScreen(
            mapData: data,
          );

        case ReviewsScreen.ALL_REVIEWS_ROUTE:
          var talentId = settings.arguments as int;
          return ReviewsScreen(
            talentId: talentId,
          );

        case SubscriptionsScreen.SUBSCRIPTIONS_ROUTE:
          return SubscriptionsScreen();

        case EditProfileScreen.EDIT_PROFILE_ROUT:
          var popScreenCheck = false;
          if (settings.arguments != null) {
            popScreenCheck = settings.arguments as bool;
          }
          return EditProfileScreen(
            popScreenCheck: popScreenCheck,
          );

        case AddSubscriptionsScreen.ADD_SUBSCRIPTIONS_SCREEN:
          var showSKip = false;
          var subscriptionId = 0;
          if (settings.arguments != null) {
            var data = settings.arguments as Map;
            showSKip = data[AddSubscriptionsScreen.SHOW_SKIP_KEY];
            subscriptionId =
                data[AddSubscriptionsScreen.CURRENT_SUBSCRIPTION_key];
          }
          return AddSubscriptionsScreen(
            showSkip: showSKip,
            currentSubscriptionId: subscriptionId,
          );

        case FeedbackFormScreen.FEEDBACK_FORM_SCREEN_ROUTE:
          return FeedbackFormScreen();

        case RecruitersPostsScreen.RECRUITERS_POST_SCREEN_ROUTE:
          return RecruitersPostsScreen(
            isApplied: settings.arguments as bool,
          );

        case RecruitersPostDetailsScreen.RECRUITERS_POST_DETAILS_SCREEN_ROUTE:
          return RecruitersPostDetailsScreen(
            postId: settings.arguments as int,
          );

        case RecruitersTabsScreen.RECRUITERS_TABS_ROUTE:
          return RecruitersTabsScreen();

        case RecruitersJobPostsTabsScreen.RECRUITERS_JOB_POSTS_TABS_ROUTE:
          return RecruitersJobPostsTabsScreen(
            dataMap: settings.arguments as Map,
          );

        case ChatDetailsScreen.CHAT_DETAILS_SCREEN_ROUTE:
          Map data = settings.arguments as Map;

          return ChatDetailsScreen(
            chat_receiver_uid:
                data[ChatDetailsScreen.CHAT_DETAILS_CHAT_RECEIVER_ID_KEY],
            chatName: data[ChatDetailsScreen.CHAT_DETAILS_NAME_KEY],
            myUserId: data[ChatDetailsScreen.CHAT_DETAILS_MY_USER_ID_KEY],
          );
      }
      return Scaffold(
        body: Center(
          child: Text(
            "route",
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                color: MyColors.WHITE_COLOR,
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      );
    });

  }
}
