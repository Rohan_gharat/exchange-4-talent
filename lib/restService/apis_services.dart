import 'dart:convert';

import 'package:e4t/model/AdsModel/adsModel.dart';
import 'package:e4t/model/ReviewsModel/addReviewModel.dart';
import 'package:e4t/model/ReviewsModel/reviewsModel.dart';
import 'package:e4t/model/TradeBarterModel/tradeBarterModel.dart';
import 'package:e4t/model/authModel/login_model.dart';
import 'package:e4t/model/authModel/otpModel.dart';
import 'package:e4t/model/categoriesModel/categoriesModel.dart';
import 'package:e4t/model/categoriesModel/subcategory_by_category_model.dart';
import 'package:e4t/model/categoriesModel/users_by_category_model.dart';
import 'package:e4t/model/favouriteModel/update_favourite_model.dart';
import 'package:e4t/model/feedbackModel/feedbackModel.dart';
import 'package:e4t/model/homeModel/home_model.dart';
import 'package:e4t/model/recruitersJobsModel/recruiters_jobs_model.dart';
import 'package:e4t/model/recruitersModel/ApplyJobModel.dart';
import 'package:e4t/model/recruitersModel/recruitersPostByIdModel.dart';
import 'package:e4t/model/recruitersModel/recruitersPostModel.dart';
import 'package:e4t/model/subscriptionsModel/coupon_code_model.dart';
import 'package:e4t/model/subscriptionsModel/paymentCheckModel.dart';
import 'package:e4t/model/subscriptionsModel/razorPayCredentialsModel.dart';
import 'package:e4t/model/subscriptionsModel/subcriptions_model.dart';
import 'package:e4t/model/subscriptionsModel/subscriptionHistoryModel.dart';
import 'package:e4t/model/userModel/updateUserPhotoModel.dart';
import 'package:e4t/model/userModel/uploadImageModel.dart';
import 'package:e4t/model/userModel/user_data_model.dart';
import 'package:e4t/model/userModel/user_delete_model.dart';
import 'package:e4t/utils/helper_class.dart';
import 'package:http/http.dart' as http;

import '../model/addressModel/addressModel.dart';
import '../model/hireTalentModel/hireTalentModel.dart';
import '../model/subscriptionsModel/verifyCouponModel.dart';
import '../model/userModel/user_model.dart';

class ApisServices {
  static const BASE_URL =
      "https://www.exchange4talentglobal.com/tms/public/api/";

  static Future<LoginModel?> login(
      {required phoneNo, required phoneCode}) async {
    var url = "${BASE_URL}v3/login";
    Map body = {
      "PhoneNo": phoneNo,
      "phone_code": phoneCode,
    };
    var response = await http.post(
      Uri.parse(url),
      body: body,
    );
    if (response.statusCode == 200) {
      var model = LoginModel.fromJson(jsonDecode(response.body));
      if (model.status == 200) {
        return model;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  static Future<OTPModel?> otp(
      {required phoneNo, required otp, required phoneCode}) async {
    var url = "${BASE_URL}v3/otp";
    Map body = {
      "PhoneNo": phoneNo,
      "mobile_otp": otp,
      "phone_code": phoneCode,
    };
    var response = await http.post(
      Uri.parse(url),
      body: body,
    );

    print(
        "otp response: ${response.body} :: body: ${body} :: code: ${response.statusCode}");
    if (response.statusCode == 200) {
      var model = OTPModel.fromJson(jsonDecode(response.body));
      return model;
    } else {
      return null;
    }
  }

  static Future<List<CategoriesModel>?> getCategories({required token}) async {
    var url = "${BASE_URL}categories";
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': token,
    });
    if (response.statusCode == 200) {
      var jsonString = jsonDecode(response.body);

      return List<CategoriesModel>.from(
              jsonString["categories"].map((x) => CategoriesModel.fromJson(x)))
          .toList();
    } else {
      return null;
    }
  }

  static Future<UserDataModel?> updateTradeBarterData(
      {required token, required tradeBarterValue, required userId}) async {
    var url = "${BASE_URL}v2/updateusers/$userId/update";
    var request = http.MultipartRequest("POST", Uri.parse(url));
    request.headers["Content-Type"] = 'application/json; charset=UTF-8';
    request.headers["Accept"] = 'application/json';
    request.headers["Authorization"] = token;

    request.fields[HelperClass.TRADE_BARTER_KEY] = "$tradeBarterValue";

    var response = await request.send();
    var streamRes = await http.Response.fromStream(response);

    if (streamRes.statusCode == 200) {
      return UserDataModel.fromJson(jsonDecode(streamRes.body));
    } else {
      return null;
    }
  }

  static Future<UpdateUserPhotoModel?> updateProfilePhoto(
      {required token, required String file_path}) async {
    var url = "${BASE_URL}addImage";
    var request = http.MultipartRequest("POST", Uri.parse(url));
    request.headers["Content-Type"] = 'application/json; charset=UTF-8';
    request.headers["Accept"] = 'application/json';
    request.headers["Authorization"] = token;
    http.MultipartFile multipartFile =
        await http.MultipartFile.fromPath("avatar", file_path);
    request.files.add(multipartFile);

    var response = await request.send();
    var streamRes = await http.Response.fromStream(response);

    if (streamRes.statusCode == 200) {
      return UpdateUserPhotoModel.fromJson(jsonDecode(streamRes.body));
    } else {
      return null;
    }
  }

  static Future<UploadImageModel?> uploadPhoto(
      {required token, required String file_path}) async {
    var url = "${BASE_URL}v1/uploadImages";
    var request = http.MultipartRequest("POST", Uri.parse(url));
    request.headers["Content-Type"] = 'application/json; charset=UTF-8';
    request.headers["Accept"] = 'application/json';
    request.headers["Authorization"] = token;
    http.MultipartFile multipartFile =
        await http.MultipartFile.fromPath("image", file_path);
    request.files.add(multipartFile);

    var response = await request.send();
    var streamRes = await http.Response.fromStream(response);

    if (streamRes.statusCode == 200) {
      return UploadImageModel.fromJson(jsonDecode(streamRes.body));
    } else {
      return null;
    }
  }

  static Future<UploadImageModel?> uploadVideo(
      {required token, required String file_path}) async {
    var url = "${BASE_URL}v1/uploadVideos";
    var request = http.MultipartRequest("POST", Uri.parse(url));
    request.headers["Content-Type"] = 'application/json; charset=UTF-8';
    request.headers["Accept"] = 'application/json';
    request.headers["Authorization"] = token;
    http.MultipartFile multipartFile =
        await http.MultipartFile.fromPath("video", file_path);
    request.files.add(multipartFile);

    var response = await request.send();
    var streamRes = await http.Response.fromStream(response);

    if (streamRes.statusCode == 200) {
      return UploadImageModel.fromJson(jsonDecode(streamRes.body));
    } else {
      return null;
    }
  }

  static Future<UserDataModel?> getUserDataById(
      {required int userId, required String token}) async {
    var url = "${BASE_URL}v1/getSingleUser";
    Map data = {
      "user_id": userId,
    };

    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
      body: jsonEncode(data),
    );

    if (response.statusCode == 200) {
      var model = UserDataModel.fromJson(jsonDecode(response.body));
      return model;
    } else {
      return null;
    }
  }

  static Future<List<SubscriptionsModel>?> getSubscriptions(
      {required token, required subscription_id}) async {
    var url = "${BASE_URL}v1/subscriptions";
    Map body = {
      "subscription_id": subscription_id,
    };
    var response = await http.post(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body));
    if (response.statusCode == 200) {
      var jsonString = jsonDecode(response.body);
      return List<SubscriptionsModel>.from(jsonString["subscriptions"]
          .map((x) => SubscriptionsModel.fromJson(x))).toList();
    } else {
      return null;
    }
  }

  static Future<GetTradeBarterModel?> getAllUsersPagination({
    required token,
    required url,
    required isAllUsers,
    subscriptionId,
  }) async {
    try {
      var response;
      if (!isAllUsers) {
        Map body = {
          "subscription_id": subscriptionId,
        };

        response = await http.post(
          Uri.parse(url),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': token,
          },
          body: jsonEncode(body),
        );
      } else {
        response = await http.get(Uri.parse(url), headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        });
      }

      if (response.statusCode == 200) {
        return GetTradeBarterModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<HomeModel?> getHomeData({required token}) async {
    var url = "${BASE_URL}v1/home";
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': token,
    });

    if (response.statusCode == 200) {
      var jsonString = jsonDecode(response.body);
      if (jsonString["status"] == 200) {
        return HomeModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  static Future<SubcategoryByCategoryModel?> getSubCategoriesByCategory(
      {required token, required categoryId}) async {
    var url = "${BASE_URL}subcategoryBycategories/$categoryId";
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': token,
    });
    if (response.statusCode == 200) {
      return SubcategoryByCategoryModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<UsersByCategoryModel?> getUsersByCategory(
      {required token, required categoryId}) async {
    var url = "${BASE_URL}v1/getSubcategoriesByCategory1";
    Map body = {
      "category_id": categoryId,
    };
    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
      body: jsonEncode(body),
    );
    if (response.statusCode == 200) {
      return UsersByCategoryModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<List<UserModel>?> getFavourites({required token}) async {
    var url = "${BASE_URL}favourites";
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': token,
    });
    if (response.statusCode == 200) {
      var jsonString = jsonDecode(response.body);
      if (jsonString["status"] == 200) {
        return List<UserModel>.from(
            jsonString["data"].map((x) => UserModel.fromJson(x))).toList();
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  static Future<UpdateFavouriteModel?> updateFavouriteStatus(
      {required token, talent_id}) async {
    var url = "${BASE_URL}addfavourites";
    Map body = {
      "talent_id": talent_id,
    };
    try {
      var response = await http.post(Uri.parse(url),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': token,
          },
          body: jsonEncode(body));

      if (response.statusCode == 200) {
        return UpdateFavouriteModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<GetTradeBarterModel?> searchUserPagination({
    required token,
    required url,
    required searchText,
  }) async {
    try {
      Map body = {
        "search": searchText,
      };

      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        return GetTradeBarterModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<GetTradeBarterModel?> getAllUsersByCategory({
    required token,
    required url,
    required category_id,
  }) async {
    try {
      Map body = {
        "category_id": category_id,
      };

      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        return GetTradeBarterModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<List<AddressModel>?> getCountriesList() async {
    var url = "${BASE_URL}countries";
    var response = await http.post(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });

    if (response.statusCode == 200) {
      var jsonString = jsonDecode(response.body);
      return List<AddressModel>.from(
        jsonString["countries"].map(
          (x) => AddressModel.fromJson(x),
        ),
      )
          .where(
            (element) =>
                element.id != null &&
                element.name != null &&
                element.name?.trim() != "",
          )
          .toList();
    } else {
      return null;
    }
  }

  static Future<List<AddressModel>?> getStatesList(
      {required token, required countyId}) async {
    var url = "${BASE_URL}states/$countyId";
    var response = await http.post(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': token,
    });

    if (response.statusCode == 200) {
      var jsonString = jsonDecode(response.body);
      return List<AddressModel>.from(
              jsonString["states"].map((x) => AddressModel.fromJson(x)))
          .where(
            (element) =>
                element.id != null &&
                element.name != null &&
                element.name?.trim() != "",
          )
          .toList();
    } else {
      return null;
    }
  }

  static Future<List<AddressModel>?> getCitiesList(
      {required token, required stateId}) async {
    var url = "${BASE_URL}cities/$stateId";
    var response = await http.post(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': token,
    });

    if (response.statusCode == 200) {
      var jsonString = jsonDecode(response.body);
      return List<AddressModel>.from(
              jsonString["cities"].map((x) => AddressModel.fromJson(x)))
          .where(
            (element) =>
                element.id != null &&
                element.name != null &&
                element.name?.trim() != "",
          )
          .toList();
    } else {
      return null;
    }
  }

  static Future<VerifyCouponModel?> verifyCoupon(
      {required token,
      required int subscription_id,
      required String coupon_code}) async {
    var url = "${BASE_URL}verify_coupne";
    Map body = {
      "subscription_id": subscription_id,
      "coupon_code": coupon_code,
    };

    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
      body: jsonEncode(body),
    );

    if (response.statusCode == 200) {
      return VerifyCouponModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<VerifyCouponModel?> checkout({
    required token,
    required int subscription_id,
    required String coupon_code,
    required int price,
  }) async {
    var url = "${BASE_URL}v1/checkout";
    Map body = {
      "subscription_id": subscription_id,
      "coupon_code": coupon_code,
      "price": price,
    };

    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
      body: jsonEncode(body),
    );

    if (response.statusCode == 200) {
      return VerifyCouponModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<VerifyCouponModel?> checkout_apple({
    required token,
    required int subscription_id,
    required String coupon_code,
    required int price,
    required bool paymentType,
    required status,
  }) async {
    var url = "${BASE_URL}v2/checkout";
    Map body = {
      "subscription_id": subscription_id,
      "coupon_code": coupon_code,
      "price": price,
      "paymentType": paymentType,
      "status": "$status",
    };

    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
      body: jsonEncode(body),
    );

    if (response.statusCode == 200) {
      return VerifyCouponModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<SubscriptionHistoryModel?> getSubscriptionHistory(
      {required token}) async {
    var url = "${BASE_URL}userSubscriptionsHistory";
    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
    );

    if (response.statusCode == 200) {
      return SubscriptionHistoryModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<ReviewsModel?> getReviews(
      {required token, required talent_id}) async {
    var url = "${BASE_URL}allreview";
    Map body = {
      "talent_id": talent_id,
    };
    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
      body: jsonEncode(body),
    );

    if (response.statusCode == 200) {
      return ReviewsModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<AddReviewModel?> addReviews(
      {required token,
      required rating,
      required talent_id,
      required review}) async {
    var url = "${BASE_URL}addRatingsReview";

    Map body = {
      "rating": rating,
      "talent_id": talent_id,
      "review": review,
    };

    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
      body: jsonEncode(body),
    );

    if (response.statusCode == 200) {
      return AddReviewModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<HireTalentModel?> hireTalent({
    required token,
    required talent_id,
  }) async {
    var url = "${BASE_URL}v2/hireTalentBySubscriptionUsers";

    Map body = {
      "talent_id": talent_id,
    };

    var response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
      body: jsonEncode(body),
    );

    if (response.statusCode == 200) {
      return HireTalentModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<UserDeleteModel?> deleteUserData({required token}) async {
    var url = "${BASE_URL}users/destroy";

    var response = await http.delete(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token,
      },
    );

    if (response.statusCode == 200) {
      return UserDeleteModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<AdsModel?> getAds() async {
    var url = "${BASE_URL}v1/ads";

    var response = await http.get(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      return AdsModel.fromJson(jsonDecode(response.body));
    } else {
      return null;
    }
  }

  static Future<GetTradeBarterModel?> getTradeBarterUsers(
      {required token, required url}) async {
    try {
      var response = await http.get(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
      );

      if (response.statusCode == 200) {
        return GetTradeBarterModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<PaymentCheckModel?> checkPayment(
      {required token, required order_id}) async {
    var url = "${BASE_URL}payment/check";
    Map body = {
      "order_id": order_id,
    };

    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        return PaymentCheckModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<GetAvailableCouponCodesModel?> getAvailableCouponCodes() async {
    var url = "${BASE_URL}availableCoupons";
    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );

      if (response.statusCode == 200) {
        return GetAvailableCouponCodesModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<FeedbackModel?> addFeedback({var token, var feedback}) async {
    var url = "${BASE_URL}feedbackform";
    var body = {
      "feedback": feedback,
    };
    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );
      if (response.statusCode == 200) {
        return FeedbackModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<GetRecruitersPostsModel?> getRecruitersPosts({
    required var userId,
    required var token,
    required var url,
  }) async {
    var body = {
      "userId": userId,
    };

    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        return GetRecruitersPostsModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<GetRecruitersPostByIdModel?> getRecruitersPostById({
    required var post_id,
    required var token,
  }) async {
    var body = {
      "post_id": post_id,
    };
    var url = "${BASE_URL}getRecruiterSinglePost";
    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        return GetRecruitersPostByIdModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<ApplyJobModel?> applyJob({
    required var userId,
    required var postId,
    required var token,
  }) async {
    var url = "${BASE_URL}applyJobs";

    var body = {
      "userId": userId,
      "postId": postId,
    };
    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );
      //
      if (response.statusCode == 200) {
        return ApplyJobModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<GetRecruitersJobsModel?> getRecruitersJobs({
    required var userId,
    required var token,
    required var url,
  }) async {
    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
      );

      if (response.statusCode == 200) {
        return GetRecruitersJobsModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<GetRecruitersPostsModel?> getRecruitersJobPosts({
    required var recruitersId,
    required var token,
    required var url,
  }) async {
    var body = {
      "recruiterId": recruitersId,
    };

    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        return GetRecruitersPostsModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<ApplyJobModel?> removeJob({
    required var postId,
    required var token,
  }) async {
    var url = "${BASE_URL}removePost";

    var body = {
      "postId": postId,
    };
    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': token,
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        return ApplyJobModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<bool> addFcmToken({
    required var authToken,
    required var fcmToken,
  }) async {
    var url = "${BASE_URL}fcm-token";

    var body = {"token": fcmToken};
    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': authToken,
        },
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        if (data["status"] == 200) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  static void sendChatNotifications({
    required var name,
    required var chatid,
    required int isgroupchat,
    required var profilepic,
    required var senderid,
    required int issecretservice,
    required String message,
    required List<String> receivers,
    required authToken,
  }) async {
    try {
      Map body = {
        HelperClass.NOTIFICATION_CHAT_NAME_KEY: name,
        HelperClass.NOTIFICATION_CHAT_ID_KEY: chatid,
        HelperClass.NOTIFICATIONS_IS_GROUP_CHAT_KEY: isgroupchat,
        HelperClass.NOTIFICATIONS_PROFILE_PIC_KEY: profilepic,
        HelperClass.NOTIFICATIONS_SENDER_ID_KEY: senderid,
        HelperClass.NOTIFICATION_IS_SECRET_SERVICE_KEY: issecretservice,
        HelperClass.NOTIFICATION_CHAT_MESSAGE_KEY: message,
        "receivers": receivers,
      };

      var url = "${BASE_URL}chatNotification";
      http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': authToken,
        },
        body: jsonEncode(body),
      );
    } catch (e) {}
  }

  static Future<FeedbackModel?> logout({
    required var authToken,
    required var fcmToken,
  }) async {
    var url = "${BASE_URL}logout";

    var body = {
      "fcm_token": fcmToken,
      "token": authToken,
    };
    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': authToken,
        },
        body: jsonEncode(body),
      );
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        return FeedbackModel.fromJson(data);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
